import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Ex_exception04 {
	public static void main(String[] args) {
		try {
			
			int a = 10/2;
			System.out.println(a);
			int b = 10/0;
			System.out.println(b);
			
			FileInputStream fis = new FileInputStream("abc.txt");
		}catch(ArithmeticException m){
			System.out.println("0으로 못나눠요."+m.getMessage());
			
		} catch (FileNotFoundException f) {
			f.printStackTrace();

		} catch (Exception e) { //모든 예외를 처리하는 블록.
			e.printStackTrace();
		}finally{
			System.out.println("종료");
		}
		
		
	}//main()
}//class
