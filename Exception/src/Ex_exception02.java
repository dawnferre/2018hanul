public class Ex_exception02 {
	public static void main(String[] args) {
		//가스불을 켠다 - 요리시작 - 요리끝 - 가스를 잠근다.- 종료
		
		try {
			System.out.println("가스불을 켠다.");
			System.out.println("요리 시작");
			int a = 10/0; //예외 발생
			System.out.println("요리 끝");
//			System.out.println("가스를 잠근다.");
//			System.out.println("종료");
			
		} catch (Exception e) {
			System.out.println("예외발생");
//			System.out.println("가스를 잠근다.");
//			System.out.println("종료");
		}finally{ //반드시 실행되는 구문 _ 생략가능
			System.out.println("가스를 잠근다.");
			System.out.println("종료");
		}
		
	}//main()
}//class
