import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Ex_exception01 {
	public static void main(String[] args) {
		// 3개의 길이를 갖는 정수형배열 arr 선언
		int[] arr = new int[3];
		arr[0] =10;
		arr[1] =20;
		arr[2] =30;
//		arr[3] = 40;
//		System.out.println(arr[3]); //ArrayOutofBoundsException - 미확인 예외
		
		System.out.println("arr[0]의 값 : "+arr[0]);
		System.out.println("arr[1]의 값 : "+arr[1]);
		System.out.println("arr[2]의 값 : "+arr[2]);
//		System.out.println("arr[3]의 값 : "+arr[3]);
		
//		String str = 100;//에러 -오류
		String str2 = "100";
		System.out.println(str2);
		int pstr= Integer.parseInt(str2);
		System.out.println(Integer.parseInt(str2));
		System.out.println(pstr);
		
		String str3 ="100a";
		System.out.println(str3);
//		int pstr2 = Integer.parseInt(str3);//NumberFormatException
//		System.out.println(pstr2);
		
		int x =10;
		int y =0;
		System.out.println(y/x);
//		System.out.println(x/y);//0으로 나눌수 없다 _ ArithmeticException
		
		//"abc.txt" 파일을 읽어들이시오. FileinputStream class
		//확인 예외. 
		try {
			FileInputStream  fs = new FileInputStream("abc.txt");
		} catch (FileNotFoundException e) {
			System.out.println("해당하는 파일이 존재하지 않습니다.");
			e.printStackTrace(); //메모리상에서 예외가 발생하는 곳을 찾아서 try-catch 처리해달라는 
//			System.out.println("못찾아요.");
		}
		
	}//main()
}//class
