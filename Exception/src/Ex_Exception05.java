public class Ex_Exception05 {
	public static void main(String[] args) {
		//1~100까지의 누적합(sum)을 구하여 출력하시오.
		//단, 누적합이 888이상이 되면, 반복문을 중지.(break;)
		//try-catch 불럭을 이용하여 예외처리를 하시오.
		int sum =0;
		int cnt =0;
		for(int i =1; i<101; i++){
			sum+=i;
			cnt++;
			if(sum >= 888){
				System.out.println(i);
				break;
			}
		}
		System.out.println(sum);
		System.out.println(cnt);
		
		sum =0;
		cnt =0;
		try{
		for(int i =1; i<101; i++){
			sum+=i;
			cnt++;
			if(sum >= 888){
//				System.out.println(i);
//				break;
				throw new Exception("888이상이 되었습니다.");
				//예외를 강제로 발생시켜 catch 처리로 넘김.
			}
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println(sum);
		System.out.println(cnt);
	}//main()
}//class
