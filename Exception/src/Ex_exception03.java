public class Ex_exception03 {
	public static void main(String[] args) {
		//try-with-resource 문법은 애초에 리소스(변수에 대입된 값)를 밖에서 사용하지 못하도록 설계돼 있기 때문에 바깥 변수를 사용한다는 생각 자체가 오류이다.
		try {
			int x = 10;
			int y = 100/x; 
			//예외가 발생할 수 있다.
			System.out.println(y);
			
		} catch (Exception e) {
			System.out.println("x의 값이 잘못되었습니다.");
			System.out.println(e.getMessage()); //예외의 출처를 메모리상에서 추적하면서 결과를 출력(상세)
			e.printStackTrace();
		}finally{
			System.out.println("종료");
		}
	}//main()
}//class
