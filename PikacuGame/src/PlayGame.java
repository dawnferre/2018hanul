import java.util.Scanner;

public class PlayGame {
	Scanner scan;
	public PlayGame(Character ch,Scanner scan){
		this.scan = scan;
		System.out.println("게임을 시작합니다.");
		
		lev:
		while(true){
			System.out.println(ch.name+"랑 무엇을 할까요?");
			System.out.println("1.먹이주기\t2.잠자기\t3.놀아주기\t4.운동하기\t5.종료하기");
			int menu = scan.nextInt();
			if(menu == 0 || menu<0 || menu>5){
				System.out.println("아닛...제대로 입력해주세요");
				continue;
			}else{
				switch(menu){
				case 1:
					ch.eat();
					break;
				case 2:
					ch.sleep();
					break;
				case 3:
					ch.play();
					if(!ch.checkEnergy()){
						System.out.println("에너지가 부족해서 "+ch.name+"가 죽어버렸어요 ㅜㅜ");
						break lev;
					}
					break;
				case 4:
					ch.train();
					if(!ch.checkEnergy()){
						System.out.println("에너지가 부족해서 "+ch.name+"가 죽어버렸어요 ㅜㅜ");
						break lev;
					}
					break;
				case 5:
					System.out.println("게임을 종료합니다.");
					break lev;
				
				default:
					System.out.println("제대로 입력해주세요 ㅠㅠ \n"+ch.name+"이 너무 심심해요");
					break;
				}
				ch.levelUp();
				ch.printInfo();
			}
			
		}
		scan.close();
	}
}
