import java.util.Scanner;

public class GameMain {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Character character = null;

		System.out.println("게임을 시작합니다 \n캐릭터를 선택해주세요. 1. 피카츄 \t 2.이상해씨 \t 3.꼬북이");
		int c = scan.nextInt();
		
		if(c ==1){
			character = new Pikachu();
			new PlayGame(character,scan);
		}else if(c ==2){
			character = new Lee();
			new PlayGame(character,scan);
			
		}else if(c ==3){
			character = new Gobook();
			new PlayGame(character,scan);
		}else{
			System.out.println("잘못입력하셨습니다.");	
		}
		
		
	}
}
