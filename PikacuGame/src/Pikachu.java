
public class Pikachu extends Character {
	// pikachu → character

	// default 생성자 - 경험치, 에너지,
	public Pikachu() {
		System.out.println("피카츄 캐릭터가 생성되었습니다.");
		// 상속 부모 클래스에 생성된 변수, 메소드는 따로 선언없이 바로 가져다 쓸 수 있음.
		emp = 30;
		energy = 50;
		level = 0;
		name ="피카츄";

		printInfo();

	}

	@Override
	public void eat() {
		energy += 10;

	}

	@Override
	public void sleep() {
		energy += 5;
	}

	@Override
	public boolean play() {
		energy += -20;
		emp += 50;
		levelUp();
		// 에너지 감소 되었으니 - checkEnergy
		return checkEnergy();
	}

	@Override
	public boolean train() {
		energy += -10;
		emp += 15;
		levelUp();
		return checkEnergy();
	}

	@Override
	public void levelUp() {
		if(emp >=40){
			level ++;
			emp -=40;
			System.out.println("레벨업이 되었습니다.");
		}
		
	}

}
