
public class Lee extends Character{
	
	public Lee(){
		System.out.println("이상해씨 캐릭터가 생성되었습니다.");
		// 상속 부모 클래스에 생성된 변수, 메소드는 따로 선언없이 바로 가져다 쓸 수 있음.
		emp = 20;
		energy = 30;
		level = 0;
		name ="이상해씨";
		printInfo();
		
	}

	@Override
	public void eat() {
		energy +=5;
	}

	@Override
	public void sleep() {
		energy +=20;
	}

	@Override
	public boolean play() {
		energy -=10;
		emp +=15;
		levelUp();
		return checkEnergy();
	}

	@Override
	public boolean train() {
		energy -=10;
		emp +=25;
		levelUp();
		return checkEnergy();
	}

	@Override
	public void levelUp() {
		if(emp >=35){
			level ++;
			emp -=35;
			System.out.println("레벨업이 되었습니다.");
		}
	}

}
