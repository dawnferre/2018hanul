
public class Gobook extends Character {

	
	public Gobook(){
		System.out.println("꼬북이 캐릭터가 생성되었습니다.");
		// 상속 부모 클래스에 생성된 변수, 메소드는 따로 선언없이 바로 가져다 쓸 수 있음.
		emp = 40;
		energy = 50;
		level = 0;
		name ="꼬북이";

		printInfo();
	}
	
	@Override
	public void eat() {
		energy +=15;
	}

	@Override
	public void sleep() {
		energy +=10;
	}

	@Override
	public boolean play() {
		energy -=30;
		emp +=15;
		levelUp();
		return checkEnergy();
	}

	@Override
	public boolean train() {
		energy -=20;
		emp +=30;
		levelUp();
		return checkEnergy();
	}

	@Override
	public void levelUp() {
		if(emp >=50){
			level ++;
			emp -=50;
			System.out.println("레벨업이 되었습니다.");
		}
	}

}
