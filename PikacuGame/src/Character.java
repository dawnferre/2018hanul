//부모클래스 _ 추상클래스
public abstract class Character {
	protected int emp =0;
	protected int energy =0;
	protected int level =0;
	protected String name =null;
	
	public abstract void eat();
	public abstract void sleep();
	public abstract boolean play(); 
	public abstract boolean train(); 
	public abstract void levelUp();
	
	//남아있는 에너지 체크
	public boolean checkEnergy(){
		if(energy >= 0){
			return true;
		}else{
			return false;
		}
	}
	
	public void printInfo(){
		System.out.println("+++++++++++++++++++++++++++++++");
		System.out.println("현재 캐릭터의 정보를 출력합니다.");
		System.out.println("에너지 :"+energy);
		System.out.println("경험치 :"+emp);
		System.out.println("레벨 :"+level);
		System.out.println("+++++++++++++++++++++++++++++++");
		
	}
}
