package com.hanul.book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BookDAO {
	Connection conn;
	PreparedStatement pst;
	ResultSet rs;
	//pk체크
	public boolean numCheck(int check){
		conn = SingleConn.getConn();
		String sql="select * from tblbook where num=?";
		boolean result = false;
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, check);
			rs= pst.executeQuery();
			if(rs.next()){
				result= true;
			}else{
				result = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	//입력
	public int insertBookInfo(BookDTO dto) {
		conn = SingleConn.getConn();
		String sql ="insert into tblbook values(?,?,?,?,?)";
		int num = 0;
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, dto.getNum());
			pst.setString(2, dto.getTitle());
			pst.setString(3, dto.getCompany());
			pst.setString(4, dto.getName());
			pst.setInt(5, dto.getCost());
			
			num = pst.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return num;
	}
	public void selectMemberAll() {
		conn = SingleConn.getConn();
		String sql="select * from tblbook order by num";
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			System.out.println("번호\t책제목\t출판사\t저자\t단가");
			while(rs.next()){
				System.out.print(rs.getInt("num")+"\t");
				System.out.print(rs.getString("title")+"\t");
				System.out.print(rs.getString("company")+"\t");
				System.out.print(rs.getString("name")+"\t");
				System.out.println(rs.getInt("cost")+"\t");
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		
	}
	public int deleteMember(int numCheck) {
		String sql="delete from tblbook where num=?";
		int result =0;
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, numCheck);
			result = pst.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	public int updateMember(BookDTO dto) {
		String sql="update tblbook set title=?, company=?,name=?,cost=? where num=?";
		int result =0;
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, dto.getTitle());
			pst.setString(2, dto.getCompany());
			pst.setString(3, dto.getName());
			pst.setInt(4, dto.getCost());
			pst.setInt(5, dto.getNum());
			result = pst.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
		
	}
	public void selectBookTitle(String title) {
		conn = SingleConn.getConn();
		String sql="select * from tblbook where title like ?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, "%"+title+"%");
			rs = pst.executeQuery();
			System.out.println("번호\t책제목\t출판사\t저자\t단가");
			while(rs.next()){
				System.out.print(rs.getInt("num")+"\t");
				System.out.print(rs.getString("title")+"\t");
				System.out.print(rs.getString("company")+"\t");
				System.out.print(rs.getString("name")+"\t");
				System.out.println(rs.getInt("cost")+"\t");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	public ResultSet orderBook(int num) {
		conn = SingleConn.getConn();
		String sql ="select title, cost from tblbook where num=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, num);
			rs = pst.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}
	public void dbClose(){
		try {
			if(rs!= null){
				rs.close();
			}
			if(pst!=null){
				pst.close();
			}
			if(conn!=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
