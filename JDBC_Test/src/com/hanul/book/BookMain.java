package com.hanul.book;

import java.util.Scanner;

public class BookMain {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		while(true){
			System.out.println("=====도서관리=====");
			System.out.print("I:도서등록\nD:도서삭제\nU:도서정보수정\nS:제목검색 \nA:목록보기 \nO:도서주문\nE:종료\n");
			System.out.println("==============");
			System.out.print("입력해주세요:");
			String result =scan.next();
			
			if(result.equalsIgnoreCase("I")){// 도서등록
				new BookInput(scan).insertBook();
				continue;
			}else if(result.equalsIgnoreCase("d")){//도서삭제
				new BookInput(scan).deleteInput();
				continue;
			}else if(result.equalsIgnoreCase("u")){//도서정보수정
				new BookInput(scan).updateInput();
				continue;
			}else if(result.equalsIgnoreCase("s")){//제목검색
				new BookInput(scan).selectInputTitle();
				continue;
			}else if(result.equalsIgnoreCase("a")){//목록보기
				new BookInput(scan).selectAll();
				
				continue;
			}else if(result.equalsIgnoreCase("o")){//도서주문
				new BookInput(scan).orderinput();
				continue;
			}else if(result.equalsIgnoreCase("e")){
				System.out.println("도서관리 프로그램이 종료됩니다.");
				break;
			}else{
				System.out.println("입력값이 잘못되었습니다.");
				continue;
			}
		}
		scan.close();
	}
}
