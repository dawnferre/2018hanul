package com.hanul.book;

import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Scanner;

public class BookInput {
	Scanner scan;
	BookDTO dto = new BookDTO();
	BookDAO dao = new BookDAO();
	
	public BookInput(Scanner scan) {
		this.scan = scan;
	}

	public void insertBook() {

		while (true) {
			// System.out.println("도서 정보 입력화면입니다.");
			System.out.println("입력하실 정보를 넣어주세요.");
			System.out.print("번호 : ");
			int num = Integer.parseInt(scan.next());
			if (dao.numCheck(num)) {
				System.out.println("존재하는 번호입니다.");
				continue;
			} else {
				System.out.print("책제목: ");
				String title = scan.next();
				System.out.print("출판사: ");
				String company = scan.next();
				System.out.print("저자: ");
				String name = scan.next();
				System.out.print("단가 : ");
				int cost = Integer.parseInt(scan.next());

				dto.setNum(num);
				dto.setName(name);
				dto.setTitle(title);
				dto.setCompany(company);
				dto.setCost(cost);

				if (dao.insertBookInfo(dto) > 0) {
					System.out.println("도서정보가 성공적으로 입력되었습니다.");
				} else {
					System.out.println("정보입력이 실패하였습니다.");
				}
				break;
			}
		}

	}

	public void selectAll() {
		dao.selectMemberAll();
	}

	public void deleteInput() {
		while (true) {
			System.out.println("삭제할 도서번호를 입력해주세요.");
			int num = Integer.parseInt(scan.next());
			if (dao.numCheck(num)) {
				if (dao.deleteMember(num) > 0) {
					System.out.println(num + "번의 회원정보가 삭제되었습니다.");
				} else {
					System.out.println("회원정보 삭제를 실패하였습니다.");
				}
				break;
			} else {
				System.out.println("존재하지 않는 도서번호입니다.");
				continue;
			}
		}
	}

	public void updateInput() {
		while (true) {
			System.out.println("업데이트할 도서번호를 입력해주세요.");
			int num = Integer.parseInt(scan.next());
			
			if (dao.numCheck(num)) {
				System.out.print("책제목: ");
				String title = scan.next();
				System.out.print("출판사: ");
				String company = scan.next();
				System.out.print("저자: ");
				String name = scan.next();
				System.out.print("단가 : ");
				int cost = Integer.parseInt(scan.next());

				dto.setNum(num);
				dto.setName(name);
				dto.setTitle(title);
				dto.setCompany(company);
				dto.setCost(cost);

				if (dao.updateMember(dto) > 0) {
					System.out.println(num + "번의 회원정보가 업데이트 되었습니다.");
				} else {
					System.out.println("회원정보 업데이트가 실패하였습니다.");
				}
				break;
			} else {
				System.out.println("존재하지 않는 도서번호입니다.");
				continue;
			}
		}

	}

	public void selectInputTitle() {
		System.out.println("검색하실 제목을 입력해주세요.");
		String title = scan.next();
		dao.selectBookTitle(title);
	}

	public void orderinput() {
		System.out.println("도서 주문화면입니다.");
		while (true) {
			System.out.println("주문하실 도서번호를 입력 :");
			int num = Integer.parseInt(scan.next());
			if (dao.numCheck(num)) {
				System.out.println("주문하실 수량을 입력 :");
				int cnt = Integer.parseInt(scan.next());
				
				ResultSet rs = dao.orderBook(num);
				try {
					if (rs.next()) {
						DecimalFormat decimal = new DecimalFormat("￦#,###");
						String cost = decimal.format(rs.getInt("cost"));
						String totalcost = decimal.format(rs.getInt("cost") * cnt);
						System.out.println("주문하신 도서명은 " + rs.getString("title") + "이고, 단가는 " + cost + "원이며, 주문수량은 " + cnt
								+ "권입니다.");
						System.out.println("총 단가는 " + totalcost + "입니다.");
					}
				} catch (Exception e) {
				}
				break;
			} else {
				System.out.println("입력하신 " + num + "번 도서는 없습니다.");
				continue;
			}
		}
	}
	
}
