create table tblBook(
	num number primary key not null,
	title varchar2(30),
	company varchar2(20),
	name varchar2(10),
	cost number
);

insert into tblbook values(1,'Java','한빛','김윤명',27000);
insert into tblbook values(2,'JSP','혜지원','오정원',33000);
