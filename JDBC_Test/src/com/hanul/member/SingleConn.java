package com.hanul.member;
import java.sql.Connection;
import java.sql.DriverManager;

public class SingleConn {
	// default생성자 : dtoclass에서는 만들어야 한다(직렬화과정에서는 반드시 필요함)
	public SingleConn() {
	}

	// 생성자에 넣는 방법이외에 클래스로 바로 만드는
	// 뷰에서는 메소드에 만들어 사용할 것.
	// 연결방법은 다양하게 있다.

	// 연결객체
	private static Connection conn; //static으로 걸어주는 거면 메인단에서 한번 처리하고 끝내면 되는것.

	// 초기화 블럭(static) :가장 먼저 메모리에 로딩 ==>실행
	static { // 127.0.0.1
		String url = "jdbc:oracle:thin:@192.168.0.16:1521/xe";
		String user = "system";
		String password = "0000";
	
		//정적로딩 : 드라이버를 직접 심어주는 것.
		try {
			//동적로딩
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url, user, password);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static Connection getConn(){
		return conn;
	}
}
