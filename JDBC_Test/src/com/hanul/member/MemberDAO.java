package com.hanul.member;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class MemberDAO {
	// 1.
	private Connection conn = SingleConn.getConn(); // 연결객체
	private PreparedStatement pst; // 전송객체
	private ResultSet rs; // 결과객체

	// 번호검색 메소드
	public ResultSet checkNum(int num) {
		conn = SingleConn.getConn(); // 클래스 연결(db접속)_메소드내에서 선언.
		String sql = "select * from tblmem where num=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, num);
			rs = pst.executeQuery();

		} catch (Exception e) {
			System.out.println("CheckNum");
			e.printStackTrace();
		}

		return rs;
	}// checkNum();

	// 회원 등록(삽입)메소드 : 선생님은 int자체를 넘겨줌.
	public int insertMember(MemberDTO dto) {
		String sql = " insert into tblmem values(?,?,?,?,?)";
		int sucess = 0;
		try {
			// statment : sql내에 모든 변수가 다 들어가야 처리된다.
			pst = conn.prepareStatement(sql);
			pst.setInt(1, dto.getNum());
			pst.setString(2, dto.getName());
			pst.setInt(3, dto.getAge());
			pst.setString(4, dto.getAddr());
			pst.setString(5, dto.getTel());

			sucess = pst.executeUpdate();

		} catch (Exception e) {
			System.out.println("InsertMember");
			e.printStackTrace();
		}
		return sucess;
	}

	// 전체회원검색_출력검색.
	public void selectMemberAll() {
		String sql = "select *from tblmem order by num";
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();

			System.out.println("등록번호\t이름\t나이\t주소\t전화번호");
			// 출력을 여기서 함.
			while (rs.next()) {
				System.out.print(rs.getInt("num") + "\t");
				System.out.print(rs.getString("name") + "\t");
				System.out.print(rs.getInt("age") + "\t");
				System.out.print(rs.getString("addr") + "\t");
				System.out.println(rs.getString("tel"));
			}
		} catch (Exception e) {
			System.out.println("selectMemberAll");
		}
	}// selectMemberAll()

	public int deleteMember(int num) {
		String sql = "delete from tblmem where num =?";
		int result = 0;
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, num);

			result = pst.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

	}

	// 회원정보 수정 메소드
	public int updateMember(MemberDTO dto) {
		String sql = "update tblmem set name=?, age=?,addr=?,tel=? where num =?";
		int succ = 0;
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, dto.getName());
			pst.setInt(2, dto.getAge());
			pst.setString(3, dto.getAddr());
			pst.setString(4, dto.getTel());
			pst.setInt(5, dto.getNum());

			succ = pst.executeUpdate();
		} catch (Exception e) {
			System.out.println("updatemember");
			e.printStackTrace();
		}

		return succ;
	}

	// 이름검색
	public void selectMeberName(String selectname) {
		String sql = "select * from tblmem where name like ? order by num";

		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, "%" + selectname + "%");
			rs = pst.executeQuery();

			System.out.println("등록번호\t이름\t나이\t주소\t전화번호");
			// 출력을 여기서 함.
			while (rs.next()) {
				System.out.print(rs.getInt("num") + "\t");
				System.out.print(rs.getString("name") + "\t");
				System.out.print(rs.getInt("age") + "\t");
				System.out.print(rs.getString("addr") + "\t");
				System.out.println(rs.getString("tel"));
			}

		} catch (Exception e) {
			System.out.println("selectMemberName");
			e.printStackTrace();
		}

	}

	public ArrayList<MemberDTO> selectMemberAddr(String selectaddr) {
		String sql ="select * from tblmem where addr like ? order by num";
		ArrayList<MemberDTO> list = new ArrayList<>();
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, "%" + selectaddr + "%");
			rs= pst.executeQuery();
			while (rs.next()) {
				MemberDTO dto = new MemberDTO();
				dto.setNum(rs.getInt("num"));
				dto.setName(rs.getString("name"));
				dto.setAge(rs.getInt("age"));
				dto.setAddr(rs.getString("addr"));
				dto.setTel(rs.getString("tel"));
				
				list.add(dto);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	public void dbclose(){
		try {//역순처리
			rs.close();
			pst.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
