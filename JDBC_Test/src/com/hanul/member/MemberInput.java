package com.hanul.member;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;


public class MemberInput {
	Scanner scan;
	MemberDAO dao = new MemberDAO();

	public MemberInput(Scanner scan) {
		this.scan = scan;
	}

	// insertinput : 회원등록 서브메뉴
	public void insertInput() {
		System.out.println("신규회원 등록화면입니다.");
		while (true) {
			System.out.print("번호를 입력하세요 : ");
			int num = Integer.parseInt(scan.next());
			// enter처리를 못해줌 (다음 문자열에 엔터가 들어가게 된다 ) ==nextint
			// http://allg.tistory.com/17

			// 엔터처리가 되버리면 바로 그 다음 중복섬사가 수행되기 때문에 오류가 뜸
			// 결과 자체로 넘겨줌.
			ResultSet rs = dao.checkNum(num); // 번호에 대한 중복검사 수행.

			try {
				if (rs.next() == true) {
					System.out.println("회원등록번호가 중복이 됩니다.");
					continue;
				} else {
					System.out.print("이름을 입력하세요 : ");
					String name = scan.next();
					System.out.print("나이를 입력하세요 : ");
					int age = Integer.parseInt(scan.next());
					System.out.print("주소를 입력하세요 : ");
					String addr = scan.next();
					System.out.print("전화번호를 입력하세요 : ");
					String tel = scan.next();

					MemberDTO dto = new MemberDTO(num, name, age, addr, tel);
					if (dao.insertMember(dto) > 0) {
						System.out.println(num + "번 회원님의 정보가 등록되었습니다.");
					} else {
						System.out.println("정보등록이 실패하였습니다.");
					}

					break;
				}
			} catch (Exception e) {
				System.out.println("insertInput");
				e.printStackTrace();
			}
		}
	}

	public void selectAll() {
		System.out.println("전체 회원 목록입니다.");
		// 회원정보 전체를 뿌려줌._어차피 목록출력을 dao자체해서 해주기 때문에 많이 달라지게 된다.
		dao.selectMemberAll();
	}// selectAll

	public void deleteInput() {
//		System.out.println("회원을 삭제하는 화면입니다.");
		while (true) {
			System.out.println("삭제할 회원번호를 입력해주세요.");
			int num = scan.nextInt();
			ResultSet rs = dao.checkNum(num);
			try {
				if (rs.next() == true) {
//					System.out.println("삭제");
					//리턴값을 제대로 받지 않으면 결과는 실행되지만, 넘어오는 값이 없어 잘못된 결과가 출력될 수 있다.
					if(dao.deleteMember(num)>0){
						System.out.println(num+"번 회원님의 정보가 삭제되었습니다.");
					}else{
						System.out.println("정보삭제가 실패되었습니다.");
					}
					
					
					break;
				} else {
					System.out.println("삭제할 회원번호가 없습니다.");
					continue;
				}
			} catch (Exception e) {
				System.out.println("deleteInput 예외");
				e.printStackTrace();
			}
		}

	}

	public void updateInput() {
		
		
	}

	public void selectName() {
		System.out.println("회원이름 검색화면입니다.");
		System.out.println("조회하실 회원이름을 입력해주세요.");
		String name = scan.next();
		dao.selectMeberName(name);
		
	}

	public void selectAddr() {
		System.out.println("회원 주소 검색화면입니다.");
		System.out.println("조회하실 주소를 입력해주세요.");
		String selectaddr = scan.next();
		ArrayList<MemberDTO> list =dao.selectMemberAddr(selectaddr);
		if(list.isEmpty()){ //list결과값을 null확인하는 메소드
			System.out.println("검색결과가 없습니다.");
		}else{
			System.out.println("등록번호\t이름\t나이\t주소\t전화번호");
			for (MemberDTO dto : list) {
				System.out.print(dto.getNum() + "\t");
				System.out.print(dto.getName() + "\t");
				System.out.print(dto.getAge() + "\t");
				System.out.print(dto.getAddr() + "\t");
				System.out.println(dto.getTel() + "\t");
			}
			
		}
		
	}

	public void selectTel() {
		
	}
}
