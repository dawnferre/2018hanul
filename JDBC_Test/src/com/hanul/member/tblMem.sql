--file로 만들고 이름은  .sql로 주면  위에 타입 등이 나오게 된다...신기..
--띄어쓰기 있으면 cmd에서 안먹음..
create table tblMem(
	num number primary key not null,
	name varchar2(10),
	age number,
	addr varchar2(50),
	tel varchar2(20)
);

-- table 제거 
drop table tblMem;

--레코드 삽입
insert into tblMem (필드명 (데이터를 다 넣어줄때는 굳이 안써도 가능.)) values(넣어줄 값을 순서대로(table필드순으로));
insert into tblMem (필드명 (데이터를 다 넣어줄때는 굳이 안써도 가능.)) values(넣어줄 값을 순서대로(table필드순으로));
insert into tblMem  values(1,'hong',33,'광주광역시 서구 쌍촌동','010-111-1111');
insert into tblMem  values(2,'park',30,'광주광역시 남구 봉선동','010-222-2222');

--레코드조회
select num, name, age, addr, tel from tblMem;
select * from tblMem;