package com.hanul.product;

public class CartDTO {
	int code;
	String name;
	String company;
	int cost;
	int su;
	
	int price;
	
	public CartDTO() {
	}
	
	
	public CartDTO(int code, String name, String company, int cost) {
		super();
		this.code = code;
		this.name = name;
		this.company = company;
		this.cost = cost;
	}


	public CartDTO(int code, int su) {
		super();
		this.code = code;
		this.su = su;
	}
	
	public CartDTO(int code, int su, int price) {
		super();
		this.code = code;
		this.su = su;
		this.price = price;
	}
	


	public int getSu() {
		return su;
	}
	public void setSu(int su) {
		this.su = su;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	
}
