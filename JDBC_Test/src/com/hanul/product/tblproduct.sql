create table jepum(
	code number primary key not null,
	name varchar2(30),
	company varchar2(30),
	cost number
);

insert into jepum values (1000,'노트북','삼성전자',1000000);
insert into jepum values (1001,'컴퓨터','엘지전자',1200000);
insert into jepum values (1002,'세탁기','삼성전자',500000);

create table cart(
	code number,
	su number
);