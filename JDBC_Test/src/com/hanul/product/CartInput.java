package com.hanul.product;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Scanner;


public class CartInput {
	Scanner scan;
	CartDAO dao = new CartDAO();
	CartDTO dto = new CartDTO();
	ResultSet rs;
	public CartInput(Scanner scan) {
		this.scan = scan;
	}

	public void orderInput() {
		System.out.println("주문 화면입니다.");
		String totalcost = null;
		while(true){
			System.out.print("원하시는 제품코드를 입력해주세요.:");
			int code = Integer.parseInt(scan.next());
			if(dao.codeCheck(code)){
				System.out.print("원하시는 수량을 입력해주세요.:");
				int su = Integer.parseInt(scan.next());
				rs = dao.orderJepum(code);
				try {
					if(rs.next()){
						DecimalFormat decimal = new DecimalFormat("￦#,###");
						String cost = decimal.format(rs.getInt("cost"));
						totalcost = decimal.format(rs.getInt("cost") * su);
						System.out.println("제품 : "+rs.getString("name"));
						System.out.println("단가 : "+cost);
						System.out.println("가격 : "+totalcost);
						
						dto = new CartDTO(code, su);
						dao.insertCart(dto);
						
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			}else{
				System.out.println("입력하신 번호가 없습니다.");
				continue;
			}
		}
	}

	public void insertInput() {
		System.out.println("제품등록화면입니다.");
		System.out.println("등록할 정보를 입력해주세요.");
		while(true){
			System.out.print("제품번호:");
			int code = Integer.parseInt(scan.next());
			if(dao.codeCheck(code)){
				System.out.println("중복되는 번호입니다.");
				continue;
			}else{
				System.out.print("제품종류(이름):");
				String name = scan.next();
				System.out.print("제조사:");
				String company = scan.next();
				System.out.print("단가:");
				int cost = Integer.parseInt(scan.next());
				
				dto = new CartDTO(code, name, company, cost);
				if(dao.insertJepum(dto)>0){
					System.out.println("정보등록이 성공했습니다.");
				}else{
					System.out.println("정보등록이 실패하였습니다.");
				}
				break;
			}
		}
	}

	public void selectAll() {
		new CartDAO().selectMemberAll();
		
	}
	public void showList(){
		
		rs =dao.getShowList(dto);
		System.out.println("코드\t종류\t제조사\t단가\t수량\t가격");
		try {
			while(rs.next()){
				System.out.print(rs.getInt("code")+"\t");
				System.out.print(rs.getString("name")+"\t");
				System.out.print(rs.getString("company")+"\t");
				System.out.print(rs.getInt("cost")+"\t");
				System.out.print(rs.getInt("su")+"\t");
				System.out.println((rs.getInt("cost")*rs.getInt("su")));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void selectName() {
		System.out.println("입력하실 제품을 입력해주세요.");
		String name = scan.next();
		dao.selectNameList(name);
		
	}

	public void deleteInput() {
		while(true){
			System.out.println("삭제하실 제품번호를 입력해주세요.");
			int code = Integer.parseInt(scan.next());
			if(!dao.codeCheck(code)){
				System.out.println("번호가 없습니다.");
				continue;
			}else{
				if(dao.deleteJepum(code)>0){
					System.out.println("제품삭제가 성공하였습니다.");
				}else{
					System.out.println("제품삭제가 실패하였습니다.");
				}
				break;
			}
		}
		
	}

	public void updateInput() {
		while(true){
			System.out.println("업데이트하실 제품번호를 입력해주세요.");
			int code = Integer.parseInt(scan.next());
			if(!dao.codeCheck(code)){
				System.out.println("번호가 없습니다.");
				continue;
			}else{
				System.out.print("제품종류(이름):");
				String name = scan.next();
				System.out.print("제조사:");
				String company = scan.next();
				System.out.print("단가:");
				int cost = Integer.parseInt(scan.next());
				
				dto = new CartDTO(code, name, company, cost);
				if(dao.updateJepum(dto)>0){
					System.out.println("업데이트가 완료되었습니다.");
				}else{
					System.out.println("업데이트가 실패하였습니다.");
				}
				break;
			}
		}
		
	}

}
