package com.hanul.product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import practice.MemberDTO;

public class CartDAO {
	Connection conn = SingleConn.getConn();
	PreparedStatement pst;
	ResultSet rs;
	public void selectMemberAll() {
		String sql ="select * from jepum order by code";
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			System.out.println("코드\t종류\t제조사\t단가");
			while(rs.next()){
				System.out.print(rs.getInt("code")+"\t");
				System.out.print(rs.getString("name")+"\t");
				System.out.print(rs.getString("company")+"\t");
				System.out.println(rs.getInt("cost")+"\t");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public boolean codeCheck(int code){
		String sql ="select * from jepum where code=?";
		boolean check = false;
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, code);
			rs = pst.executeQuery();
			if(rs.next()){
				check = true;
			}else{
				check = false;
			}
			
		} catch (Exception e) {
			e.getStackTrace();
		}
		return check;
	}
	public ResultSet orderJepum(int code) {
		String sql ="select name, cost from jepum where code =?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, code);
			rs = pst.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	public ResultSet getShowList(CartDTO dto) {
		String sql ="select c.code, name, company, cost ,su from jepum e, cart c where e.code =c.code";
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	public void insertCart(CartDTO dto){
		String sql ="insert into cart values(?,?)";
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, dto.getCode());
			pst.setInt(2, dto.getSu());
			pst.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void selectNameList(String name) {
		String sql ="select * from jepum where name like ?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, "%"+name+"%");
			rs = pst.executeQuery();
			System.out.println("코드\t종류\t제조사\t단가");
				while(rs.next()){
						System.out.print(rs.getInt("code")+"\t");
						System.out.print(rs.getString("name")+"\t");
						System.out.print(rs.getString("company")+"\t");
						System.out.println(rs.getInt("cost")+"\t");
				}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public int insertJepum(CartDTO dto) {
		int num =0;
		String sql="insert into jepum values(?,?,?,?)";
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, dto.getCode());
			pst.setString(2, dto.getName());
			pst.setString(3, dto.getCompany());
			pst.setInt(4, dto.getCost());
			
			num = pst.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return num;
	}
	public int deleteJepum(int code) {
		int num =0;
		String sql="delete from jepum where code=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, code);
			
			num = pst.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return num;
		
	}
	public int updateJepum(CartDTO dto) {
		int num =0;
		String sql="update jepum set name=?, company=?, cost =? where code=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, dto.getName());
			pst.setString(2, dto.getCompany());
			pst.setInt(3, dto.getCost());
			pst.setInt(4, dto.getCode());
			
			num = pst.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return num;
		
	}
	public void dbClose(){
		try {
			if(rs!= null){
				rs.close();
			}
			if(pst!=null){
				pst.close();
			}
			if(conn!=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
