package com.hanul.product;
import java.sql.Connection;
import java.sql.DriverManager;

public class SingleConn {
	public SingleConn() {
	}

	private static Connection conn; //static으로 걸어주는 거면 메인단에서 한번 처리하고 끝내면 되는것.
	static { // 127.0.0.1
		String url = "jdbc:oracle:thin:@192.168.0.16:1521/xe";
		String user = "system";
		String password = "0000";
		try {
			//동적로딩
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url, user, password);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public static Connection getConn(){
		return conn;
	}
}
