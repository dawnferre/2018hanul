package practice;

import java.sql.Connection;
import java.sql.DriverManager;

public class SingleConn {
	Connection conn;
	//굳이 여기서는 pst, rs안써도 되는데
//	PreparedStatement pst;
//	ResultSet rs;
	
	//연결까지만. -- default
	public SingleConn() {
		String url = "jdbc:oracle:thin:@192.168.0.16:1521/xe";
		String user = "hr";
		String password = "hr";
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url, user, password);
			
//			System.out.println("db연결");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
