package practice;

import java.util.ArrayList;
import java.util.Scanner;

public class MemberInput {
	Scanner scan;
	SingleConn single = new SingleConn();

	public MemberInput(Scanner scan) {
		this.scan = scan;
	}
	//등록화면
	public void insertInput() {
		while (true) {
			System.out.println("등록할 정보를 입력해주세요.");
			System.out.print("등록번호: ");

			int num = scan.nextInt();
			//중복검사 : id중복검사랑 같은 원리임.
			boolean check = new MemberDAO().checkNum(num);
			if (check) { //기존에 해당되는 번호가 있다.
				System.out.println("중복되는 회원번호가 있어요!");
				continue;
			} else {
				System.out.print("이름: ");
				String name = scan.next();
				System.out.print("나이: ");
				int age = scan.nextInt();
				System.out.print("주소: ");
				String addr = scan.next();
				System.out.print("전화번호: ");
				String tel = scan.next();


				MemberDTO dto = new MemberDTO();
				dto.setNum(num);
				dto.setName(name);
				dto.setAge(age);
				dto.setAddr(addr);
				dto.setTel(tel);
				
				//field 메소드를 사용할 수 있어여...
				
				boolean result = new MemberDAO().insertMember(dto);

				if (result) {
					System.out.println("회원정보가 성공적으로 등록되었습니다.");
				} else {
					System.out.println("회원정보등록이 실패햐였습니다.");
				}

				searchAll();
				break;
			}
		}
	}
	//검색결과화면
	public void searchAll() {
		ArrayList<MemberDTO> list = new MemberDAO().selectMemberAll();

		System.out.println("등록번호\t이름\t나이\t주소\t전화번호");
		for (MemberDTO dto : list) {
			System.out.print(dto.getNum() + "\t");
			System.out.print(dto.getName() + "\t");
			System.out.print(dto.getAge() + "\t");
			System.out.print(dto.getAddr() + "\t");
			System.out.println(dto.getTel() + "\t");
		}
	}
	//정보변경화면
	public void updateInput() {
		System.out.println("변경하실 회원번호를 입력해주세요.");
		int num = scan.nextInt();
		System.out.print("이름: ");
		String name = scan.next();
		System.out.print("나이: ");
		int age = scan.nextInt();
		System.out.print("주소: ");
		String addr = scan.next();
		System.out.print("전화번호: ");
		String tel = scan.next();

		MemberDTO dto = new MemberDTO();
		dto.setNum(num);
		dto.setName(name);
		dto.setAge(age);
		dto.setAddr(addr);
		dto.setTel(tel);

		boolean result = new MemberDAO().updateMember(dto);
		if (result) {
			System.out.println("회원정보가 성공적으로 변경되었습니다.");
		} else {
			System.out.println("회원정보변경이 실패햐였습니다.");
		}

		searchAll();
	}
	//이름검색화면
	public void searchNameInput() {
		System.out.println("검색하실 이름을 입력해주세요.");
		String name = scan.next();

		ArrayList<MemberDTO> list = new MemberDAO().searchMemberName(name);

		System.out.println("등록번호\t이름\t나이\t주소\t전화번호");
		for (MemberDTO dto : list) {
			System.out.print(dto.getNum() + "\t");
			System.out.print(dto.getName() + "\t");
			System.out.print(dto.getAge() + "\t");
			System.out.print(dto.getAddr() + "\t");
			System.out.println(dto.getTel() + "\t");
		}
	}
	//주소검색화면
	public void searchAddrInput() {
		System.out.println("검색하실 주소를 입력해주세요.");
		String addr = scan.next();

		ArrayList<MemberDTO> list = new MemberDAO().searchMemberAddr("%"+addr+"%");

		System.out.println("등록번호\t이름\t나이\t주소\t전화번호");
		for (MemberDTO dto : list) {
			System.out.print(dto.getNum() + "\t");
			System.out.print(dto.getName() + "\t");
			System.out.print(dto.getAge() + "\t");
			System.out.print(dto.getAddr() + "\t");
			System.out.println(dto.getTel() + "\t");
		}

	}
	//전화번호검색화면
	public void searchTelInput() {
		System.out.println("검색하실 전화번호를 입력해주세요.");
		String addr = scan.next();

		ArrayList<MemberDTO> list = new MemberDAO().searchMemberTel(addr);

		System.out.println("등록번호\t이름\t나이\t주소\t전화번호");
		for (MemberDTO dto : list) {
			System.out.print(dto.getNum() + "\t");
			System.out.print(dto.getName() + "\t");
			System.out.print(dto.getAge() + "\t");
			System.out.print(dto.getAddr() + "\t");
			System.out.println(dto.getTel() + "\t");
		}

	}
	//정보삭제화면- 예외처리해주세여
	public void deleteInput() {
		System.out.println("삭제하실 회원번호를 입력해주세요.");
		int num = scan.nextInt();
		boolean result = new MemberDAO().deleteMember(num);
		if (result) {
			System.out.println("회원정보가 삭제되었습니다.");
		} else {
			System.out.println("정보삭제가 실패하였습니다.");
		}

		searchAll();
	}

}
