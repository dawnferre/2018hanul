package practice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MemberDAO {
	Connection conn=new SingleConn().conn;
	PreparedStatement pst=null;
	ResultSet rs=null;
	
	public void disconn(){
			try {
				if(conn!= null){
					conn.close();
				}
				if(pst!= null){
					pst.close();
				}
				if(rs != null){
					rs.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			
		}
	}
	public boolean insertMember(MemberDTO dto){
		boolean insert = false;
		String sql ="insert into tblMempratice values(?,?,?,?,?)";
		try {
			pst =conn.prepareStatement(sql);
			pst.setInt(1,dto.getNum());
			pst.setString(2, dto.getName());
			pst.setInt(3, dto.getAge());
			pst.setString(4, dto.getAddr());
			pst.setString(5, dto.getTel());
			
			int result = pst.executeUpdate();
			if(result>0){
				insert = true;
			}else{
				insert = false;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return insert;
		
	}
	public boolean updateMember(MemberDTO dto){
		boolean update = false;
		String sql="update tblMempratice set name=?,age=?,addr=?,tel=? where num=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, dto.getName());
			pst.setInt(2, dto.getAge());
			pst.setString(3, dto.getAddr());
			pst.setString(4, dto.getTel());
			pst.setInt(5,dto.getNum());
			
			int result = pst.executeUpdate();
			if(result>0){
				update = true;
			}else{
				update = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return update;
	}
	
	//나는 출력을 넘겨서 함.
	public ArrayList<MemberDTO> selectMemberAll(){
		ArrayList<MemberDTO> list = new ArrayList<>();
		String sql ="select * from tblMempratice order by num";
		try {
			pst =conn.prepareStatement(sql);
			rs = pst.executeQuery();
			while(rs.next()){
				MemberDTO dto = new MemberDTO();
				dto.setNum(rs.getInt("num"));
				dto.setName(rs.getString("name"));
				dto.setAge(rs.getInt("age"));
				dto.setAddr(rs.getString("addr"));
				dto.setTel(rs.getString("tel"));
				
				list.add(dto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		
		return list;
	}
	public ArrayList<MemberDTO> searchMemberName(String name){
		ArrayList<MemberDTO> list = new ArrayList<>();
		String sql="select * from tblmempratice where name=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, name);
			
			rs = pst.executeQuery();
			while(rs.next()){
				MemberDTO dto = new MemberDTO();
				dto.setNum(rs.getInt("num"));
				dto.setName(rs.getString("name"));
				dto.setAge(rs.getInt("age"));
				dto.setAddr(rs.getString("addr"));
				dto.setTel(rs.getString("tel"));
				
				list.add(dto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return list;
	}
	public ArrayList<MemberDTO> searchMemberAddr(String addr){
		ArrayList<MemberDTO> list = new ArrayList<>();
		String sql="select * from tblmempratice where addr like ?";
		try {
			pst = conn.prepareStatement(sql);
//			pst.setString(1, "%"+addr+"%");
			pst.setString(1, addr);
			
			rs = pst.executeQuery();
			while(rs.next()){
				MemberDTO dto = new MemberDTO();
				dto.setNum(rs.getInt("num"));
				dto.setName(rs.getString("name"));
				dto.setAge(rs.getInt("age"));
				dto.setAddr(rs.getString("addr"));
				dto.setTel(rs.getString("tel"));
				
				list.add(dto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return list;
	}
	public ArrayList<MemberDTO> searchMemberTel(String tel){
		ArrayList<MemberDTO> list = new ArrayList<>();
		String sql="select * from tblmempratice where tel like ?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, "%"+tel+"%");
			
			rs = pst.executeQuery();
			while(rs.next()){
				MemberDTO dto = new MemberDTO();
				dto.setNum(rs.getInt("num"));
				dto.setName(rs.getString("name"));
				dto.setAge(rs.getInt("age"));
				dto.setAddr(rs.getString("addr"));
				dto.setTel(rs.getString("tel"));
				
				list.add(dto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return list;
	}
	
	public boolean deleteMember(int num){
		boolean delete = false;
		String sql="delete from tblmempratice where num=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, num);
			int result = pst.executeUpdate();
			
			if(result>0){
				delete=true;
			}else{
				delete = false;
			}
				
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return delete;
	}
	
	public boolean checkNum(int num){
		boolean check = false;
		ArrayList<Integer> list = new ArrayList<>();
		String sql="select num from tblmempratice";
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			
			if(rs.next()== true){
				check = true;
			}else{
				check = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return check;
	}
	//처음 만들었던 메소드
	public boolean checkNum2(int num){
		boolean check = false;
		ArrayList<Integer> list = new ArrayList<>();
		String sql="select num from tblmempratice";
		try {
			pst = conn.prepareStatement(sql);
			
			rs = pst.executeQuery();
			//쫌 꼬아논 기분이 드는데..
			//여기서 처리하지 말고, input으로 넘기자.
			while(rs.next()){
				MemberDTO dto = new MemberDTO();
				dto.setNum(rs.getInt("num"));
				
				list.add(dto.getNum());
			}
			for (int i = 0; i < list.size(); i++) {
				if(list.get(i) ==num){
					check = false;
				}else{
					check = true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return check;
	}
}
