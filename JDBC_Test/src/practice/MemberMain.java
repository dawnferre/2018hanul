package practice;

import java.util.Scanner;

public class MemberMain {
	public static void main(String[] args) {
		System.out.println("===회원관리 ===");

		Scanner scan = new Scanner(System.in);
		while (true) {
			System.out.println("원하는 목록을 입력해주세요.");
			System.out.print("I:회원등록\nD:회원삭제\nU:회원정보수정\nS:전체회원검색 \nN:이름검색\nA:주소검색 \nT:전화번호검색\nE:종료 \n입력하세요 :");
			String num = scan.next();
			
			if (num.equalsIgnoreCase("I")) {//회원등록
				new MemberInput(scan).insertInput();
				continue;
			} else if (num.equalsIgnoreCase("d")) {//회원삭제
				new MemberInput(scan).deleteInput();
				continue;
			} else if (num.equalsIgnoreCase("u")) {//회원정보수정
				new MemberInput(scan).updateInput();
				continue;
			} else if (num.equalsIgnoreCase("s")) {//전체회원검색
				new MemberInput(scan).searchAll();
				continue;
			} else if (num.equalsIgnoreCase("n")) {//이름검색
				new MemberInput(scan).searchNameInput();
				continue;
			} else if (num.equalsIgnoreCase("a")) {//주소검색
				new MemberInput(scan).searchAddrInput();
				continue;
			} else if (num.equalsIgnoreCase("t")) {//전화번호검색
				new MemberInput(scan).searchTelInput();
				continue;
			} else if (num.equalsIgnoreCase("e")) {//종료
				System.out.println("프로그램이 종료됩니다.");
				break;

			}else{ //예외값 처리
				System.out.println("입력값이 맞는 경우가 없습니다.");
				continue;
			}
		}
		scan.close();
	}
}
