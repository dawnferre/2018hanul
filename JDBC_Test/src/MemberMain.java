
import java.util.Scanner;

import com.hanul.member.MemberInput;

public class MemberMain {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		while(true){
			menuPrint();
			System.out.println("메뉴를 입력하세요.");
			String menu = scan.next();
			if(menu.equalsIgnoreCase("I")){
				new MemberInput(scan).insertInput(); //신규회원 등록화면 호출
				continue;
			}else if(menu.equalsIgnoreCase("e")){ //종료
				System.out.println("회원관리 프로그램이 종료되었습니다.");
				System.exit(0);
				break;
			}else if(menu.equalsIgnoreCase("s")){//전체회원검색
				new MemberInput(scan).selectAll();
				continue;
			}else if(menu.equalsIgnoreCase("d")){//회원삭제
				new MemberInput(scan).deleteInput();
				continue;
			}else if(menu.equalsIgnoreCase("u")){//회원정보수정
				new MemberInput(scan).updateInput();
				continue;
			}else if(menu.equalsIgnoreCase("n")){//회원이름검색
				new MemberInput(scan).selectName();
				continue;
			}else if(menu.equalsIgnoreCase("a")){//회원주소검색
				new MemberInput(scan).selectAddr();
				continue;
			}else if(menu.equalsIgnoreCase("t")){//회원전화번호검색
				new MemberInput(scan).selectTel();
				continue;
			}else{
			
				System.out.println("메뉴를 잘못 입력하였습니다.");
				continue;
			}
			
		}
		scan.close();
	}//main
	//초기화면
	public static void menuPrint(){
		System.out.println("=====회원관리=====");
		System.out.print("I:회원등록\nD:회원삭제\nU:회원정보수정\nS:전체회원검색 \nN:이름검색\nA:주소검색 \nT:전화번호검색\nE:종료\n");
		System.out.println("==============");
	}
}//class
