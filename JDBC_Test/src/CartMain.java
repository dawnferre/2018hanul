import java.util.Scanner;

import com.hanul.product.CartDAO;
import com.hanul.product.CartInput;


public class CartMain {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		while(true){
			System.out.println("=====제품관리=====");
			System.out.print("I:제품등록\nD:제품삭제\nU:도서정보수정\nS:제품검색 \nA:전체검색 \nO:제품주문\nV:주문내역보기\nE:종료\n");
			System.out.println("==============");
			System.out.print("입력해주세요:");
			String result =scan.next();
			if(result.equalsIgnoreCase("I")){//제품등록
				new CartInput(scan).insertInput();
				continue;
			}else if(result.equalsIgnoreCase("d")){//제품삭제
				new CartInput(scan).deleteInput();
				continue;
			}else if(result.equalsIgnoreCase("u")){//
				new CartInput(scan).updateInput();
				continue;
			}else if(result.equalsIgnoreCase("s")){//제품검색
				new CartInput(scan).selectName();
				continue;
			}else if(result.equalsIgnoreCase("a")){//전체검색
				new CartInput(scan).selectAll();
				continue;
			}else if(result.equalsIgnoreCase("o")){//제품주문
				new CartInput(scan).orderInput();
				continue;
			}else if(result.equalsIgnoreCase("v")){//주문내역보기
				new CartInput(scan).showList();
				continue;
			}else if(result.equalsIgnoreCase("e")){//프로그램연결
				System.out.println("제품관리 프로그램이 종료됩니다.");
				break;
			}else{
				System.out.println("등록된 명령이 없습니다.");//예외처리
				new CartDAO().dbClose();
				continue;
			}
			
		}
	
	scan.close();
	}
}
