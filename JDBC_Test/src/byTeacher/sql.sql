--Jepum Table 생성
create table Jepum(
	code number primary key not null,
	name varchar2(30),
	company varchar2(30),
	cost number
);

--레코드 삽입
insert into Jepum values(1000, '노트북', '삼성전자', 1000000);
insert into Jepum values(1001, '컴퓨터', 'LG전자', 1200000);
insert into Jepum values(1002, '세탁기', '삼성전자', 500000);
commit;

--레코드 검색
select * from Jepum order by code asc;

--Cart Table 생성
create table Cart(
	code number,
	su number
);
commit;