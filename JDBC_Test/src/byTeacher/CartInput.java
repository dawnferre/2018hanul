package byTeacher;

import java.io.BufferedReader;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class CartInput {
	private BufferedReader br;

	public CartInput(BufferedReader br){
		this.br = br;
	}

	// �Է� �޼���
	public void insertJepum() {
		try {
			CartDAO dao = new CartDAO();
			System.out.println("��ǰ��� ȭ���Դϴ�.");
			System.out.println("----------------------------");
			while (true) {
				System.out.print("��ǰ��ȣ�� �Է��ϼ��� : ");
				int code = Integer.parseInt(br.readLine());
				ResultSet rs = dao.checkCode(code);
				if (rs.next() == true) {
					System.out.println("��ǰ��ȣ�� �ߺ��Ǿ����ϴ�.");
					break;
				} else {
					System.out.print("��ǰ���� �Է��ϼ��� : ");
					String name = br.readLine();
					System.out.print("����ȸ�縦 �Է��ϼ��� : ");
					String company = br.readLine();
					System.out.print("��ǰ�ܰ��� �Է��ϼ��� : ");
					int cost = Integer.parseInt(br.readLine());

					CartDTO dto = new CartDTO(code, name, company, cost);

					int succ = dao.insertJepum(dto);
					if (succ > 0) {
						System.out.println("�Է��Ͻ� ��ǰ(" + name + ")�� ����Ͽ����ϴ�.");
					} else {
						System.out.println("�Է��Ͻ� ��ǰ(" + name + ")�� ��� ���� �Ͽ����ϴ�.");
					}
				}
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}// insertJepum()

	// ��ü��ǰ �˻� �޼���
	public void searchAllJepum() {
		System.out.println("��ü ��ǰ �˻� ȭ���Դϴ�.");
		System.out.println("----------------------------");
		System.out.println("CODE\tNAME\tCOMPANY\t\tCOST");
		CartDAO dao = new CartDAO();
		DecimalFormat df = new DecimalFormat("\\#,##0");
		ArrayList<CartDTO> list = dao.searchAllJepum();
		for (CartDTO dto : list) {
			System.out.print(dto.getCode() + "\t");
			System.out.print(dto.getName() + "\t");
			System.out.print(dto.getCompany() + "\t");
			System.out.println(df.format(dto.getCost()));
		}
	}// selectAllJepum()

	// ��ǰ�� �˻� �޼���
	public void searchName() {
		try {
			System.out.println("��ǰ�� �˻� ȭ���Դϴ�.");
			System.out.println("----------------------------");
			System.out.print("�˻��� ��ǰ���� �Է��ϼ��� : ");
			String searchData = br.readLine();

			CartDAO dao = new CartDAO();
			ArrayList<CartDTO> list = dao.searchName(searchData);
			if (list.size() != 0) {
				for (CartDTO dto : list) {
					System.out.print(dto.getCode() + "\t");
					System.out.print(dto.getName() + "\t");
					System.out.print(dto.getCompany() + "\t");
					System.out.println(dto.getCost());
				}
			} else {
				System.out.println("�˻��Ͻ� " + searchData + "��(��) �����ϴ�.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}// searchName

	// ��ǰ ���� �޼���
	public void updateJepum() {
		try {
			System.out.println("��ǰ ���� ȭ���Դϴ�.");
			System.out.println("----------------------------");
			System.out.print("������ ��ǰ��ȣ�� �Է��ϼ��� : ");
			int code = Integer.parseInt(br.readLine());
			CartDAO dao = new CartDAO();
			ResultSet rs = dao.checkCode(code);
			if (rs.next() == true) {
				System.out.print("������ ��ǰ���� �Է��ϼ��� : ");
				String name = br.readLine();
				System.out.print("������ ����ȸ�縦 �Է��ϼ��� : ");
				String company = br.readLine();
				System.out.print("������ �����ܰ��� �Է��ϼ��� : ");
				int cost = Integer.parseInt(br.readLine());

				CartDTO dto = new CartDTO(code, name, company, cost);
				int succ = dao.updateJepum(dto);
				if (succ > 0) {
					System.out.println("�Է��Ͻ� ��ǰ�ڵ�(" + code + ")�� �����Ͽ����ϴ�.");
				} else {
					System.out.println("�Է��Ͻ� ��ǰ�ڵ�(" + code + ")�� ���� ���� �Ͽ����ϴ�.");
				}
			} else {
				System.out.println("�Է��Ͻ� ��ǰ��ȣ�� ��ϵǾ� ���� �ʽ��ϴ�.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}// updateJepum()

	// ��ǰ ���� �޼���
	public void deleteJepum() {
		try {
			System.out.println("��ǰ ���� ȭ���Դϴ�.");
			System.out.println("----------------------------");
			System.out.print("������ ��ǰ��ȣ�� �Է��ϼ��� : ");
			int code = Integer.parseInt(br.readLine());
			CartDAO dao = new CartDAO();
			ResultSet rs = dao.checkCode(code);
			if (rs.next() == true) {
				int succ = dao.deleteJepum(code);
				if (succ > 0) {
					System.out.println(code + "�� ��ǰ�ڵ尡 �����Ǿ����ϴ�.");
				} else {
					System.out.println(code + "�� ��ǰ�ڵ尡 ���������߽��ϴ�.");
				}
			} else {
				System.out.println("�Է��Ͻ� ��ǰ�ڵ�(" + code + ")�� ��ϵǾ� ���� �ʽ��ϴ�.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}// deleteJepum

	// ��ǰ �ֹ� �޼���
	public void orderJepum() {
		try {
			CartDAO dao = new CartDAO();
			System.out.println("��ǰ �ֹ� ȭ���Դϴ�.");
			System.out.println("----------------------------");
			System.out.print("�ֹ��� ��ǰ��ȣ�� �Է��ϼ��� : ");
			int code = Integer.parseInt(br.readLine());
			ResultSet rs = dao.checkCode(code);
			if (rs.next() == true) {
				System.out.print("�ֹ������� �Է��ϼ��� : ");
				int su = Integer.parseInt(br.readLine());
				CartDTO dto = new CartDTO(code, su);
				rs = dao.orderJepum(dto);
				DecimalFormat df = new DecimalFormat("\\#,##0��");
				if (rs.next()) {
					System.out.println("�ֹ��Ͻ� ��ǰ�� " + rs.getString("name") + "�̸�, ������ " + su + "�� �̰�, " + "�� ������ "
							+ df.format(su * rs.getInt("cost")) + "�Դϴ�.");
				}

			} else {
				System.out.println("�Է��Ͻ� ��ǰ��ȣ�� ��ϵǾ� ���� �ʽ��ϴ�.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}// orderJepum()

	// �ֹ��������� �޼���
	public void orderView() {
		try {
			System.out.println("�ֹ� ���� ���� ȭ���Դϴ�.");
			System.out.println("----------------------------");
			CartDAO dao = new CartDAO();
			ResultSet rs = dao.orderView();
			DecimalFormat df = new DecimalFormat("\\#,##0");
			System.out.println("CODE\tNAME\tCOMPANY\t\tCOST\t\tSU\tPRICE");
			while (rs.next()) {
				System.out.println(rs.getInt("code") + "\t" + rs.getString("name") + "\t" + rs.getString("company")
						+ "\t" + df.format(rs.getInt("cost")) + "\t" + rs.getInt("su") + "\t"
						+ df.format(rs.getInt("su") * rs.getInt("cost")));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}// orderView
}
