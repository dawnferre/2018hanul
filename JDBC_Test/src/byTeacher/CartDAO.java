package byTeacher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CartDAO {
	private Connection conn;
	private PreparedStatement ps;
	private ResultSet rs;
	ArrayList<CartDTO> list = new ArrayList<CartDTO>();

	// ��ǰ��� �޼���
	public int insertJepum(CartDTO dto) {
		conn = SingleConn.getConn();
		String sql = "insert into jepum values(?, ?, ?, ?)";
		int succ = 0;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, dto.getCode());
			ps.setString(2, dto.getName());
			ps.setString(3, dto.getCompany());
			ps.setInt(4, dto.getCost());
			succ = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return succ;
	}// insertJepum

	// ��ǰ��ȣ �ߺ��˻�
	public ResultSet checkCode(int code) {
		try {
			conn = SingleConn.getConn();
			String sql = "select * from jepum where code = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, code);
			rs = ps.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}// checkCode()

	// ��ü ��ǰ �˻� �޼���
	public ArrayList<CartDTO> searchAllJepum() {
		try {
			conn = SingleConn.getConn();
			String sql = "select * from jepum order by code asc";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				int code = rs.getInt("code");
				String name = rs.getString("name");
				String company = rs.getString("company");
				int cost = rs.getInt("cost");

				CartDTO dto = new CartDTO(code, name, company, cost);
				list.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}// searchAllJepum()

	// ��ǰ�� �˻� �޼���
	public ArrayList<CartDTO> searchName(String searchData) {
		try {
			conn = SingleConn.getConn();
			String sql = "select * from jepum where name like ? order by code asc";
			ps = conn.prepareStatement(sql);
			ps.setString(1, "%" + searchData + "%");
			rs = ps.executeQuery();
			while (rs.next()) {
				int code = rs.getInt("code");
				String name = rs.getString("name");
				String company = rs.getString("company");
				int cost = rs.getInt("cost");

				CartDTO dto = new CartDTO(code, name, company, cost);
				list.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}// searchName

	// ��ǰ���� �޼���
	public int updateJepum(CartDTO dto) {
		conn = SingleConn.getConn();
		String sql = "update jepum set name = ?, company = ?, cost =? where code =?";
		int succ = 0;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, dto.getName());
			ps.setString(2, dto.getCompany());
			ps.setInt(3, dto.getCost());
			ps.setInt(4, dto.getCode());
			succ = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return succ;
	}// updateJepum()

	// ��ǰ���� �޼���
	public int deleteJepum(int code) {
		conn = SingleConn.getConn();
		String sql = "delete from jepum where code = ?";
		int succ = 0;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, code);
			succ = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return succ;
	}// deleteJepum()

	// ��ǰ�ֹ� �޼���
	public ResultSet orderJepum(CartDTO dto) {
		try {
			conn = SingleConn.getConn();
			String sql = "insert into cart values(?, ?)";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, dto.getCode());
			ps.setInt(2, dto.getSu());
			ps.executeUpdate();

			sql = "select j.name, j.cost, c.su from jepum j, cart c where j.code = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, dto.getCode());
			rs = ps.executeQuery();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}// orderJepum()

	// ��ü �ֹ� ���� ���� �޼���
	public ResultSet orderView() {
		try {
			conn = SingleConn.getConn();
			String sql = "select j.code, j.name, j.company, j.cost, c.su from jepum j, cart c where j.code = c.code order by j.code asc";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}// orderView()
}
