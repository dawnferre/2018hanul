
public class SleepThread extends Thread{
	private int num;
	
	//int num == 대기시간 (1/1000초 = 밀리세컨즈)
	public SleepThread(int num){
		this. num = num;
	}
	
	@Override
	public void run() {
		for(int i = 1; i< 11; i++){
			System.out.print(num +", ");
			try {
				sleep(500);//0.5초 대기
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}//run
	
}
class SleepThread2 extends Thread{
	private int num;

	@Override
	public void run() {
		for(int i = 1; i< 11; i++){
			System.out.print(i+", ");
		}
	}//run
	
}
