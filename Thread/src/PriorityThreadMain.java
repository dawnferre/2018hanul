
public class PriorityThreadMain {
	public static void main(String[] args) {
		PriorityThread th1 = new PriorityThread();
		PriorityThread th2 = new PriorityThread();
		PriorityThread th3 = new PriorityThread();
//		PriorityThread th4 = new PriorityThread();
		
		th1.setPriority(Thread.MIN_PRIORITY);
		th2.setPriority(Thread.MAX_PRIORITY);
		th3.setPriority(Thread.NORM_PRIORITY);
		th1.start();
		th2.start();
		th3.start();
//		th4.start();
	}//main
}//class
