import java.awt.Frame;

//자바에서는 다중상속이 불가능
//이미 다른 클래스를 상속받은 상태에서 thread 구현 ==>문제가 발생
//이러한 문제점을 해결하기 위해 runnable 인터페이스 제공 ==> 다중상속이 가능.
public class MyThread04 extends Frame implements Runnable{
	
	@Override
	public void run() {
		for(int i = 1; i<11; i++){
			System.out.print(i+", ");
		}
	}//run
}//class

//nested class
class MyThread05 implements Runnable{

	@Override
	public void run() {
		//영어 대문자 출력
		for(char i='A'; i<='Z'; i++){
			System.out.print(i +", ");
		}
	}
	
}