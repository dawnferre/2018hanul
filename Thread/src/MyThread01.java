//자바에서 thread 활용하기 위해서.
//1. thread class를 상속받아 구현한다.
public class MyThread01 extends Thread{
	//쓰레드가 실행할 코드 (1~30 출력) == run메소드에서 재정의해준다.
	@Override
	public void run() {
		//출력할 코드를 run 메소드에 적어줌.
		for (int i = 1; i < 31; i++) {
			if(i ==30){
				System.out.print(i);
			}else{
				System.out.print(i+", ");
			}
		}
	}//run()
	//내부 nested class 표시는 mythread01$test.class로 한다.(내부위치)
	class test2{
		
	}
}//class1

//.java 에 여러 클래스를 구현할 수 있다 == nested class
//1. 처음 클래스는 접근지정자 설정 가능
//2. 그다음 클래스는 그냥 class class이름

//외부 nested class_접급제어자 만들필요없음.
class MyThread02 extends Thread {
	//a~z까지 출력.
	@Override
	public void run() {
		for(char i ='a'; i<='z'; i++){
			if(i =='z'){
				System.out.print(i);
			}else{
				System.out.print(i+", ");
			}
		}
	}
}//class2

