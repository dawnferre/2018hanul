
public class MyThread04Main {
	public static void main(String[] args) {
		//객체생성.
		MyThread04 run1 = new MyThread04();
		MyThread05 run2 = new MyThread05();
		
		
		//Runnable 인터페이스 상속된 쓰레드는 진짜 쓰레드 X ==> start()가 나오지 않는다.
		//thread 객체를 생성하고 runnable 받은 변수로 처리하여 진짜 쓰레드로 변환
		Thread th = new Thread(run1);
		Thread th1 = new Thread(run2);
		
		th.start();
		th1.start();
		
	}//main
}//class
