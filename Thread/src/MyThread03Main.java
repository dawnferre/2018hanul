
public class MyThread03Main {
	public static void main(String[] args) {
		//하나의 작업 _ MyThread03 => 객체를 여러개 생성 , 객체별로 동시에 작업이 수행됨.
		MyThread03 th0 = new MyThread03();
		MyThread03 th1 = new MyThread03();
		MyThread03 th2 = new MyThread03();
		MyThread03 th3 = new MyThread03();
		
		//run 메소드 호출 == start()
		//순서는 cpu에 따라 다 달라지고, 실행할때마다 달라진다.
		th0.start();
		th1.start();
		th2.start();
		th3.start();
	}//main
}//class
