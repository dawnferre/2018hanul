//쓰레드는 우선순위를 설정할 수 있다.
// 1- 10까지 설정하며, 숫자가 높을수록 우선순위가 높다.
//기본 우선순위(norm_ priority)은 5로 설정.
//getPriority()메소드로 우선순위를 확인
//setPriority()메소드로 우선순위를 설정.
public class PriorityThread extends Thread {
	@Override
	public void run() {
		for(int i =0; i<10; i++){
			
			System.out.println(getName()+"우선순위 :"+ getPriority()+",hello");
		}
		System.out.println();
	}
}
