import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Random;


//Awt (abstract window toolkit): 자바로 Gui(Graphic user interface)을 구축하기 위한 클래스
public class TestAwtThread extends Frame {//awt구현하기 위해 frame class상속
	int x, y;
	public static void main(String[] args) {
		  new TestAwtThread(); //객체생성시 default생성자 바로 실행
	}//main
	public TestAwtThread(){
		super("Test_Awt_thread"); //frame 창 제목표시줄에 표시되는 내용
		// * 라벨의 크기가 크기 때문에 좌표찍는것이 덮여져 보이지 않게 된다(포토샵 레이어처럼)
		//뭐가 위로 올것인지, 아래로 올것인지는 패널의 머지기능을 이용해서 위아래지정을 할 수 있다. 
		
//		Label label = new Label("내부 내용을 나타내는 라벨입니다.",Label.CENTER);
		setSize(300,300); //frame창의 크기 지정.
		setVisible(true);
//		add(label);
		//frame창의 닫기 단추를 누르면 창이 종료되는 이벤트_ 내부 익명클래스
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				dispose();
				System.exit(0);
			}
		});//addWindowListener
		
		//thread 시작
		x =-1; 
		y =-1; // 프레임 범위의 값을 우선 준다.
							//자기자신인  twt를 받기 때문에 
		Running th1 = new Running(this);
		Running th2 = new Running(this);
		Running th3 = new Running(this);
		
		th1.start();
		th2.start();
		th3.start();
		
	}//default 생성자
	
	//thread class 생성_ 프레임창에 점을 찍는 작업을 수행(좌표 랜덤생성)
	class Running extends Thread{
		TestAwtThread tat;
		Random ran;
		Dimension di;
		
		public Running(TestAwtThread tat){
			this.tat = tat;
			ran = new Random();
			di = tat.getSize(); // 프레임에 해당하는 크기를 가지고 와야함.
		}// default 생성자
		@Override
		public void run() {
			while(true){
				//x,y좌표의 값을 가지고 옴._ int범위내에서 약 9만개 좌표를 사용가능.
				x = ran.nextInt(di.width)+1;
				y = ran.nextInt(di.height)+1;
				tat.repaint(); // 중복되는 좌표제거
				try {
					Thread.sleep(1000); //1초의 간격을 둠
				} catch (Exception e) {
					e.printStackTrace();
				}
						
			}
		}
	}//inner class
	
	//생성된 좌표에 색상을 칠하는 메소드 재정의
	@Override
		public void paint(Graphics g) {
			g.setColor(Color.red);
			
			if(x!= -1 || y!= -1){
				g.fillOval(x, y, 10, 10);
			}
	}
}//class
