
public class MyThread01Main {
	public static void main(String[] args) {
		MyThread01 th = new MyThread01();
		MyThread02 th1 = new MyThread02();
		
		System.out.println();
//		th.run();
//		th1.run();
		
		//thread01,02 가 동시에 출력하게끔 호출 >> start() == 순서가 뒤죽박죽..(먼저 수행한다 해서 순차적으로 끝나는것은 아니다.)
		//thread 실행시마다 다른 결과를 얻을 수 있다.		
		th.start();
		th1.start();
		
		System.out.println();
		//A~Z출력
		for(char a ='A'; a<='Z'; a++){
			System.out.print(a+", ");
		}
	}//main
}//class
