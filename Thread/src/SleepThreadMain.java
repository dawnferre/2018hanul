
public class SleepThreadMain {
	public static void main(String[] args) {
		SleepThread th = new SleepThread(4);
		SleepThread th3 = new SleepThread(0);
		SleepThread2 th2 = new SleepThread2();
		
		th.start();
		// sleep 초 안에 다른 th2 쓰레드가 생성됨. 
		th2.start();
		th3.start();
		
		//ms 밀리세컨 --> 1/1,000초
		//㎛ 마이크로세컨 --> 1/1,000,000초(um)
		//ns 나노 --> 1/1,000,000,000초
		//ps 피코 --> 10^9
		//fs 펨토 --> 10^12
		//as 아토 --> 10^15
	}
}
