import com.hanul.poly01.Animal;
import com.hanul.poly01.Cat;
import com.hanul.poly01.Dog;

public class Poly01Main01 {
	public static void main(String[] args) {
		//Dog 객체를 생성하고 cry() 메서드를 호출.
		Dog dog = new Dog(); //객체생성_일반적인 방식.
		dog.cry();
		
		//dog 객체를 부모(animal)쪽으로 생성하고 cry()메소드를 호출.
		Animal animal =new Dog();//객체생성 : 업캐스팅(upcasting) ==>다형성
		animal.cry();
		
		Animal animal2 = new Cat();
		animal2.cry();//객체생성 : 업캐스팅(upcasting) ==>다형성
//		animal2.night(); // 부모는 가지고 있는 메소드한에서만 .통제가능 ==? 다형성불가.
		//즉, 부모가 하위 클래스의 객체를 통제할 수 있다.
		
		//알파상태 즉, 하위클래스만 가지는 메소드에는 일반 객체호출로 부를 수 있음 
		Cat cat = new Cat();
		cat.night();
	}//main
}//class
