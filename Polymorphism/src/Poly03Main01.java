import com.hanul.poly03.Radio;
import com.hanul.poly03.Remocon;
import com.hanul.poly03.TV;

public class Poly03Main01 {
	public static void main(String[] args) {
		//리모콘인터페이스로 티비 소리 올리고 내린후 인터넷 접속
		Remocon con = new TV();
		//바디없는 부모클래스 쪽으로 upcasting 해주기 때문에 자식클래스의 바디를 가지고 올 수 있다.
		con.volumUP();
		con.volumDown();
		con.internet();
		
		System.out.println("-----------------------");
		//변수 재할당.
		con = new Radio();
		con.volumUP();
		con.volumDown();
		con.internet();
		
		System.out.println("-----------------------");

		TV tv = new TV();
		Radio ra = new Radio();
		
		display(tv);
		display(ra);
	}//main
	
	public static void display(Remocon con){
		con.volumUP();
		con.volumDown();
		con.internet();
		
	}
}//class
