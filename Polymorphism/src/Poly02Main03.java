import com.hanul.poly02.Animal;
import com.hanul.poly02.Cat;
import com.hanul.poly02.Dog;

public class Poly02Main03 {
	public static void main(String[] args) {
		//Dog객체와 Cat객체를 생성 : 일반적인 생성
		Dog dog = new Dog();
		Cat cat = new Cat();
		
		
		//생성된 객체를 배열에 저장하시오 .. >> 타입[] 배열명
		Animal[] animal ={dog, cat};
		for (int i = 0; i < animal.length; i++) {
			animal[i].cry();
			
			if(animal[i] instanceof Cat){
				((Cat)animal[i]).night(); 
				//부모에서 upcasting 설정후 뽑아줘야 부모클래스에서 자식클래스 메소드를 가져다 쓸 수 있음.
			}
		}
		
		
		
	}
}
