import com.hanul.poly02.Animal;
import com.hanul.poly02.Cat;
import com.hanul.poly02.Dog;

public class Poly02Main04 {
	public static void main(String[] args) {
		//dog 객체와 cat객체를 배열에 저장. :다형성 배열
		Animal[] animal = new Animal[2]; //사용하고자하는 다형성 배열 선언 /생성
		animal[0] = new Dog();
		animal[1] = new Cat();
		
		display(animal);
		
	}
	//cry()메소드를 정의하고 cry()메소드를 동작.
	public static void display(Animal[] animal){
		for (int i = 0; i < animal.length; i++) {
			animal[i].cry();
			if(animal[i] instanceof Cat){
				((Cat)animal[i]).night();
			}
		}
	}
}
