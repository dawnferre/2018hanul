import com.hanul.poly02.Animal;
import com.hanul.poly02.Cat;
import com.hanul.poly02.Dog;

public class Poly02Main02 {
	public static void main(String[] args) {
		Dog dog = new Dog(); //일반적인 객체 생성
		Cat cat = new Cat(); // -
		
		display(dog); //실인수
		display(cat);
		
	}//main()
	//overloading :메소드 2개.     가인수 → 다형성인수(부모인수로 설정해서,자식인수상관없이 넣을수 있음)
	public static void display(Animal animal){
		//부모의 형태를 넣으면 객체로 생성된 자식 클래스를 쓸 수 있다.
		animal.cry();
//		Cat.night() 오류 ㅣ 동작되지 않는다.
		
		if(animal instanceof Cat){ //*****instanceof : 타입을 정확하게 알아본다. (자식타입구분할때 가능.)
			((Cat)animal).night(); //downcasting
			
		}
	} 
		
}//class
