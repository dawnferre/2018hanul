package com.hanul.poly02;

public abstract class Animal { //** abstract : 
	//추상클래스 : 추상메소드를 하나라도 가지고 있는 클래스

	
	//추상메소드 : body가 없는 메소드 (정의만 되어있음)
	public abstract void cry(); 
	
}
