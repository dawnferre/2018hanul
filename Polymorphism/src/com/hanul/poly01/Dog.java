package com.hanul.poly01;

public class Dog extends Animal{
	//dog class : (하위클래스) animal Class(상위클래스)로부터 상속(inheritance)
	@Override
	public void cry() {//override()
		System.out.println("강아지 _멍멍");
	}
	
}
