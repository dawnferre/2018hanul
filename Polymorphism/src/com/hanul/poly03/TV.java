package com.hanul.poly03;

public class TV implements Remocon{
	//Remocon interface 상속 → override(필수!)
		
	@Override
	public void volumUP() {
		System.out.println("tv_소리키우기");
	}

	@Override
	public void volumDown() {
		System.out.println("tv_소리줄이기");
		
	}

	@Override
	public void internet() {
		System.out.println("tv_인터넷연결");
		
	}

}
