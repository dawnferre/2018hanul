package com.hanul.poly03;

public class Radio implements Remocon {

	@Override
	public void volumUP() {
		System.out.println("라디오_소리키우기");
	}

	@Override
	public void volumDown() {
		System.out.println("라디오_소리줄이기");

	}

	@Override
	public void internet() {
		System.out.println("라디오_인터넷안돼요오");
	}

}
