package com.hanul.poly04;

public class A {
	public void display(){
		System.out.println("나는 A이다.");
	}
}
/*
 * 
 *  *class 만들면 생략된 표기법 3가지가 있다.
 *  	-public class A extends Object(즉, extends Object)
 *  	-import java.lang *;
 *  	-public A(){} >> default 생성자.
 *  
 * 
 * 
 * */
 