import com.hanul.poly02.Animal;
import com.hanul.poly02.Cat;
import com.hanul.poly02.Dog;

public class Poly02Main01 {
	public static void main(String[] args) {
		Animal animal = new Dog();
		animal.cry();
		
		Animal animal2 = new Cat();
		animal2.cry();
		
//		Animal animal3 = new Animal();  :  추상클래스이기때문에 불가.
		
	}
}
