import com.hanul.poly04.A;
import com.hanul.poly04.B;

public class Poly04Main01 {
	public static void main(String[] args) {
		//A객체와 B객체를 생성하고 display()메소드를 호출
		A a = new A(); //일반적인 객체 생성
		B b = new B(); 
		a.display();
		b.display();
		
		//A객체를 UpCasting 으로 생성하고 display()메서드 호출
		Object ob = new A();//upcasting으로 부모호출.
		((A) ob).display(); //downcasting으로 메서드 호출.
		
		Object ob1 = new B();
		((B) ob1).display();
		
	}//main
}//class
