import com.hanul.poly01.Animal;
import com.hanul.poly01.Cat;
import com.hanul.poly01.Dog;

public class Poly01Main02 {
	public static void main(String[] args) {
		Dog dog = new Dog();
		dog.cry();
		//다형성
		Animal animal = new Cat();
		animal.cry();
		
		Cat cat = new Cat();
		cat.cry();
		cat.night();
		
//		Cat cat = animal; : 부모를 자식 클래스에 그냥 넘기긴 어렵다.
		Cat cat1 = (Cat)animal; //downcasting
		cat.night();
		//downcasting의 다른 방법
		((Cat)animal).night();
	}//main
}//class
