import com.hanul.dao.SungjukDAO;
import com.hanul.dto.SungjukDTO;

public class SungjukMain {
	public static void main(String[] args) {
		SungjukDTO[] sj = new SungjukDTO[3];
		sj[0] = new SungjukDTO("홍길동", 2018001, "컴퓨터공학", 87.5F, 93.5F);
		sj[1] = new SungjukDTO("김길동", 2018002, "정보통신과", 67.4F, 65.5F);
		sj[2] = new SungjukDTO("고길동", 2018003, "정보보안과", 78.5F, 88.3F);
		
		SungjukDAO dao = new SungjukDAO();
		dao.getSumAvg(sj);
		dao.display(sj);
		
	}

}
