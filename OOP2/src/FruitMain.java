import com.hanul.dao.FruitDAO;
import com.hanul.dto.FruitDTO;
//패키지가 다를땐 import 
public class FruitMain {
	public static void main(String[] args) {
		FruitDTO[] dto = new FruitDTO[5];
		FruitDTO[] dto2 = new FruitDTO[5];
		
		//객체 생성시 배열변수를 넘겨주는것으로 바꿔보기(***)
		
		FruitDAO dao = new FruitDAO(); 
		//fruit 배열에 변수 지정._ dao메소드 호출.
		dto[0] = new FruitDTO("사과", 570, 10, dao.getPrice(570, 10) );
		dto[1] = new FruitDTO("복숭아", 700, 8, dao.getPrice(700, 8));
		dto[2] = new FruitDTO("귤", 500, 11, dao.getPrice(500, 11));
		dto[3] = new FruitDTO("딸기", 370, 7, dao.getPrice(370, 7));
		dto[4] = new FruitDTO("포도", 1070, 13, dao.getPrice(1070, 13));
		
		dao.display(dto);
		
		System.out.println("+++++++++++++++++++++++++++++++++");
		//overloading메소드 사용_dao메소드 호출X
		dto2[0] = new FruitDTO("사과", 570, 10);
		dto2[1] = new FruitDTO("복숭아", 700, 8);
		dto2[2] = new FruitDTO("귤", 500, 11);
		dto2[3] = new FruitDTO("딸기", 370, 7);
		dto2[4] = new FruitDTO("포도", 1070, 13);
		
		dao.display_overload(dto2);
	}//main()
}//class
