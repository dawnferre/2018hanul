package com.hanul.dto;

public class FruitDTO {
//	- 멤버변수 name, cost, su , price선언
	private String name;
	private int cost;
	private int su;
	private int price;
//	 - default 생성자 메서드
	public FruitDTO(){
		
	}
//	 - 생성자 메소드 초기화
	public FruitDTO(String name, int cost, int su, int price) {
	super();
	this.name = name;
	this.cost = cost;
	this.su = su;
	this.price = price;
}
	//메소드 overloading :메소드의 이름이 반드시 같고 매개변수의 개수가 달라야한다. 개수가 같을 경우에는 매개변수의 타입이 달라야 한다.
	public FruitDTO(String name, int cost, int su) {
		super();
		this.name = name;
		this.cost = cost;
		this.su = su;
	}

//	 - get/set 메소드 생성
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public int getSu() {
		return su;
	}
	public void setSu(int su) {
		this.su = su;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
	
}
