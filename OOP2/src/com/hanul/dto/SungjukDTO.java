package com.hanul.dto;

public class SungjukDTO {
	private String name;
	private int hakbun;
	private String major;
	private float java;
	private float sql;
	private float sum;
	private float avg;
	
	public SungjukDTO(){}


	public SungjukDTO(String name, int hakbun, String major, float java, float sql) {
		super();
		this.name = name;
		this.hakbun = hakbun;
		this.major = major;
		this.java = java;
		this.sql = sql;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getHakbun() {
		return hakbun;
	}


	public void setHakbun(int hakbun) {
		this.hakbun = hakbun;
	}


	public String getMajor() {
		return major;
	}


	public void setMajor(String major) {
		this.major = major;
	}


	public float getJava() {
		return java;
	}


	public void setJava(float java) {
		this.java = java;
	}


	public float getSql() {
		return sql;
	}


	public void setSql(float sql) {
		this.sql = sql;
	}


	public float getSum() {
		return sum;
	}


	public void setSum(float sum) {
		this.sum = sum;
	}


	public float getAvg() {
		return avg;
	}


	public void setAvg(float avg) {
		this.avg = avg;
	}	
	
}
