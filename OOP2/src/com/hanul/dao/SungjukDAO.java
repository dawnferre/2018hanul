package com.hanul.dao;

import java.text.DecimalFormat;

import com.hanul.dto.SungjukDTO;

public class SungjukDAO {

	public void getSumAvg(SungjukDTO[] sj) {
		for (int i = 0; i < sj.length; i++) {
			sj[i].setSum(sj[i].getJava() + sj[i].getSql());
			sj[i].setAvg(sj[i].getSum() / 2);
		}
	}//getSumAvg()

	public void display(SungjukDTO[] sj) {
		DecimalFormat df = new DecimalFormat("0.0");
		System.out.println("�̸�\t�й�\t�а�\t\tJAVA\tSQL\t����\t���");
		System.out.println("=================================================================");
		for (int i = 0; i < sj.length; i++) {
			System.out.print(sj[i].getName() + "\t");
			System.out.print(sj[i].getHakbun() + "\t");
			System.out.print(sj[i].getMajor() + "\t");
			System.out.print(sj[i].getJava() + "\t");
			System.out.print(sj[i].getSql() + "\t");
			System.out.print(sj[i].getSum() + "\t");
			System.out.print(df.format(sj[i].getAvg()) + "\n");
		}
		System.out.println("=================================================================");
	}//display()

}//class
