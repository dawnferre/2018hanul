package com.hanul.dao;

import com.hanul.dto.FruitDTO;

public class FruitDAO {
	//가격 구하는 메소드
	
	//배열을 변수로 받아서 사용가능(선생님 코드 참고)
	public int getPrice(int cost, int su){
		int price  = cost* su;
		
		return price;
	}
	
	//출력하는 메소드 _ 입력을 받고 바로 출력
	public void display(FruitDTO[] dto){
//		FruitDAO dao = new FruitDAO()
		System.out.print("과일명\t");
		System.out.print("단가\t");
		System.out.print("수량\t");
		System.out.println("가격\t");
		
		for (int i = 0; i < dto.length; i++) {
			System.out.print(dto[i].getName()+"\t");
			System.out.print(dto[i].getCost()+"\t");
			System.out.print(dto[i].getSu()+"\t");
			System.out.println(dto[i].getPrice()+"\t");
		}
	}
	//입력을 바로 받지 않고 배열값을 받아서 처리함.
	public void display_overload(FruitDTO[] dto){
		FruitDAO dao = new FruitDAO();
		
		System.out.print("과일명\t");
		System.out.print("단가\t");
		System.out.print("수량\t");
		System.out.println("가격\t");
		
		for (int i = 0; i < dto.length; i++) {
			System.out.print(dto[i].getName()+"\t");
			System.out.print(dto[i].getCost()+"\t");
			System.out.print(dto[i].getSu()+"\t");
			System.out.println(dao.getPrice(dto[i].getCost(), dto[i].getSu())+"\t");
		}
	}
}
