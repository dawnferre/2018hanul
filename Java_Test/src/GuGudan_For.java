import java.text.DecimalFormat;
import java.util.Scanner;

public class GuGudan_For {
	public static void main(String[] args) {
		GuGudan_For gu = new GuGudan_For();//객체호출
		Scanner scan = new Scanner(System.in);
		int num=0;
		while(true){
			System.out.print("출력하고 싶은 단을 입력하세요 :");
			num = scan.nextInt();//입력을 받음
			if(num <=1 ||num >=10){//예외처리
				System.out.println("구구단은 2단~9단까지만 가능합니다.");
				continue;
			}else{
				gu.showGuGudanByfor(num);//메소드호출
				break;
			}
			
		}//while
		scan.close();

	}//main
	
	//출력메소드
	public void showGuGudanByfor(int num){
		System.out.println(num+"단을 출력합니다.");
			for(int j = 1; j<10; j++){
				//자리수 맞춰주기
				DecimalFormat decimal = new DecimalFormat("00");
				String result =decimal.format(num*j);
				
				System.out.println(num+"x"+j+"="+result);
			}
	}
}//class
