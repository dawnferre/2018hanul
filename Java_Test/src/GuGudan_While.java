import java.text.DecimalFormat;
import java.util.Scanner;

public class GuGudan_While {
	public static void main(String[] args) {
		GuGudan_While gu = new GuGudan_While();//객체호출
		Scanner scan = new Scanner(System.in);
		int num=0;
		while(true){
			System.out.print("출력하고 싶은 단을 입력하세요 :");
			num = scan.nextInt();//입력을 받음
			if(num <=1 ||num >=10){//예외처리
				System.out.println("구구단은 2단~9단까지만 가능합니다.");
				continue;
			}else{
				gu.showGuGudanByWhile(num);//메소드호출
				break;
			}
			
		}//while
		scan.close();
	}//main
	
	public void showGuGudanByWhile(int num){
		System.out.println(num+"단을 출력합니다.");
		int j = 1;
		while(num<10){
			if(j<10){
				DecimalFormat format = new DecimalFormat("00");
				String result = format.format(num*j);
				
				System.out.println(num+"x"+j+"="+result);//자리수 맞춤.
				j++;//j값을 늘려줌(조건에 부합할때까지)
				continue;// 다시 반복문이 돌때까지 처리
				
			}else{// j가 조건에 부합하지 않으면
				break; //while 빠져나감.
			}
		}
	}
}
