/** 대입 연산자*/
public class Operator06 {
	public static void main(String[] args) {
		//대입(할당)연산자 : =(equal)은 할당과 선언 , 대입을 한다.
		//L-Value = R-Value : R-Value값(연산결과)을 L-Value로 대입(할당)
		
		int a = 10;//선언, 할당 == 초기화
		System.out.println("a의 값 : " + a);
		
		int b = a; //선언, 할당 == 초기화
		System.out.println("b의 값 : " + b);
		
		b = a + a;//재할당
		System.out.println("b의 값 : " + b);// 20
		
		a = a + a; //재할당
		System.out.println("a의 값 : " + a);
		// 복합 대입 연산자 (191P~)
		//복합 대입 연산자 (**) : 연산에 사용되는 변수와 연산의 결과가 저장(할당, 대입)되는 변수가 같을 경우 사용함. 
		a += a;
		System.out.println("==============");
		System.out.println("복합 대입 연산자 사용");
		System.out.println("a의 값 : " + a);
		
		int x = 10; //초기화
		x = x + 100;
		System.out.println("x의 값 : " + x);
		
		int y = 10;
		y += 100; // == y = y+100;
		System.out.println("y의 값 : " + y);
		
		x += y;// == x = x + y;
		x -= y;// == x = x - y;
		x *= y;// == x = x * y;
		x /= y;// == x = x / y;
		
		
		 
	}//main()
}//class
