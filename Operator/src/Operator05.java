/**논리 연산자*/
public class Operator05 {
	public static void main(String[] args) {
		//논리연산자 : 여러 개의 조건을 종합적으로 판단 , 조건문작성
		//연산의 결과는 true, false 변환
		//AND : 조건A && 조건B = 조건A도 참이고, 조건B도 참일 결우 ==> 참으로 출력
		//OR : 조건A || 조건B = 둘 중 하나라도 만족하면(참일 경우) ==> 참으로 출력
		
		//AND
		System.out.println(10 > 5 && 20 > 5);// true 
		System.out.println(10 > 5 && 5 > 20);// false
		System.out.println(5 > 10 && 20 > 5);// false
		// 처음 비교 연산에서 결과 false가 나오기 때문에 , 그다음 코드가 불필요한 코드가 된다.
		System.out.println(5 > 10 && 5 > 20);// false
		
		System.out.println("=======");
		//OR
		System.out.println(10 > 5 || 20 > 5);// true 
		System.out.println(10 > 5 || 5 > 20);// true
		System.out.println(5 > 10 || 20 > 5);// true
		System.out.println(5 > 10 || 5 > 20);// false
		
	}//main()
}//class
