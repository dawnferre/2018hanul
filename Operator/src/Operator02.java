/* class이름 수정시 , 단축키f2 또는 마우스 우클릭 =refactor - rename */
public class Operator02 {

	public static void main(String[] args) {
		//자동증감연산자 :++(증가),--(감소)  >> 반복문에서 많이 사용
		//별도의 연산에 대한 결과를 처리하지 않는 연산자. >> 단항연산
		//자동증감연산자의 위치가 변수의 앞인지 뒤인지 따라 결과가 다르다.
		
		int a = 0;
		int b = 10;
		
		//이항연산은 결과를 담을 변수가 필요함.(따라서 그대로 연산시, 오류발생) 
		int addResult = a + b;
		
		//단항연산자는 필요없음.
		System.out.println(++a); //결과 : 1 (기존 값에 먼.저. 1이 증가됨)
		System.out.println(--b); //결과 : 9 (기존 값에 먼.저. 1이 감소됨)
		
		int c = 0;
		int d = 10;
		
		System.out.println(c++); //결과 : 0 (기존 값 먼저 출력 후 연산되어 연산이 우선 되지않음)
								 // 메모리상의 값 : 1
		System.out.println(d--); //결과 : 10 (기존 값 먼저 출력 후 연산되어 연산이 우선 되지않음)
								 // 메모리상의 값 : 9
		System.out.println(c); //결과: 1
		System.out.println(d); //결과: 9
		
		int x = 5;
		int y = 10;
		// Q. x++ + ++x + y++ 의 결과는?
		System.out.println( x++ + ++x  + y++); // 결과 : 22
		
		
		/*
		 *  연산  출력  메모리
		 *  x++  5   6
		 *  ++x  7   7
		 *  y++  10  11
		 *  
		 * */
		
		
	}//main()
}//class
