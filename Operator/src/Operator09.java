
public class Operator09 {
	public static void main(String[] args) {
		// 0으로 시작되는 것은 8진수로 인식
		int n = 015;
		// 0x으로 시작되는 것은 16진수로 인식
		int k = 0x15;
		
		int a = 16;
		int b = 0X11;//17
		int c = 011;//9
		
		System.out.println(n);
		System.out.println(k);
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		
	}//main()

}//class
