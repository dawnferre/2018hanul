/**특수 기호문자*/
public class Operator08 {
	public static void main(String[] args) {
		//프로그램에서 사용되는 특수기호문자(Escape Sequence)
		//출력문에서 사용함.
		//역슬래쉬(\)를 먼저 입력하고 사용한다.
		//문자 리터럴의 표기방법_155P~
		System.out.println("1.HelloWorld");
		System.out.println("2.Hello" + "\t" + "world"); // \t:띄어 쓰기(일정간격: 8칸) 
		System.out.println("2.Hello\tWorld"); // 문자 출력문안에 사용가능.
		System.out.println("3.Hello\nWorld"); // 줄바꾸기(new line)
		System.out.println("4.Hello\bWorld"); // backspace, 커서를 한칸 뒤로 이동함. 커서가 이동되면서 바로 앞에 문자 뒤에 그다음 결과를 출력함.
		System.out.println("5.Hello\rWorld"); // Carriage return (커서를 맨앞으로 이동 후, 그다음 결과를 출력함.)
		System.out.println("6.Hello\"World"); // "를 화면에 표시해주세요. (문자열을 묶는 단위가 아닌 문자)
		System.out.println("7.Hello\'World"); //'를 화면에 표시해주세요.
		System.out.println("8.Hello\\World"); //\를 \화면에 표시해주세요.
		
		
		//\t의 사용예시 _표로 정렬되거나 같은 라인정리를 쉽게 하기위해서
		System.out.println("번호"+"\t"+"이름"+"\t"+"주소"); // 이상한 일이네.. 위에서는 1칸만 띄어쓰고 , 여기선 원래로 잘 나눠있네..
		System.out.println("1\tKim\t광주 서구");
		System.out.println("100\tAn\t광주 동구");
		System.out.println("3\tJin\t광주 북구");
		System.out.println("10\tSun\t광주 남구");
		//print()와 \n으로 사용해서 줄바꿈을 해놓음.
		System.out.print("번호"+"\t"+"이름"+"\t"+"주소\n"); // 이상한 일이네.. 위에서는 1칸만 띄어쓰고 , 여기선 원래로 잘 나눠있네..
		System.out.print("1\tKim\t광주 서구\n");
		System.out.print("100\tAn\t광주 동구\n");
		System.out.print("3\tJin\t광주 북구\n");
		System.out.print("10\tSun\t광주 남구");
		
	}//main()
}//class
