
public class Operator03 {

	public static void main(String[] args) {
	 // 비트연산자 : 개발자의 입장에서 비트를 직접 조작할 수 있다는 장점
	 // 현재는 거의 사용하지 않음.
	 // 논리연산과 관련되어 있어 개념은 숙지하고 가야함.
	 
	 // 1. &(AND) = 두 수의 비교일때 모두 1일때(모두 참일때) 1로 출력.
	 // 2. |(OR) = 하나만 1일때(참일때) 1로 출력
	 // 3. ^(XOR) = 서로 다를때(같지 않을때) 1로 출력
	 // 4. ~(NOT) = 2진법으로 표현한 수 0,1을 각각 바꾸어줌.(0>>1, 1>>0)
		
		int x = 3;
		int y = 2;
		
		//2진법으로 계산하여 값이 나옴
		System.out.println(x&y); //결과 : 2 0011 & 0010 = 0010
		System.out.println(x|y); //결과 : 3 0011 | 0010 = 0011
		System.out.println(x^y); //결과 : 1 0011 ^ 0010 = 0001
	}//main()

}//class
