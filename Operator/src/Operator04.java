/** 비교연산자 */
public class Operator04 {
	public static void main(String[] args) {
		//비교연산자는 조건식 작성에 사용, 결과는 true, false 로 나오게 된다.
		// >,>=,<,<= : 수학에서의 연산과 같은 부등호연산(관계연산)
		// 수학에서는 =는 같다라는 의미지만, 프로그램에서는 변수선언 , 할당(대입)연산자
		// 프로그램에서의 같다는 == 이며 같지않다는 !=로 나타낸다.
		int a = 10;
		int b = 5;
		
		System.out.println(a==b);// false
		System.out.println(a!=b);//true
		System.out.println(a>b);// true
		System.out.println(a>=b);// true
		System.out.println(a<b);// false
		System.out.println(a<=b);// false
		
		System.out.println(a>10);// false
		System.out.println(a>=10);// true
		
		System.out.println(!(3<5)); //()은 true지만, !가 있어 false로 값이 바뀜.
	}//main()
}//class
