
public class Operator01 {

	public static void main(String[] args) {
		//산술 연산자 :+,-,*,/,%
		//%(나머지연산) : 주어진 수가 짝수/홀수, 배수의 판단에 사용.
		
		int x = 10; // 변수 선언, 할당, 나열
		int y = 3;
		//== int x = 10, y = 3;
		
		//이항연산: 항이 두개가 필요한 연산 <==> 단항연산
		int addResult = x + y; // 연산결과를 변수에 할당.(+)
		int subResult = x - y; // 뺄셈
		int multiResult = x * y; //곱셈
		int divResult = x / y; //나눗셈.3.33333333.... int자료형에 따라 정수만 들어감.(자료손실)
		int modResult = x % y; //나머지 연산
		
		System.out.println("덧셈: "+addResult); //결과: 13
		System.out.println("뺄셈: "+subResult); //결과: 7
		System.out.println("곱셈: "+multiResult); //결과: 30
		System.out.println("나눗셈: "+divResult); //결과: 3 (소수가 나오지만, 자료형에 의해 자료손실)
		System.out.println("나머지: "+modResult); //나머지: 1
		
		int a = 3;
		int b = 5;
		//***
		System.out.println("a + b: "+a+b); //결과 = a+b : 35
		//** 출력문안에 +는 연산자가 아니라 결합을 시키는 역할, 즉, 문자열+문자열(로 취급)
		System.out.println("a + b: "+(a+b)); //결과 = a+b :8
		//연산을 원할 시에는 ()으로 묶어준다.
		System.out.println("a - b: "+(a-b)); //결과 = a-b :-2
		
		//먼저 ()가 나오는 경우, 계산 먼저 한다.(최상위 연산자)
		System.out.println(a+b+"= a + b"); //결과 = 8=a+b ()미사용
		System.out.println((a+b)+"= a + b"); //결과 = 8=a+b ()사용
		
		
	}//main()

}//class
