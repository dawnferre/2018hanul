/**삼항 연산자*/
public class Operator07 {
	public static void main(String[] args) {
		// 조건연산자(삼항연산자) : 3개의 항으로 구성되는 연산자
		// 간단한 조건을 처리할 경우 편리 = > 실제 실무에서는 조건문을 사용함(if/else)
		// 조건? true:false;
		// 비교 연산자와 논리연산자를 사용해서 true.false 값이 나올 수 있게 조건문을 작성해야 한다.
		
		int a = 10;
		char c1 = a==10?'O':'X';
		System.out.println(c1);
		
		char c2 = a!=10?'O':'X';
		System.out.println("a!=10의 값은 "+c2);

		char result;
		if(a == 10){
			result = 'O';
		}else{
			result ='X';
			
		}//if()
		System.out.println("result값 = "+result);
		
		//문장을 넣고싶을 때에는 String을 사용함(다양한 타입들이 있지..)
		String result2;
		if(c2 =='O'){
			result2 = "true";
		}else{
			result2 = "false";
		}
		System.out.println("result2값 = "+result2);
	}//main()
}//class
