public class Casting01 {
	public static void main(String[] args) {
		int a = 20;	//정수형 변수 a를 선언하고 값(20)을 할당 한 상태 ▶ 초기화
		double b;	//실수형 변수 b를 선언만 한 상태
		b = a;			//변수 a의 값을 변수 b에 할당
						// → 자동형변환(묵시적형변환) : 작은타입이 큰타입으로 자동으로 변환 ▶ UpCasting
		System.out.println("변수 a : " + a);	//변수 a : 20
		System.out.println("변수 b : " + b);	//변수 b : 20.0
		
		int c;					//정수형 변수 c를 선언만 한 상태
		double d = 34.5;	//실수형 변수 d를 선언하고 값(3.45)을 할당 한 상태 ▶ 초기화
		//c = d;				//오류 : 큰타입을 작은타입에 할당
		c = (int) d;			//강제로 형 변환(double→int) ▶ DownCasting
		System.out.println("변수 c : " + c);	//변수 c : 34 → 0.5의 손실발생
		System.out.println("변수 d : " + d);	//변수 d : 34.5
		
		int x = 128;
		byte y = (byte) x;	//강제형변환, 명시적형변환, 일반적인 의미의 형변환 ▶ DownCasting
		System.out.println("변수 y : " + y);	//변수 y : -128 → 데이터 손실 발생 ▶ 심각한 문제
		
		char i = 'A';
		int j = i;		//UpCasting(char→int)
		System.out.println("변수 i : " + i);	//변수 i : A
		System.out.println("변수 j : " + j);	//변수 j : 65 → ASCII Code 값
		int k = j + 1;
		System.out.println("변수 k : " + k);	//변수 k : 66
		System.out.println("DownCasting k : " + (char) k);	//DownCasting k : B
		
		String str1 = "12";	//사용자정의 자료형(String Class Type) str1을 선언하고 할당 : 문자열
		String str2 = "12";
		System.out.println(str1 + str2);	//출력값 : 1212 ▶ 문자열 연쇄(결합, 연결) : Concatenation
		int su1 = Integer.parseInt(str1);	//문자(str1)→정수(su1) 변환 : Integer Class ▶ Wrapper Class
		int su2 = Integer.parseInt(str2);
		System.out.println(su1 + su2);	//출력값 : 24
	}//main()
}//class

/*
○ Casting(형 변환) : 데이터의 크기를 변환하는 과정
	① UpCasting : 자동으로 형변환 되는 경우(작은타입→큰타입)
	 					▶ 자동형변환, 묵시적형변환, 프로모션	
	② DownCasting : 강제로 형변환 되는 경우(큰타입→작은타입)
	 					▶ 강제형변환, 명시적형변환, 일반적인 의미의 Casting
	※ 형변환시 데이터의 손실이 발생할 수 있다 → 손실이 없을때만 사용
*/
