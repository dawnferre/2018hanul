public class Variable06 {
	public static void main(String[] args) {
		//논리형, 부울형(true, false) : boolean(1Byte) ▶ 조건처리
		boolean t = true;
		boolean f = false;
		
		System.out.println("변수 t : " + t);
		System.out.println("변수 f : " + f);
	}//main()
}//class