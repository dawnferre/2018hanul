public class Casting02 {
	public static void main(String[] args) {
		char upper = 'A';
		char lower = 'a';
		
		System.out.println("upper: "+upper);		//출력값 : A
		System.out.println("lower: "+lower);		//출력값 : a
		
		System.out.println((int)upper);	//출력값 : 65_upcasting(자동 형변환); 아스키코드로 설정되어 나옴.
		System.out.println((int)lower);	//출력값 : 97
	}//main()
}//class

/*
※ 기본 자료 형 변환 규칙(Casting)
	_boolean제외

===UpCasting(자동 형 변환, 묵시적 형 변환, 프로모션)====>
	byte(1) → short(2) → int(4) → long(8) → float(4) → double(8)
              char(2) ↗
<===DownCasting(강제 형 변환, 명시적 형 변환, Casting)===

예) 아래의 명령은 몇 번의 형 변환이 발생하는가?
	 double num = 3.5f + 12;
	 ① 3.5f + 12.0f(int→float)
	 ② double num = 15.5f(float→double)
*/