public class Casting03 {
	public static void main(String[] args) {
		//wrapper class 적용
		//String class type 저장된 문자 10(str1)과 문자 20(str2)을 더하기 연산을 할 수 있을까?
		
		String str1 = "10";
		String str2 = "20";
		System.out.println("String :"+str1 + str2);	//출력값 : 1020
		
		int su1 = Integer.parseInt(str1);	//su1 변수에는 정수 10이 기억 → Integer : Wrapper Class
		int su2 = Integer.parseInt(str2);
		System.out.println("Int: "+(su1 + su2));	//출력값 : 30, String하고 붙이면 제대로 되지 않으니 ()를 받음
				// + ,- String하고 붙어 사용하게 되면 String으로 인식, ()로 우선순위를 표시해야한다.
				// 문자열 연결 연산자(171P~),연산자의 우선순위(194P~)
					
		String str3 = "10.5";
		String str4 = "20.3";
		System.out.println(str3 + str4);	//출력값 : 10.520.3
		
		double su3 = Double.parseDouble(str3);	//su3 변수에는 실수 10.5가 기억 → Double
		double su4 = Double.parseDouble(str4);
		System.out.println(su3 + su4);	//출력값 : 30.8
	}//main()
}//class