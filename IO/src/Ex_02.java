import java.io.IOException;
import java.io.InputStream;

public class Ex_02 {
	public static void main(String[] args) {
		//[즉, 입력스트림은 enter도 입력으로 받아들이기 때문에 확인하고 제대로 데이터를 입력받을 줄 알아야한다.]
		
		//scan.nextLine() : 엔터까지 가지고 와서 처리하겠다고 scanner메소드
		InputStream in = System.in; 
		System.out.print("영문자 한글자를 입력하세요."); 

		try {
			//1.사용자가 문자를 입력
			//2.enter key 입력 ; 프로그램종료 의미 X, =>CR/LF
			 int data = in.read(); // blocking method :사용자의 입력을 기다리는 상태 메소드
			 
			 System.out.println("입력하신 영문자는 "+(char)data+"입니다.");//형변환(downcasting)
			 
			 
			 data = in.read(); //enter ==>CR로 받아들임. enter를 하나의 글자로 받아들임.(CR/LF)
			 System.out.println(data); //ASCII 코드 :13 (CR) =공백문자 ;
			 System.out.println((char)data);
			 
			 

			 data = in.read(); //enter ==>LF로 받아들임. 
			 System.out.println(data); //ASCII 코드 :10 (LF)= 공백문자 ;
			 System.out.println((char)data);
			 
			 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		/*
		 * CR(Carriage Return) : 커서를 줄의 맨앞으로 이동시켜준다.
		 * LF(Line Feed) : 커서를 한 줄 밑으로 이동.
		 * 종료 = terminate :Ctrl+Z(break명령)
		 * */
		
	}//main
}
