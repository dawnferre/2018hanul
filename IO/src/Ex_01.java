import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class Ex_01 {
	public static void main(String[] args) {
		//키보드에서 영문자 1글자를 읽어서 출력
		InputStream in = System.in; //노드스트림 : 가장 먼저 연결되는 스트림.
		System.out.print("영문자 한글자를 입력하세요."); //대기 (입력받기 위한)_ blocked state
		try {
			 int data = in.read(); //입력코드는 int로 받음 ._처음 한글자만 받는다.
			 								//int로 받기 때문에 형변환을 해준다.
			 System.out.println("입력하신 영문자는 "+(char)data+"입니다.");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
		//Scanner class 활용
		Scanner scan = new Scanner(System.in); 
		scan.close();
	}//main
}//class
