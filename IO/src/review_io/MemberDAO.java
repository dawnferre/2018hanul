package review_io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class MemberDAO {
	public ArrayList<memberDTO> getFileRead(){
		ArrayList<memberDTO> list = new ArrayList<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader("member.txt"));
			String data;
			memberDTO dto;
			while((data= br.readLine())!= null){
				String[] re = data.split("\t");
				dto = new memberDTO(Integer.parseInt(re[0]), re[1], Integer.parseInt(re[2]), re[3], re[4]);
				list.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
		
	}
	//file저장하는 메소드.
	public void getfileWrite(ArrayList<memberDTO> list){
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("reviewMemSort.txt"));
			int cnt =0;
			//마지막 line(빈칸)나오는 것을 방지.
			for(memberDTO dto:list){
				if(cnt < list.size()-1){
					bw.write(dto.getCode()+"\t"+dto.getName()+"\t"+dto.getAge()+"\t"+dto.getAddress()+"\t"+dto.getPhone()+"\n");
					cnt++;
				}else{
					bw.write(dto.getCode()+"\t"+dto.getName()+"\t"+dto.getAge()+"\t"+dto.getAddress()+"\t"+dto.getPhone());
					break;
				}
			}
			bw.flush();
			bw.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void display(ArrayList<memberDTO> list){
		System.out.println("list값 분류");
		for(memberDTO dto:list){
			System.out.print(dto.getCode()+"\t");
			System.out.print(dto.getName()+"\t");
			System.out.print(dto.getAge()+"\t");
			System.out.print(dto.getAddress()+"\t");
			System.out.println(dto.getPhone()+"\t");
		}
	}
	public ArrayList<memberDTO> sortAsc(ArrayList<memberDTO> list){
		System.out.println("오름차순");
		memberDTO temp;
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.size(); j++) {
				if(list.get(i).getName().compareTo(list.get(j).getName())<0){//오름차순
					temp =list.get(i);
					list.set(i, list.get(j));
					list.set(j, temp);
				}
			}
		}
		for(memberDTO dto:list){
			System.out.print(dto.getCode()+"\t");
			System.out.print(dto.getName()+"\t");
			System.out.print(dto.getAge()+"\t");
			System.out.print(dto.getAddress()+"\t");
			System.out.println(dto.getPhone()+"\t");
		}
		
		return list;
		
	}
}
