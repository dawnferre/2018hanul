package review_io;

public class MemberCopyMain {
	public static void main(String[] args) {
		MemberDAO dao = new MemberDAO();
		//파일읽어오기
		dao.getFileRead();
		//list목록보여주기
		dao.display(dao.getFileRead());
		//list 오름차순 보여주기
//		dao.sortAsc(dao.getFileRead());
		//그냥 list 파일로 저장
//		dao.getfileWrite(dao.getFileRead());
		//오름차순 분류로 된 list 파일로 저장(오름차순메소드list받아오기때문에 출력도 한번에 된다.
		dao.getfileWrite(dao.sortAsc(dao.getFileRead()));
	}
}
