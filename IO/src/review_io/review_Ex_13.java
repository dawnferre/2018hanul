package review_io;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class review_Ex_13 {
	public static void main(String[] args) {
			int id = 123435;
			String name = "홍길동";
			String phone ="010-1234-5678";
		try {
			DataOutputStream dos = new DataOutputStream(new FileOutputStream("review_data.txt"));
			//암호화
			dos.writeInt(id);
			dos.writeUTF(name);
			dos.writeUTF(phone);//숫자들어가있는 문자데이터는 안되나??_중간중간 암호화가 안되는 부분이 있을 수 있다.
			System.out.println("파일이 생성되었습니다.");
			
			DataInputStream dis = new DataInputStream(new FileInputStream("review_data.txt"));
			id = dis.readInt();
			name =dis.readUTF();
			phone = dis.readUTF();
			
			System.out.println("저장된 파일의 내용은 "+id+","+name+","+phone+" 입니다.");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}//main
	
}//class
