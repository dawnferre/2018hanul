package review_io;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;

public class Review_Ex_16 {
	public static void main(String[] args) {
		//url : uniform resource location :웹에서 특정 정보(자원)가 위치한 주소
		//특정 url의 소스보기 == html보기 ==> 파일저장 urlhtml.txt
		
		try {
			URL url =new URL("https://www.naver.com");
			
			InputStream is = url.openStream();
			InputStreamReader isr = new InputStreamReader(is, "ms949");
			//ex_10참고= read,writer로 바꾸어서 사용.
			
			OutputStream os = new FileOutputStream("UrlHtml_review.txt");
			OutputStreamWriter osw = new OutputStreamWriter(os, "ms949");
			
			int data;
			while((data = isr.read())!=-1){
//				System.out.println(data);
				osw.write(data);
			}
			osw.flush();
			osw.close();
			isr.close();
			System.out.println("저장이 완료되었습니다.");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}//main
}//class
