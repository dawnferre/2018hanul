import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Ex_08 {
	public static void main(String[] args) {
		//키보드에서 문자열(문자)을 여러번 입력받아 출력 >>버퍼활용
		//단, 'quit' 문자열이 입력되면 종료
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String data;
		try {
			while(true){
				
				System.out.print("문자열을 여러번 입력하세요. 단 'quit'를 입력시 종료됩니다.");
				data=br.readLine();
				
//				data.equals("quit") 도 같은 말
				if(data.trim().equalsIgnoreCase("quit")){ //boolean으로 나오는 게 :equals
//				if(data.compareToIgnoreCase("quit")==0){ //숫자로 리턴해주는게 : compare
					//공백무시 trim() :공백까지 제거해서 완벽하게 문자비교
					System.out.println("종료되었습니다.");
					break;
				}
				System.out.println(data);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}//main
}//class
