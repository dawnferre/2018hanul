import java.io.Serializable;

public class MemberDTO implements Serializable{
	//버전에 대한 정보를 써라.
	private static final long serialVersionUID = 1L;
	
	int code;
	String name;
	int age;
	String address;
	String phone;
	
	
	public MemberDTO(int code, String name, int age, String address, String phone) {
		super();
		this.code = code;
		this.name = name;
		this.age = age;
		this.address = address;
		this.phone = phone;
	}
	public MemberDTO(){
		
		
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
}
