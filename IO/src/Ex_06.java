import java.io.InputStream;

public class Ex_06 {
	public static void main(String[] args) {
		//키보드에서 영문자 여러개를 입력받아 출력
		//단, 입력한 글자중에 'q'라는 문자가 있으면 종료(그뒤에 문자는 출력X)
		int data;
		
		InputStream is = System.in;
		System.out.println("영문자 여러개를 입력해주세요. 단, q를 입력시 종료됩니다.");
		try {
			//1.여러글자입력시_q까지 입력이 되고 종료됨.
			while((data = is.read())!=-1){ //즉, 값이 입력된다면
				if((char)data=='q'){
					//q를 입력하고 종료시킬거면 조건문안에 q를 찍어주는 print를 넣어주고 종료시키면 된다.
					System.out.println("입력하신 문자는 "+(char)data+"입니다.");
					System.out.println("종료됩니다.");
					break;
				}
				System.out.println("입력하신 문자는 "+(char)data+"입니다.");
			}
			
			/*//2.if문 조건을 while에 줬을 때
			while((data =is.read())!='q'){
				System.out.println("입력하신 문자는 "+(char)data+"입니다.");
			}
			System.out.println("종료됩니다.");*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}//main
}//class
