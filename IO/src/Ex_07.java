import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Stream;

public class Ex_07 {
	//Scanner의 원리
	public static void main(String[] args) {
		//한글 여러글자를 입력받아 출력_ buffered활용
		
//		InputStream is = System.in;//노드
//		InputStreamReader isr = new InputStreamReader(is);//브릿지1_문자
//		BufferedReader br = new BufferedReader(isr);//브릿지2
		
		//2.줄여서 쓰는 방법
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		//출력을 할때는 입력값 하나씩 접근,  ==> 접근횟수가 늘어나면 속도저하
		//하나씩 읽는게 아니라 일정단위로 접근하자.(buffered) ==> 속도향상
		
		System.out.println("한글 여러글자를 입력해주세요.");
		try {
			String data= br.readLine();
			//read : 1글자
			//readline :enter키 앞에까지 읽음.(공백은 상관 ㄴㄴ)
			System.out.println(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}//main
}//class
/*
 * * 버퍼의 필요성
 * 	- 일반적인 입출력은 여러 글자를 입력한다 하더라도 실제처리될 때에는 한글자씩 입출력이 된다.
 * 	- 입력한 글자의 개수만큼 접근이 이뤄진다 : 속도저하의 원인.
 *  - 위와 같은 비효율성을 개선하고자 여러글자를 입력하더라도  버퍼를 이용하여 입력한 글자를 한 번에 입출력할 수 있도록 지원해준다.
 *  - 버퍼의 기능을 지원하는 클래스 >> bufferedReader
 *  - 버퍼의 기본 크기 : 1024바이트(512글자를 한꺼번에 가지고 올 수 있다.)
 * 
 * 
 */