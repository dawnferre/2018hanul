import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

public class Ex_13 {
	public static void main(String[] args) {
		//암호화기법
		//DataInputStream ,DataOutputStream: 기본데이터타입(PDT), 문자열(String)을 쉽게 입출력 가능.
		//정의된 자료형의 데이터를 byte변환하여 입출력 ==> 암호화가 이루어짐.(보안성이 필요할때)
		//복호화과정이 필요함. 
		
		//사용자가 입력한 내용을 파일로 저장 === data.txt :fileoutputStream ->dataoutputstream
		//파일에 저장된 내용을 화면에 출력 :fileinputstream -->datainputstream
		
		int i = 12345;
		float f = 3.145f;
		String s = "홍길동";
		
		try {
			FileOutputStream fos = new FileOutputStream("data.txt");
			
			DataOutputStream dos = new DataOutputStream(fos);
			//암호화 과정
			dos.writeInt(i); //int ==>byte
			dos.writeFloat(f);//float ==>byte
			//String 타입을 받을때 utf
			dos.writeUTF(s);//String ==> byte
			dos.close();
			System.out.println("data.txt파일이 생성되었습니다.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//복호화
		try {
			FileInputStream fis = new FileInputStream("data.txt");
			DataInputStream dis = new DataInputStream(fis);
			//복호화과정_dis를 이용해 byte를 기존타입으로 변환한다. _기존타입을 모를때는 어떤식으로 받지?
			
			int ii =dis.readInt();
			float ff =dis.readFloat();
			String ss =dis.readUTF();
			System.out.println("data.txt 안의 내용은 "+ii+"\t"+ff+"\t"+ss+" 입니다.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}//main
}//class
