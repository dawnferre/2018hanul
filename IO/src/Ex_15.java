import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Ex_15 {
	public static void main(String[] args) {
		
		MemberDTO dto = new MemberDTO();
		dto.setCode(44);
		dto.setName("김철수");
		dto.setAge(30);
		dto.setAddress("광주 서구 쌍촌동");
		dto.setPhone("010-1234-5678");
		
		//객체를 네트워킹을 통해서 사용할때는  직렬화가 반드시 필요하다 (보안적인 부분)
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("member.ser"));
			oos.writeObject(dto); //객체의 직렬화.== 암호화
			System.out.println("파일이 생성되었습니다.");
			
			//복호화 == 파일을 읽어서 화면에 출력.
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("member.ser"));
			dto =(MemberDTO) ois.readObject();//객체의 역직렬화 == > 객체의 타입을 동일하게 한다.
			System.out.println("파일 내의 정보를 출력합니다.");
			System.out.println("코드: "+dto.getCode());
			System.out.println("이름: "+dto.getName());
			System.out.println("나이: "+dto.getAge());
			System.out.println("주소: "+dto.getAddress());
			System.out.println("전화번호: "+dto.getPhone());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}//main
}//class
