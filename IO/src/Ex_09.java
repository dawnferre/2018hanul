import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Ex_09 {
	public static void main(String[] args) {
		//파일에서 바이트 단위로 입출력하는 스트림 :fileinputStream, fileoutputStream.
		//이미지를 복사하여 해당파일 저장하기(copy & paste). 파일, 영상, 음악 ,아스키코드 등
		//1.원본이미지 준비.: 이미지를 복사하여 io 프로젝트에 붙여넣기 ==>filename :pic.jpg
			//경로설정을 하지 않으면 현재 프로젝트에서 작업이 이루어진다.
			//다른위치에서 작업할 경우, 절대경로표기 >> D:\Study_java\workspace\IO\pic.jpg(우클릭 :properties;location)
			//오류발생 ; \를 바로 인식못하고 이스케이프문자로 인식.(ex.\t,\n) ==>\\를 두번써줘서 (\)로 그냥 인식.
		
		//프로그램을 실행 후 refresh(F5)하면 복사된 파일을 확인 할 수 있다.
		try {
			//1.경로설정 : 원본파일의 위치
//			String path="D:\\Study_java\\workspace\\IO\\pic.jpg";
			//2.fileinputstream 설정
			FileInputStream fis = new FileInputStream("pic.jpg"); //원본파일
			//3. fileoutputstre am설정
			FileOutputStream fos = new FileOutputStream("copy1.jpg");//사본파일
			
			int data, cnt =0;
			//read() :한번에 1바이트씩 제공(size만큼 왔다갔다한다.)
			while((data=fis.read())!= -1){//EOF(End of File) :종료
				cnt++;
				System.out.println(data); //숫자가 찍어짐._이미지파일의 픽셀값(rgb) //3개씩 묶어서 각각 R,G,B값으로 표현
				fos.write(data); //실제 데이터를 넣어줌.(파일을 생성)
			}
			fos.flush();
			fos.close(); //반드시 닫아줘야한다 ***
			fis.close();
			System.out.println("접근횟수 : "+cnt); //파일용량하고 똑같음.
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
				
		
		
		
		
	}//main
}//class
