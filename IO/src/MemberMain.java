import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class MemberMain {
	public static void main(String[] args) {
		/*
		 * member.txt 파일에서 데이터를 읽어서  == filereader , bufferedreader;
		 * 각 항목은 tab을 구분되어 잇음
		 * arraylist 구조로 데이터화
		 * 성명을 기준으로 오름차순 정렬 후 화면에 출력. ==>sortNameASC, display
		 * membersort.txt ==> filesave() : filewriter,bufferedwriter
		 * */
		MemberMain main = new MemberMain();
		
		main.display(main.fileRead());
		System.out.println("--------------------------");
		;
		main.fileSave(main.sortNameAsc(main.fileRead()));;
	}//main
	//splite해서 list넘기는 거 이해해보자.
	public ArrayList<MemberDTO> fileRead(){
		ArrayList<MemberDTO> list = new ArrayList<>();
		String data = null;
		MemberDTO dto =null;
		try {
			BufferedReader br = new BufferedReader(new FileReader("member.txt"));
			while((data =br.readLine())!=null){
				String[] re = data.split("\t");
				dto = new MemberDTO(Integer.parseInt(re[0]), re[1], Integer.parseInt(re[2]), re[3], re[4]);
					
				list.add(dto);

			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
		
	}
	public void fileSave(ArrayList<MemberDTO> list){
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("membersort.txt"));
			//향상for문으로 할때는 어떻게 해야할까? _ 선생님 코드 참고
//			for(int i = 0; i<list.size(); i++){
//				MemberDTO dto = list.get(i);
//				if(i==list.size()-1){
//					bw.write(dto.getCode()+"\t"+dto.getName()+"\t"+dto.getAge()+"\t"+dto.getAddress()+"\t"+dto.getPhone());
//					
//				}else{
//					
//					bw.write(dto.getCode()+"\t"+dto.getName()+"\t"+dto.getAge()+"\t"+dto.getAddress()+"\t"+dto.getPhone()+"\n");
//				}
//				
//				bw.flush();
//			}
			
			
			int cnt =0;
			for(MemberDTO dto:list){
				if(cnt<list.size()-1){
					
					bw.write(dto.getCode()+"\t"+dto.getName()+"\t"+dto.getAge()+"\t"+dto.getAddress()+"\t"+dto.getPhone()+"\n");
					cnt++;
				}else{
					bw.write(dto.getCode()+"\t"+dto.getName()+"\t"+dto.getAge()+"\t"+dto.getAddress()+"\t"+dto.getPhone());
					break;
				}
				bw.flush();
			}
			bw.close();
			

			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void display(ArrayList<MemberDTO> list){
		//정렬전 출력
		for(MemberDTO dto :list){
			System.out.print(dto.getCode()+"\t");
			System.out.print(dto.getName()+"\t");
			System.out.print(dto.getAge()+"\t");
			System.out.print(dto.getPhone()+"\t");
			System.out.print(dto.getAddress()+"\n");
		}
	}
	//dto로 분리해볼것.
	public ArrayList<MemberDTO> sortNameAsc(ArrayList<MemberDTO> list){//이름 오름차순 정렬
		MemberDTO temp;
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.size(); j++) {
				if(list.get(i).getName().compareTo(list.get(j).getName())<0){//오름차순
					temp = list.get(i);
					list.set(i, list.get(j));
					list.set(j, temp);
				}
			}
		}
		for(MemberDTO dto :list){
			System.out.print(dto.getCode()+"\t");
			System.out.print(dto.getName()+"\t");
			System.out.print(dto.getAge()+"\t");
			System.out.print(dto.getPhone()+"\t");
			System.out.print(dto.getAddress()+"\n");
		}
		
		return list;
	}
}//class
