import java.io.FileReader;
import java.io.IOException;

public class Ex_11 {
	public static void main(String[] args) {
		// 파일에서 문자단위로 입출력하는 스트림:filereader, filewriter
		//한글이 포함된 member.txt 파일에서 데이터를 읽어서 출력 : console window
		
		char[] arr = new char[110];
		FileReader fr = null; 
		try {
			fr = new FileReader("member.txt");//원본파일
			int data ,cnt =0;
			while((data= fr.read(arr))!=-1){
				cnt++;
//				System.out.print(data+","+(char)data+"  ");
				//한꺼번에 가지고 오려면 배열을 이용해도 가능하다.(줄 구분해서 가져오진 않는다.)
				for (int i = 0; i < arr.length; i++) {
					System.out.println(arr[i]);
					
				}
			}
			System.out.println("접근횟수 : "+cnt);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			
			try {
				
				fr.close(); //try-catch문에 close를 넣는건 오류발생시 닫히지 않아서 문제가 생긴다.
							// 구문밖으로 빼거나 finally를 이용해서 실행할 수 있게끔 사용한다.
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}//main
}//class
