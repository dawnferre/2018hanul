import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class review_12 {
	public static void main(String[] args) {
		//txt파일 줄단위로 읽어오기.
		BufferedReader br =null;
		BufferedWriter bw = null;
		try {
			//문자스트림 reader + writer
			FileReader fr = new FileReader("member.txt");
			 br = new BufferedReader(fr);
			
			FileWriter fw = new FileWriter("copy.txt");
			 bw = new BufferedWriter(fw);
			 
			String data ;
			int cnt =0;
			while((data= br.readLine())!=null){
				cnt++;
				System.out.println(data);
				bw.write(data);
				bw.newLine(); //파일을 복사할때 줄단위로 읽어오는걸 줄단위로 끊어준다., newLine()안주면 1줄로 형성된다.
			}
			System.out.println(cnt);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				bw.flush();
				bw.close();
				br.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	}//main
}//class
