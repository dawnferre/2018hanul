import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Ex_14 {
	public static void main(String[] args) {
		//현재까지의 게임정보(상태)를 암호화하여 game.data파일에 저장.
		//game.data파일의 내용을 화면에 출력
		String id ="hanul";
		String user ="한울";
		int level = 5;
		int money = 123450;
		float jumsu = 3456.78f;
		try {
			DataOutputStream dos = new DataOutputStream(new FileOutputStream("game.data"));
			dos.writeUTF(id);
			dos.writeUTF(user);
			dos.writeInt(level);
			dos.writeInt(money);
			dos.writeFloat(jumsu);
			System.out.println("파일이 생성되었습니다.");
			
			DataInputStream dis = new DataInputStream(new FileInputStream("game.data"));
			id = dis.readUTF();
			user = dis.readUTF();
			level =dis.readInt();
			money =dis.readInt();
			jumsu = dis.readFloat();
			System.out.println("생성된 파일의 내용은 id= "+id+", user= "+user+", level= "+level+", money= "+money+",jumsu= "+jumsu+" 입니다.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}//main
}//class
