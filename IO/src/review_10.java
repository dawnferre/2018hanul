import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class review_10 {
	public static void main(String[] args) {
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		try {
			FileInputStream fis = new FileInputStream("ko.jpg");//원본
			bis = new BufferedInputStream(fis);
			
			FileOutputStream fos = new FileOutputStream("copy3.jpg");//복사본
			 bos= new BufferedOutputStream(fos);
			int data ;
			while((data =bis.read())!=-1){
				bos.write(data); // 원본을 읽고 나서 그대로 write에 뿌려주고 저장시키기.
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			
			try {
				bos.flush();
				bos.close();
				bis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	}//main
}//class
