import java.io.IOException;
import java.io.InputStreamReader;

public class Ex_04 {
	public static void main(String[] args) {
		//키보드에서 한글 한글자를 입력받아 출력하시오.
		//문자스트림.(유니코드)
		//[각각 선언해서 하는 방법]
		//1.
//		InputStream is = System.in; //노드스트림 먼저.
		//2.
//		InputStreamReader isr = new InputStreamReader(is); // 브릿지스트림

		//[하나로 연결하는 방법]_코드 길이를 줄일 수 있음
		InputStreamReader isr = new InputStreamReader(System.in); // 브릿지스트림
		System.out.print("한글 한글자를 입력하시오.");
		try {
			int data =isr.read();
			System.out.println("입력하신 글자는 "+data+"입니다."); //data: int값 = 유니코드
			System.out.println("입력하신 글자는 "+(char)data+"입니다."); 
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		
	}//main
}//class
