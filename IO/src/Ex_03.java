import java.io.InputStream;

public class Ex_03 {
	public static void main(String[] args) {
		//키보드에서 영문자 여러글자를 입력받아 출력.
		InputStream is =System.in; //노드스트림
		System.out.println("영문자 여러글자를 입력하세요.");
//		try {
//			//몇개의 글자인지 알수 없을때 while 반복문을 사용한다.
//			while(true){
//				int data = is.read();
//				System.out.println("입력하신 영문자는 "+(char)data+"입니다.");
//				if(data == -1){//즉, 입력값이 없을때 ctrl+Z 입력시 break명령을 통해서 while문 탈출
//					System.out.println("종료되었습니다.");
//					break;
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		//조금 더 간단한 방법
		try {
			int data;
			while((data = is.read())!= -1){ 
				System.out.println("입력하신 영문자는 "+(char)data+"입니다.");
			}
			//terminate : ctrl+z ==? data == -1
			System.out.println("종료되었습니다.");
		} catch (Exception e) {
			e.getStackTrace();
		}
	}//main
}//class
