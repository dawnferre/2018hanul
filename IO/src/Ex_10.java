import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Ex_10 {
	public static void main(String[] args) {
		//버퍼를 사용해서 만드는 방법 : 입출력의 효율을 높이기 위해서
		//버퍼의 기본 크기 : 1024 byte(사이즈조정가능하다.) => 마지막 버퍼의 경우엔 기본크기보다 작은 용량이 버퍼에 남아있다.
		//버퍼를 사용할 경우 반드시 종료해야 한다.
		//마지막 버퍼의 경우, 기본크기보다 작은 용량이 버퍼에 남아있다.
		//따라서 버퍼에 남아있는 내용을 강제로 전송(flush)해준다.
		try {
//			FileInputStream fis = new FileInputStream("ko.jpg");//원본파일
//			BufferedInputStream br = new BufferedInputStream(fis);
			BufferedInputStream br = new BufferedInputStream(new FileInputStream("ko.jpg"));//입력버퍼
			
//			FileOutputStream fos = new FileOutputStream("copy2.jpg");//사본파일
//			BufferedOutputStream bos = new BufferedOutputStream(fos);
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("copy2.jpg"));//출력버퍼
			int data, cnt =0;
			while((data =br.read()) != -1){
				cnt++;
				System.out.println(data);
				bos.write(data);
			}
			bos.flush(); //버퍼에 남아있는 내용(기본전송단위를 채우지 못해서 남아있을 때)을 강제전송(전송완료는 안되어 있는 상태)
			//위에 선언한 순서반대로 닫아줌.
			//flush()를 써주지 않아도 close 호출시 강제로 flush내부호출이 있기 때문에 close만 해줘도 모든 원본파일 그대로 전송가능하다.
			bos.close(); 
			br.close();
			System.out.println("접근횟수 : "+cnt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}//main
}//class
