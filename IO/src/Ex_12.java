import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class Ex_12 {
	public static void main(String[] args) {
		try {
			//브릿지									노드
			BufferedReader br = new BufferedReader(new FileReader("member.txt"));
			String data;
			int cnt = 0;
			//한 줄단위로 
			while((data =br.readLine())!=null){
				cnt++;
				System.out.println(data);
			}
			System.out.println("접근횟수 : "+cnt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Scanner scan = new Scanner("member.txt");
		System.out.println();
	}//main
}//class
