import java.io.InputStream;
import java.io.InputStreamReader;

public class Ex_05 {
	public static void main(String[] args) {
		int data;
		InputStream ir = System.in;//노드
		InputStreamReader irs = new InputStreamReader(ir);//브릿지
		
		System.out.println("문자 여러글자를 입력해주세요.");
		try {
			
				//1.입력받은 값/2.을 할당/받은 것 == -1
			while((data =irs.read())!= -1){
					System.out.println("입력하신 데이터는 "+(char)data+"입니다.");
				
			}
			System.out.println("종료되었습니다.");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
