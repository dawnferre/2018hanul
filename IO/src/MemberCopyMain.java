import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class MemberCopyMain {
	public static void main(String[] args) {
		//member.txt파일에서 라인단위(readLine())로 읽어서
		//membercopy.txt 파일에 저장하시오.
		String path =""; //다른 위치에 있는 파일경로는 파일경로를 지정해서 사용하게 된다.
		String copypath="";
		try {
			BufferedReader br = new BufferedReader(new FileReader("member.txt"));//입력버퍼
			BufferedWriter bw = new BufferedWriter(new FileWriter("membercopy.txt"));//출력버퍼
			String data; //열려진 파일의 내용을 읽어서 저장할 변수를 초기화.
			while((data=br.readLine())!=null){
				bw.write(data);
				bw.newLine();//줄바꿈 _ 복사된 파일에 줄바꿈때문에 한줄이 더 생겨서 가게 된다.
				
				
				// tap으로 나누었기 때문에 splite으로 각각의 변수로 담아서 dto로 저장 ==>dto내에 있는 내용을 뿌려준다 (Arraylist)
				
			}
			if(data == null){
				
				System.out.println("파일이 복사되었습니다.");
			}
			bw.flush();
			bw.close();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}//main
}//class
