package com.hanul.study;

import java.io.Serializable;

public class SearchDTO implements Serializable{
	private String part;
	private String search;
	
	
	public SearchDTO() {
	}


	public SearchDTO(String part, String search) {
		super();
		this.part = part;
		this.search = search;
	}


	public String getPart() {
		return part;
	}


	public void setPart(String part) {
		this.part = part;
	}


	public String getSearch() {
		return search;
	}


	public void setSearch(String search) {
		this.search = search;
	}
	
}
