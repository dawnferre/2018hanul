package com.hanul.study;


import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;


public class MemberDAO {
	//기존 JDBC모델에서는 connection (연결처리)를 먼저 만들었지만, mybatis의 졍우 sqlSessionfactoiry생성
	//getConn()과 같은 역할
	private static SqlSessionFactory sqlMapper;
	static{
		String resource ="com/hanul/mybatis/SqlMapConfig.xml";
		try {
			InputStream inputStream = Resources.getResourceAsStream(resource);
			sqlMapper = new SqlSessionFactoryBuilder().build(inputStream); 
			
		} catch (Exception e) {
			e.getStackTrace();
		}
		
	}//static:초기화블럭, 무조건 메모리에 올려놓고 사용하고자 할때
	
	// 순서확인하기!!!
	//회원가입
	public int insert(MemberDTO dto){
		//1/sqlSessionFactory에서  session활성화
		SqlSession session = sqlMapper.openSession();
		int succ = 0;
		/*2.session 설정 : session에서 insert메소드 등을 직접 메소드를 지정해줌.*/
		/*key,value : key는 메소드 이름과 같이 맞춰준다.*/
		succ = session.insert("insert", dto); //sql문장작성 : memberMapper.xml
		
		//3.jdbc: autocommit가능 <-> mybatis: commit필요;
		session.commit(); //테이블에 변경사항 저장( 반드시!!!)
		
		return succ;
	}//insert()
	
	//전체회원목록검색하기
	public List<MemberDTO> getAllList(){
		SqlSession session = sqlMapper.openSession();
		
		List<MemberDTO> list = null;
		//selectOne(1개만찾을때), selectList(결과가 여러개인 경우)
		list =session.selectList("getAllList");
		
		return list;
		
	}
	public int delete(String id){
		SqlSession session = sqlMapper.openSession();
		int succ = 0;
		succ = session.delete("delete", id); //sql문장작성 : memberMapper.xml
		
		session.commit(); //테이블에 변경사항 저장( 반드시!!!)
		
		return succ;
	}
	
	public MemberDTO getAllbyId(String id){
		SqlSession session = sqlMapper.openSession();
		
		MemberDTO dto =session.selectOne("getAllbyId", id);
		
		return dto;
		
	}
	
	public int update(MemberDTO dto){
		SqlSession session = sqlMapper.openSession();
		
		int succ = session.update("update", dto);
		
		session.commit();
		return succ;
	}
	public List<MemberDTO> search(String part, String search){
		SqlSession session = sqlMapper.openSession();
		SearchDTO dto = new SearchDTO(part, "%"+search+"%");
		List<MemberDTO> list =session.selectList("search", dto);
		
		return list;
	}
}
