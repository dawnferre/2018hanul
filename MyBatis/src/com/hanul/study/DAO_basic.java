package com.hanul.study;

import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class DAO_basic {
	//sqlSessionFactory  ==> sqlMapper
	//전역변수 설정
		private static SqlSessionFactory sqlMapper;
		static{
			String resource ="";
			try {	//** try-catch를 사용하지 않으면 오류난다.
				//Resources: MyBatis(구, iBatis)==> jar설정했기 때문에 import
				InputStream inputStream = Resources.getResourceAsStream(resource);
				sqlMapper = new SqlSessionFactoryBuilder().build(inputStream); 
				
			} catch (Exception e) {
				e.getStackTrace();
			}
			
		}//static:초기화블럭, 무조건 메모리에 할당해서 쓸 때 사용함.
}
