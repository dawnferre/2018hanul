<%@page import="com.hanul.study.MemberDTO"%>
<%@page import="java.util.List"%>
<%@page import="com.hanul.study.MemberDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	request.setCharacterEncoding("utf-8");
	String part = request.getParameter("part"); //넘겨줄때 value값으로 이미 db의 컬럼값으로 지정했기  때문에 
	String search = request.getParameter("search");
	
	MemberDAO dao = new MemberDAO();
	List<MemberDTO> list =dao.search(part,search);
	pageContext.setAttribute("list", list);
	
	//선생님은 searachList.jsp를 만들어서 넘겨줍니다.
	//넘겨준 페이지에서 검색결과가 없으면 체크 처리 해줌.
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
*{
	margin: 0 auto;
}
	a{
		text-decoration:  none;
		cursor: pointer;
		color: black;
		
	}
	a:hover{
		color: blue;
	}
</style>
<script>
	function fnDelete(id){
		
// 		alert(id);
		if(confirm("삭제하시겠습니까?")){
			location.href ="delete.jsp?id="+id;
		}else{
			
		}
		
	}
	
	function fnCheck(){
		var search = document.form_name.search.value;
		
		if(search.length == 0){
			alert("검색조건을 입력하시기 바랍니다.");
			 document.form_name.search.focus();
			 return false;
		}else{
			return true;
		}
	}
</script>
</head>
<body>
<form action="search.jsp" name="form_name" method="post" onsubmit="return fnCheck()" >
	<table border="1">
	<caption><b>전체회원 목록보기</b></caption>
		<tr>
			<th>이름</th>
			<th>아이디</th>
			<th>비번</th>
			<th>나이</th>
			<th>주소</th>
			<th>전화번호</th>
			<th>삭제</th>
		</tr>
		<% if(list.size() != 0){ %>
			<c:forEach var ="j" items="${list}">
			<tr>
				<td>${j.irum}</td>
				<td><a href="detail.jsp?id=${j.id}">${j.id}</a></td>
				<td>${j.pw}</td>
				<td>${j.age}</td>
				<td>${j.addr}</td>
				<td>${j.tel}</td>
				<td><input type="button" value="삭제" onclick="fnDelete('${j.id}')"/></td>
			<tr>
			</c:forEach>
			<% }else{ %>
				<tr>
			<td colspan="7" align="center">
			<b>※ 검색결과가 없습니다.</b>
			</td>
		</tr>
			<% } %>
		<tr>
			<td colspan="7" align="center">
			※ 정보를 수정하고자 할때는  아이디를 클릭해주세요.<br>
			<input type="button" value="회원가입목록" onclick="location.href='MemberMain.html'"/>
			</td>
		</tr>
</table>
		<div align ="center">
		<br>
		<select name ="part">
			<option value="irum">이름</option>
			<option value="id">아이디</option>
			<option value="addr">주소</option>
			<option value="tel">전화번호</option>
		</select>
			<input type="text" name ="search" placeholder="검색하고자 하는 검색어를 입력해주세요." size="30">
			<input type="submit" value="검색" >
			<input type="button" value="전체목록보기" onclick="location.href='list.jsp'">		
		</div>
</form>
</body>
</html>