<%@page import="com.hanul.study.MemberDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	MemberDTO dto = (MemberDTO) request.getAttribute("dto");
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
	*{
		margin: 0 auto;
	}
	
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>회원상세화면</title>
<script type="text/javascript">
	function fnUpdateCheck(){
		if(confirm("업데이트 하시겠습니까?")){
			return true;
			
		}else{
			return false;
		}
	}

</script>
</head>
<body>
	<form action="update.jsp" method="post" onsubmit="return fnUpdateCheck()">
	<!-- 값이 넘겨주기 어려울때 hidden속성을 이용하여 값을 넘겨주자.  ==> 나는 input타입으로 처리했지만, 선생님은 input타입없이 값만 찍어줬기 때문에-->
<%-- 	<input type="hidden" name="id" value="${dto.id}"> --%>
	<table>
		<caption><h4>업데이트화면</h4></caption>
		<tr>
			<th>이름</th>
			<td><input type="text" name="irum" value="${dto.irum}"></td>
		</tr>
		<tr>
			<th>ID</th>
			<td><input type="text" name="id" value="${dto.id}" readonly="readonly" style="background-color:gray;"></td>
		</tr>
		<tr>
			<th>PW</th>
			<td><input type="password" name="pw" value="${dto.pw}"></td>
		</tr>
		<tr>
			<th>나이</th>
			<td><input type="number" name="age" value="${dto.age}"></td>
		</tr>
		<tr>
			<th>주소</th>
			<td><input type="text" name="addr" value="${dto.addr}"></td>
		</tr>
		<tr>
			<th>전화번호</th>
			<td><input type="text" name="tel" value="${dto.tel}"></td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<input type="submit" value="업데이트">
				<input type="button" onclick="location.href='list.jsp'" value="돌아가기">
			</td>
		</tr>
	</table>
</form>
</body>
</html>