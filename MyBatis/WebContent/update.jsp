<%@page import="com.hanul.study.MemberDTO"%>
<%@page import="com.hanul.study.MemberDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("utf-8");

%>
<!-- dto설정  -->
<jsp:useBean id="dto" class="com.hanul.study.MemberDTO">
	<jsp:setProperty property="*" name="dto"/>
</jsp:useBean>
<%
	MemberDAO dao = new MemberDAO();
	int succ = dao.update(dto);
	
	if(succ>0){
		out.println("<script>alert('업데이트성공');location.href='list.jsp';</script>");
		
	}else{
		out.println("<script>alert('업데이트실패');location.href='list.jsp';</script>");
		
	}
%>
