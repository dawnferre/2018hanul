

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloServlet
 */
@WebServlet("/hs.do")
public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//응답하기전에 설정을 맞춰줘야 하기때문에 reponse를 이용함.
		response.setContentType("text/html;charset=utf-8");//인코딩방식지정
		PrintWriter out = response.getWriter();
		out.println("<h2 style ='background-color:green'>Hello,World!!서블렛이여요</h2>");
	}

}
