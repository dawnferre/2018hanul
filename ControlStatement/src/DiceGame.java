import java.util.Random;
import java.util.Scanner;

public class DiceGame {
	public static void main(String[] args) {
		// 주사위 게임.
		Scanner scan = new Scanner(System.in); // 입력
		Random random = new Random();// 난수를 만드는 
		
		
		while(true){
			System.out.println("게임을 시작하려면 1을 입력하시고,\n게임을 종료하려면 -1을 입력하세요.");
			int userNum =scan.nextInt();
			scan.nextLine();
			if(userNum != 1 && userNum != -1){ //userNum 두값다 만족을 해야하니까 .. &&를 사용해야한다.(OR은 둘중 하나만 맞으면 되니까 탈출안됨.)
				System.out.println("맞는 숫자가 아닙니다.");
				continue;
			}//if
			if(userNum == -1){
				System.out.println("게임을 종료합니다.");
				break;
			}
			//userNum == 1
			System.out.println("게임을 시작합니다. \n 주사위를 던져주세요.");
			//사용자가 주사위를 굴린후 랜덤으로 주사위의 숫자를 할당.
			System.out.print("먼저 주사위를 굴립니다.(enter key입력)");
				int user =random.nextInt(6)+1;
				int comNum = random.nextInt(6)+1;
				scan.nextLine();//enter를 입력을 하면 , 이미 이값에 enter값이 들어가기 때문에 위에 미리 만들어놓은 공간에 넣게끔.
								// 그다음 공간을 다시 줘서 enter로 게임이 시작하게끔.
				if(user== comNum){
					System.out.println("같은 숫자네요!! 컴퓨터의 숫자 : "+comNum+", user의 숫자: "+user);
					continue;
				}else if(user >comNum){
					System.out.println("YOU WIN!! 컴퓨터의 숫자 : "+comNum+", user의 숫자: "+user);
					continue;
				}else{
					System.out.println("YOU LOSE!! 컴퓨터의 숫자 : "+comNum+", user의 숫자: "+user);
					continue;
				}
			
		}//while
		scan.close();
	}//main()
}//class
