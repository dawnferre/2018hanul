public class Ex_break {
	public static void main(String[] args) {
		//break : 반복문을 탈출하는 명령어
		//이중 for문일 경우 안쪽에 포함되어있으면 안쪽만 나오게 된다.
		int cnt =0;
		for(int i = 1; true; i++){ //무한루프
			if(i == 11){//11을 실행하기 전에 바로 break;
				System.out.println("반복을 멈춥니다.");
				break; //그냥 break;가 아니라 if문 안에 (무조건, 조건안에)
			}
			cnt++; // 아니면, 카운트.
		}
		System.out.println("조건까지의 i의 개수 : "+cnt);
	}//main()
}//class
