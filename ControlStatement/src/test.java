import java.util.Scanner;

public class test {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("만들고자 하는 마름모의 크기를 지정해주세요(그냥 입력 후 enter_홀수로 지정하여야 함.)");
		int size =scan.nextInt(); // 마름모의 사이즈.
		int space =size/2; //위아래로 나눈 부분
		int star = size- space*2;//별이 찍히는 부분
		
		for(int a = size; a>0; a--){
			//왼쪽 빈 공간을 찍는 for문
			for(int b = space; b>0; b--){
				System.out.print(" ");
				
			}//for blank
			//별을 찍는 부분.
			for(int c = star; c>0; c--){
				System.out.print("*");
				
			}//for star
			System.out.println("");
			//아래부분도 위와 조건을 맞추기 위한 전제조건
			if(a>(size/2+1)){
			space--;
			star+=2;
			}else{
				space++;
				star-=2;
			}//if
		}//for a
		
		scan.close();
	}//main()
}//class
