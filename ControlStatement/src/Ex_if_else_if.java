public class Ex_if_else_if {
	public static void main(String[] args) {
		/*
		 * 다중 if문: 여러개의 조건을 판단하여 해당조건을 만족할 경우, 실행.
		 * 
		 * if(조건1){
		 * 	조건1이 참일 경우 실행문장.
		 * }else if(조건2){
		 * 	조건 2가 참일 경우 실행문장.
		 * }else{
		 * 	//생략가능
		 *	조건 1, 조건2 등 모든 조건이 맞지 않을때.
		 * }
		 * 
		 * */
		int jumsu = 78;
		
		//내림차순으로 비교. ==>오름차순일 경우, 결과가 제대로 나오지 않음(조건이 겹쳐서 나오기 때문에)
		if(jumsu >= 90){// 90점 이상
			System.out.println("당신의 성적은 A입니다.");
		}else if(jumsu >= 80){// 80 <= x < 90
			System.out.println("당신의 성적은 B입니다.");
		}else if(jumsu >= 70){// 70 <= x < 80
			System.out.println("당신의 성적은 C입니다.");
		}else if(jumsu>= 60){// 60 <= x < 70
			System.out.println("당신의 성적은 D입니다.");
		}else if(jumsu < 60){// x < 60
			System.out.println("당신의 성적은 F입니다.");
		}//if
	}//main()
}//class
