public class Test_for01 {
	public static void main(String[] args) {
		//1부터 100까지 정수중에서 짝수의 누적합을 구하여 출력
		int evenSum = 0; //값을 초기화 선언을 해준뒤에 연산이 가능하다.
		for(int i =1; i<=100; i++){
			if(i %2 == 0){
				evenSum+=i;
			}
		}
		System.out.println("짝수의 누적합 : "+evenSum);
		
		//1~100까지 정수중에서 홀수의 누적합.
		int sum =0;
		for(int i =1; i<=100; i++){
			if(i%2 == 1){
				sum+=i;
			}
		}
		System.out.println("홀수의 누적합 : "+sum);
		
		//1~100까지 정수중에서 3의 배수의 누적합.
		int sum2 = 0;
		for(int i =1; i<=100; i++){
			if(i% 3 == 0){
				sum2+=i;
			}
		}
		System.out.println("3의 배수의 누적합 : "+sum2);
		
		//1부터 100까지의 정수 중에서 짝수의 개수를 구하여 출력
		int evenCnt = 0;
		for(int i =1; i<=100; i++){
			if(i % 2 == 0){
//				evenCnt +=1;// 조건이 참인 경우에 개수를 더해준다.
				evenCnt++;
				
			}
		}
		System.out.println("1~100개까지 짝수의 개수 : "+evenCnt);
		
		//1부터 100까지의 정수 중에서 홀수의 개수를 구하여 출력
		int cnt =0;
		for(int i =1; i<=100; i++){
			if(i%2 == 1){
				cnt+=1; // cnt++;
			}
		}
		System.out.println("1~100개까지의 홀수의 개수: "+cnt);
		
		//1~100까지의 정수 중에서 짝수의 합과 개수를 구하시오, ==> for문안에 조건을 맞춰서 구하면 됨.
	}//main()
}//class
