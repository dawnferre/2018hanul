public class Test_while {
	public static void main(String[] args) {
		//1부터10까지 정수중에서 짝수의 합을 구하시오.
		int sum =0;
		for( int i =1; i<11; i++){
			if(i %2 ==0){
				sum+=i;
			}
		}
	
		System.out.println("for문의 짝수의 합: "+sum);
		
		//while 문을 사용한 짝수, 홀수의 합.
		int evenSum =0;
		sum =0;
		int i =1; //반복변수 초기화, 시작값설정
		while(i<11){
			if(i% 2 ==0){//짝수의 경우
				evenSum +=i;
			}else{//홀수인 경우
				sum+=i;
			}
			i++;//if문 조건에 상관없이 일어나는 반복횟수기 때문에 if조건바깥에 위치하게 됨.
		}
		System.out.println("while문의 짝수의 합: "+evenSum);
		System.out.println("while문의 홀수의 합: "+sum);
		
		//while문으로 별찍기.
		i =0;
		while(i<5){
			int j =0;
			while(j<i){
				System.out.print("*");
				j++;
			}
			i++;
			System.out.println();
		}
		
	}//main()
}//class
