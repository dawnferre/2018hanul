public class Test_do_while {
	public static void main(String[] args) {
		//1~10까지의 정수 중에서 짝수의 합을 구하시오.
		int add =1;
		int sum =0;
		int sum2 =0;
		int evencnt =0;
		int cnt =0;
		do{
			if(add% 2 ==0){
				sum+=add;
				evencnt++;
			}else{
				sum2+=add;
				cnt++;
			}
			add++;
		}while(add<11);
		System.out.println("1~10까지의 짝수의 합은 "+sum+", 짝수의 개수는 "+evencnt);
		System.out.println("1~10까지의 홀수의 합은 "+sum2+", 홀수의 개수는 "+cnt);
		
		System.out.println();
		//do while문을 이용하여 구구단 출력
		int i =2;
		do{
			int j =1;
			do{
				//한자리수 처리
				if(i*j<10){
					System.out.print(i+"x"+j+"= 0"+i*j+"\t");
				}else{
					System.out.print(i+"x"+j+"= "+i*j+"\t");
					
				}
				j++;
			}while(j<10);
			System.out.println();
			i++;
		}while(i<10);
		System.out.println("+++++++++++++++++++++++++++++++++++++++++");
		
		//세로로 나온 구구단 출력( do_while문을 이용해서)
		i =1;
		do{
			int j =2;
			do{
				System.out.print(j+"x"+i+"="+i*j+"\t");
				j++;
			}while(j<10);
			System.out.println();
			i++;
		}while(i<10);
		
		System.out.println("+++++++++++++++++++++++++++++++++++++++++");
		//do_while로 별찍기.
		//while이나 for문과 다르게 처음조건이 참이든 거짓이든, 먼저 실행되기 때문에 맨 처음 명령을 한번 더 수행하는 모습을 볼 수 있다.
		
		i=0;
		do{
			int j =0;
			do{
				System.out.print("*");
				j++;
			}while(j<i);
			System.out.println();
			i++;
		}while(i<5);
	}//main()
}//class
