public class Ex_continue {
	public static void main(String[] args) {
		//continue : continue 문을 만나면 그 다음 문장은 처리되지 않고
		//반복문의 첫머리로 제어권을 옮긴다.
		
		//1부터 10까지의 수 중에서 홀수의 합 oddSum
		int oddSum = 0;
		for(int i =1; i<11; i++){
			if(i % 2 ==1){// i % 2 !=0
				oddSum+=i;
			}
	
		}
		System.out.println("for 문의 홀수의 합: "+oddSum);
		
		int evenSum =0;
		for(int i =1; i<11; i++){
			if(i % 2 !=0){//홀수
				continue; // 반복문의 처음으로 돌리기 
			}
			evenSum+=i;//if 문의 조건이 두개밖에 나오지 않아서 else는 쓰지 않아도 됨(그대신, 조건이 여러개일때는 else if로 정확하게 조건설정.)
			
		}
		System.out.println("for 문의 짝수의 합(continue사용): "+evenSum);
		
	}//main()
}//class
