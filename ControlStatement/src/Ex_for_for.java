/*3장 ppt 디버깅 테이블 확인 하기.*/
public class Ex_for_for {
	public static void main(String[] args) {
		//중첩 반복문 :반복문안에 다른 반복문이 포함되어 있는 블럭.
		for(int i =0; i<5; i++){
			for(int j =0; j< 5; j++){
				System.out.println("i값 : "+i+"   j값 : "+j);
				
			}//for j
		}//for i 
		
		/*별찍기 1
		 * 열
		 * | 
		 * * ---행 1.1
		 * **     2.2
		 * ***    3.3
		 * ****   4.4
		 * *****  5.5
		 * 
		 */
		for(int i =0; i<5; i++){ //행
			for( int j = 0; j<i; j++){// 열
				System.out.print("*");
			}
			System.out.println(); //줄바꿈 
		}
		System.out.println();
		
		for(int i =1; i<=5; i++){ //행
			for( int j = 1; j<=i; j++){// 열
				System.out.print("*");
			}
			System.out.println(); //줄바꿈 
		}
		
		System.out.println();
		/*별찍기 2
		 * 열
		 * | 
		 * ***** ---행 1.5
		 * ****       2.4
		 * ***        3.3
		 * **         4.2
		 * *          5.1
		 * 
		 */
		
		for(int i =0; i<5; i++){ //열은 현재 숫자가 늘어나는 형태
			for(int j =4; j>i; j--){ //행은 현재 숫자가 줄어드는 형태
				System.out.print("*");
			}
			System.out.println("");
		}
		
		for(int i =1; i<=5; i++){ //열은 현재 숫자가 늘어나는 형태
			for(int j =5; j>=i; j--){ //행은 현재 숫자가 줄어드는 형태
				System.out.print("*");
			}
			System.out.println("");
		}
		
		
		/*별찍기 3
		 * 열
		 * | 
		 *     * ---행 1. -4, 1
		 *    **      2. -3  2
		 *   ***      3. -2  3
		 *  ****      4. -1  4
		 * *****      5.  0  5
		 * 
		 */
		
		for(int i =1; i<5; i++){ //열은 현재 숫자가 늘어나는 형태
			for(int j =4; j>0; j--){ //행은 현재 숫자가 줄어드는 형태
				if(i<j){
					System.out.print(" ");
				}else{
					System.out.print("*");
				}
			}
			System.out.println("");
		}
		System.out.println();
		
		/*별찍기 4
		 * 열
		 * | 
		 * ***** ---행 1. -4, 5
		 *  ****      2. -3  4
		 *   ***      3. -2  3
		 *    **      4. -1  2
		 *     *      5.  0  1
		 * 
		 */
		for(int i =5; i>0; i--){ //열은 현재 숫자가 늘어나는 형태
			for(int j =5; j>0; j--){ //행은 현재 숫자가 줄어드는 형태
				if(i<j){
					System.out.print(" ");
				}else{
					System.out.print("*");
				}
			}
			System.out.println("");
		}


	}//main()
}//class
