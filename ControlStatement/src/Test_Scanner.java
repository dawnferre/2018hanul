import java.text.DecimalFormat;
import java.util.Scanner;

public class Test_Scanner {
	public static void main(String[] args) {
		//두 수를 입력받은 후에 사칙연산을 수행하고 결과를 출력.
		Scanner scan = new Scanner(System.in); //객체생성.
//		System.out.println("사칙연산을 할 수를 입력해주세요.(띄어쓰기구분 혹은 엔터도 가능합니다.)"); _syso 하나만 적고 두수 입력도 가능.
		System.out.print("첫번째 정수를 입력해주세요."); //cmd에서 커서가 아래에 깜빡임 방지_ print: 줄바꿈안하니까
		int su1 = scan.nextInt();
		
		System.out.print("두번째 정수를 입력해주세요."); //cmd에서 커서가 아래에 깜빡임 방지_ print: 줄바꿈안하니까
		int su2 = scan.nextInt();
		scan.close();//스캐너 종료.
		
		System.out.println("입력하신 수는 "+su1+","+su2+"입니다.");
		//문자로 인식되는 s1,s2의 덧셈, 뺄셈 연산은 ()를 사용하게 된다.
		System.out.println("입력하신 수의 덧셈은 "+su1+"+"+su2+"="+(su1+su2)+"입니다.");
		System.out.println("입력하신 수의 뺄셈은 "+su1+"-"+su2+"="+(su1-su2)+"입니다.");
		System.out.println("입력하신 수의 곱셈은 "+su1+"x"+su2+"="+su1*su2+"입니다.");
		//나눗셈의 결과는 실수형태의 결과가 나올수 있다.
		//형변환 처리_ 
		//만약, 결과를 소숫점 둘째자리까지만 받는다고 한다면..?  ==> DecimalFormat
		//1.
		double result = Double.parseDouble(String.format("%.2f",(double)su1/(double) su2));
		//2.decimalFormat == #= 2.0 등의 의미없는 숫자를 표시하지 않음.  0 = 의미에 무관하게 무조건 값을 찍으라
		// decimalFormat 의 결과값은 String으로 변환된다.
		DecimalFormat decimalFormat = new DecimalFormat("0.00");
		String result2 =decimalFormat.format((double)su1/(double) su2);
		//하나만 변환을 시켜도 double로 가능하기 때문에.
		String result3 =decimalFormat.format((double)su1/su2);
		//결과값이 int형으로 나오기 때문에 _ 소숫점자리가 나오지 않는다.
		String result4 =decimalFormat.format((su1/su2));

		System.out.println("입력하신 수의 나눗셈은 "+su1+"/"+su2+"="+result+"입니다.");
		System.out.println("입력하신 수의 나눗셈은 "+su1+"/"+su2+"="+result2+"입니다.");
		System.out.println("입력하신 수의 나눗셈은 "+su1+"/"+su2+"="+result3+"입니다.");
		
	}//main()
}//class
