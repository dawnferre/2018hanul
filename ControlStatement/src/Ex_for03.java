public class Ex_for03 {
	public static void main(String[] args) {
		//1부터 10까지 누적합(sum)을 구하여 출력하시오.
		int sum =0;
		for(int i =0; i<=10; i++){
			sum +=i;
			// for문안에서 출력시, 반복되는 값이 계속 출력됨(루프가 끝날때까지)
			System.out.print(sum+" ");
		}
		System.out.println("\n바깥에서 출력하면 마지막에 담긴 값(누적된 합)만 출력");
		//for문 바깥에서 출력시에는 sum 마지막에 담긴 값만 출력함.
		//********선언부가 위치에 따라 결과가 상이하게 나오기 때문에 선언부 위치가 중요하다.
		System.out.println("누적합 : "+sum);
		
	}//main()
}//class
