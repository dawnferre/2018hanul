
public class Ex_if {
	public static void main(String[] args) {
	/*
	 * 단순 if문 : 주어진 조건을 한단하여 참일 경우에만 실행.
	 * 	if(조건식){
	 * 		조건식이 참일 경우, 실행되는 문장;
	 * 	}
	 * 
	 * %% 이때 실행되는 문장이 단일문장(1줄)일 경우 blocking 생략가능.
	 * 	===> if(조건문) 실행문
	 * 
	 * */	
		int a = 10;
		if(a % 2 == 0){// 조건식에는 반드시 비교, 논리연산자를 사용 ==> 연산의 결과가 true, false
			System.out.println("입력값은 "+a+"입니다.");
			System.out.println("짝수입니다.");
		}//if
		
		int b = 9;
		if(b % 2 != 0){// b를 2로 나눈 나머지 값이 0과 같지 않으면 실행문이 실행된다. ==>즉, b의 값은 홀수이다.
			System.out.println("입력값은 "+b+"입니다.\n홀수입니다.");
		}//if
		
		int c =11;
		//단일문장이기 때문에 {} 생략가능.
		if(c % 2 != 0) 
			System.out.println("입력값은 "+c+"입니다.\n홀수입니다.");
		// 이후, 문장이 추가될 경우, 조건문 결과에 상관없이 실행되버리기때문에 {}꼭!!
		
		
	}//main()
}//class
