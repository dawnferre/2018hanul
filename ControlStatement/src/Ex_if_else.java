
public class Ex_if_else {
	 public static void main(String[] args) {
		/*
		 *  블럭 if문 : 조건을 판단하여 참과 거짓일때의 명령을 실행.
		 *  		 일반적인 의미의 if문을 뜻함.
		 *  if(){ 
		 *  	//참일때 시행되는 문장;
		 *  }else{ 
		 *  	//거짓일때 시행되는 문장;
		 *  }
		 * */
		 
		 int a = 10;
		 //if-else문은 반드시 실행이 된다.(조건이 참이든, 거짓이든)
		 if(a % 2 ==0){ //조건이 참일 경우
			 System.out.println("짝수입니다.");
		 }else{ //조건이 거짓일 경우
			 System.out.println("홀수입니다.");
		 }
	}//main()
}//class
