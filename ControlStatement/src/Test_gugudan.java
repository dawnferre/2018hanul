public class Test_gugudan {
	public static void main(String[] args) {
		//구구단
		//1열로 단수가 출력되는 형태
		for(int i =2; i<10; i++){
			for(int j =1; j<10; j++){
				System.out.print(i+"x"+j+"="+i*j+"\t");
				//옆으로 세울때는 화
			}
			System.out.println("");
			
		}

		System.out.println();
		
		//세로로 단수가 출력되는 형태
		for(int i =1; i<10; i++){
			for(int j =2; j<10; j++){
				System.out.print(j+"x"+i+"="+i*j+"\t");
				//옆으로 세울때는 화
			}
			System.out.println("");
			
		}
		
	}//main()
}//class
