import java.util.Scanner;

public class Ex_scanner {
	public static void main(String[] args) {
		//입력창에서 점수를 입력받은 후에 학점을 출력하는 프로그램: Scanner(입력)
		Scanner scanner = new Scanner(System.in); 
		//inputStream는 입력값을 받아서 사용함._키보드로부터 입력받는 객체생성.
		System.out.println("점수를 입력해주세요.");
		
		int jumsu = scanner.nextInt();
//		scanner.close();//입력완료
		//반복문을 사용해서 점수를 학점을 출혁하는 방법_ 조건이 맞지않을때 다시 입력하게끔.
		while(true){
			if(jumsu>100 || jumsu<0){
				System.out.println("다시 입력해주세요.");
				jumsu =scanner.nextInt();
				continue;
			}
			if(jumsu>=90){
				System.out.println("입력하신 점수는 "+jumsu+"점이며, A학점입니다.");
				break;
			}else if(jumsu>=80){
				System.out.println("입력하신 점수는 "+jumsu+"점이며, B학점입니다.");
				break;
			}else if(jumsu>=70){
				System.out.println("입력하신 점수는 "+jumsu+"점이며, C학점입니다.");
				break;
			}else if(jumsu>=60){
				System.out.println("입력하신 점수는 "+jumsu+"점이며, D학점입니다.");
				break;
			}else{
				System.out.println("입력하신 점수는 60점미만이며, 재수강입니다.");
				break;
			}
		}
		scanner.close(); //반복문이 끝나도 scanner가 닫혀있지 않아 메모리에 그대로 남아있기 때문에 반드시 닫아줘야된다.
	}//main()
}//class
