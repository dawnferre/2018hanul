
public class Test_if_else {
	public static void main(String[] args) {
		//성별 코드(gender) 
		//1 또는 3 = 남자(OR연산) 그외에는 여자.
		int genderCode = 3;
		if(genderCode == 1 || genderCode == 3){ //1 또는 3일경우
			System.out.println("남자입니다.");
		}else{
			System.out.println("여자입니다.");
		}
		//3항연산자로 나타낸 if_else문
		String result = (genderCode == 1 || genderCode == 3)?"남자입니다.":"여자입니다.";
		System.out.println(result);
	}//main()
}//class
