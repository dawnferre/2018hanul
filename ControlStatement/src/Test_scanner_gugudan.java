import java.util.Scanner;

public class Test_scanner_gugudan {
	public static void main(String[] args) {
		//사용자로부터 출력하고 싶은 구구단의 단을 입력받는다.
		//입력받은 단의 구구단을 출력한다.
		//입력받은 단의 범위는 2단부터 9단까지이며
		//그외의 단어 입력되면 오류메세지를 출력후 재입력받는다.
		Scanner scan = new Scanner(System.in);
		while(true){
			System.out.print("출력하고자 하는 구구단을 입력해주세요.");
			int danNum =scan.nextInt();
			
			if(danNum<2||danNum>=10){
				System.out.print("다시 입력해주세요.");
//				danNum = scan.nextInt();
				continue; //오류발생시 다시 반복문 실행.
			}else{
					System.out.println("출력하는 구구단은 "+danNum+"단입니다.");
					for(int j =1; j<10; j++){
						if(danNum*j<10){
							System.out.println(danNum+"x"+j+"= 0"+(danNum*j));
					
						}else{
							System.out.println(danNum+"x"+j+"= "+(danNum*j));
						}
					}	
				break;//조건 만족 후 실행하고 반복문 종료.
			}//if_오류체크
		}//while
		scan.close();
	}//mains()
}//class
