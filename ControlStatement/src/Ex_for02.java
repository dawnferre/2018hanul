public class Ex_for02 {
	public static void main(String[] args) {
		//1부터 10까지의 정수 중에서 짝수만 출력.( 단 한줄로 출력할 것.)
		for(int i =1; i<=10; i++){
			if(i % 2 ==0){
				System.out.print(i+" ");//빈칸 설정, print는 자동 줄바꿈이 없어 한줄로 출력하게 됨.
//				System.out.print(i+"\t");//빈칸 설정, print는 자동 줄바꿈이 없어 한줄로 출력하게 됨.
			}
		}//for
		System.out.println();//빈줄삽입, 줄바꿈
		// 10부터 1까지의 정수 중 홀수만 출력
		for(int i =10; i>=0; i--){
			if(i % 2 == 1){
				System.out.print(i+" ");
			}
		}//for
		System.out.println();
		
	}//main()
}//class
