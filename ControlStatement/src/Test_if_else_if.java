public class Test_if_else_if {
	public static void main(String[] args) {
		//성별코드(genderCode) = 1이면 남자, 2면 여자, 3이면 남자, 4이면 여자
		//그 외는 오류메세지.
		int genderCode = 5;
		//genderCode+=2; // genderCode = 5;
		
		if(genderCode == 1 || genderCode == 3){
			System.out.println("남자입니다.");
		}else if(genderCode == 2 || genderCode == 4){
			System.out.println("여자입니다.");
		}else{
			System.out.println("당신의 성별코드가 잘못 입력되었습니다.");
		}//if
		
		//3항연산자로 나타낸 경우.--:으로만 연결해서 계속 조건을 붙일 수 있다.
		String result =genderCode == 1 || genderCode == 3?"남자":
			genderCode == 2 || genderCode == 4?"여자":"당신의 성별코드가 잘못되었습니다.";
		System.out.println(result);
	}//main()
}//class
