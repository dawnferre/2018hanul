public class Ex_for01 {
	public static void main(String[] args) {
		/*
		 * 반복문 - 순환문, loop문 == for, while, do-while
		 * 
		 * 		1 			2			4
		 * for(초기값(시작값); 조건식(최종값); 반복후작업(증감값)){
		 * 	 실행문(반복할 문장);
		 * 		3
		 * }
		 *
		 * */
		//0~10까지 출력
		for(int i =0; i<=10; i++){
			System.out.println(i);
		}//for
		
		System.out.println("========");

		//10까지에서의 정수에서 짝수만
		//내가 한 것.
		for(int i =0; i<=10; i++){ //보통은, < 미만을 사용하고, i++을 바꿔서 사용되지 않는다.
			//0은 짝수가 아님.
			if(i%2 ==0&& i!=0){
			System.out.println(i);
			}
		}//for
		
		//반복값을 바꾸어  짝수로 나타낸 경우.
		for(int i =0; i<=10; i+=2){
			System.out.println(i);
		}
		
		System.out.println("========");
		
		//홀수만
		for(int i =0; i<=10; i++){
			if(i%2 ==1){ // == 조건: i%2 != 0
			System.out.println(i);
			}
		}//for
		
		// 10부터 0까지 출력
		for(int i =10; i>=0; i--){
			System.out.println(i);
		}
		//10부터 0까지 정수중에서 짝수 출력
		for(int i=10; i>=0; i-=2){
			System.out.println(i);
			
		}
		
		for(int i =10; i>=0; i--){
			if(i % 2 == 1){
				System.out.println(i);
			}
		}
		
		//3의 배수
		for(int i =10; i>=0; i--){
			if( i % 3 ==0 && i != 0){
				System.out.println(i);
			}
		}
	}//main()
}//class
