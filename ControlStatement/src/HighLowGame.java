import java.util.Random;
import java.util.Scanner;

public class HighLowGame {
	public static void main(String[] args) {
		/* 1부터 100사이의 정수 중에서 임의의 값을 얻어서 comNum에  저장.
		 * 숫자 알아맞추기 게임.
			1. 난수 발생(임의의 정수) 1. Random class
			2. 사용자의 숫자입력 = scanner
			3. 사용자의 숫자 -난수 비교
			   비교값에 따라서 띄우는 메세지가 달라야한다.
			4. 맞췄을때 맞추기까지의 시도한 수를 출력.  = do while(true), break;, continue;
			   1. 최소 한번은 실행 == do while
		 * */
		int comNum =0;//Random시도횟수
		int cnt =0;//시도횟수
		Random random = new Random();
		comNum =random.nextInt(100)+1;//+1은 1~100을 존재하는 수
		System.out.println("재미있는 숫자맞추기 게임\n숫자를 입력해주세요.");
		Scanner scan = new Scanner(System.in);
		int user = scan.nextInt();
//		System.out.println(comNum);//임의의 숫자를 입력.
		//내가 작성한 코드
		do{
			if(comNum > user){
				System.out.println("입력한 수보다 더 큰 값입니다.!! 다시 입력해주세요.");
				user = scan.nextInt();
				cnt++;
				continue;
			}else if(comNum < user){
				System.out.println("입력한 수보다 더 작은 값입니다.!! 다시 입력해주세요.");
				user = scan.nextInt();
				cnt++;
				continue;
			}
			System.out.println("빙고!!! 맞추셨네요!! 답은 "+user+"입니다.");
			break;
		}while(true);
		
		//선생님 코드
		do{
			System.out.println("숫자를 입력해주세요.");
			user = scan.nextInt();
			cnt++;
			if(comNum > user){
				System.out.println("입력한 수보다 더 큰 값입니다.!!");
				continue;
			}else if(comNum < user){
				System.out.println("입력한 수보다 더 작은 값입니다.!! ");
				continue;
			}
			System.out.println("빙고!!! 맞추셨네요!! 답은 "+user+"입니다.");
			System.out.println("당신이 시도한 횟수는 "+cnt+"입니다.");
			break;
		}while(true);
		
		
		scan.close();
		
	}//main()
}//class
