public class Ex_switch_case {
	public static void main(String[] args) {
		/*
		 * 선택문(switch_case) : 다중 if문을 간략하게 표현.
		 * switch(기준값){
		 *  case 값1:
		 *  	//값1이 참일때 실행되는 문장
		 *  	break;
		 *  case 값2:
		 *  	//값2이 참일때 실행되는 문장.
		 *  	break; //break;가 없는 경우 break;가 있는 곳까지 계속 실행됨.(조건에 상관없이 만족한다면)
		 *  .
		 *  .
		 *  .
		 *  .
		 *  case 값N:
		 *  	//값N이 참일때 실행되는 문장.
		 *  	break;
		 *  default:
		 *  	// 모든 조건이 거짓일때(만족하지 않을때) 실행되는 문장.
		 *  	break;
		 * }
		 * %% 기준값은 정수형(수치형)의 자료여야 한다. == 아스키코드로 바뀌는 문자형도 가능함(char)
		 * %% 실행문 뒤에 반드시 break;
		 * %% 비교값 (1....N)은 정수형(수치형) 자료여야 한다. 
		 * */
		
		int jumsu =78;
		
		if(jumsu>=0 && jumsu<=100){
			switch (jumsu/10) { //100가지 경우수를 사용하기 어렵기 때문에, 가짓수를 줄이기 위해서 
				//int 정수형이기때문에 소숫점은 버림.
				case 10://100  같은 조건이면 생략하고 다음 break;에 걸리게끔.
				case 9: //90-99 비교연산을 쓸수 없음.
					System.out.println("A학점");
					break;
				case 8: //80-89 비교연산을 쓸수 없음.
					System.out.println("B학점");
					break;
				case 7: //70-79 비교연산을 쓸수 없음.
					System.out.println("C학점");
					break;
				case 6: //60-69비교연산을 쓸수 없음.
					System.out.println("D학점");
					break;
		
				default: //그외 나머지.
					System.out.println("F학점");
					break;
			}//switch
		}else{
			System.out.println("점수를 잘못 입력하였습니다.");
		}//if
		
	}//main()
}//class
