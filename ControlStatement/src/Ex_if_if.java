public class Ex_if_if {
	public static void main(String[] args) {
		/*
		 * 중첩 if문 : if문안에 다른 if문이 있는 문장.
		 *  - 정확도를 높이기 위해  많이 사용되는 편.
		 *  
		 * if(조건1){
		 * 		//조건1이 참일때 실행되는 문장
		 * 		if(조건2){
		 * 		//조건1과 조건2가 참일때 실행되는 문장.
		 * 	}else if(조건4){
		 * 		//조건1은 참, 조건 2는 거짓이고 조건4는 참일때 실행되는 문장.
		 * 	}else{
		 * 		//조건1은 참이고, 조건2, 조건4는 거짓일때 실행되는 문장. 
		 * 	}
		 * 
		 * }else if(조건3){
		 * 		//조건 1은 거짓이고, 조건3은 참일때 실행되는 문장.
		 * }else{
		 * 		//모든 조건을 만족하지 못함.
		 * }
		 * */
		
		int jumsu = 78;
		//jumsu-=80; //오류 확인.
		if(jumsu <= 100 && jumsu >= 0){ //점수가 0~100사이일 경우(AND처리 ==논리곱, 교집합)  <==> OR처리 == 논리합,
			if(jumsu >= 90){// 90점 이상
				System.out.println("당신의 성적은 A입니다.");
			}else if(jumsu >= 80){// 80 <= x < 90
				System.out.println("당신의 성적은 B입니다.");
			}else if(jumsu >= 70){// 70 <= x < 80
				System.out.println("당신의 성적은 C입니다.");
			}else if(jumsu>= 60){// 60 <= x < 70
				System.out.println("당신의 성적은 D입니다.");
			}else if(jumsu < 60){
				System.out.println("당신의 성적은 F입니다.");
			}
		}else{ // 0~100사이가 아닌 것.
			System.out.println("점수가 잘못 입력되었습니다.");	
		}
		
	}//main() 
}//class
