public class Test_break {
	public static void main(String[] args) {
		//1부터 100까지의 누적합(sum)을 구하시오.
		int sum =0;
		int cnt =0;
		for(int i =1; i<=100; i++){
			
			sum+=i;
			cnt++;
			if(sum >= 1024){
				break;
			}
		}
		System.out.println("누적합 : "+ sum);
		System.out.println("멈춘갯수 : "+ cnt);
		
	}//main()
}//class
