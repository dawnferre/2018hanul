public class Test_continue {
	public static void main(String[] args) {
		//1부터 10까지의 수 중에서 홀수의 합 (oddSum)과 짝수의 합을 (evenSum)
		int oddSum =0;
		int evenSum =0;
		for(int i =1; i<11; i++){
			if(i% 2 !=0){
				oddSum+=i;
				continue; // 반복문의 처음으로 가라.
			}
			evenSum+=i; //else조건을 주지 않아서 continue를 사용하지 않으면  모든 값의 합으로 된다.(계속 실행하기 때문에.)
		}
		System.out.println(oddSum);
		System.out.println(evenSum);
	}//main()
}//class
