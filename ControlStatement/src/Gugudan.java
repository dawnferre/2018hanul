public class Gugudan {
	public static void main(String[] args) {
		//구구단을 출력하는 프로그램을 작성하라.
		for(int i =2; i<10; i++){
			for(int j=1; j<10; j++){ //이중for문 안의for문 변수 선언은 바깥for문안에서 되어있다(****)
				if(i*j<10){ //자릿수 맞추기 위해 설정된 부분.(한자리수면 앞에 0이 붙을려고)
				System.out.print(i+"x"+j+"= 0"+i*j+"\t");
				}else{
					System.out.print(i+"x"+j+"= "+i*j+"\t");
					
				}
			}
			System.out.println();
		}
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	 int in =2; 
	 	while(in<10){
	 		int j =1; //변수 선언의 위치가 다르기 때문에 돌아가지 않았음
	 		while(j<10){
	 			System.out.print(in+"x"+j+"="+in*j+"\t");
	 			j++;
	 		}//while j
	 		System.out.println();
	 		in++;
	 	}//while i
	 	
	 	System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

		//세로로 단수가 출력되는 형태
		for(int i =1; i<10; i++){
			for(int j =2; j<10; j++){
				System.out.print(j+"x"+i+"="+i*j+"\t");
				//옆으로 세울때는 화
			}
			System.out.println("");
			
		}
		
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		in =1;
		while(in<10){
			int j =2;
			while(j<10){
				System.out.print(j+"x"+in+"="+in*j+"\t");
				j++;
			}
			System.out.println();
			in++;
		}
		
	}//main()
}//class
