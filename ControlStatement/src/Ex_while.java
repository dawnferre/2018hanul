public class Ex_while {
	public static void main(String[] args) {
		/*
		 * while : 반복횟수를 모를 경우 사용 ( 선조건 - 후처리)
		 * 
		 * 초기값 설정;
		 * while(조건식){
		 * 	 실행문( 조건식이 참일 경우 실행);
		 * 	 증감값;
		 * 
		 * }
		 * -- for문 ()안의 조건들을 위치를 분리시켜 사용한 것과 동일.
		 * 
		 * */
		// Q. 1-10까지의 정수의 누적합을 사용한다.
		
		//for문을 사용한 경우
		int sum =0;
		for(int i =1; i<11;i++){
			sum+=i;
		}
		System.out.println("for문의 누적합 : "+sum);
		
		//while문을 사용한 경우
		int whileSum =0;
		int add =1; //초기값 설정, for문에서는 i값 설정한 것을 while에서는 바깥에서 선언함.
		while(add<11){ //조건식 == 최종값이 무엇인지 알 수 있다.
			whileSum+=add;
			add++;
			
		}
		System.out.println("while문의 누적합 : "+whileSum);
		
	}//main()
}//class
