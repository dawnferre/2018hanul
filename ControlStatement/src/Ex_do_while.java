public class Ex_do_while {
	public static void main(String[] args) {
		/*
		 * do_ while 문 : 선처리 -후조건
		 * 
		 * 
		 * %% 조건이 처음부터 거짓이더라도 실행문(do)는 최소 한번은 실행.
		 * 
		 * 초기값(시작값);
		 * do(
		 *  실행문;
		 *  중감식;
		 * )while(조건식);
		 * */
		//1~10까지의 정수 중에서 짝수의 합(sum)을 구하시오.
		
		int whileSum =0;
		int add =1; //초기값 설정, for문에서는 i값 설정한 것을 while에서는 바깥에서 선언함.
		do{
			whileSum+=add;
			add++;
		}while(add<11); //조건식 == 최종값이 무엇인지 알 수 있다.
		System.out.println(" do while문의 누적합 : "+whileSum);
		System.out.println();
		
		int evenSum =0;
		int sum =0;
		add =1;
		do{
			if(add%2 ==0){
			evenSum+=add;
			}else{
				sum+=add;
			}
			add++;
		}while(add<11);
		System.out.println("do while의 짝수누적합 : "+evenSum);
		System.out.println("do while의 홀수누적합 : "+sum);
		
	}//main()
}//class
