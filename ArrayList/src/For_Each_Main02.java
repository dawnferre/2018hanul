import java.util.ArrayList;

import com.hanul.study.MemberDTO;

public class For_Each_Main02 {
	public static void main(String[] args) {
		ArrayList<MemberDTO> list = new ArrayList<>();

		list.add(new MemberDTO("hone", 27, "���ֱ�����", "010-1234-5678"));//list(0)
		list.add(new MemberDTO("kim", 33, "����Ư����", "010-1134-9568"));//list(1)
		
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i).getName()+"\t");
			System.out.print(list.get(i).getAge()+"\t");
			System.out.print(list.get(i).getAddr()+"\t");
			System.out.println(list.get(i).getTel()+"\t");
		}
		System.out.println("--------------------------------");
		
		for (int i = 0; i < list.size(); i++) {
			for (int j = i+1; j < list.size(); j++) {
				MemberDTO temp = list.get(i);
				list.set(i, list.get(j));
				list.set(j, temp);
				
			}
		}
		for (MemberDTO dto : list) {
			System.out.print(dto.getName()+"\t");
			System.out.print(dto.getAge()+"\t");
			System.out.print(dto.getAddr()+"\t");
			System.out.println(dto.getTel()+"\t");
		}
		
	}// main
}// class
