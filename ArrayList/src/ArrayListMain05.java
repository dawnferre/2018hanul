import java.util.ArrayList;

public class ArrayListMain05 {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>();
		list.add("AAA");
		list.add("BBB");
		list.add("CCC");
		list.add("DDD");
		System.out.println(list);
		
		list.add(1,"EEE"); //위치지정해서 넣어준경우_ 위치부터 뒤에서 밀린다.
		System.out.println(list);

		list.set(1,"FFF"); //위치에 있는 값을 수정 _ 값을 넣어주는게 아니라 바꿔준다.
		System.out.println(list);
		
		list.remove(1); //위치에 있는 값을 없앰 _ 위치에서부터 앞으로 당겨진다.
		System.out.println(list);
		
//		list.clear(); :전체삭제
	}//main
}//class
