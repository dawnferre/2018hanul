import java.util.ArrayList;

public class ArrayListMain04 {
	public static void main(String[] args) {
		//ArrayListt<> list에 정수 10, 20,30,40,50 을 저장(add)하고 출력(get())
		ArrayList<Integer> list = new ArrayList<>(); //자바 7 이후엔 new <>안에 타입을 지정해주지 않아도 된다.
//		list.add(new Integer(10)); //wrapper class ==>자동적으로 boxing도 됨.
		
		for (int i = 1; i < 6; i++) {
			list.add(new Integer(i*10));
			//list.add(i*10) 자동 boxing이 이루어지는 상태.
		}
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).intValue());
			//처음부터 int 타입으로 지정했기 때문에 casting 굳이 필요없음.
			System.out.println(list.get(i)); 
		}
	}//main
}//class
