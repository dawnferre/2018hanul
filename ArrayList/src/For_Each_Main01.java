import java.util.ArrayList;
import java.util.Iterator;

import com.hanul.study.MemberDTO;

public class For_Each_Main01 {
	public static void main(String[] args) {
		//회원정보를 arraylist에 저장하시오.
		ArrayList<MemberDTO> list = new ArrayList<>();
		
		list.add(new MemberDTO("hone", 27, "광주광역시", "010-1234-5678"));
		list.add(new MemberDTO("kim", 33, "서울특별시", "010-1134-9568"));
		list.add(new MemberDTO("song", 42, "충주시", "010-3456-9875"));
		list.add(new MemberDTO("jeong", 18, "부산광역시", "010-3102-2045"));
		list.add(new MemberDTO("jeon", 10, "울산광역시", "010-1111-1111"));
		
		//ArrayList<>내용 출력_향상 for문(for_each) 
		for(MemberDTO dto : list){
			System.out.print(dto.getName()+"\t");
			System.out.print(dto.getAge()+"\t");
			System.out.print(dto.getAddr()+"\t");
			System.out.println(dto.getTel()+"\t");
		}
		
		System.out.println("-------------------------------");
		
		//단순 for문1.
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i).getName()+"\t");
			System.out.print(list.get(i).getAge()+"\t");
			System.out.print(list.get(i).getAddr()+"\t");
			System.out.println(list.get(i).getTel()+"\t");
		}
		
		System.out.println("-------------------------------");
		//단순 for문2.
		for (int i = 0; i < list.size(); i++) {
			MemberDTO temp =list.get(i);
			System.out.print(temp.getName()+"\t");
			System.out.print(temp.getAge()+"\t");
			System.out.print(temp.getAddr()+"\t");
			System.out.println(temp.getTel()+"\t");
		}
		
		System.out.println("-------------------------------");
		//hashtable _ Iterator(열거,나열): 사이즈정보를 알 수 없을 때
		Iterator<MemberDTO> it  = list.iterator();
		
		while(it.hasNext()){ //그 다음 값이 없다  =false
			MemberDTO it1 = it.next(); //변수로 묶어주니까된다...
			
			System.out.print(it1.getName()+"\t");
			System.out.print(it1.getAge()+"\t");
			System.out.print(it1.getAddr()+"\t");
			System.out.println(it1.getTel()+"\t");
		}
	}//main
}//class
