import java.util.ArrayList;

public class ArrayListMain03 {
	public static void main(String[] args) {
		//ArrayList<>에 정수 10,20,30,40,50 을 저장하고 출력하시오.
		//기본형 불가 
				//collection _ 기본자료형은 wrapper class사용함.
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(new Integer(10));
		list.add(new Integer(20));
		list.add(new Integer(30));
		list.add(new Integer(40));
		//boxing : 컴파일러가 자동으로 처리해줌.(wrapper class역할을 알아서 해준다.)
		list.add(50); 
		
		System.out.println((Integer)list.get(0));
		//unboxing : 컴파일러가 자동으로 처리해줌.(wrapper class역할을 알아서 해준다.)
		System.out.println(list.get(2));
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).intValue());
		}
	}//main
}//class
