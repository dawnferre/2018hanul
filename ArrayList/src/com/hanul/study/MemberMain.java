package com.hanul.study;

import java.util.ArrayList;

public class MemberMain {
	public static void main(String[] args) {
		ArrayList<MemberDTO> list = new ArrayList<MemberDTO>();
//==	ArrayList<MemberDTO> list = new ArrayList<>();
		list.add(new MemberDTO("hone", 27, "광주광역시", "010-1234-5678"));
		list.add(new MemberDTO("kim", 33, "서울특별시", "010-1134-9568"));
		list.add(new MemberDTO("song", 42, "충주시", "010-3456-9875"));
		list.add(new MemberDTO("jeong", 18, "부산광역시", "010-3102-2045"));
		
		MemberDAO dao = new MemberDAO();
		System.out.println("정렬전 출력");
		dao.display(list);
		//이름을 기준으로 오름차순 정렬
		System.out.println("오름차순정렬후 출력");
		dao.nameAscSort(list);
		dao.display(list);
		
		System.out.println("내림차순정렬");
		dao.nameDescSort(list);
		dao.display(list);
	}
}
