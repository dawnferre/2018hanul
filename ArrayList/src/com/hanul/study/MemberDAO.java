package com.hanul.study;

import java.util.ArrayList;

public class MemberDAO {
	// 출력
	public void display(ArrayList<MemberDTO> list) {

		System.out.print("이름\t");
		System.out.print("나이\t");
		System.out.print("주소\t");
		System.out.println("전화번호\t");

		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i).getName() + "\t");
			System.out.print(list.get(i).getAge() + "\t");
			System.out.print(list.get(i).getAddr() + "\t");
			System.out.println(list.get(i).getTel());
		}
	}

	// 정렬출력
	public void nameAscSort(ArrayList<MemberDTO> list) {
		// 정렬하는 알고리즘 정렬
		MemberDTO temp = null;
		// 정렬 for문 _ j는 항상 i+1부터
		for (int i = 0; i < list.size(); i++) {
			for (int j = i + 1; j < list.size(); j++) {
				// compare to = 양수 : 왼쪽이 크다, 음수: 오른쪽이 크다 ,0: 같다.
				if (list.get(i).getName().compareTo(list.get(j).getName()) > 0) { // 오름차순
					temp = list.get(i); // 먼저 기존 값을 임시변수에 저장
					list.set(i, list.get(j)); // 기존값에 새로운 값 저장
					list.set(j, temp);// 새로운 값에 임시변수를(기존값저장되어있음.) 저장 (swap)
				}
			}
		}
	}

	public void nameDescSort(ArrayList<MemberDTO> list) {
		// 정렬하는 알고리즘 정렬
		MemberDTO temp = null;
		// 정렬 for문 _ j는 항상 i+1부터
		for (int i = 0; i < list.size(); i++) {
			for (int j = i + 1; j < list.size(); j++) {
				// compare to = 양수 : 왼쪽이 크다, 음수: 오른쪽이 크다 ,0: 같다.
				if (list.get(i).getName().compareTo(list.get(j).getName()) < 0) { // 내림차순
					temp = list.get(i); // 먼저 기존 값을 임시변수에 저장
					list.set(i, list.get(j)); // 기존값에 새로운 값 저장
					list.set(j, temp);// 새로운 값에 임시변수를(기존값저장되어있음.) 저장 (swap)
				}
			}
		}

	}
}
