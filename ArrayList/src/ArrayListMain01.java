import java.util.ArrayList;
import java.util.Arrays;

import com.hanul.study.A;

public class ArrayListMain01 {
	public static void main(String[] args) {
		//3개의 크기(길이)를 갖는 정수형 배열(arr을 선언)
		int[] arr = new int[3];
		//배열에 10,20,30을 할당하시오.
		arr[0] = 10;
		arr[1] = 20;
		arr[2] = 30;
		
		for (int i = 0; i < arr.length; i++) {
			arr[i] = (i+1)*10;
			
		}
		System.out.println(Arrays.toString(arr));
		
		//A class(객체) 배열을 5개 저장하시오. -> com.hanul.study package 
//		A[] a = new A[5]; //Array 구조: 크기(길이)가 고정
		//<> : 배열 타입
		ArrayList<Object> list = new ArrayList<Object>(); //arrayList 생성
		list.add(new A()); //add안에는 무조건 class
		list.add(new A()); //add 삽입, 추가
		list.add(new A()); 
		list.add(new A()); 
		list.add(new A()); 

		for (int i = 0; i < list.size(); i++) {
			//size() :list의 길이/크기
			((A)list.get(i)).print(); //배열의 타입이 object 부모클래스라 downcasting.
			//get(): 출력(가져온다)
		}
	}//main
}//class
