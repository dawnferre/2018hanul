import java.util.ArrayList;

import com.hanul.study.A;

public class ArrayListMain02 {
	public static void main(String[] args) {
		//ArrayList<> 객체(objList) 생성 -> object객체: 모든객체
		ArrayList<Object> obList = new ArrayList<Object>();
		obList.add(new A());
		
		((A)obList.get(0)).print(); // A Class Type DownCasting : Object -> S
		
		//ArrayList<>에 특정클래스(A class)만 저장하기 위해서 제한하는 방법
		// : <>안에 특정클래스를 넣어 제한한다.
		ArrayList<A> obList2 = new ArrayList<A>();
		obList2.add(new A());
		obList2.get(0).print();
		
		//타입을 알고 있을 땐 타입을 써주는 게 낫다.
		
	}//main()
}//class
