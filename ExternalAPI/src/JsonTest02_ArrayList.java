import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class JsonTest02_ArrayList {
	public static void main(String[] args) {
		JsonDTO dto1 = new JsonDTO(10, "홍길동", 34, "광주광역시", "010-1234-5678");
		JsonDTO dto2 = new JsonDTO(20, "kim", 24, "울산광역시", "010-999-9999");
		JsonDTO dto3 = new JsonDTO(30, "Jin", 80, "부산광역시", "010-888-8888");

		// 1.jdbc연동해서
		// 2.list 리턴으로 값을 넘겨주고
		// 3.json으로 변환해서 화면에 뿌려줄때
		ArrayList<JsonDTO> list = new ArrayList<>();
		list.add(dto1);
		list.add(dto2);
		list.add(dto3);

		// key값 지정할 필요없음 (put,add를 쓰지 않음)
		// arraylist를 jsonarray로 변환
		JSONArray arr = JSONArray.fromObject(list);
//		System.out.println(arr.toString());

		String json = arr.toString();
//		System.out.println(json);

		// JSON결과값을 파일에 출력.- outputStream
//		// 키값으로 한번더 묶이지 않음.
//		try {
//			BufferedWriter bw = new BufferedWriter(new FileWriter("memberArrayList.json"));
//			bw.write(json);
//			bw.flush();
//			System.out.println("파일이 생성되었습니다.");
//			bw.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		// JSON 화면출력
		for (int i = 0; i < arr.size(); i++) {
			JSONObject jobj = arr.getJSONObject(i); // arr[i]번지의 object값을 가지고옴.
			//key값을 주지 않고 바로 json형식을 dto로 변환가능(key)
//			jobj =jobj.getJSONObject("member"); //object 내의 키값을 가진 object가지고옴.
			// 원하는 타입으로 jsonobject를 변환시켜줌.
			JsonDTO dto = (JsonDTO) JSONObject.toBean(jobj, JsonDTO.class);
			System.out.print(dto.getNum() + "\t");
			System.out.print(dto.getName() + "\t");
			System.out.print(dto.getAge() + "\t");
			System.out.print(dto.getAddr() + "\t");
			System.out.println(dto.getTel() + "\t");

		}
	}// main
}// class
