package byTeacher;

import java.net.URL;
import java.util.List;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;

public class HtmlParser {
	public static void main(String[] args) {
		String url = "http://su.or.kr/03bible/daily/qtView.do?qtType=QT1";
		try {
			URL net = new URL(url);
			Source source = new Source(net);
			//System.out.println(source);
			List<Element> list = source.getAllElements(HTMLElementName.LI);
			
			String html1 = null;	//제목
			String html2 = null;	//날짜, 구절
			String content = null; //본문
			for (int i = 0; i < list.size(); i++) {
				String data = list.get(i).getContent().toString();
				//System.out.println(data);
				if(data.contains("subject")){
					html1 = data;
				}else if(data.contains("book_line\"")){
					html2 = data;
				}else if( data.contains("table")){
					content = data;
				}
			}
			
			//System.out.println(html1);
			//System.out.println(html2);
			
			int spos = html1.indexOf(">");
			int epos = html1.indexOf("</p>");
			//System.out.println("spos : " + spos + ", epos : " + epos);
			String subject = html1.substring(spos + 1, epos);
			System.out.println("제목 : " + subject);
			
			spos = html2.indexOf(">");
			epos = html2.indexOf("]");
			//String date = html2.substring(spos + 1, epos + 1);
			//System.out.println(date);
			int eposDate = html2.indexOf("&nbsp");
			int sposDate = html2.indexOf("[");
			String date1 = html2.substring(spos + 1, eposDate);
			String date2 = html2.substring(sposDate, epos + 1); 
			System.out.println("날짜 : " + date1.trim() + ", " + date2.trim());
			
			//내용출력
			String[] sp = content.split("</tr>");
			for (int i = 0; i < sp.length-1; i++) {
				
//				System.out.println(sp[i].toString());
				int contentnum = sp[i].indexOf("<th>");
				int contentnumend = sp[i].indexOf("</th>");
				
				int content1= sp[i].indexOf("<td>");
				int content2= sp[i].indexOf("</td>");
				String num = sp[i].substring(contentnum+4, contentnumend);
				String gul = sp[i].substring(content1+4, content2);
				System.out.println(num+". "+gul);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}//class









