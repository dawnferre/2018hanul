import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

//Java gui구현 :awt(abstract window toolkit) ->Swing(그래픽제공)
public class LottoGame {
	//컴포넌트에 j가 붙으면 swing! 아니면 awt!
	//컴포넌트 정의(선언)
	JFrame frame;
	JPanel panel;// 버튼들을 묶어주는 컨테이너
	JButton btn1;//번호생성
	JButton btn2;//clear
	JTextArea output; //생성된 번호 출력
	public static void main(String[] args) {
		new LottoGame(); //생성자 호출
	
		
	}//main
	//생성자
	public LottoGame() {
		//컴퍼넌트 생성
		frame = new JFrame("Lotto Game"); //프레임생성 + 제목설정
		panel = new JPanel();
		btn1 = new JButton("번호생성");// 버튼생성 + 텍스트설정
		btn2 = new JButton("초기화");// 버튼생성 + 텍스트설정
		output = new JTextArea();
		
		//화면띄우기
		//frame -window 정의
		frame.setSize(300, 300);
		frame.setVisible(true);
		frame.setLocation(1200, 300); //처음 위치한 부분을 좌표로 지정.
//		frame.setLocationRelativeTo(null); //프레임을 화면 중앙에 설정하겠다.
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //닫기 버튼설정 ==> 프로그램종료
		
		
		//나머지 컴퍼넌트 추가 _프레임에 
		panel.add(btn1);//시작버튼 추가.
		panel.add(btn2);//클리어버튼 추가.
		
		frame.getContentPane().add(panel, BorderLayout.NORTH);  //패널을 프레임에 추가 + 패널위치설정.
		frame.getContentPane().add(new JScrollPane(output)); //프레임에 추가 + 위치설정.+ scroll설정
//		( textarea이 안되면 최소화시키고 나서 다시 열어볼것.)
		
		
		//이벤트 핸들링(리스너 연동) :별도의 클래스로 구현  ==>내부 nestedclass
		btn1.addActionListener(new StartButtonListner(
					
				));
		btn2.addActionListener(new ClearButtonListner(
					
				));
		
	}
	//인터페이스 클래스 (actionlistener를 implements)
	//번호생성 클릭시 번호가 생성되는
	class StartButtonListner implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			int[] lotto = new int[6];
			Random random = new Random();
			for (int i = 0; i < lotto.length; i++) {
				lotto[i] = random.nextInt(45)+1;
				
				for (int j = 0; j < i; j++) {
					if(lotto[i] == lotto[j]){
						i--;
						break;
					}
				}
					
			}
			for (int i = 0; i < lotto.length; i++) {
//				System.out.print(lotto[i]+"\t");
//				output.setText(lotto[i]+" "); //왜 숫자 하나?
				output.append(lotto[i]+" ");
			}
			output.append("\n"); //줄바꿈.
			System.out.println();
			
		}
		
	}
	
	//번호가 삭제시 번호가 없어지는
	class ClearButtonListner implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) { 
			output.setText(" "); //text로 빈칸으로 만들어주면 된다.
		}
		
	}
}//class

