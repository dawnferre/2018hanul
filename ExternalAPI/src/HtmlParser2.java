import java.net.URL;
import java.util.List;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;


//매일 업데이트 된다고 해도
// 한번 코드를 따면 가지고 이용하기 쉽다.
public class HtmlParser2 {
	public static void main(String[] args) {
		String url ="http://www.su.or.kr/03bible/daily/qtView.do?qtType=QT1";
		try {
			URL addr = new URL(url);
			Source source = new Source(addr);
//			System.out.println(source);
			List<Element> list =source.getAllElements(HTMLElementName.LI);
			List<Element> list2 =source.getAllElements(HTMLElementName.P);
	
			String html1 = null; //제목
			String time1 = null; //time
			String content = null; //본문
			for (int i = 0; i < list.size(); i++) {
				String data =list.get(i).getContent().toString(); //li에 있는  내용 확인
//				System.out.println(data);
				
				//조건 : class이름으로 비교함.
				if(data.contains("subject")){
					html1 = data;
				}else if( data.contains("table")){
					content = data;
				}
			
			}
//			System.out.println(html1);
//			System.out.println(content);
			//날짜
			for (int i = 0; i < list2.size(); i++) {
				String data2 =list2.get(i).getContent().toString(); //li에 있는  내용 확인
//				System.out.println(data2);
				
				if(data2.contains("&nbsp;&nbsp;")){ //어제 날짜로 조건을 줬기때문에 결과가 나오지 않았던것.
					time1 = data2;
				}
			}
//			System.out.println(time1);
			
			//태그로 접근해서 parse위치를 가지고 오는것.
			int startposition = html1.indexOf(">"); //>부터 시작되는 값
			int endposition = html1.indexOf("</p>");
//			System.out.println(startposition+","+endposition);
			String result =html1.substring(startposition+1, endposition);
			System.out.println(result);
			
			int start = time1.indexOf("2"); //ㅔ
			int end = time1.indexOf("]");
			int endpositionDate = time1.indexOf("&nbsp;"); //날짜까지만 추출
			int startpositionDate = time1.indexOf("[");
//			String result2 = time1.substring(start, end+1);
			String result3 = time1.substring(start, endpositionDate);
			String result4 = time1.substring(startpositionDate, end+1);
			System.out.print(result3+", ");
			System.out.println(result4.trim());
			
			
			//내용출력
			String[] sp = content.split("</tr>");
			for (int i = 0; i < sp.length-1; i++) {
				
//				System.out.println(sp[i].toString());
				int contentnum = sp[i].indexOf("<th>");
				int contentnumend = sp[i].indexOf("</th>");
				
				int content1= sp[i].indexOf("<td>");
				int content2= sp[i].indexOf("</td>");
				String num = sp[i].substring(contentnum+4, contentnumend);
				String gul = sp[i].substring(content1+4, content2);
				System.out.println(num+". "+gul);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}//main
}//class
