import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.FileWriter;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class JsonTest01 {
	public static void main(String[] args) {
		//객체생성후 데이터 집어넣기
		JsonDTO dto1 = new JsonDTO(10, "홍길동", 34, "광주광역시", "010-1234-5678");
		JsonDTO dto2 = new JsonDTO(20, "kim", 24, "울산광역시", "010-999-9999");
		JsonDTO dto3 = new JsonDTO(30, "Jin", 80, "부산광역시", "010-888-8888");
		
		//jsonarray, jsonobject 객체생성
		JSONArray arr =new JSONArray();
		JSONObject obj = new JSONObject();
		
		//object -put 사용(key, value지정)
		obj.put("member", dto1);//key는 동일, value값만 다르게 넣어둠.
		arr.add(obj);
		obj.put("member", dto2);
		arr.add(obj);
		obj.put("member", dto3);
		arr.add(obj);
		
		String json=arr.toString();
//		System.out.println(json);
		
		//JSON결과값을 파일에 출력.-  outputStream
//		try {
//			BufferedWriter bw = new BufferedWriter(new FileWriter("member.json"));
//			bw.write(json);
//			bw.flush();
//			System.out.println("파일이 생성되었습니다.");
//			bw.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		//JSON 화면출력
		for (int i = 0; i < arr.size(); i++) {
			JSONObject jobj =arr.getJSONObject(i); //arr[i]번지의 object값을 가지고옴.
			jobj =jobj.getJSONObject("member"); //object 내의 키값을 가진 object가지고옴.
			JsonDTO dto =(JsonDTO) JSONObject.toBean(jobj,JsonDTO.class); 
			//class의 형태로 변환되어 가지고 오겠다 (class라면 .class필수적임)
			//원하는 타입으로 jsonobject를 변환시켜줌.
			System.out.print(dto.getNum()+"\t");
			System.out.print(dto.getName()+"\t");
			System.out.print(dto.getAge()+"\t");
			System.out.print(dto.getAddr()+"\t");
			System.out.println(dto.getTel()+"\t");
			
		}
		
		
		
	}//main
}//class
