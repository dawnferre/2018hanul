package practice.html;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;

public class HtmlPractice {
	public static void main(String[] args) {
		String addr = "https://www.daum.net/";
		try {
			URL url = new URL(addr);
			Source source = new Source(url);
			
			List<Element> list = source.getAllElements(HTMLElementName.H4);
			
			String title = null;
			
			for (int i = 0; i < list.size(); i++) {
				String data = list.get(i).getContent().toString();
//				System.out.println(data);
				
				if(data.contains("�̽�")){
					title = data;
				}
			}
			System.out.println(title);
			
			
			List<Element> list2 = source.getAllElements(HTMLElementName.SPAN);
			ArrayList<String> issueList = new ArrayList<>();
			int cnt =1;
			for (int i = 0; i < list2.size(); i++) {
				String data = list2.get(i).getContent().toString();
//				System.out.println(data);
				if(data.contains("class=\"link_issue\" tabindex=\"-1\"")){
					int start = data.indexOf(">");
					int end = data.indexOf("</a>");
					data = data.substring(start+1, end);
					issueList.add(cnt+"�� :"+data);
					cnt++;
				}
				
			}
			
			for(String result :issueList){
				System.out.println(result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}//main
}//class
