package practice.html;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XmlParser {

	public static void main(String[] args) throws Exception {
		BufferedReader br;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder;
		Document doc = null;
		try {

			String addr = "http://api.gwangju.go.kr/xml/lineInfo";
			URL url = new URL(addr);

			HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();

			// 응답읽기
			br = new BufferedReader(new InputStreamReader(urlconnection.getInputStream(), "utf-8"));
			String result = null;
			String line;
			while ((line = br.readLine()) != null) {
				result = result + line.trim();
			}

			// xml파싱하기
			InputSource is = new InputSource(new StringReader(result));
			builder = factory.newDocumentBuilder();
			doc = builder.parse(is);
			XPathFactory xpathFactory = XPathFactory.newInstance();
			XPath xpath = xpathFactory.newXPath();
			XPathExpression expr = xpath.compile("/line_list/line/");
			NodeList nodelist = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for (int i = 0; i < nodelist.getLength(); i++) {
				NodeList child = nodelist.item(i).getChildNodes();
				for (int j = 0; j < child.getLength(); j++) {
					Node node = child.item(j);
					System.out.println("버스이름" + node.getNodeName());
				}
			}

		} catch (Exception e) {
			e.getStackTrace();
		}
	}// main
}// class
