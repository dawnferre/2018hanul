package practice.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class DepartmentsDAO {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	//기본생성자에 연결코드를 넣음.
	public DepartmentsDAO(){
		String url = "jdbc:oracle:thin:@localhost:1521/xe";
		String user = "hr";
		String password = "hr";
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url, user, password);
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
	}
	//마지막에 닫는 코드
	public void disconn(){
		if(conn!=null | pst!= null | rs!=null){
			try {
				conn.close();
				pst.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//부서 조회하는 코드
	public ArrayList<DepartmentsDTO> selectDep(){
		String sql ="select * from departments";
		ArrayList<DepartmentsDTO> list = new ArrayList<>();
//		DepartmentsDTO[] list = new DepartmentsDTO[50];
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			int temp = 0;
			while(rs.next()){
				DepartmentsDTO dto =  new DepartmentsDTO();
				dto.setDepartment_id(rs.getInt("department_id"));
				dto.setDepartment_name(rs.getString("department_name"));
				dto.setManager_id(rs.getInt("manager_id"));
				dto.setLocation_id(rs.getInt("location_id"));
				list.add(dto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return list;
	}
	
}
