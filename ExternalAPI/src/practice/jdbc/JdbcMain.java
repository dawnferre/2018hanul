package practice.jdbc;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;

import net.sf.json.JSONArray;

public class JdbcMain {
	public static void main(String[] args) {
		DepartmentsDAO dao = new DepartmentsDAO();
		ArrayList<DepartmentsDTO> list = dao.selectDep();
//		System.out.println("부서코드 \t부서이름\t매니저id\t위치코드");
//		for(DepartmentsDTO dto:list){
//			System.out.print(dto.getDepartment_id()+"\t");
//			System.out.print(dto.getDepartment_name()+"\t");
//			System.out.print(dto.getManager_id()+"\t");
//			System.out.println(dto.getLocation_id()+"\t");
//		}
		JSONArray arr = JSONArray.fromObject(list);
		// JSON결과값을 파일에 출력.- outputStream
		// 키값으로 한번더 묶이지 않음.
		String json = arr.toString();
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("DepArrayList.json"));
			bw.write(json);
			bw.flush();
			System.out.println("파일이 생성되었습니다.");
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}//main
}//class
