

--tblMember 생성
--테이블 생성_(회원가입)
create table tblJoin(
		MEMBER_ID VARCHAR2(20) primary key not null,
		MEMBER_PW VARCHAR2(20),
		MEMBER_NAME VARCHAR2(20),
		MEMBER_EMAIL VARCHAR2(40),
		PHOTODATA VARCHAR2(999)
); 
--unique constraint 조건 추가
-- UNIQUE 제약조건 추가
ALTER TABLE tbljoin ADD UNIQUE (member_email);
-- 제약조건 조회


--전체 레코드 검색
drop table tbljoin;
SELECT * FROM TBLJOIN;
delete from tbljoin;




--tblMember 생성
--테이블 생성_(회원가입)
create table tblCommunity(
		COM_NUM VARCHAR2(20) NOT NULL,
		USER_ID VARCHAR2(20) PRIMARY KEY NOT NULL,
		COM_TITLE VARCHAR2(100) NOT NULL,
		COM_CONTENT VARCHAR2(999),
		COM_PHOTO VARCHAR2(999),
		COM_HASHTAG VARCHAR2(500) NOT NULL,
		COM_DATE DATE
); 

--테이블 전체보기
SELECT * FROM TBLCOMMUNITY;

drop table TBLCOMMUNITY;

--자동증가값 설정(b_num  ==> b_num_seq) default: 1씩 증가
create sequence com_num_seq start with 1;

--자동증가값 삭제(해제)
drop sequence com_num_seq;
--com_num_seq.nextval을 이용해 사용함.



--모든 레코드 삭제
delete from TBLCOMMUNITY;

--sequence의 문제점
--1. delete all해도 sequence는 reset되지 않는다(처음 삭제된 글포함해서 무조건)
--2. 20개단위로 숫자가 미리 정렬 (미리가지고와서 숫자를 내줌) - 20개를 채우지 않고 프로그램종료를 하면 그뒤의 20개 숫자를 씀.
--( 1~10까지 쓰면 ==>그담날 21,22,23...으로 새 20개단위의 시퀀스를 부여함 
--★★ client에게는 연속된 번호를 보여줘야 하기 때문에  다른 시퀀스 



