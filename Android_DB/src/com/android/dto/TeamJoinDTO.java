package com.android.dto;

import java.io.Serializable;
import java.sql.ResultSet;

public class TeamJoinDTO implements Serializable{
	private String member_id;
	private String member_pw;
	private String member_name;
	private String member_email;
	private String photoData;
	public TeamJoinDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public TeamJoinDTO(String member_id, String member_pw, String member_name, String member_email, String photoData) {
		super();
		this.member_id = member_id;
		this.member_pw = member_pw;
		this.member_name = member_name;
		this.member_email = member_email;
		this.photoData = photoData;
	}

	public String getMember_id() {
		return member_id;
	}

	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}

	public String getMember_pw() {
		return member_pw;
	}

	public void setMember_pw(String member_pw) {
		this.member_pw = member_pw;
	}

	public String getMember_name() {
		return member_name;
	}

	public void setMember_name(String member_name) {
		this.member_name = member_name;
	}

	public String getMember_mail() {
		return member_email;
	}

	public void setMember_mail(String member_email) {
		this.member_email = member_email;
	}

	public String getPhotoData() {
		return photoData;
	}

	public void setPhotoData(String photoData) {
		this.photoData = photoData;
	}
	
	
	

}
