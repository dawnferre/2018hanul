package com.android.dto;

import java.io.Serializable;
import java.sql.Date;

public class TeamCommunityDTO implements Serializable {
	private int com_num;
	private String user_id;
	private String com_title;
	private String com_content;
	private String com_photo;
	private String com_hashtag;
	private Date com_date;
	
	public TeamCommunityDTO() {
		// TODO Auto-generated constructor stub
	}

	public TeamCommunityDTO(String user_id, String com_title, String com_content, String com_photo,
			String com_hashtag) {
		super();
		this.user_id = user_id;
		this.com_title = com_title;
		this.com_content = com_content;
		this.com_photo = com_photo;
		this.com_hashtag = com_hashtag;
	}

	public int getCom_num() {
		return com_num;
	}

	public void setCom_num(int com_num) {
		this.com_num = com_num;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getCom_title() {
		return com_title;
	}

	public void setCom_title(String com_title) {
		this.com_title = com_title;
	}

	public String getCom_content() {
		return com_content;
	}

	public void setCom_content(String com_content) {
		this.com_content = com_content;
	}

	public String getCom_photo() {
		return com_photo;
	}

	public void setCom_photo(String com_photo) {
		this.com_photo = com_photo;
	}

	public String getCom_hashtag() {
		return com_hashtag;
	}

	public void setCom_hashtag(String com_hashtag) {
		this.com_hashtag = com_hashtag;
	}

	public Date getCom_date() {
		return com_date;
	}

	public void setCom_date(Date com_date) {
		this.com_date = com_date;
	}
	
	

	
}
