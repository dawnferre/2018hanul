package com.android.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.android.dto.TeamJoinDTO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class TeamJoinDAO {
	// sqlSessionFactory ==> sqlMapper
	// 전역변수 설정
	private static SqlSessionFactory sqlMapper;
	static{
				String resource ="com/hanul/mybatis/SqlMapConfig.xml";
				try {	//** try-catch를 사용하지 않으면 오류난다.  
					//Resources: MyBatis(구, iBatis)==> jar설정했기 때문에 import
					InputStream inputStream = Resources.getResourceAsStream(resource);
					sqlMapper = new SqlSessionFactoryBuilder().build(inputStream); 
					
				} catch (Exception e) {
					e.getStackTrace();
				}
				
			}// static:초기화블럭, 무조건 메모리에 할당해서 쓸 때 사용함.


	// insert회원
	public int insertJoin(TeamJoinDTO dto,String realImgPath,String photoData_path){
		
		imageDecorder(photoData_path, realImgPath);
		SqlSession session = sqlMapper.openSession();
		 int succ =session.insert("insertJoin", dto);
		session.commit();
		
		return succ;
	}
	
	public void imageDecorder(String base64Image, String pathFile){
		
		FileOutputStream imageOutFile = null;
		System.out.println(base64Image);
		try{
			imageOutFile = new FileOutputStream(pathFile);
			base64Image = base64Image.replaceAll("\\s+", "");
			byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
			imageOutFile.write(imageByteArray);			
			
		}catch(Exception e){
			e.getStackTrace();
			System.out.println("여기1"+e.getMessage());
		}finally {
			if(imageOutFile != null){
				try {
					imageOutFile.close();
				} catch (IOException e) {					
					e.getStackTrace();
					System.out.println("여기2"+e.getMessage());
				}
			}
		}
	}
	
	//테이블 전체 목록 불러오기)_Json목록으로 보여주기(id확인, email확인)
	public String getAllListJSON(){
		List<TeamJoinDTO> list = null;
		SqlSession session = sqlMapper.openSession();
		list = session.selectList("getAllList");
		//최종 완성될 JSONObject 선언(전체)
        JSONObject jsonObject = new JSONObject();
 
        //person의 JSON정보를 담을 Array 선언
        JSONArray personArray = new JSONArray();
 
        //person의 한명 정보가 들어갈 JSONObject 선언
        JSONObject personInfo = new JSONObject();
        for(TeamJoinDTO dto:list){
        	//정보 입력
        	personInfo.put("member_id", dto.getMember_id());
        	personInfo.put("member_pw", dto.getMember_pw());
        	personInfo.put("member_name", dto.getMember_name());
        	personInfo.put("member_email",dto.getMember_mail() );
        	personInfo.put("photodata", dto.getPhotoData());
        	//Array에 입력
        	personArray.add(personInfo);
        	
        }
 
        //전체의 JSONObject에 사람이란 name으로 JSON의 정보로 구성된 Array의 value를 입력
        jsonObject.put("member", personArray);
 
       
        //JSONObject를 String 객체에 할당
        String jsonInfo = jsonObject.toString();
 
//        System.out.print(jsonInfo);
 
		return jsonInfo;
	}

}
