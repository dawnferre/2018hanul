package com.android.dao;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.android.dto.TeamCommunityDTO;


public class TeamCommunityDAO {
	// sqlSessionFactory ==> sqlMapper
		// 전역변수 설정
		private static SqlSessionFactory sqlMapper;
		static{
					String resource ="com/hanul/mybatis/SqlMapConfig.xml";
					try {	//** try-catch를 사용하지 않으면 오류난다.  
						//Resources: MyBatis(구, iBatis)==> jar설정했기 때문에 import
						InputStream inputStream = Resources.getResourceAsStream(resource);
						sqlMapper = new SqlSessionFactoryBuilder().build(inputStream); 
						
					} catch (Exception e) {
						e.getStackTrace();
					}
					
				}// static:초기화블럭, 무조건 메모리에 할당해서 쓸 때 사용함.


		// insert회원
		public int insertCom(TeamCommunityDTO dto,String realImgPath,String photoData_path){
			
			imageDecorder(photoData_path, realImgPath);
			SqlSession session = sqlMapper.openSession();
			 int succ =session.insert("insertCom", dto);
			session.commit();
			
			return succ;
		}
		
		public void imageDecorder(String base64Image, String pathFile){
			
			FileOutputStream imageOutFile = null;
			System.out.println(base64Image);
			try{
				imageOutFile = new FileOutputStream(pathFile);
				base64Image = base64Image.replaceAll("\\s+", "");
				byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
				imageOutFile.write(imageByteArray);			
				
			}catch(Exception e){
				e.getStackTrace();
				System.out.println("여기1"+e.getMessage());
			}finally {
				if(imageOutFile != null){
					try {
						imageOutFile.close();
					} catch (IOException e) {					
						e.getStackTrace();
						System.out.println("여기2"+e.getMessage());
					}
				}
			}
		}

}
