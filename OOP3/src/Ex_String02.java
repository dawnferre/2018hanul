public class Ex_String02 {
	public static void main(String[] args) {
		
		//api 찾아보는 습관 들일것.
		
		
		String str1 = new String("APPLE"); //객체생성 => 참조형 자료구조
		String str2 = new String("ORANGE"); //객체생성 => 참조형 자료구조
		String str3 = new String("BANANA"); //객체생성 => 참조형 자료구조
		String str4 = new String("APPLE"); //객체생성 => 참조형 자료구조
		String str5 = new String("apple");
		
		
		//문자열의 대소관계
//		if(str1>str2) 문자열끼리는 연산자로 비교 불가.
		//문자열의 비교는 유니코드(아스키코드)값을 비교해야된다.
		// comparedTo() 결과 ==> int로 반환(양수, 음수, 0으로 반환)
		//대문자<소문자, A<Z, a<z
		//A...Z/a..Z
		
		System.out.println(str1.compareTo(str2)); // -14 : str2 >str1
		System.out.println(str2.compareTo(str1)); // 14 : str2 >str1
		
		System.out.println(str1.compareTo(str3)); //-1 
		System.out.println(str1.compareTo(str4)); //0은 같다. str1 = str4;
		System.out.println(str1.compareTo(str5));//-32
		
		if(str1.compareTo(str2)>0){//양수 즉, str1>str2
			System.out.println(str1+"이(가)"+ str2+" 보다 크다");
		}else if(str1.compareTo(str2)<0){//음수 즉, str1<str2
			System.out.println(str1+"이(가)"+ str2+" 보다 작다");
		
		}else{
			System.out.println(str1+"이(가)"+ str2+" 는 같은 문자열이다.");
			
		}
		
		//문자열이 같은지 다른지를 판단하는  메소드 : equals(), equalsIgnoredCase()
		//return값 : true, false 로 변환.
//		System.out.println(str1.equals(str2));
		if(str1.equals(str2)){
			System.out.println(str1+"과(와) "+str2+"는 같은 문자다.");
		}else{
			System.out.println(str1+"과(와) "+str2+"는 다른 문자다.");
			
		}
		
		//대소문자 상관없이 문자열을 비교 (APPLE, apple은 같은 동일한 문자로 인식한다.)
//		System.out.println(str1.equalsIgnoreCase(str5));
		if(str1.equalsIgnoreCase(str5)){
			System.out.println(str1+"과(와) "+str5+"는 같은 문자다.");
		}else{
			System.out.println(str1+"과(와) "+str5+"는 다른 문자다.");
			
		}
		
		
	}//main
}//class
