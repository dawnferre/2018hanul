public class Ex_String01 {
	public static void main(String[] args) {
		//String Class : 문자열을 조작하기 위한 기능을 담고있는 클래스 >> SunMicroSystem(Oracle):API
		//JRE System Library > java.lang.package > String.class
		//https://docs.oracle.com/javase/8/docs/api/ 에서 api문서 검색 및 활용방법 가능.
		
		
		//String class를 참조하기 때문에  String객체를 생성하여 값을 넣는것과 똑같은 결과가 나오게 된다.
		String str1 = "apple"; //apple이라는 문자열을 str1변수에 할당(초기화)
		String str2 = new String("APPLE"); //APPLE이라는 문자열을 str2객체에 할당.
		
		System.out.println("str1의 내용  : "+str1);
		System.out.println("str2의 내용  : "+str2);
		
		//문자열의 길이확인
		System.out.println("str1의 길이 : "+str1.length()); 
		System.out.println("str2의 길이 : "+str2.length());
		
		//대문자,소문자로 변경
		System.out.println("str1을 대문자로 변경 : "+str1.toUpperCase());
		System.out.println("str2을 소문자로 변경 : "+str2.toLowerCase());
		
		//특정문자만 추출 ★★★
		System.out.println(str1.substring(1));
		System.out.println(str1.substring(1,4)); //index 1부터 3앞에까지만
		
		//특정문자의 존재여부 (존재: index값을 반환, 실패 :-1반환)
		System.out.println(str1.indexOf("l")); //index 0부터 시작되는데 index3 =즉 4번째에 있음.
		
		int i = str1.indexOf("le");
		System.out.println(i);//-1이 나오는 경우 : 해당문자는 존재하지 않는다.
		
		if(i == -1){
			System.out.println("검색실패");
			System.out.println("문자열을 찾을 수 없어요.");
		}else{
			System.out.println("검색성공");
			System.out.println("찾으시는 문자열은 "+(i+1)+"번째에 있습니다.");
		
		}
		
		//문자열을 분리★
		String str3 = "대:300@중:200@소:100"; //@을 기준으로 문자열을 분리 ==> 배열로 받음(나눠지는 수는 2개이상.)
		String[] result =str3.split("@");
		// @을 기준으로 세가지가 for문을 이용해 배열에서 결과를 출력할 수 있음.
		for (int j = 0; j < result.length; j++) {
			System.out.println(result[j]);
		}
		
		//주어진 문자열에서 특정문자 한글자를 출력.
		System.out.println(str2.charAt(4));//주어진 숫자 index 값에 해당되는 문자를 추출함.
		
		//문자열을 치환(찾아서 바꿈) 
		System.out.println(str2.replaceAll(str2, "kk")); //문자열을 kk으로 바꿈.
		System.out.println(str2.replace("PP", "kk")); //PP -> kk로 찾아서 바꿈.
		
	}//main
}//class
