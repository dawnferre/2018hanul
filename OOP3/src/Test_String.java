import java.util.Arrays;

public class Test_String {
	public static void main(String[] args) {
		//str1의 문자열을 @를 기준으로 분리하고, 출력하시오.
		String  str1 ="사과@바나나@딸기@오렌지@메론@레몬@블루베리@수박";
		String[] result =str1.split("@");
		//메소드로 빼서 사용하는 방법
		Test_String ts = new Test_String();
//		ts.printArray(result);
		ts.nameSort(result);
		//toString 으로 바로 뽑는 방법
//		System.out.println(Arrays.toString(result));
	}//main
	//출력메소드 설정.
	public String[] printArray(String[] result){
		for (int i = 0; i < result.length; i++) {
			System.out.println(result[i]);
		}
		return result;
	}
	//과일이름을 기준으로 내림차순 정렬후 출력.
	public void nameSort(String[] result){
		String temp =null;
		//내침차순 정렬 정리.
		for (int i = 0; i < result.length; i++) {
			for (int j = i+1; j < result.length; j++) {
				temp =result[i];
				if(result[i].compareTo(result[j])<0){
					result[i]= result[j];
					result[j] = temp;
					
				}
			}
		}
		System.out.println(Arrays.toString(result));
	}
}//class
