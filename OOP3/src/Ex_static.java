public class Ex_static {
	public static void main(String[] args) {
		print1();
		
		Ex_static ex = new Ex_static();
		ex.print2();
//		ex.print1();//이미 메모리에 할당되어있어서 굳이 객체를 호출하여 생성하지 않아도 된다.
	}//main
	public static void print1(){
		System.out.println("print1 display");
	}
	public void print2(){
		System.out.println("print2 display");
		
	}
	static{
		
		System.out.println("static 초기화 블럭");
		//static :프로그램 시작전에 먼저 메모리에 할당, 초기화되고 프로그램 종료시에 소면.
		//* 초기화 블럭: static{~~~} >> 메인 메소드보다 먼저 실행한다. 
	}
			
	
}//class
