public class Ex_String03 {
	public static void main(String[] args) {
		String str1 = new String("APPLE"); //참조형 자료구조 : 새롭게 메모리를 할당하는 의미
		String str2 = new String("APPLE");
		String str3 = "APPLE";//String class str3선언 ===> 값을 할당 ,
		String str4 = "APPLE";
		
		//Q.str1 ==str2?
		//str1와 str2는 기본적으로 같은 문자열 ===>compare TO로비교하면 같은 값을 얻을 수 있음.
		if(str1.compareTo(str2)>=0){
			System.out.println("같음");
		}else{
			System.out.println("다름");
		}
		//2번째 방법. equals
		if(str1.equals(str2)){
			System.out.println("같음");
		}else{
			System.out.println("다름");
		}
		
		if(str3==str4){
			System.out.println("같음");
		}else{
			System.out.println("다름");
		}
		
	}//main
}//class
