import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Test_birthday {
	public static void main(String[] args) {
		//생년월일 8자리 입력 (ex.1992.10.02)
		//입력한 생년월일 출력
		//출력한 생일이 맞으면 Y, 틀리면 N 입력
		//Y,N 외에 입력 ==>Y또는 N으로 입력하세요. 다시 반복.
		//소문자 , 대문자 상관없이.
		//n 입력시 다시 반복, y입력시 생년월일 출력 과 나이출력
		
		Test_birthday tb = new Test_birthday();     
		tb.setBirthday();
		
	}//main
	
	//출력메소드
	public void getBirthAge(String birthday){
		
		Date date = new Date();//오늘날짜와 현재 시간을 가져온다.
		SimpleDateFormat day = new SimpleDateFormat("yyyyMMdd"); // =deimalformat과 같이 string이 나옴..
		System.out.println("입력하신 생일은 "+birthday.substring(0, 4)+"년 "
							+birthday.substring(4, 6)+"월"+birthday.substring(6, 8)+"일입니다.");
		String year = birthday.substring(0, 4);
		String pryear = day.format(date).substring(0, 4);
//		System.out.println(year);
//		System.out.println(pryear);
		System.out.println("나이는 "+((Integer.parseInt(pryear)-Integer.parseInt(year))+1)+"세 "
							+ "(만"+(Integer.parseInt(pryear)-Integer.parseInt(year))+"세)입니다.");
	}
	//입력받는 메소드
	public void setBirthday(){
		Test_birthday tb = new Test_birthday();     
		Scanner scan = new Scanner(System.in);
		while(true){
			System.out.println("생년월일을 입력해주세요.(ex.97년 4월 20일  =>19970420)");
			String birthday = scan.next();
			if(!birthday.isEmpty()){
				System.out.println("입력하신 생일은"+birthday+"입니다. 맞으면 Y, 아니면 N을 입력하세요.");
				String ans = scan.next();
				if(ans.equalsIgnoreCase("Y")){
					tb.getBirthAge(birthday);
					break;
				}else if(ans.equalsIgnoreCase("N")){
					continue;
				}else{
					System.out.println("대답은 Y또는 N으로 입력하세요.");
					ans = scan.next();
					continue;
				}
			}
		}
		scan.close();
	}
}//class
