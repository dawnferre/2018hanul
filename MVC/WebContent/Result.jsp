<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>누적합 결과 02</title>
</head>
<body>
<%
	request.setCharacterEncoding("utf-8");
	int su1 = (int)(request.getAttribute("su1"));
	int su2 = (int)(request.getAttribute("su2"));
	int sum = (int)(request.getAttribute("sum"));
	
%>
<!-- 호출하는 방법1. -->
<h3>입력하신 수 는 <%=su1 %>,<%=su2 %>이며, 두수사이의 수들의 누적합은 <%=sum %>입니다.</h3>
<!-- 호출하는 방법2. -->
<h3>입력하신 수 는 ${su1},${su2}이며, 두수사이의 수들의 누적합은 ${sum}입니다.</h3>
</body>
</html>