<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>두수의 홀수의 합</title>
</head>
<body>
	<%
		int su1 = (int)(request.getAttribute("su1"));
		int su2 = (int)(request.getAttribute("su2"));
		int sum = (int)(request.getAttribute("sum"));
	%>
	
	입력하신 두수는 <%=su1 %>,<%=su2 %>이며 두수 사이의 홀수의 합은 <%=sum %>입니다.
	입력하신 두수는 ${ su1 },${ su2 }이며 두수 사이의 홀수의 합은 ${ sum }입니다.
</body>
</html>