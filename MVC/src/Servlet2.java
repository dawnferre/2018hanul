import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.study.MemberDTO;
@WebServlet("/s05.do")
public class Servlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		//s04.do에서 넘겨준 바인딩 객체를 받는다.
		MemberDTO dto = (MemberDTO) request.getAttribute("dto");
		//한글처리
		response.setContentType("text/html;charset=utf-8");
		PrintWriter pw = response.getWriter();
			pw.println("<table border='1'>");
			
			pw.println("<tr>");
			pw.println("<th>이름</th>");
			pw.println("<th>아이디</th>");
			pw.println("<th>비밀번호</th>");
			pw.println("<th>나이</th>");
			pw.println("<th>주소</th>");
			pw.println("<th>전화번호</th>");
			
			pw.println("</tr>");
			pw.println("<tr>");
			pw.println("<td>"+dto.getIrum()+"</td>");
			pw.println("<td>"+dto.getId()+"</td>");
			pw.println("<td>"+dto.getPw()+"</td>");
			pw.println("<td>"+dto.getAge()+"</td>");
			pw.println("<td>"+dto.getAddr()+"</td>");
			pw.println("<td>"+dto.getTel()+"</td>");
			pw.println("</tr>");
			
			pw.println("</table>");
	}

}
