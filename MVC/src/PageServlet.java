import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/page.do")
public class PageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) 
								throws ServletException, IOException {
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		
//		System.out.println("id :"+id);
//		System.out.println("pw :"+pw);
		//한글처리
		response.setContentType("text/html;charset=utf-8");

		//프레젠테이션로직
		PrintWriter output = response.getWriter();
		output.println("<h2>id : "+id+"<h2>");
		output.println("<h2>pw : "+pw+"<h2>");
	}

}
