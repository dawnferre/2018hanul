import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/sc01.do")
public class SuCalc01 extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) 
							throws ServletException, IOException {
		// * 넘어오는 값은 무조건 String
		// 태그의 name 속성 
		int su1 =Integer.parseInt(request.getParameter("su1")); //클라이언트의 요청 ==> 입력한 값이 넘어오게 됨
		int su2 =Integer.parseInt(request.getParameter("su2")); //클라이언트의 요청 ==> 입력한 값이 넘어오게 됨
		
		//su1~su2까지의 누적합을 구하는 로직 : 비지니스 로직 ==> Modelpart(Class,Dto,Dao 등으로 호출하여 결정한다.)
		int sum = 0;
		//수 입력받은 것에 의한 처리
		if(su1<su2){
			for(int i =su1; i<=su2; i++){
				sum += i;
			}
			
		}else{
			for(int i =su2; i<=su1; i++){
				sum += i;
			}
			
			
		}
		
		response.setContentType("text/html;charset=utf-8");
		
		PrintWriter output = response.getWriter();
		output.println("<html>");
		output.println("<head>");
		output.println("<title>두수의 누적합 결과01</title>");
		output.println("</head>");
		output.println("<body>");
		
		if(su1<su2){
			output.println("<h2> 입력받은 두 수 : "+su1+","+su2+"</h2><br>");
		}else{
			output.println("<h2> 입력받은 두 수 : "+su2+","+su1+"</h2><br>");
			
		}
		output.println("<h2> 두수 사이의 누적합 : "+sum+"</h2><br>");
		output.println("</body>");
		output.println("</html>");
		
	}

}
