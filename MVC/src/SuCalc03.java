

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.study.TwoOddHap;

@WebServlet("/sc03.do")
public class SuCalc03 extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) 
					throws ServletException, IOException {
		
		int su1 = Integer.parseInt(request.getParameter("su1"));
		int su2 = Integer.parseInt(request.getParameter("su2"));
		
		//비즈니스로직  ==> DB연동
		int sum = new TwoOddHap().sum(su1, su2);
		
		// 연결로직.
		request.setAttribute("su1", su1);
		request.setAttribute("su2", su2);
		request.setAttribute("sum", sum);
		
		//프레젠테이션 로직==> 값을 호출
		RequestDispatcher rd = request.getRequestDispatcher("OddResult.jsp");//페이지 호출
		rd.forward(request, response); //forward ==> 페이지전환
	}

}
