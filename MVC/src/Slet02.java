

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.study.MemberDAO;
import com.hanul.study.MemberDTO;

@WebServlet("/s02.do")
public class Slet02 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) 
						throws ServletException, IOException {
		
		
		//비즈니스 로직
		MemberDAO dao = new MemberDAO();
		ArrayList<MemberDTO> list =dao.selectAll();
		
		response.setContentType("text/html;charset=utf-8");
		//프레젠테이션 로직:결과를 응답(.jsp,.html) 
		PrintWriter pw = response.getWriter();
		pw.println("<h2>회원가입전체목록</h2>");
		pw.println("<table border='1'>");
		for(MemberDTO dto:list){
			pw.println("<tr>");
			pw.println("<td>"+dto.getIrum()+"</td>");
			pw.println("<td>"+dto.getId()+"</td>");
			pw.println("<td>"+dto.getPw()+"</td>");
			pw.println("<td>"+dto.getAge()+"</td>");
			pw.println("<td>"+dto.getAddr()+"</td>");
			pw.println("<td>"+dto.getTel()+"</td>");
			pw.println("<td><a href='s03.do?id="+dto.getId()+"'>삭제</a></td>");
			pw.println("<td><button onclick='location.href=\"s03.do?id="+dto.getId()+"\"' style='background-color:cyan;'>삭제</button></td>");
			
			pw.println("</tr>");
		}
		pw.println("</table>");
		pw.println("<br><a href ='MemberMain.html'>회원가입화면</a>");
	}

}
