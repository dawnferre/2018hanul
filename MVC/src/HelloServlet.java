import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** 마우스 클릭시 , 옆에 뜨게 해주는 주석
 * Servlet implementation class HelloServlet
 */

@WebServlet("/hs.do")
public class HelloServlet extends HttpServlet {

	protected void service(HttpServletRequest request, HttpServletResponse response)
												throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		//코드 구현 
		//화면에 우선찍어줌
		PrintWriter output = response.getWriter();
		output.println("<html>");
		output.println("<head>");
		output.println("<title>hello,Servlet</title>");
		output.println("</head>");
		output.println("<body>");
		output.println("<h2>Hello, Servlet - servlet연결로 지금 만들었어요!!</h2><br>");
		output.println("<h2>안녕, 서블릿</h2>");
		output.println("</body>");
		output.println("</html>");
		
	}//service()

}//class
