import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.study.MemberDAO;
import com.hanul.study.MemberDTO;

@WebServlet("/s01.do")//mapping annotation
public class Slet01 extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) 
					throws ServletException, IOException {
		request.setCharacterEncoding("utf-8"); //한글처리
		//1.클라이언트의 요청을 받는다.== 매개변수를 가져온다.
		MemberDTO dto =new MemberDTO();
		
		dto.setIrum(request.getParameter("irum"));
		dto.setId(request.getParameter("id"));
		dto.setPw(request.getParameter("pw"));
		dto.setAge(Integer.parseInt(request.getParameter("age")));
		dto.setAddr(request.getParameter("addr"));
		dto.setTel(request.getParameter("tel"));
		
		//비즈니스 로직. == dto, dao(db접속 == db저장)
		MemberDAO dao = new MemberDAO();
		int result =dao.insert(dto);
		response.setContentType("text/html;charset=utf-8");
		//프레젠테이션 로직:결과를 응답(.jsp,.html) 
		PrintWriter pw = response.getWriter();
		if(result>0){
			pw.println("<h2>회원가입 성공!</h2>");
			pw.println("<br>");
			pw.println("<a href ='MemberMain.html'>회원가입화면</a>");
			pw.println("<a href ='s02.do'>전체회원 목록보기</a>");
		}else{
			pw.println("<h2>회원가입실패입니다...코드확인좀요</h2>");
			pw.println("<br>");
			pw.println("<a href ='MemberMain.html'>회원가입화면</a>");
			pw.println("<a href ='s02.do'>전체회원 목록보기</a>");
			
		}
		
	}

}
