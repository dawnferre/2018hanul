

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.study.TwoHap;

@WebServlet("/sc02.do")
public class SuCalc02 extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) 
						throws ServletException, IOException {
		
		//1. client 의 요청을 받는다 ( HttpServletRequest) ==> 매개변수를 가지고 오는 것.
		int su1 = Integer.parseInt(request.getParameter("su1"));
		int su2 = Integer.parseInt(request.getParameter("su2"));
		
		//2. 비즈니스 로직 : 별도의 클래스 생성  ==>com.hanul.study 패키지를 작성하여 만든다.
		int sum =new TwoHap().sum(su1, su2);
		
		//3.프리젠테이션 로직 : 처리한 결과를 넘겨줌.(응답,HttpServletResponse) >>.jsp  
		//객체를 바인딩 해준다.
		request.setAttribute("su1", su1);
		request.setAttribute("su2", su2);
		request.setAttribute("sum", sum);
		//값을 보내주는 페이지를 방법
		//1.forward 2. rewrite
		RequestDispatcher rd = request.getRequestDispatcher("Result.jsp");//페이지 호출
		rd.forward(request, response); //forward ==> 페이지전환
		
		
	}

}
