

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/s07.do")
public class Slet07 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) 
						throws ServletException, IOException {
		//바인딩 객체를 보내기 때문에 객체로 받아줘야 한다. 기본타입은 X,class 형태로 받아야한다.(WrapperClass 이용)
		//기본타입으로 받게 되면 서버 오류뜬다.(타입이 안맞아서 null값으로 인식하기 때문에)
		Integer su1 = (Integer) request.getAttribute("su1");
		Integer su2 = (Integer) request.getAttribute("su2");
		Integer result = (Integer) request.getAttribute("result");
		
		
		response.setContentType("text/html;charset=utf-8");
		PrintWriter pw = response.getWriter();
		pw.println("<h2>넘어온 수 1: "+su1+", 수 2: "+su2+"</h2>");
		pw.println("<h2>두 수 사이의 누적합 :"+result+"</h2>");
	}

}
