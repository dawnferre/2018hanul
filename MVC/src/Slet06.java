
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.study.TwoHap;
@WebServlet("/s06.do")
public class Slet06 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) 
						throws ServletException, IOException {
		//클라이언트 요청을 받는다.
		int su1 = Integer.parseInt(request.getParameter("su1"));
		int su2 = Integer.parseInt(request.getParameter("su2"));
		
		//비즈니스 로직
		int result = new TwoHap().sum(su1, su2);
		
		//프레젠테이션 로직 ==>s07.do 거기서 결과를 출력하게끔 넘겨줌.
		//1.바인딩 객체
		request.setAttribute("result", result);
		request.setAttribute("su2", su2);
		request.setAttribute("su1", su1);
		//다른 페이지로 넘겨주는 방식
		RequestDispatcher rd = request.getRequestDispatcher("s07.do");
		//받은 요청과 보낼 응답을 보낸다.
		rd.forward(request, response);
		
	}

}
