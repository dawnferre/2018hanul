import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** 마우스 클릭시 , 옆에 뜨게 해주는 주석
 * Servlet implementation class HelloServlet
 */

//기본형식
@WebServlet("/Basic.do")
public class BasicServlet extends HttpServlet {

	protected void service(HttpServletRequest request, HttpServletResponse response)
												throws ServletException, IOException {
		
		//코드 구현
		
	}//service()

}//class
