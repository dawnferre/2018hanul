

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.study.MemberDAO;

@WebServlet("/s03.do")
public class Slet03 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		
		MemberDAO dao = new MemberDAO();
		int num =dao.deleteMem(id);
		response.setContentType("text/html;charset=utf-8");
		
		PrintWriter pw = response.getWriter();
		
		if(num>0){
			pw.println("<h2>회원삭제가 완료되었습니다.</h2>");
			pw.println("<br><a href ='MemberMain.html'>회원가입화면</a>");
			pw.println("<a href ='s02.do'>전체회원 목록보기</a>");
			
		}else{
			pw.println("<h2>회원삭제실패..코드확인해주세요.</h2>");
			pw.println("<br><a href ='MemberMain.html'>회원가입화면</a>");
			pw.println("<a href ='s02.do'>전체회원 목록보기</a>");
		}

	}

}
