

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/s09.do")
public class Slet09 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
//	s08.do =	response.sendRedirect("s09.do?su1="+su1+"&su2="+su2+"&result="+result); //그대로 넘어감.
		
		//redirect는 매개변수로 보내주기 때문에  기본형으로 받을 수 있음.
		int su1 = Integer.parseInt(request.getParameter("su1"));
		int su2 = Integer.parseInt(request.getParameter("su2"));
		int result = Integer.parseInt(request.getParameter("result"));
		

		response.setContentType("text/html;charset=utf-8");
		PrintWriter pw = response.getWriter();
		pw.println("<h2>넘어온 수 1: "+su1+", 수 2: "+su2+"</h2>");
		pw.println("<h2>두 수 사이의 누적합 :"+result+"</h2>");
	}

}
