import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.study.MemberDTO;

@WebServlet("/s04.do")
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		/*	동적 페이지 로딩: 
				forward :요청객체 request ==> 일반적으로 많이 사용함.
				rewrite : 응답객체 response
		 * 회원 한명의 정보를 다른 페이지(s05.do)로 넘기시오.
		 * 
		 * */
		
		String irum ="한울";
		String id ="hanul";
		String pw ="education";
		
		int age = 33;
		String addr ="광주시 서구 쌍촌동";
		String tel ="062-382-7797";
		
		MemberDTO dto = new MemberDTO(irum, id, pw, age, addr, tel);
		
		//바인딩 객체 만들기
		request.setAttribute("dto", dto);
		
		//forward방식으로 페이지 전환 ==> 동적로딩
		RequestDispatcher rd = request.getRequestDispatcher("s05.do"); //값을 넘겨줄 페이지 호출
		rd.forward(request, response);//페이지가 내부에서 동적으로 변경된다.
		
		
				
	}

}
