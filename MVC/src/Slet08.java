

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.study.TwoHap;
@WebServlet("/s08.do")
public class Slet08 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		//클라이언트 요청을 받는다.
				int su1 = Integer.parseInt(request.getParameter("su1"));
				int su2 = Integer.parseInt(request.getParameter("su2"));
				
				//비즈니스 로직
				int result = new TwoHap().sum(su1, su2);
				
				//프레젠테이션 로직:s09.do넘긴다 >> response sendRedirect
				
				//redirect 방식- get방식처럼 <-> forward- post방식
				//forward = 바인딩객체를 넘겨줘서 
				response.sendRedirect("s09.do?su1="+su1+"&su2="+su2+"&result="+result); //그대로 넘어감.
				
		
	}

}
