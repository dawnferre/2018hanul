
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;

public class Review_Ex_16 {
	public static void main(String[] args) {
		//url : uniform resource location :
		
		try {
			URL url =new URL("https://www.naver.com");
			
			InputStream is = url.openStream();
			//프로젝트가 utf-8이면 url이 깨지지 않음.
			InputStreamReader isr = new InputStreamReader(is, "utf-8");
			
			OutputStream os = new FileOutputStream("UrlHtml_review.txt");
			OutputStreamWriter osw = new OutputStreamWriter(os, "utf-8");
			
			int data;
			while((data = isr.read())!=-1){
//				System.out.println(data);
				osw.write(data);
			}
			osw.flush();
			osw.close();
			isr.close();
			System.out.println("파일이 생성되었습니다.");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}//main
}//class
