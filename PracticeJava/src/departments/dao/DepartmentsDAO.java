package departments.dao;
import java.sql.*;

import departments.dto.DepartmentsDTO;
public class DepartmentsDAO {
	
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	
	String url = "jdbc:oracle:thin:@localhost:1521/xe";
	String user = "hr";
	String password = "hr";
	public DepartmentsDAO(){
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url,user,password);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			e.getMessage();
		}
		
	}
	public DepartmentsDTO[] selectDepInfo(){
		DepartmentsDTO[] dtoArr = new DepartmentsDTO[27];
		String sql ="select * from departments";
		try {
			pst = conn.prepareStatement(sql);
			rs= pst.executeQuery();
			
			int i =0; //�迭�μ�����.
			while(rs.next()){
				DepartmentsDTO dto = new DepartmentsDTO();
				dto.setDepartment_id(rs.getInt("department_id"));
				dto.setDepartment_name(rs.getString("department_name"));
				dto.setMansger_id(rs.getInt("manager_id"));
				dto.setLocation_id(rs.getInt("location_id"));
				
				dtoArr[i++]=dto;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			e.getMessage();
		}finally{
			try {
				rs.close();
				pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				e.getMessage();
			}
		}
		
		return dtoArr;
	}//selectdepinfo()
}//dao()
