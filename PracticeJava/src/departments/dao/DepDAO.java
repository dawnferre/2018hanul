package departments.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import departments.dto.DepDTO;

public class DepDAO {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	//기본생성자에 연결코드를 넣음.
	public DepDAO(){
		String url = "jdbc:oracle:thin:@localhost:1521/xe";
		String user = "hr";
		String password = "hr";
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url, user, password);
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
	}
	//마지막에 닫는 코드
	public void disconn(){
		if(conn!=null | pst!= null | rs!=null){
			try {
				conn.close();
				pst.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//마지막에 닫는 코드 == rs없을떼
	public void disconn2(){
		if(conn!=null | pst!= null ){
			try {
				conn.close();
				pst.close();
//				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//부서 조회하는 코드
	public DepDTO[] selectDep(){
		String sql ="select * from departments";
		DepDTO[] list = new DepDTO[50];
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			int temp = 0;
			while(rs.next()){
				DepDTO dto =  new DepDTO();
				dto.setDepartment_id(rs.getInt("department_id"));
				dto.setDepartment_name(rs.getString("department_name"));
				dto.setManager_id(rs.getInt("manager_id"));
				dto.setLocation_id(rs.getInt("location_id"));
				list[temp++] = dto;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return list;
	}
	//각각의 정보를 조회하는 
	public DepDTO selectEach(int department_id){
		DepDTO dto = new DepDTO();
		String sql ="select * from departments where department_id=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, department_id);
			rs = pst.executeQuery();
			if(rs.next()){//어차피 정보 하나만  꺼내올꺼기 때문에.
//				dto.setDepartment_id(rs.getInt("department_id"));
				dto.setDepartment_name(rs.getString("department_name"));
				dto.setManager_id(rs.getInt("manager_id"));
				dto.setLocation_id(rs.getInt("location_id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dto;
	}
	//부서삽입저장하는 코드
	public boolean insertDep(DepDTO dto){
		boolean in = false;
		String sql ="insert into departments (department_id, department_name, manager_id, location_id) values (?,?,?,?)";
		try {
			pst = conn.prepareStatement(sql);
			
			pst.setInt(1, dto.getDepartment_id());
			pst.setString(2, dto.getDepartment_name());
			pst.setInt(3, dto.getManager_id());
			pst.setInt(4, dto.getLocation_id());
			
			int num= pst.executeUpdate();
			if(num>0){//삽입되었다
				in = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn2();
		}
		return in;
	}
	//부서테이블 업데이트 하는 코드
	public boolean updateDep(DepDTO dto){
		boolean up = false;
		String sql ="update departments set department_name =?, manager_id=?, location_id=? where department_id =?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, dto.getDepartment_name());
			pst.setInt(2,dto.getManager_id());
			pst.setInt(3, dto.getLocation_id());
			pst.setInt(4, dto.getDepartment_id());
			
			int num = pst.executeUpdate();
			if(num>0){
				up = true;
			}else{
				up =false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn2();
		}
		return up;
	}
	public boolean deleteDep(int department_id){
		boolean del = false;
		String sql ="delete from departments where department_id=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, department_id);
			int num = pst.executeUpdate();
			if(num>0){
				del = true;
			}else{
				del = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return del;
	}
}
