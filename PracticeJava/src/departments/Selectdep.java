package departments;

import departments.dao.DepartmentsDAO;
import departments.dto.DepartmentsDTO;

public class Selectdep {
	public static void main(String[] args) {
		DepartmentsDAO dao = new DepartmentsDAO();
		//리턴갑이 배열이기때문에 배열로 넣어줌.
		DepartmentsDTO[] arr = dao.selectDepInfo();
		
	System.out.print("부서 코드\t");
	System.out.print("부서 이름\t");
	System.out.print("매니저 코드\t");
	System.out.println("위치 코드\t");
	
	//향상 for문 for(새변수(배열안의 하나의 데이터타입): 배열변수)
	for(DepartmentsDTO dto : arr){
		System.out.print(dto.getDepartment_id()+"\t");
		System.out.print(dto.getDepartment_name()+"\t");
		System.out.print(dto.getMansger_id()+"\t");
		System.out.println(dto.getLocation_id()+"\t");
	}
	}
}
