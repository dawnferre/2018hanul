package departments;

import java.util.Scanner;

import departments.dao.DepDAO;
import departments.dto.DepDTO;

public class DepNew {
	Scanner scan;
	public DepNew(Scanner scan){
		this.scan = scan;
	}
	public void showInsertDep(){
		DepDAO dao = new DepDAO();
		DepDTO dto = new DepDTO();
		//정보입력칸
		System.out.println("새로 저장될 부서정보를 입력하시오.");
		System.out.println("부서이름 :");
		dto.setDepartment_name(scan.next());
		System.out.println("매니저ID : ");
		dto.setManager_id(scan.nextInt());
		System.out.println("위치코드 : ");
		dto.setLocation_id(scan.nextInt());
		
		if(dao.insertDep(dto)){
			System.out.println("입력이 완료되었습니다.");
		}else{
			System.out.println("입력이 실패하였습니다.");
		}
	}
}
