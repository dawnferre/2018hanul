package departments;

import java.util.Scanner;

import departments.dao.DepDAO;

public class Departments {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		while(true){
			System.out.println("1.부서조회  2.부서삽입  3.부서변경  4.부서삭제 0.프로그램종료");
			System.out.println("원하는 번호를 눌러주세요.");
			
			int ans = scan.nextInt();
			if(ans ==1){
				System.out.println("부서조회");
				new DepartmentList(scan).showDep();
				continue;
			}else if(ans ==2){
//				System.out.println("부서삽입");
				new DepNew(scan).showInsertDep();
				continue;
			}else if(ans ==3){
//				System.out.println("부서변경");
				new DepUpdate(scan).showUpdateDep();
				continue;
//				break;
			}else if(ans ==4){
				System.out.println("삭제할 부서코드를 적어주세요.");
				int department_id = scan.nextInt();
				if(new DepDAO().deleteDep(department_id)){
					System.out.println("삭제가 완료되었습니다.");
				}else{
					System.out.println("삭제가 실패했습니다.");
				}
				continue;
			}else if(ans ==0){
				System.out.println("프로그램을 종료합니다.");
				break;
			}else{
				System.out.println("준비되지않은 번호입니다.");
				continue;
			}
		}
	}
}
