package departments;

import java.util.Scanner;

import departments.dao.DepDAO;
import departments.dto.DepDTO;


public class DepartmentList {
	Scanner scan;
	
	public DepartmentList(Scanner scan){
		this.scan = scan;
	}
	
	public void showDep(){
		DepDAO dao = new DepDAO();
		DepDTO[] list = dao.selectDep();
		
		System.out.println("부서코드\t부서이름\t매니저ID\t위치코드");
		for(DepDTO dto :list){
			if(dto == null){
				continue;
			}else{
				System.out.print(dto.getDepartment_id()+"\t");
				System.out.print(dto.getDepartment_name()+"\t");
				System.out.print(dto.getManager_id()+"\t");
				System.out.println(dto.getLocation_id()+"\t");
			}
		}
	}
}
