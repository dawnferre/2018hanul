package departments;
import java.util.Scanner;

import departments.dao.DepDAO;
import departments.dto.DepDTO;

public class DepUpdate {
	Scanner scan;
	public DepUpdate(Scanner scan){
		this.scan = scan;
	}
	
	public void showUpdateDep(){
		DepDAO dao = new DepDAO();
		DepDTO dto = new DepDTO();
		int dep = 0;
		while(true){
			System.out.println("변경하고자 하는 부서코드를 적어주세요.");
			
			dep = scan.nextInt();
			if(dto ==null){
				System.out.println("부서코드가 없습니다.");
				continue;
			}else{
				dto =dao.selectEach(dep);
				//선택한 정보 확인.
				System.out.println("부서이름\t매니저ID\t위치코드");
				System.out.print(dto.getDepartment_name()+"\t");
				System.out.print(dto.getManager_id()+"\t");
				System.out.println(dto.getLocation_id()+"\t");
				break;
			}
		}
		System.out.println("-------------------------------------------------------");
		System.out.println("부서이름: ");
		String dep_name = scan.next();
		System.out.println("매니저ID: ");
		int manager = scan.nextInt();
		System.out.println("위치코드: ");
		int loca = scan.nextInt();
		dto.setDepartment_id(dep);
		dto.setDepartment_name(dep_name);
		dto.setManager_id(manager);
		dto.setLocation_id(loca);
		
		if(dao.updateDep(dto)){
			System.out.println("정보가 변경되었습니다.");
		}else{
			System.out.println("변경이 실패하였습니다.");
		}
	}
}
