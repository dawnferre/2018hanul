package departments.dto;

public class DepartmentsDTO {
	int department_id;
	String department_name;
	int mansger_id;
	int location_id;
	public DepartmentsDTO(){
		
	}
	
	public int getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(int department_id) {
		this.department_id = department_id;
	}
	public String getDepartment_name() {
		return department_name;
	}
	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}
	public int getMansger_id() {
		return mansger_id;
	}
	public void setMansger_id(int mansger_id) {
		this.mansger_id = mansger_id;
	}
	public int getLocation_id() {
		return location_id;
	}
	public void setLocation_id(int location_id) {
		this.location_id = location_id;
	}
	
	public DepartmentsDTO(int department_id, String department_name, int mansger_id, int location_id) {
		super();
		this.department_id = department_id;
		this.department_name = department_name;
		this.mansger_id = mansger_id;
		this.location_id = location_id;
	}
	
	
}
