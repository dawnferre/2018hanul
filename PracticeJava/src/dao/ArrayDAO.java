package dao;

import java.util.ArrayList;

import dto.ArrayDTO;

public class ArrayDAO {
	
	public ArrayList<ArrayDTO> display( ArrayList<ArrayDTO> arrlist ){
		
		System.out.println("이름\t나이");
		for (int i = 0; i < arrlist.size(); i++) {
			System.out.println(arrlist.get(i).getName()+"\t"+arrlist.get(i).getAge());
		}
		
		return arrlist;
	}
	
}
