package star;

import java.util.Scanner;

public class Star_for3 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		while(true){
			System.out.println("원하는 다이아몬드 별의 갯수를 입력하세요.");
			int num = scan.nextInt();
			if(num % 2 == 0){
				System.out.println("짝수는 별이 너무 안예쁘게 나와요..ㅠㅠ ");
				continue;
			}else{

				for(int i=0; i<num; i++){

					for(int j=0; j<=num-i; j++){

						System.out.print(" ");

					}

					for(int k=0; k<=i*2; k++){

						System.out.print("*");

					}

					System.out.println();

				}//위 for

				for(int i=num-2; i>=0; i--){

					for(int j=i; j<=num; j++){

						System.out.print(" ");

					}

					for(int k=0; k<=i*2; k++){

						System.out.print("*");

					}

					System.out.println();

				}//아래for

				break;
			}//if
		}//endof while
		
	}//main
}//class
