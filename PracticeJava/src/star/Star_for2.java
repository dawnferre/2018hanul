package star;

public class Star_for2 {
	public static void main(String[] args) {
		// 피라미드 모양의 별찍기
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 3 - i; j++) {
				System.out.print(" ");
			}
			for (int j = 0; j < 2 * i + 1; j++) {
				System.out.print("●");
			}
			System.out.println(" ");
		}

		System.out.println();
		
		System.out.println("다이아몬드 별찍기1.");
		
		for (int i = 0; i < 5; i++) {
//			int x = 4;
			for (int j = 0; j < 5 - i; j++) { // 별 앞에 공백을 찍는 for문
				System.out.print(" ");
			}
			for (int k = 0; k <= i; k++) { // 별을 찍는 for문
				if (k == 0) { // *을 홀수개 찍기위해서 하나와
					System.out.print("*");
				} else { // 두개를 나눠서 찍는다.
					System.out.print("**");
				}
			} // close for()
			System.out.println("");
		} // close for()
		
		for(int i =5; i>= 0; i--){
			for(int j =0; j< 5-i; j++){
				System.out.print(" ");
			}
			for(int k =0; k<=i; k++){
				if(k ==0){
					System.out.print("*");
				}else{
					System.out.print("**");
				}
			}
			System.out.println(" ");
		}
		
		System.out.println();
		
		System.out.println("다이아몬드 별찍기2");
		
		for(int i=0; i<5; i++){

			for(int j=0; j<=5-i; j++){

				System.out.print(" ");

			}

			for(int k=0; k<=i*2; k++){

				System.out.print("*");

			}

			System.out.println();

		}

		for(int i=3; i>=0; i--){

			for(int j=i; j<=5; j++){

				System.out.print(" ");

			}

			for(int k=0; k<=i*2; k++){

				System.out.print("*");

			}

			System.out.println();

		}



	}// main
}// class
