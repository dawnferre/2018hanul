package star;

public class Star_for1 {
	public static void main(String[] args) {
		
		for(int i =0; i<6; i++){
			for(int j = 0; j<i; j++){
				System.out.print("*");
			}
			System.out.println("");
		}
		System.out.println();
		for(int i =0; i<5; i++){
			for(int j = 5; j>i; j--){
				System.out.print("*");
			}
			System.out.println("");
		}
		for(int i =0; i<6; i++){ //열은 현재 숫자가 늘어나는 형태
			for(int j =6; j>=0; j--){ //행은 현재 숫자가 줄어드는 형태
				if(i<j){ 
					System.out.print(" ");
				}else{
					System.out.print("*");
				}
			}
			System.out.println("");
		}
		for(int i =6; i>=0; i--){ //열은 현재 숫자가 늘어나는 형태
			for(int j =6; j>=0; j--){ //행은 현재 숫자가 줄어드는 형태
				if(i<j){ 
					System.out.print(" ");
				}else{
					System.out.print("*");
				}
			}
			System.out.println("");
		}
	}//main
}//class
