package brainJava;

public class AppCdInfo extends CdInfo implements Lendable {
	String borrower;
	String checkOutDate;
	byte state;
	
	//일반상속으로 받은 메소드
	public AppCdInfo(String registerNO, String title) {
		super(registerNO, title);
	}

	//인터페이스로 받은 상속의 메소드
	@Override
	public void checkOut(String borrower, String date) {
		if(state != 0){ //상태가 0이 아니면 대출된 상태임.
			return; //checkout 메소드밖으로 나옴.
		}else{
			this.borrower = borrower;
			this.checkOutDate = date;
			this.state =1;
			System.out.println("*"+title+" CD이(가) 대출되었습니다.");
			System.out.println("대출인: "+borrower);
			System.out.println("대출일: "+date+"\n");
		}
	}

	@Override
	public void checkIn() {
		this.borrower =null;
		this.checkOutDate = null;
		this.state = 0; //대출상태를 대출되지 않은 상태로 변경.
		System.out.println("*"+title+"이(가) 반납되었습니다.\n");
	}

}
