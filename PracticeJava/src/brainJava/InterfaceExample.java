package brainJava;

public class InterfaceExample {
	public static void main(String[] args) {
		SeparateVolume obj1 = new SeparateVolume("863?774개", "개미", "베르베르");
		AppCdInfo obj2 = new AppCdInfo("863-774","베르베르");
		obj1.checkOut("김영숙","20060315");
		obj2.checkOut("박희경","20060317");
		obj1.checkIn();
		obj2.checkIn();
	}
}
