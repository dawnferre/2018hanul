package godofjava;

public class StringSample {
	public static void main(String[] args) {
		StringSample ss = new StringSample();
		ss.convert();
		ss.convertUtf16();
		
	}//main
	public void convert(){
		StringSample ss = new StringSample();
		try{
//			String korean ="한글";
			String korean ="자바의 신 최고 !!!";
			byte[] array1 = korean.getBytes();
			ss.printByByteArray(array1);
			String korean2 = new String(array1);
			System.out.println(korean2);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void printByByteArray(byte[] array){
		int cnt =0;
		for(byte data:array){
			System.out.print(data+" ");
			cnt++;
		}
		System.out.println();
		System.out.println("byte의 갯수 : "+cnt);
	}
	
	//utf-16
	public void convertUtf16(){
		StringSample ss = new StringSample();
		try{
			String korean ="자바의 신 최고 !!!";
			byte[] array1 = korean.getBytes("utf-16");
			ss.printByByteArray(array1);
			String korean2 = new String(array1,"utf-16"); 
			//변환시 동일한 케릭터셋으로 변환하여야 한다. 만약 기본으로 설정된 케릭터셋과 맞지 않을경우, 임의로 넣어준다.
			System.out.println(korean2);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}//class
