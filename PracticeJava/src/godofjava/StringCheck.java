package godofjava;

import java.util.Arrays;

public class StringCheck {
	public static void main(String[] args) {
		StringCheck sample = new StringCheck();
		
		String[] addresses = new String[]{"서울시 구로구 신도림동","경기도 성남시 분당구 정자동 개발공장","서울시 구로구 개봉동"};
		
//		sample.checkAddress(addresses);
//		sample.containsAddress(addresses);
//		sample.checkMatch();
//		sample.checkIndexOf();
//		System.out.println();
//		sample.checkLastIndexOf();
//		sample.checkSubString();
//		sample.checkSplit();
//		sample.checkTrim();
		sample.checkReplace();
		sample.checkFormat();
	}//main
	public void checkAddress(String[] address){
		int startCnt =0, endCnt = 0;
		String starText ="서울시";
		String endText ="동";
		
		for(String add:address){
			if(add.startsWith(starText)){
				startCnt++;
			}
			if(add.endsWith(endText)){
				endCnt++;
			}
		}
		System.out.println(starText+startCnt);
		System.out.println(endText+endCnt);
	}
	public void containsAddress(String[] addr){
		int containCount = 0;
		String text ="구로";
		for(String add:addr){
			if(add.contains(text)){
				containCount++;
			}
		}
		System.out.println(text+containCount);
	}
	public void checkMatch(){
		String text ="This is a text";
		String compare1 ="is";
		String cmpare2 = "this";
		System.out.println(text.regionMatches(2, compare1, 0, 1));
		System.out.println(text.regionMatches(5, compare1, 0, 2));
		System.out.println(text.regionMatches(true,0, cmpare2, 0, 4));
	}
	
	public void checkIndexOf(){
		String text = "Java technology is both a programming language and a platform.";
		System.out.println(text.indexOf('a'));
		System.out.println(text.indexOf("a "));
		System.out.println(text.indexOf('a',20));
		System.out.println(text.indexOf("a ", 20));
		System.out.println(text.indexOf('z'));
		
	}
	public void checkLastIndexOf(){
		String text = "Java technology is both a programming language and a platform.";
		System.out.println(text.lastIndexOf('a'));
		System.out.println(text.lastIndexOf("a "));
		System.out.println(text.lastIndexOf('a',20));
		System.out.println(text.lastIndexOf("a ", 20));
		System.out.println(text.lastIndexOf('z'));
		
	}
	public void checkSubString(){
		String text = "Java Technology";
		String technology =text.substring(5);
		System.out.println(technology);
		
	}
	public void checkSplit(){
		String text = "Java technology is both a programming language and a platform.";
		String[] splitArray = text.split(" ");
		for(String temp:splitArray){
			System.out.println(temp);
		}
	}
	public void checkTrim(){
		String[] strings = new String[]{"a","b","   c","d   ","e  f"," "};
		System.out.println(Arrays.toString(strings));
		for(String temp:strings){
			System.out.print(temp+"  ");
			System.out.print(temp.trim()+"  ");
			
		}
	}
	public void checkReplace(){
		String text ="The String class represents character strings";
		System.out.println(text.replace('s','z'));
		System.out.println(text);
		System.out.println(text.replace("tring","trike"));
		System.out.println(text.replaceAll(" ","|"));
		System.out.println(text.replaceFirst(" ","|"));
		
		
	}
	public void checkFormat(){
						//String          정수형                   소숫점있는 숫자, %=%%
		String text ="제 이름은 %s입니다. 지금까지 %d권의 책을 썼고, 하루에 %f %%의 시간을 책을 쓰는데 할애하고 있습니다.";
		String real = String.format(text, "이상민",7,10.5);
		System.out.println(real);
	}
}//class
