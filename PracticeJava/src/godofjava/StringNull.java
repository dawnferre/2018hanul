package godofjava;

public class StringNull {
	public static void main(String[] args) {
		StringNull sn = new StringNull();
//		sn.nullCheck(null);
		System.out.println(sn.nullCheck2(null));
	} //main
	public boolean nullCheck(String text){
		int textLength = text.length();
		System.out.println(textLength);
		
		if(text == null){
			return true;
		}else{ //앞에것이 null이면 무조건 뒤에 코드는 실행되지 않기 때문에.. deadcode
			return false;
		}
	}
	public boolean nullCheck2(String text){
		if(text == null){
			return true;
		}else{
			int textLength = text.length();
			System.out.println(textLength);
			return false;
		}

		
	}
	
}//class
