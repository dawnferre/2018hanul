package godofjava;

public class Stringcompare {
	public static void main(String[] args) {
		Stringcompare sc = new Stringcompare();
//		sc.checkString();
		sc.checkCompare();
	}//main
	public void checkString(){
		String text = "자바의 신";
		System.out.println("text.length : "+text.length());
		System.out.println("text.empty : "+text.isEmpty());
	}
	
	public void checkCompare(){
		String text1="Check value";
		String text2="Check value";
		if(text1 == text2){
			System.out.println("same");
		}else{
			System.out.println("different");
		}
		
		if(text1.equals(text2)){
			System.out.println("same");
		}
		String text3 ="check value";
		if(text1.equalsIgnoreCase(text3)){
			System.out.println("ignoreCase same ");
		}
	}
}//class
