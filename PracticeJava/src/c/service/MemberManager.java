package c.service;

import c.model.MemberDTO;

public interface MemberManager {
	public boolean addMember(MemberDTO dto);
	public boolean removeMember(String name, String phone);
	public boolean updateMember(MemberDTO dto);
}
