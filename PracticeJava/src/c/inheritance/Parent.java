package c.inheritance;

public class Parent {
	public Parent() {
		System.out.println("Parent constructor");
	}
	
	public Parent(String name){
		System.out.println("Parent(String) constructor");
	}
	
	public void printName(){
		System.out.println("printName()- Parent");
	}
}
