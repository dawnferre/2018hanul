package c.inheritance;

public class Cat extends Animal{

	@Override
	public void cry() {
		System.out.println("야옹");
		
	}
	//abstract parent class 에서 상속받는다 해도
	//abstract 만 붙은 메소드를 강제로  받게 되지만, 다른 상속메소드는 내가 써도 되고 안써도 된다.

}
