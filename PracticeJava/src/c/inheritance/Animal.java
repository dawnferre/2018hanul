package c.inheritance;

public abstract class Animal {
	String name;
	String kind;
	int legCount;
	int iq;
	boolean hasWing;
	
	public void move(){
		
	}
	public void eat(){
		
	}
	public abstract void cry();
}
