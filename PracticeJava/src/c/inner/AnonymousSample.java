package c.inner;

import java.util.EventListener;

public class AnonymousSample {
	public static void main(String[] args) {
		AnonymousSample as = new AnonymousSample();
		as.setButtonListener();
	}

	public void setButtonListener() {
		MagicButton mb = new MagicButton();
		MagicButtonListener listener = new MagicButtonListener();
//		mb.setListener(listener);
		mb.onClickProcess();
	}
}

class MagicButtonListener implements EventListener {
	public void onClick() {
		System.out.println("magic button clicked!!");
	}
}
