package c.inner.practice;

public class InputBox {
	public InputBox(){
		
	}
	
	KeyEventListner listener;
	public void setKeyListener(KeyEventListner slistener){
		this.listener = slistener;
	}
	//상수선언
	public static final int KEY_DOWN = 2;
	public static final int KEY_UP = 4;
	
	public void listenerCalled(int eventType){
		if(eventType == KEY_DOWN){
			listener.onKeyDown();
		}else if (eventType == KEY_UP){
			listener.onKeyUp();
		}
	}
}
