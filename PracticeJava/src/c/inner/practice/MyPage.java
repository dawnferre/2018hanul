package c.inner.practice;

public class MyPage {
	InputBox in;
	public static void main(String[] args) {
		MyPage my = new MyPage();
		my.setUI();
		my.pressKey();
	}//main
	
	public void setUI(){
		 in= new InputBox();
		KeyEventListner listener = new KeyEventListner() {
			
			@Override
			public void onKeyUp() {
				System.out.println("KEY UP");
			}
			
			@Override
			public void onKeyDown() {
				System.out.println("KEY DOWN");
				
			}
		};
		in.setKeyListener(listener);
	}
	public void pressKey(){
		in.listenerCalled(in.KEY_DOWN);
		in.listenerCalled(in.KEY_UP);
		
	}
}//class	
