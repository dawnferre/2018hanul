package c.inner.practice;

public interface KeyEventListner {
	public void onKeyDown();
	public void onKeyUp();
}
