package c.inner;


public class MagicButton {
	//생성자
	public MagicButton(){
		
	}
	private EventListener listener;
	public void setListener(EventListener listener){
		this.listener=listener;
	}
	public void onClickProcess(){
		if(listener != null){
			listener.onClick();
		}
	}
}

interface EventListener{
	public void onClick();
}