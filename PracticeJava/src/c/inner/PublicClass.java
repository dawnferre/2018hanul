package c.inner;

public class PublicClass {
	static class StaticNested{
			private int value= 0;
			//value 출력
			public int getValue(){
				return value;
			}
			//입력받은 값으로 value지정
			public void setValue(int value) {
				this.value = value;
			}
		}
	}

class JustNotPublicClass{
 
}
