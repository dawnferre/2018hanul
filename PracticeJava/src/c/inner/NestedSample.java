package c.inner;

import c.inner.PublicClass.StaticNested;

public class NestedSample {
	public static void main(String[] args) {
		NestedSample ns = new NestedSample();
		ns.makeStaticNestedObject();
	}//main
	
	public void makeStaticNestedObject(){
		PublicClass.StaticNested stne = new StaticNested();
		stne.setValue(3);
		System.out.println(stne.getValue());
	}
}//class
