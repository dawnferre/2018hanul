package c.middle;

import java.util.ArrayList;

public class CaculateSalary {
	public static void main(String[] args) {
		CaculateSalary cs = new CaculateSalary();
		cs.calculateSalaries();
	}// main

	public long getSalaryIncrease(Employee employee) {
		long increase = 0;
		switch (employee.getType()) {
		case 1: // owener
			increase = employee.getSalary() + (long) (employee.getSalary() * -0.95);
			break;
		case 2: // manager
			increase = employee.getSalary() + (long) (employee.getSalary() * 0.1);
			break;
		case 3: // designer
			increase = employee.getSalary() + (long) (employee.getSalary() * 0.2);
			break;
		case 4: // architect
			increase = employee.getSalary() + (long) (employee.getSalary() * 0.3);
			break;
		case 5: // developer
			increase = employee.getSalary() + (long) (employee.getSalary() * 1);
			break;

		}
		return increase;
	}

	public long getSalaryIncrease2(Employee employee) {
		long increase = 0;
		if (employee.getType() == 1) {
			increase = employee.getSalary() + (long) (employee.getSalary() * -0.95);
		} else if (employee.getType() == 2) {
			increase = employee.getSalary() + (long) (employee.getSalary() * 0.1);
		} else if (employee.getType() == 3) {
			increase = employee.getSalary() + (long) (employee.getSalary() * 0.2);
		} else if (employee.getType() == 4) {
			increase = employee.getSalary() + (long) (employee.getSalary() * 0.3);
		} else if (employee.getType() == 5) {
			increase = employee.getSalary() + (long) (employee.getSalary() * 1);
		}

		return increase;

	}

	public void calculateSalaries() {
		ArrayList<Employee> emplist = new ArrayList<>();
		emplist.add(new Employee("leeDaeri", 1, 1000000000));
		emplist.add(new Employee("kimManager", 2, 100000000));
		emplist.add(new Employee("WhangDesign", 3, 70000000));
		emplist.add(new Employee("ParkArchi", 4, 80000000));
		emplist.add(new Employee("LeeDevelop", 5, 60000000));

		System.out.println("이름 :연봉인상율에 따른 예상연봉");
		for (int i = 0; i < emplist.size(); i++) {

			//왜 케이스문은 안되지??_break를 어디다 버려놓고..ㅠㅠ
			
//			long increase = getSalaryIncrease2(emplist.get(i));
			long increase2= getSalaryIncrease(emplist.get(i));
			System.out.print(emplist.get(i).getName() + "=\t");
//			System.out.println(increase);
			System.out.println(increase2);

		}
	}
}// class
