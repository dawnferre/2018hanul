package c.exception;

public class ThrowSample {
	public static void main(String[] args){
		ThrowSample th = new ThrowSample();
		th.throwSample(13);
		try {
			th.throwsSample(13);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}//main()
	public void throwSample(int number){
		try{
			if(number>12){
				throw new Exception("number is over than 12.");
			}
			System.out.println("number is "+number);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void throwsSample(int number) throws Exception{
//		try{
		if(number>12){
			throw new Exception("number is over than 12.");
		}
		System.out.println("number is "+number);
//		}catch(Exception e){
//			e.printStackTrace();
//		}
	}
}//class
