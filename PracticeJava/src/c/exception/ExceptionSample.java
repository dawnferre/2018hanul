package c.exception;

public class ExceptionSample {
	public static void main(String[] args) {
		ExceptionSample sam = new ExceptionSample();
//		sam.arrayOutOfBounds();
		sam.arrayOutOfBoundsTryCatch();
	}//main()
	public void arrayOutOfBounds(){
		int[] intArray = new int[5];
		System.out.println(intArray[5]);
	}
	public void arrayOutOfBoundsTryCatch(){
		try{
			int[] intArray = new int[5];
			System.out.println(intArray[5]);
			System.out.println("this code is run.");
			//this code is run. << 실행되지 않는 이유 : 이미 그 위에서 오류발생 바로 catch구문으로 넘어가게 된다.
		}catch(Exception e){
			System.err.println("Exception occured."); //오류를 발생하는 부분에 쓰는 println
		}
		System.out.println("this code must run.");
	}
}//class
