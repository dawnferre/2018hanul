package c.exception;

public class Calculator {
	public static void main(String[] args) {
		Calculator cal = new Calculator();
		try {
			cal.printDivide(1, 2);
			cal.printDivide(1, 0);
		} catch (Exception e) {
			System.out.println("Second value can't be Zero.");
			
		}
	}//main()
	public void printDivide(double d1, double d2) throws Exception{
			double result = d1/d2;
//			try {
				if(d2 ==0){
					throw new Exception("Second value can't be Zero.");
				}
			System.out.println(result);
//		} catch (ArithmeticException e) {
//				System.out.println("Second value can't be Zero.");
//		}
	}
}//class
