package c.exception;

public class CustomException {
	public static void main(String[] args) {
		CustomException cu = new CustomException();
		try {
			cu.throwMyException(13);
		} catch (MyException e) {
			e.printStackTrace();
		}
	}//main()
	public void throwMyException(int num)throws MyException{
		if(num>12){
			throw new MyException("num is over than 12");
		}
	}
}//class
