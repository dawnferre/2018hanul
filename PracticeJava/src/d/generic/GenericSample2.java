package d.generic;

public class GenericSample2 {
	public static void main(String[] args) {
		GenericSample2 sample = new GenericSample2();
		sample.checkGenericDTO();
	}//main

	public void checkGenericDTO() {
		CastingDTO<String> dto1 = new CastingDTO<String>();
		dto1.setObject(new String());
	}
}
