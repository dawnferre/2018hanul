package d.generic.practice;

public class Maxfinder {
	public static void main(String[] args) {
		Maxfinder sample = new Maxfinder();
//		sample.testGetMax();
		sample.testGetMin();
	}//main
	public void testGetMax(){
		System.out.println(getMax(1,2,3));//제네릭으로 순서상관없이 비교가능.
		System.out.println(getMax(2,3,1));
		System.out.println(getMax(3,2,1));
		System.out.println(getMax("a","b","c"));
		System.out.println(getMax("b","c","a"));
		System.out.println(getMax("c","b","a"));
	}
	public void testGetMin(){
		System.out.println(getMin(1,2,3));
		System.out.println(getMin(2,3,1));
		System.out.println(getMin(3,2,1));
		System.out.println(getMin("a","b","c"));
		System.out.println(getMin("b","c","a"));
		System.out.println(getMin("c","b","a"));
	}
			//comparable 클래스를 상속받은 T
	public <T extends Comparable<T>> T getMax(T...a){//매개변수: 범위
		T maxT = a[0]; //범위의 첫번째 값을 제네릭
		for(T tempT:a){ //향상for문을 이용한 값을  출력
			if(tempT.compareTo(maxT)>0){//즉, 최대값
				maxT= tempT; //임시변수저장값을 바꾸어줌.
			}
		}
		return maxT;
	}
	public <T extends Comparable<T>> T getMin(T...a){
		T minT = a[0];
		for(T tempT:a){
			if(tempT.compareTo(minT)<0){
				minT= tempT;
			}
		}
		return minT;
	}
}
