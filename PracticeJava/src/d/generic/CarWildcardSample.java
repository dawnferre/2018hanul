package d.generic;

public class CarWildcardSample {
	public static void main(String[] args) {
		CarWildcardSample sample = new CarWildcardSample();
		sample.callBoundedWildcardMethod();
	}//main

	public void callBoundedWildcardMethod() {
		WildcardGeneric<Car> wildcard = new WildcardGeneric<>();
		wildcard.setWildCard(new Car("Mustang"));//값을 지정.
		boundedWildcardMethod(wildcard);// 값을 넘겨줌.
	}

	public void boundedWildcardMethod(WildcardGeneric<? extends Car> wildcard) {
		Car value = wildcard.getWildCard();//지정한 값을 불러오기.
		System.out.println(value);//값을 출력.
	}
	
}
