package d.generic;

public class Bus extends Car{
	public Bus(String name){
		super(name); //부모클래스에 값을 넘겨주고 있음.
	}
	public String toString(){
		return "Bus name ="+name;
	}
}
