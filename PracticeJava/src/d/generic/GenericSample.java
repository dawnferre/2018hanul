package d.generic;

public class GenericSample {
	public static void main(String[] args) {
		
	}//main
	
	public void checkCatingDTO(){
		CastingDTO dto1 = new CastingDTO();
		dto1.setObject(new String());
		CastingDTO dto2 = new CastingDTO();
		dto2.setObject(new StringBuffer());
		CastingDTO dto3 = new CastingDTO();
		dto3.setObject(new StringBuilder());
		
		String temp1 = (String) dto1.getobject();
		StringBuffer temp2 = (StringBuffer) dto2.getobject();
		StringBuilder temp3 = (StringBuilder) dto3.getobject();
		
		System.out.println(temp1);
		System.out.println(temp2);
		System.out.println(temp3);
	}
}
