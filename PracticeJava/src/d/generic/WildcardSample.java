package d.generic;

public class WildcardSample {
	public static void main(String[] args) {
		WildcardSample sample = new WildcardSample();
		sample.callWildcardMethod();
	}//main

	public void callWildcardMethod() {
		WildcardGeneric<String> wildcard = new WildcardGeneric<>();
		wildcard.setWildCard("A");
		wildcardStringMethod(wildcard);
		
	}

	public void wildcardStringMethod(WildcardGeneric<String> wildcard) {
		String value = wildcard.getWildCard();
		System.out.println(value);
	}
}
