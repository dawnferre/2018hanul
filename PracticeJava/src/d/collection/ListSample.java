package d.collection;

import java.util.ArrayList;


public class ListSample {
	public static void main(String[] args) {
		ListSample ls = new ListSample();
//		ls.checkArrayList1();
//		ls.checkArrayList2();
//		ls.checkArrayList3();
		ls.checkArrayList6();
		
	}//main

	public void checkArrayList1() {
		ArrayList list1 =new ArrayList();
		list1.add(new Object());
		list1.add(new Object());
	}
	public void checkArrayList2() {
		ArrayList<String> list2 =new ArrayList<>();
		list2.add("A");
		list2.add("B");
		list2.add("C");
		list2.add("D");
		list2.add("E");
		
		list2.add(1,"A1");
		
		for(String str : list2){
			System.out.println(str);
		}
		
	}
	public void checkArrayList3() {
		ArrayList<String> list2 =new ArrayList<String>();
		list2.add("A");
		list2.add("B");
		list2.add("C");
		list2.add("D");
		list2.add("E");
		
		list2.add(1,"A1");

		ArrayList<String> list3 = new ArrayList<>();
		list3.add("0 ");
		list3.addAll(list2);
		
		for(String str : list3){
			System.out.println(str);
		}
		
	}
	public void checkArrayList4() {
		ArrayList<String> list4 = new ArrayList<>();
		list4.add("A");
		
		ArrayList<String> list5 = list4;
		list4.add("opps");
		for(String temp : list5){
			System.out.println("list5"+temp);
		}
		
	}
	public void checkArrayList5() {
		ArrayList<String> list = new ArrayList<>();
		list.add("A");
		list.add("b");
		int listSize = list.size();
		for(int loop=0;loop<list.size();loop++){
			System.out.println("System.get("+loop+")="+list.get(loop));
		}
	}
	public void checkArrayList6() {
		ArrayList<String> list = new ArrayList<>();
		list.add("A");
		String[] strList = list.toArray(new String[0]);
		System.out.println(strList[0]);
		//치명적인 문제점 : arraylist 객체의 데이터 크기가 매개변수로 넘어간 배열의 크기보다 크면 모든값이 null로 넘어간다.
		//즉,데이터가 있는 공간만 들어가고, 만약 5개 리스트를 2개의 배열로 받으려고 하면 아예 안들어가고 null로 바뀐다.
	}
}//class
