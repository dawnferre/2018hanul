package d.collection;

import java.util.Stack;

public class StackSample {
	public static void main(String[] args) {
		StackSample ss = new StackSample();
		ss.checkPeek();
		ss.checkPop();
	}//main

	public void checkPeek() {
		Stack<Integer> intStack = new Stack<>();
		for(int i =0; i<5;i++){
			intStack.push(i);
			System.out.println(intStack.peek());
		}
		System.out.println("size="+intStack.size());
	}
	
	public void checkPop(){
		Stack<Integer> intStack = new Stack<>();
		for(int i =0; i<5;i++){
			intStack.push(i);
			System.out.println(intStack.pop());
		}
		System.out.println("size="+intStack.size());
		
	}
}//class
