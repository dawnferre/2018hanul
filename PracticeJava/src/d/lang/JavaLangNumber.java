package d.lang;

public class JavaLangNumber {
	public static void main(String[] args) {
		JavaLangNumber jalang = new JavaLangNumber();
		jalang.numberTypeCheck();
	}

	public void numberTypeCheck() {
		String value ="3";
		String value2 ="5";
		byte byte1 = Byte.parseByte(value);
		byte byte2 = Byte.parseByte(value2);
		System.out.println(byte1+byte2);
		
		Integer int1 = Integer.parseInt(value);
		Integer int2 = Integer.parseInt(value2);
		System.out.println(int1+int2+"7");
		
	}
}
