package d.lang.practice;

public class NumberObject {
	public static void main(String[] args) {
		NumberObject sample = new NumberObject();
		long result =sample.parseLong("rd123");
		System.out.println(result);
//		sample.printOtherBase(1024);
	}
	public long parseLong(String data){
		long result=0;
		try{
			result = Long.parseLong(data);
			System.out.println(result);
		}catch(NumberFormatException e){
			System.out.println(data +"is not a number.");
			result = -1;
		}
		return result;
	}
	
	public void printOtherBase(long value){
		String result1 = Long.toBinaryString(value);
		String result2 = Long.toOctalString(value);
		String result3 = Long.toHexString(value);
		
		System.out.println("original :"+value);
		System.out.println("2진수 :"+result1);
		System.out.println("8진수 :"+result2);
		System.out.println("16진수 :"+result3);
	}
}
