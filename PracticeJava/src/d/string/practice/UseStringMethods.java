package d.string.practice;

public class UseStringMethods {
	public static void main(String[] args) {
		String str ="The String class represents character strings.";
		UseStringMethods usm = new UseStringMethods();
		usm.printWords(str);
		usm.findStirng(str, "string");
		usm.findAnyCaseString(str, "string");
		usm.countChar(str, 's');
		usm.printContainWords(str, "ss");
	}
	public void printWords(String str){
		String[] sp =str.split(" ");
		for(String temp:sp){
			System.out.println(temp);
		}
	}
	public void findStirng(String str, String findstr){
		int num =str.indexOf(findstr);
		System.out.println("string is appreared at "+num);
	}
	public void findAnyCaseString(String str, String findstr){
		int num =str.toLowerCase().indexOf(findstr);
		System.out.println("string is appreared at "+num);
	}
	public void countChar(String str, char c){
		char[] ch =str.toCharArray();
		int cnt =0;
		for(char a:ch){
			if(a =='s'){
				cnt++;
			}
//			System.out.println(a);
		}
		System.out.println("Char 's' count is "+cnt);
	}
	public void printContainWords(String str, String findstr){
		String[] st =str.split(" ");
		int cnt =0;
		for(String temp:st){
//			temp.contains(findstr);
			if(temp.contains(findstr)){
				cnt++;
			}
		}
		if(cnt>0){
			System.out.println("class contains ss");
		}else{
			System.out.println("no");
		}
	}
}
