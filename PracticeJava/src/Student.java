
public class Student {
	String name;
	String address;
	String phone;
	String email;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Student(String name, String address, String phone, String email) {
		super();
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.email = email;
	}

	public boolean equals(Object obj){
		if(this == obj) return true;
		if(obj == null) return false;
		
		if(getClass() !=obj.getClass()) return false;
		
		Student str = (Student)obj;
		
		if(name == null){
			if(str.name != null){ 
				return false;
			}else if(!name.equals(str.name)){
				return false;
			}
		}

		if(email == null){
			if(str.email != null){ 
				return false;
			}else if(!email.equals(str.email)){
				return false;
			}
		}
		
		if(phone == null){
			if(str.phone != null){ 
				return false;
			}else if(!phone.equals(str.phone)){
				return false;
			}
		}
		
		if(address == null){
			if(str.address != null){ 
				return false;
			}else if(!address.equals(str.address)){
				return false;
			}
		}
		
		return true;
	}
}
