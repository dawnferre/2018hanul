package Array;

import java.util.ArrayList;
import java.util.Scanner;

import dao.ArrayDAO;
import dto.ArrayDTO;

public class ArrayMain {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		ArrayDAO dao = new ArrayDAO();
		ArrayList<ArrayDTO> list = null;
		while(true){
			System.out.println("1. 정보 등록  2. 정보 삭제  3. 종료");
			int num = scan.nextInt();
			if(num == 1){
				list = new ArList().insertList(scan);
				dao.display(list);
				continue; //위에 반복할 대상이 없다면, 하위 반복문이 계속 돌게 된다. 위치중요함.
			}else if(num ==2){
				new ArList().deleteList(scan,dao.display(list));
				continue;
			}else if(num ==3){
				break;
			}
		}
	}
}
