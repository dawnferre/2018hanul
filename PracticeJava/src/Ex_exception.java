import java.util.Scanner;

public class Ex_exception {
	public static void main(String[] args) {
				Scanner rd = new Scanner(System.in); //키보드 입력
				int divisor = 0;
				int dividend = 0;
					System.out.print("나뉨수를 입력하시오:");
					dividend = rd.nextInt();
					System.out.print("나눗수를 입력하시오:");
					divisor = rd.nextInt();
				//결과가 나오는 구간에 예외처리를 해줘야한다.	
				try{
				System.out.println(dividend+"를 "+divisor+"로 나누면 몫은 "+dividend/divisor+"입니다.");
				// 상위 예외 처리 _ 모든 예외처리가능
				}catch(Exception e){
					//오류의 원인도 알 수 있음.
					System.out.println("예외발생"+e.getMessage());
				}
	}//main()
}//class
