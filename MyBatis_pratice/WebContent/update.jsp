<%@page import="com.yeongju.study.TestMemberDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
 	request.setCharacterEncoding("utf-8");
 %>
 <jsp:useBean id="dto" class="com.yeongju.study.TestMemberDTO">
	 <jsp:setProperty property="*" name="dto"/>
 </jsp:useBean>

<%
	TestMemberDAO dao = new TestMemberDAO();
	
	int succ =dao.update(dto);
	
	if(succ>0){
		out.println("<script>alert('업데이트완료!');location.href='list.jsp';</script>");
	}else{
		out.println("<script>alert('업데이트실패!..코드확인');location.href='list.jsp';</script>");
	}
%>
