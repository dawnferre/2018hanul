<%@page import="com.yeongju.study.TestMemberDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("utf-8");
	String id = request.getParameter("id");
	TestMemberDAO dao = new TestMemberDAO();
	int succ =dao.delete(id);
	
	if(succ>0){
		out.println("<script>alert('삭제성공');location.href='list.jsp';</script>");
	}else{
		out.println("<script>alert('삭제실패..코드확인!');location.href='list.jsp';</script>");
	}
%>
