<%@page import="com.yeongju.study.TestMemberDTO"%>
<%@page import="java.util.List"%>
<%@page import="com.yeongju.study.TestMemberDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
      <!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	request.setCharacterEncoding("utf-8");
	TestMemberDAO dao = new TestMemberDAO();
	List<TestMemberDTO> list =dao.getAllList();
	pageContext.setAttribute("list",list);
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>회원목록</title>
<style>
	*{
		margin: 0 auto;
	}
</style>
<script>
	function fnDeleteCheck(id){
		
		if(confirm("정말로 삭제하시겠습니까?")){
			location.href="delete.jsp?id="+id;	
		}
	}
	function fnUpdateCheck(id){
			location.href="list_personal.jsp?id="+id;	
	}
</script>
</head>
<body>
<table border ="1">
	<caption><h4>회원목록</h4></caption>
	<tr>
		<th>이름</th>
		<th>아이디</th>
		<th>비밀번호</th>
		<th>나이</th>
		<th>주소</th>
		<th>전화번호</th>
		<th>이메일</th>
		<th>업데이트</th>
		<th>삭제</th>
	</tr>
	<c:forEach  var="j" items="${list}">
			<tr>
				<td>${j.name}</td>
				<td>${j.id}</td>
				<td>${j.pw}</td>
				<td>${j.age}</td>
				<td>${j.addr}</td>
				<td>${j.tel}</td>
				<td>${j.email}</td>
				<td><input type="button" value="업데이트" onclick="fnUpdateCheck('${j.id}')"></td>
				<td><input type="button" value="삭제" onclick="fnDeleteCheck('${j.id}')"></td>
			<tr>
	</c:forEach>
	<tr>
		<td colspan="9">
			<input type="button" value="회원가입" onclick="location.href='TestMain.html'">
		</td>
	</tr>
</table>
</body>
</html>