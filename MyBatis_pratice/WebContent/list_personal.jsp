<%@page import="com.yeongju.study.TestMemberDTO"%>
<%@page import="com.yeongju.study.TestMemberDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("utf-8");
	String id = request.getParameter("id");
	TestMemberDAO dao = new TestMemberDAO();
	TestMemberDTO dto =dao.selectPersonal(id);
	
	pageContext.setAttribute("dto",dto);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
*{
	margin: 0 auto;
}
</style>
<script>
	function fnUpdateCheck(){
		if(confirm("업데이트 하시겠습니까?")){
			return true;
		}else{
			return false;
		}
	}
</script>
</head>
<body>
<form action="update.jsp" method="post" onsubmit="return fnUpdateCheck()">
	<table>
		<caption><h4>업데이트화면</h4></caption>
		<tr>
			<th>이름</th>
			<td><input type="text" name="name" value="${dto.name}"></td>
		</tr>
		<tr>
			<th>ID</th>
			<td><input type="text" name="id" value="${dto.id}" readonly="readonly" style="background-color:gray;"></td>
		</tr>
		<tr>
			<th>PW</th>
			<td><input type="text" name="pw" value="${dto.pw}"></td>
		</tr>
		<tr>
			<th>나이</th>
			<td><input type="text" name="age" value="${dto.age}"></td>
		</tr>
		<tr>
			<th>주소</th>
			<td><input type="text" name="addr" value="${dto.addr}"></td>
		</tr>
		<tr>
			<th>전화번호</th>
			<td><input type="text" name="tel" value="${dto.tel}"></td>
		</tr>
		<tr>
			<th>EMAIL</th>
			<td>
				<input type="text" name="email" value="${dto.email}">
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<input type="submit" value="업데이트">
				<input type="reset" value="취소하기">
				<input type="button" value="회원목록">
			</td>
		</tr>
	</table>
</form>
</body>
</html>