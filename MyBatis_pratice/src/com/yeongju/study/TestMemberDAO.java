package com.yeongju.study;

import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class TestMemberDAO {
	//전역변수 설정 == mybatis설정했는데도 jar 안먹히네.
		private static SqlSessionFactory sqlMapper;
		static{
			String resource ="com/yeongju/mybatis/SqlMapConfig.xml";
			try {	//** try-catch를 사용하지 않으면 오류난다.
				InputStream inputStream = Resources.getResourceAsStream(resource);
				sqlMapper = new SqlSessionFactoryBuilder().build(inputStream); 
				
			} catch (Exception e) {
				e.getStackTrace();
			}
			
		}//static:초기화블럭, 무조건 메모리에 할당해서 쓸 때 사용함.
		
	public int insert(TestMemberDTO dto){
		SqlSession session =sqlMapper.openSession();
		int succ = session.insert("insert", dto);
		
		session.commit();
	
		return succ;
	}
	
	public List<TestMemberDTO> getAllList(){
		SqlSession session =sqlMapper.openSession();
		List<TestMemberDTO> list =session.selectList("select");
		
		return list;
	}
	public int delete(String id){
		SqlSession session =sqlMapper.openSession();
		int succ =session.delete("delete",id);
		session.commit(); //테이블에 변경사항 저장( 반드시!!!)
		
		return succ;
	}
	
	public int update(TestMemberDTO dto){
		SqlSession session =sqlMapper.openSession();
		int succ =session.update("update",dto);
		session.commit(); //테이블에 변경사항 저장( 반드시!!!)
		
		return succ;
	}
	
	public TestMemberDTO selectPersonal(String id){
		SqlSession session =sqlMapper.openSession();
		TestMemberDTO dto =session.selectOne("select2", id);
		
		return dto;
		
	}
}
