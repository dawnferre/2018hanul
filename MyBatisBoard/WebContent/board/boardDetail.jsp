<%@page import="com.hanul.dto.BoardDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <!--jstl- fn설정 -->
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%
	BoardDTO dto =(BoardDTO)request.getAttribute("dto");
	// 줄바꿈 
	//http://all-record.tistory.com/148 : 처리해주는 방법
	//textarea에서 바꿔주는 부분을  html태그로 바꿔줘야함.
	pageContext.setAttribute("br", "<br/>");
	pageContext.setAttribute("cn", "\n");

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>상세보기 </title>
<script type="text/javascript">
	function fnCheck(pwd,num){
		var pwd_insert = document.form.b_pwd.value; //입력되는 값의 크기 및 길이
		if(pwd_insert.length==0){
			alert("비밀번호를 입력해주세요.");
		}else{
			if(pwd_insert == pwd){
				if(confirm("정말 삭제하시겠습니까?")){
					location.href="boardDelete.do?b_num="+num;
				}else{
					document.form.b_pwd.value="";
					document.form.b_pwd.focus();
				}
			}else{
				alert("비밀번호가 일치하지 않습니다.");
				document.form.b_pwd.value="";
				document.form.b_pwd.focus();
			}
		}
	}
	function fnUpdateCheck(pwd,num){
		var pwd_insert = document.form.b_pwd.value; //입력되는 값의 크기 및 길이
		if(pwd_insert.length==0){
			alert("비밀번호를 입력해주세요.");
		}else{
			if(pwd_insert == pwd){
					location.href="boardUpdateForm.do?b_num="+num;
			}else{
				alert("비밀번호가 일치하지 않습니다.");
				document.form.b_pwd.value="";
				document.form.b_pwd.focus();
			}
		}
	}
</script>
</head>
<body>
<p align="center">[게시물상세보기]</p>
<form name="form">
<table align="center" border="2" width="50%">
	<tr>
		<td width="25%">작성자 :</td>
		<td width="25%">${dto.b_writer}</td>
		<td width="25%">조회수:</td>
		<td width="25%">${dto.b_readcount}</td>
	</tr>
	<tr>
		<td>제목</td>
		<td colspan="3">${dto.b_subject}</td>
	</tr>
	<tr>
		<td>내용</td>
		<!-- jstl을 이용해서 줄바꿈 처리 -->
		<td colspan="3">${fn:replace(dto.b_content,cn,br)}</td>
	</tr>
	<tr>
		<td>비밀번호</td>
		<td colspan="3">
			<input type="password" name="b_pwd" id="b_pwd">
			<input type="button" value="수정" onclick="fnUpdateCheck('${dto.b_pwd}','${dto.b_num}');">
			<input type="button" value="삭제" onclick="fnCheck('${dto.b_pwd}','${dto.b_num}');">
			<input type="button" value="돌아가기" onclick="location.href='boardList.do'">
		</td>
	</tr>
</table>
</form>
</body>
</html>