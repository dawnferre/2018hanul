<%@page import="com.hanul.dto.BoardDTO"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
    	List<BoardDTO> list =(List<BoardDTO>)request.getAttribute("list");
    
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>검색결과</title>
<style>
	a{
		text-decoration: none;
		color: black;
	}
	a:hover{
		color: blue;
		cursor: pointer;
	}
</style>
</head>
<body>
	<p align="center">검색하신 결과</p>
	<p align="center">※글보기는 제목을 눌러주시면 됩니다.</p>
<form action="boardSearch.do" method="post">
	<table align="center" border="2" width="50%">
		<tr>
			<th>게시물번호(임의)</th>
<!-- 			<th>게시물번호(실제)</th> -->
			<th>제목</th>
<!-- 			<th>내용</th> -->
			<th>작성자</th>
			<th>날짜</th>
			<th>조회수</th>
		</tr>
		<%if(list.size()== 0){%>
			<tr align="center">
				<td colspan="6">※검색된  글 목록이 없습니다.</td>
			</tr>
		<% }else{
				for(int i = 0; i<list.size();i++){
		%>
			<tr>
				<td><%=list.size()-i%></td>
<%-- 				<td><%=list.get(i).getB_num() %></td> --%>
				<td><a href="boardDetail.do?b_num=<%=list.get(i).getB_num()%>"><%=list.get(i).getB_subject() %></a></td>
<%-- 				<td><%=list.get(i).getB_content() %></td> --%>
				<td><%=list.get(i).getB_writer()%></td>
				<td><%=list.get(i).getB_date()%></td>
				<td><%=list.get(i).getB_readcount()%></td>
			</tr>
			<% }%>
		<% }%>
		<tr align="center">
		
			<td colspan="6">
					<select name="part">
						<option value="b_subject">제목</option>
						<option value="b_content">내용</option>
						<option value="b_writer">작성자</option>
					</select>
				<input type="text" name="search" required ="required" placeholder="검색조건을 입력하세요." >
				<input type="submit"  value ="검색하기">
				<input type="button" value="전체목록으로 돌아가기" onclick="location.href='boardList.do'">
			</td>
		</tr>
	</table>
</form>
</body>
</html>