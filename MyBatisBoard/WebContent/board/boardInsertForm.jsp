<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>글쓰기</title>
</head>
<body>
<p align="center">[글쓰기화면]</p>
<form action="boardInsert.do" method="post">

<table align="center" border ="2">
	<tr>
		<th>제목</th>
		<td><input type="text" name ="b_subject"></td>
	</tr>
	<tr>
		<th>작성자</th>
		<td><input type="text" name ="b_writer"></td>
	</tr>
	<tr>
		<th>내용</th>
		<td><textarea rows="10" cols="50" name="b_content"></textarea> </td>
	</tr>
	<tr>
		<th>비밀번호</th>
		<td><input type="password" name ="b_pwd"></td>
	</tr>
	<tr>
	<td colspan="2" align="right">
		<input type="submit" value="저장하기">
		<input type="reset" value="취소하기">
		<input type="button" value="목록으로돌아가기" onclick="location.href='boardList.do'">
	</td>
	</tr>
</table>
</form>
</body>
</html>