<%@page import="com.hanul.dto.BoardDTO"%>
<%@page import="com.hanul.dao.BoardDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <!--jstl- fn설정 -->
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%
	String b_num = request.getParameter("b_num");    
	BoardDAO dao = new BoardDAO();
	BoardDTO dto = dao.getEachList(Integer.parseInt(b_num));
	pageContext.setAttribute("dto",dto);
	
	pageContext.setAttribute("br", "<br/>");
	pageContext.setAttribute("cn", "\n");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
	hello{
	font-size: small;
	}
</style>
</head>
<body>
<p align="center">[게시물업데이트]</p>
<form action="boardUpdate.do" method="post">
	<input type="hidden" name =b_writer value="${dto.b_writer}">
	<input type="hidden" name =b_num value="${dto.b_num}">
<table align="center" border="2" width="50%">
	<tr>
		<td width="25%">작성자 :</td>
		<td width="25%">${dto.b_writer}</td>
		<td width="25%">조회수:</td>
		<td width="25%">${dto.b_readcount}</td>
	</tr>
	<tr>
		<td>제목</td>
		<td colspan="3"><input type="text" value="${dto.b_subject}" name="b_subject"></td>
	</tr>
	<tr>
		<td>내용</td>
		<!-- jstl을 이용해서 줄바꿈 처리 -->
		<td colspan="3"><textarea name="b_content" rows="10" cols="50">${dto.b_content}</textarea></td>
	</tr>
	<tr>
		<td>수정할 비밀번호</td>
		<!-- jstl을 이용해서 줄바꿈 처리 -->
		<td colspan="3">
		<input type="password" value="${dto.b_pwd}" name="b_pwd">
			<br><hello>※수정하지 않으시면 기존 비밀번호 그대로 저장됩니다.</hello>
		</td>
		
	</tr>
	<tr align="right">
		<td colspan="4">
			
			<input type="submit" value="수정">
			<input type="button" value="돌아가기" onclick="location.href='boardDetail.do?b_num=${dto.b_num}'">
		</td>
	</tr>
</table>
</form>
</body>
</html>