package com.hanul.dao;

import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.hanul.dto.BoardDTO;
import com.hanul.dto.BoardSearchDTO;

public class BoardDAO {
	//sqlSessionFactory  ==> sqlMapper
	//전역변수 설정  - 밖에서 mapper를 사용하기 위해서
		static SqlSessionFactory sqlMapper;
		static{
			String resource ="com/hanul/mybatis/SqlMapConfig.xml";
			try {	//** try-catch를 사용하지 않으면 오류난다.
				//Resources: MyBatis(구, iBatis)==> jar설정했기 때문에 import
				InputStream inputStream = Resources.getResourceAsStream(resource);
				sqlMapper = new SqlSessionFactoryBuilder().build(inputStream); 
				
			} catch (Exception e) {
				System.out.println("SqlSessionFactory오류");
				e.getStackTrace();
			}
			
		}//static:초기화블럭, 무조건 메모리에 할당해서 쓸 때 사용함.
		
		public List<BoardDTO> getAllList() {
			SqlSession session = sqlMapper.openSession();
			
			List<BoardDTO> list = session.selectList("select");
			
			return list;
			
		}

		public int insertInfo(BoardDTO dto) {
			SqlSession session = sqlMapper.openSession();
			int succ = session.insert("insert", dto);
			session.commit();
			//alert창을 사용하고자 할때는 succ를 return해야한다.
			//사용하지 않을 경우에는 succ를 return할 필요가 없다(succ가 필요없다)
			return succ;
		}

		public BoardDTO getEachList(int b_num) {
			SqlSession session = sqlMapper.openSession();
			BoardDTO dto = session.selectOne("getEachList",b_num);
			
			
			return dto;
		}
		
		//조회수증가
		public void readCountUpdate(int b_num){
			SqlSession session = sqlMapper.openSession();
			session.update("reCountUpdate",b_num);
			session.commit();
			
		}

		public void boardDelete(int b_num) {
			SqlSession session = sqlMapper.openSession();
			session.update("delete",b_num);
			session.commit();
			
		}


		public int updateAll(BoardDTO dto) {
			SqlSession session = sqlMapper.openSession();
			int succ =session.update("updateAll",dto);
			session.commit();
			return succ;
		}

		public List<BoardDTO> searchByPart(BoardSearchDTO dto) {
			SqlSession session = sqlMapper.openSession();
			List<BoardDTO> list =session.selectList("searchByPart",dto);
			session.commit();
			return list;
		}

}
