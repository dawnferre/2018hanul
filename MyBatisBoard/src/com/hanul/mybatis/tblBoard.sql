create table tblboard(
	--게시판글번호
	b_num number primary key not null,
	--게시판제목
	b_subject varchar2(50),
	--작성글 비밀번호
	b_pwd varchar2(20),
	-- 작성내용
	b_content varchar2(2000),
	--작성자
	b_writer varchar2(20),
	--작성일
	b_date varchar2(20),
	--접속수 
	b_readcount number
);
--모든 레코드 검색
select * from tblBoard;

--자동증가값 설정(b_num  ==> b_num_seq) default: 1씩 증가
create sequence b_num_seq start with 1;

--자동증가값 삭제(해제)
drop sequence b_num_seq;


--임의의 레코드 삽입
insert into TBLBOARD values(b_num_seq.nextval,'subject','pwd','content','writer',sysdate,0);

--모든 레코드 삭제
delete from TBLBOARD;

--sequence의 문제점
--1. delete all해도 sequence는 reset되지 않는다(처음 삭제된 글포함해서 무조건)
--2. 20개단위로 숫자가 미리 정렬 (미리가지고와서 숫자를 내줌) - 20개를 채우지 않고 프로그램종료를 하면 그뒤의 20개 숫자를 씀.
--( 1~10까지 쓰면 ==>그담날 21,22,23...으로 새 20개단위의 시퀀스를 부여함 
--★★ client에게는 연속된 번호를 보여줘야 하기 때문에  다른 시퀀스 

--테이블 삭제
drop table tblBoard;
