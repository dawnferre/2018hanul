package com.hanul.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.action.Action;
import com.hanul.action.ActionForward;
import com.hanul.action.BoardDeleteAction;
import com.hanul.action.BoardDetailAction;
import com.hanul.action.BoardInsertAction;
import com.hanul.action.BoardListAction;
import com.hanul.action.BoardSearchAction;
import com.hanul.action.BoardUpdateAction;

@WebServlet("/BoardFrontController.do")
public class BoardFrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) 
												throws ServletException, IOException {
		//1.client가 어떤 요청을 했는지 파악.
		request.setCharacterEncoding("utf-8");
		
		//*.do의 값을 추출하는 것.
		String url =request.getRequestURI(); //url-pattern 값: /MybatisBoard/*.do
		String ctx = request.getContextPath(); //Context root 값 : /MybatisBoard
		String comman = url.substring(ctx.length()); //실제요청할 페이지 값: /*.do
		
//		System.out.println(url);
//		System.out.println(ctx);
//		System.out.println(comman);
		
		//2.클라이언트 요청과 실제 처리할 Action Class연결 : Handler Mapping
		Action action = null;
		ActionForward forward = null;
		
		if(comman.equals("/boardList.do")){
			action =new BoardListAction(); //다형성 , 부모쪽으로 객체를 생성함.
			forward = action.execute(request, response); //controller의  request, response를 받아옴.
		}else if(comman.equals("/insertForm.do")){
			//화면만띄워줄때  _ DB접속등이 필요없기때문에
			forward = new ActionForward();
			forward.setPath("board/boardInsertForm.jsp");
			forward.setRedirect(false); //true는 원 주소 (.jsp)가 찍힘.
		}else if(comman.equals("/boardInsert.do")){
			action = new BoardInsertAction(); 
			forward = action.execute(request, response); //controller의  request, response를 받아옴.
		}else if(comman.equals("/boardDetail.do")){
			action = new BoardDetailAction();
			forward = action.execute(request, response); //controller의  request, response를 받아옴.
		}else if(comman.equals("/boardDelete.do")){
			action = new BoardDeleteAction();
			forward = action.execute(request, response); //controller의  request, response를 받아옴.
		}else if(comman.equals("/boardUpdateForm.do")){
			//화면만띄워줄때  _ DB접속등이 필요없기때문에
			forward = new ActionForward();
			forward.setPath("board/boardUpdateForm.jsp");
			forward.setRedirect(false); //true는 원 주소 (.jsp)가 찍힘.
		}else if(comman.equals("/boardUpdate.do")){
			action = new BoardUpdateAction();
			forward = action.execute(request, response); //controller의  request, response를 받아옴.
		}else if(comman.equals("/boardSearch.do")){
			action = new BoardSearchAction();
			forward = action.execute(request, response); //controller의  request, response를 받아옴.
		}
		
		
		//3.페이지전환 ==> sendRedirect, forward 
		if(forward != null){
			if(forward.isRedirect()){		//true : sendRedirect 
				response.sendRedirect(forward.getPath());
			}else{							//false : forward
				RequestDispatcher rd = request.getRequestDispatcher(forward.getPath());
				rd.forward(request, response);
			}
		}
	}

}
