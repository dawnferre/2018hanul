package com.hanul.dto;

import java.io.Serializable;

public class BoardSearchDTO implements Serializable{
	String part;
	String search;
	
	public BoardSearchDTO() {
	}

	public BoardSearchDTO(String part, String search) {
		super();
		this.part = part;
		this.search = search;
	}

	public String getPart() {
		return part;
	}

	public void setPart(String part) {
		this.part = part;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}
	
	

}
