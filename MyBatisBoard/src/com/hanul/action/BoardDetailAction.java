package com.hanul.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.dao.BoardDAO;
import com.hanul.dto.BoardDTO;

public class BoardDetailAction implements Action{

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");
		int b_num =Integer.parseInt(request.getParameter("b_num"));
		BoardDAO dao = new BoardDAO();
		//조회수를 올리고
		dao.readCountUpdate(b_num);
		//조회수가 올라가고나서 글검색을 해야 조회수가 정확하게 나옴.
		BoardDTO dto =dao.getEachList(b_num);
		
		request.setAttribute("dto", dto);
		
		ActionForward forward = new ActionForward();
		forward.setPath("board/boardDetail.jsp"); //jsp는 board폴더로 따로 넘어가게끔 설계했기 때문에 webcontent자체내에 파일이 아니라면 경로명 지정해줌.
		forward.setRedirect(false);
		
		return forward;
	}

}
