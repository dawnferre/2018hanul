package com.hanul.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.dao.BoardDAO;
import com.hanul.dto.BoardDTO;
import com.hanul.dto.BoardSearchDTO;

public class BoardSearchAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");
		String part = request.getParameter("part");
		String search = request.getParameter("search");
		
		BoardSearchDTO dto = new BoardSearchDTO(part,"%"+search+"%");
		BoardDAO dao = new BoardDAO();
		List<BoardDTO> list =dao.searchByPart(dto);
		
		//넘겨주기 
		request.setAttribute("list", list);
		
		ActionForward forward = new ActionForward();
		forward.setPath("board/boardSearch.jsp"); //jsp는 board폴더로 따로 넘어가게끔 설계했기 때문에 webcontent자체내에 파일이 아니라면 경로명 지정해줌.
		forward.setRedirect(false);
		
		return forward;
	}

}
