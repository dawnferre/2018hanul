package com.hanul.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.dao.BoardDAO;
import com.hanul.dto.BoardDTO;

public class BoardListAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		//Dao와 연동하여 게시판 전체목록을 가져오는 작업을 수행함.
		
		BoardDAO dao = new BoardDAO();
		List<BoardDTO> list =dao.getAllList();
		request.setAttribute("list", list);
		
		//boardListAction.java작업을 마무리 했다 ==> boardfrontController -==> ActionForward
		// 1. viewpage(path) :boardList.jsp
		// 2. 페이지 전환방식 ( idRedirect) : true(sendRedirect), false
		
		ActionForward forward = new ActionForward();
		forward.setPath("board/boardList.jsp"); //jsp는 board폴더로 따로 넘어가게끔 설계했기 때문에 webcontent자체내에 파일이 아니라면 경로명 지정해줌.
		forward.setRedirect(false);
		
		return forward;
	}

}
