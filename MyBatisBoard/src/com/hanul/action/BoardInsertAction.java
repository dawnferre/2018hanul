package com.hanul.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.dao.BoardDAO;
import com.hanul.dto.BoardDTO;

public class BoardInsertAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String b_writer = request.getParameter("b_writer");
		String b_subject = request.getParameter("b_subject");
		String b_content = request.getParameter("b_content");
		String b_pwd = request.getParameter("b_pwd");
		
		BoardDTO dto = new BoardDTO(b_subject, b_pwd, b_content, b_writer);
		//alert 사용할 경우, succ를 return받는다.
		//response객체에서 응답을 해준다.: printWriter
		response.setContentType("text/html;charset=utf-8");
		
		
		PrintWriter out = response.getWriter();

		BoardDAO dao = new BoardDAO();
		int succ =dao.insertInfo(dto);
		
		if(succ>0){
			out.println("<script>alert('게시글이 등록되었습니다.'); location.href='boardList.do';</script>");
		}else{
			out.println("<script>alert('게시글이 등록이 실패하였습니다.'); location.href='boardList.do';</script>");
		}
		return null;
		
		//alert 창을 사용하지 않을 경우에는 ActionForward 객체를 생성해서 return해야된다.
//		ActionForward forward = new ActionForward();
//		forward.setPath("boardList.do");
//		forward.setRedirect(true);
//		return forward;
	}

}
