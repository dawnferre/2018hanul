package com.hanul.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Action  {
		//다형성을 이용한 방법 
	
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response) 
			throws ServletException,IOException; 
}
