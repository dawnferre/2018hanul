
public class BookMain05 {
	public static void main(String[] args) {
		//책 2권의 정보를 저장할 객체배열을 선언
		BookDto[] b = new BookDto[3];
		
		//bookdto 의 생성자메서드를 이용하여 초기화 ==>내용 (값) 입력
		 b[0]= new BookDto("JAVA", 27000, "김윤명", "한빛미디어");
		 b[1]= new BookDto("ANDROID", 40000, "정재곤", "이지스퍼블리싱");
		 b[2]= new BookDto("JSP", 37000, "박곤", "시나공");
		 
		 System.out.print("제목"+"    ");
		 System.out.print("가격"+"    ");
		 System.out.print("저자"+"    ");
		 System.out.println("출판사"+"    ");
		 //객체배열의 내용을 출력(for)
		 for (int i = 0; i < b.length; i++) {
			System.out.print(b[i].getTitle()+" ");
			System.out.print(b[i].getPrice()+" ");
			System.out.print(b[i].getAuth()+" ");
			System.out.println(b[i].getPub()+" ");
			System.out.println("=============");
		}
		 
		 //가격을 기준으로 오름차순 정렬
		 BookDto temp; //dto가 하나로 묶여있기 때문에 변수에 상관없이 dto변수를 생성해 임시로 제공한다.
		for (int i = 0; i < b.length; i++) {
			for (int j = i+1; j < b.length; j++) {
				if(b[i].getPrice() >b[j].getPrice()){
					temp = b[i];//오름차순
					b[i] = b[j];
					b[j] = temp;
				}
			}
		}
		//오름차순 정렬된 것을 출력하는 방식.
		for (int i = 0; i < b.length; i++) {
			System.out.print(b[i].getTitle()+" ");
			System.out.print(b[i].getPrice()+" ");
			System.out.print(b[i].getAuth()+" ");
			System.out.println(b[i].getPub()+" ");
		}
		
	}//main()
}//ckass
