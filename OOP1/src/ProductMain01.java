public class ProductMain01 {
	public static void main(String[] args) {
		Product pro = new Product();//객체 생성.- 객체가 사용할 메모리를 new 연산자로 할당받는다.
		
		pro.num=1;
		pro.name ="컴퓨터";
		
		System.out.println(pro.num);
		System.out.println(pro.name);

		//기존값 삭제됨 (실행명령이후에)_ 다음값(즉, 마지막에 넣는 값)이 남아있게됨.
		pro.num =2;
		pro.name="노트북";
		System.out.println(pro.num);
		System.out.println(pro.name);
		
		//새로운 객체생성.
		Product pro2 = new Product();
		
		pro2.num =2;
		pro2.name="노트북";
		System.out.println(pro2.num);
		System.out.println(pro2.name);
		
		Product pro3 = new Product();
		
		pro3.num =3;
		pro3.name="태블릿";
		System.out.println(pro3.num);
		System.out.println(pro3.name);
		
		
	}//main()
}//class
