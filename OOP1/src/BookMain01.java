public class BookMain01 {
	public static void main(String[] args) {
		Book bo = new Book(); //b1 : 인스턴스 변수, 포인터변수, 레퍼런스변수, 참조변수
		bo.title="JAVA";
		bo.price=27000;
		bo.auth ="김윤명";
		bo.pub="한빛미디어";
		

		Book bo2 = new Book();
		bo2.title="Android";
		bo2.price=40000;
		bo2.auth ="정재곤";
		bo2.pub="이지스퍼블리싱";
		

		bo.print();
		System.out.println("===============");
		bo2.print();
	}//main()
}//class
