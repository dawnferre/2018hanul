import java.util.Scanner;

public class BookMain04 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		for(int i =0; i<2; i++){
			Book bo = new Book();
			System.out.print("제목을 입력해주세요.");
			bo.title = scan.nextLine();
			System.out.print("가격을 입력해주세요.");
			bo.price = Integer.parseInt(scan.nextLine());
			System.out.print("저자를 입력해주세요.");
			bo.auth = scan.nextLine();
			System.out.print("출판사를 입력해주세요.");
			bo.pub = scan.nextLine();
		
			bo.print();
		}
		scan.close();
	}//main()
}//ckass
