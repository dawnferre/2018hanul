import java.util.Scanner;

public class BookMain02 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		Book bo = new Book();
		Book bo2 = new Book();
		
		System.out.print("제목을 입력해주세요.");
		bo.title = scan.nextLine();
		System.out.print("가격을 입력해주세요.");
		bo.price = Integer.parseInt(scan.nextLine());
		System.out.print("저자를 입력해주세요.");
		bo.auth = scan.nextLine();
		System.out.print("출판사를 입력해주세요.");
		bo.pub = scan.nextLine();

		
		System.out.print("제목을 입력해주세요.");
		bo2.title = scan.nextLine();
		System.out.print("가격을 입력해주세요.");
		bo2.price = Integer.parseInt(scan.nextLine());
		System.out.print("저자를 입력해주세요.");
		bo2.auth = scan.nextLine();
		System.out.print("출판사를 입력해주세요.");
		bo2.pub = scan.nextLine();
		
		scan.close();
		bo.print();
		bo2.print();
		
	}//main()
}//ckass
