public class PersonDAO_tea {
//	- 출력메소드: display()
//	- 나이의 오름차순 정렬 메소드 정의 : ageAscSort()
	//배열만 지정해주는 메소드
	//insertArray에서 직접 배열을 받아 오름차순으로 정렬
	public PersonDTO[] ageAscSort(PersonDTO[] dto){
		PersonDTO temp;
		for (int i = 0; i < dto.length; i++) {
			for (int j = i+1; j < dto.length; j++) {
				if(dto[i].getAge() >dto[j].getAge()){//오름차순 : 나이가 적은 순으로 정렬
					temp= dto[i];
					dto[i]= dto[j];
					dto[j] = temp;
				}
			}
		}
		return dto;
	}
	public PersonDTO[] heightDescSort(PersonDTO[] dto){
		PersonDTO temp;
		for (int i = 0; i < dto.length; i++) {
			for (int j = i+1; j < dto.length; j++) {
				if(dto[i].getHeight()<dto[j].getHeight()){//오름차순 : 나이가 적은 순으로 정렬
					temp= dto[i];
					dto[i]= dto[j];
					dto[j] = temp;
				}
			}
		}
		return dto;
	}
	//배열을 받아 출력함.
	public void getDisplayResult(PersonDTO[] dto){
		PersonDAO_tea dao = new PersonDAO_tea();
		
		System.out.print("이름\t");
		System.out.print("나이\t");
		System.out.print("키\t");
		System.out.print("몸무게\t");
		System.out.println("성별\t");

		for (int i = 0; i < dto.length; i++) {
			System.out.print(dto[i].getName()+"\t");
			System.out.print(dto[i].getAge()+"\t");
			System.out.print(dto[i].getHeight()+"\t");
			System.out.print(dto[i].getWeight()+"\t");
			System.out.println(dto[i].getGender()+"\t");
		}
		System.out.println("===================================");
	}
	//이름에 대해 오름차순으로 정렬된 배열지정
	public PersonDTO[] getNameAscSort(PersonDTO[] dto){
		PersonDTO temp;
		PersonDAO_tea dao = new PersonDAO_tea();
		dto = dao.heightDescSort(dto);
		System.out.println("이름순으로 오름차순 정렬");
		
		System.out.print("이름\t");
		System.out.print("나이\t");
		System.out.print("키\t");
		System.out.print("몸무게\t");
		System.out.println("성별\t");
		
		for (int i = 0; i < dto.length; i++) {
			for (int j = 0; j < dto.length; j++) {
				if(dto[i].getName().compareTo(dto[j].getName())>0){
					//string1 compare to (String2)>0 오름차순
					temp=dto[i];
					dto[i]= dto[j];
					dto[j] =temp;
				}
			}
		}
		return dto;
	}
	//이름에 대해 오름차순으로 정렬된 배열을 받아 출력함.
	public void getNameAscResult(PersonDTO[] dto){
		PersonDAO_tea dao = new PersonDAO_tea();
		dto = dao.getNameAscSort(dto);
		System.out.println("이름순으로 오름차순 정렬");
		
		System.out.print("이름\t");
		System.out.print("나이\t");
		System.out.print("키\t");
		System.out.print("몸무게\t");
		System.out.println("성별\t");
		
		for (int i = 0; i < dto.length; i++) {
			System.out.print(dto[i].getName()+"\t");
			System.out.print(dto[i].getAge()+"\t");
			System.out.print(dto[i].getHeight()+"\t");
			System.out.print(dto[i].getWeight()+"\t");
			System.out.println(dto[i].getGender()+"\t");
		}
	}
	//키에 대해 내림차순으로 정렬된 배열을 받아 출력함.
	public void getHeightDescSortResult(PersonDTO[] dto){
		PersonDAO_tea dao = new PersonDAO_tea();
		dto = dao.heightDescSort(dto);
		System.out.println("키순으로 내림차순 정렬");
		
		System.out.print("이름\t");
		System.out.print("나이\t");
		System.out.print("키\t");
		System.out.print("몸무게\t");
		System.out.println("성별\t");
		
		for (int i = 0; i < dto.length; i++) {
			System.out.print(dto[i].getName()+"\t");
			System.out.print(dto[i].getAge()+"\t");
			System.out.print(dto[i].getHeight()+"\t");
			System.out.print(dto[i].getWeight()+"\t");
			System.out.println(dto[i].getGender()+"\t");
		}
	}
	//오름차순으로 정렬된 배열을 받아 출력함.
	public void getAgeAscSortResult(PersonDTO[] dto){
		PersonDAO_tea dao = new PersonDAO_tea();
		dto = dao.ageAscSort(dto);
		System.out.println("나이순으로 오름차순 정렬");
		
		System.out.print("이름\t");
		System.out.print("나이\t");
		System.out.print("키\t");
		System.out.print("몸무게\t");
		System.out.println("성별\t");
		
		for (int i = 0; i < dto.length; i++) {
			System.out.print(dto[i].getName()+"\t");
			System.out.print(dto[i].getAge()+"\t");
			System.out.print(dto[i].getHeight()+"\t");
			System.out.print(dto[i].getWeight()+"\t");
			System.out.println(dto[i].getGender()+"\t");
		}
	}
}
