public class PersonDTO {
//	멤버변수 선언 (name, age, height, weight, gender)
	private String name;
	private int age;
	private float height;
	private float weight;
	private String gender;
	
//	default 생성자 정의_ IO에서 필요하기때문에(자동으로 생성되더라도.)
	public PersonDTO(){
		
	}

//	- 생성자 메소드 초기화(다섯개의 멤벼변수를 하나로 묶어준다.)
	public PersonDTO(String name, int age, float height, float weight, String gender) {
		super();
		this.name = name;
		this.age = age;
		this.height = height;
		this.weight = weight;
		this.gender = gender;
	}

//	- get/set 메소드 정의.
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
}
