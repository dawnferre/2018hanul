import java.util.Scanner;

public class BookMain03 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		//객체배열 선언 및 생성.
		Book[] bo = new Book[2]; 

		for (int i = 0; i < bo.length; i++) {
			bo[i] = new Book();
			
			System.out.print("제목을 입력해주세요.");
			bo[i].title = scan.nextLine();
			System.out.print("가격을 입력해주세요.");
			bo[i].price = Integer.parseInt(scan.nextLine());
			System.out.print("저자를 입력해주세요.");
			bo[i].auth = scan.nextLine();
			System.out.print("출판사를 입력해주세요.");
			bo[i].pub = scan.nextLine();
		
		}
		scan.close();
		for (int i = 0; i < bo.length; i++) {
			bo[i].print();
			
		}
	}//main()
}//ckass
