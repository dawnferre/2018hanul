public class PersonDAO {
//	- 출력메소드: display()
//	- 나이의 오름차순 정렬 메소드 정의 : ageAscSort()
	//배열만 지정해주는 메소드
	public PersonDTO[] InsertArray(){
		PersonDTO[] dto = new PersonDTO[5]; //객체 배열 선언
		dto[0]= new PersonDTO("kim", 27, 175.5f, 75.5f, "여");
							//float 타입을 맞춰줘야한다.(f를 붙여주기. 붙여주지 않으면 double로 인식)
		dto[1]= new PersonDTO("sun", 56, 165, 54, "남");
		dto[2]= new PersonDTO("yeong", 47, 160, 74, "여");
		dto[3]= new PersonDTO("jin", 11, 177, 50, "남");
		dto[4]= new PersonDTO("ken", 37, 188, 64, "남");
		
		
		return dto;
	}
	//insertArray에서 직접 배열을 받아 오름차순으로 정렬
	public PersonDTO[] ageAscSort(){
		PersonDTO temp;
		PersonDAO dao = new PersonDAO();
		PersonDTO[] dto = dao.InsertArray();
		for (int i = 0; i < dto.length; i++) {
			for (int j = i+1; j < dto.length; j++) {
				if(dto[i].getAge() >dto[j].getAge()){//오름차순 : 나이가 적은 순으로 정렬
					temp= dto[i];
					dto[i]= dto[j];
					dto[j] = temp;
				}
			}
		}
		return dto;
	}
	//배열을 받아 출력함.
	public void getDisplayResult(){
		PersonDAO dao = new PersonDAO();
		PersonDTO[] dto = dao.InsertArray();
		
		System.out.print("이름\t");
		System.out.print("나이\t");
		System.out.print("키\t");
		System.out.print("몸무게\t");
		System.out.println("성별\t");

		for (int i = 0; i < dto.length; i++) {
			System.out.print(dto[i].getName()+"\t");
			System.out.print(dto[i].getAge()+"\t");
			System.out.print(dto[i].getHeight()+"\t");
			System.out.print(dto[i].getWeight()+"\t");
			System.out.println(dto[i].getGender()+"\t");
		}
		System.out.println("===================================");
	}
	//오름차순으로 정렬된 배열을 받아 출력함.
	public void getAgeAscSortResult(){
		PersonDAO dao = new PersonDAO();
		PersonDTO[] dto = dao.ageAscSort();
		System.out.println("나이순으로 오름차순 정렬");
		
		System.out.print("이름\t");
		System.out.print("나이\t");
		System.out.print("키\t");
		System.out.print("몸무게\t");
		System.out.println("성별\t");
		
		for (int i = 0; i < dto.length; i++) {
			System.out.print(dto[i].getName()+"\t");
			System.out.print(dto[i].getAge()+"\t");
			System.out.print(dto[i].getHeight()+"\t");
			System.out.print(dto[i].getWeight()+"\t");
			System.out.println(dto[i].getGender()+"\t");
		}
	}
}
