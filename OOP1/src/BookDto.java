public class BookDto {
//멤버변수 
	//변수선언
	private String title;
	private int price;
	private String auth;
	private String pub;
	
	//기본생성자 메소드(default Constructor method) _ 클래스와 메소드의 이름이 동일, 리턴타입 X
	


	public BookDto(){
		//기본 생성자 메소드 (빈깡통) ==> 생략가능.
		
	}

	//정의된 멤버변수를 매개변수로 값을 받아 초기화하는 생성자 메소드 정의
	//이클립스에서 제공.
	//generate constructor field
	//하나의 묶여있는 클래스로 인식됨.
	public BookDto(String title, int price, String auth, String pub) {
		super();//상속을 받은 object class
		this.title = title; //this는 메소드 내 변수 , 뒤에 this붙지않은 것은 클래스변수
		this.price = price;
		this.auth = auth;
		this.pub = pub;
	}
	//source menuBar - generate getter/setter
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getPub() {
		return pub;
	}

	public void setPub(String pub) {
		this.pub = pub;
	}
	

}
