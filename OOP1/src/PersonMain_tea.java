public class PersonMain_tea {
	public static void main(String[] args) {
		PersonDTO[] dto = new PersonDTO[5]; //객체 배열 선언
		dto[0]= new PersonDTO("박시안", 27, 175.5f, 75.5f, "여");
							//float 타입을 맞춰줘야한다.(f를 붙여주기. 붙여주지 않으면 double로 인식)
		dto[1]= new PersonDTO("김연", 56, 165, 54, "남");
		dto[2]= new PersonDTO("송지한", 47, 160, 74, "여");
		dto[3]= new PersonDTO("최영", 11, 177, 50, "남");
		dto[4]= new PersonDTO("소유리", 37, 188, 64, "남");
		
		
		PersonDAO_tea dao = new PersonDAO_tea();
		dao.getDisplayResult(dto); //데이터 넣은대로 출력
		dao.getAgeAscSortResult(dto); // 나이순으로 오름차순 정렬해서.
		dao.getHeightDescSortResult(dto);
		dao.getNameAscResult(dto);;
		
		//메인에서 배열을 설정해서 넘겨줘도 괜찮음.
	}//main()
}//class
