public class StudentMain {
	public static void main(String[] args) {
		Student stu = new Student();
		stu.name="김사진";
		stu.kor=80;
		stu.math=78;
		stu.eng=65;
		
		stu.getSum();
		stu.getAvg();
		stu.print(); //결과값이 메소드전에 실행되면 print가 먼저되기 때문에
					// 함수로 지정한 경우, print()는 제일 나중에.
		System.out.println("=========================");
		Student stu2 = new Student();
		stu2.name="박은수";
		stu2.kor=70;
		stu2.math=88;
		stu2.eng=95;
		
		stu2.getSum();
		stu2.getAvg();
		stu2.print(); //결과값이 메소드전에 실행되면 print가 먼저되기 때문에
		// 함수로 지정한 경우, print()는 제일 나중에.
	}//main()
}//class
