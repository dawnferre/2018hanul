public class ProductMain02 {
	public static void main(String[] args) {
		Product pro = new Product();//객체 생성.- 객체가 사용할 메모리를 new 연산자로 할당받는다.
		
		pro.num=1;
		pro.name ="컴퓨터";
		
//		System.out.println(pro.num);
//		System.out.println(pro.name);
		pro.print();
	
		//새로운 객체생성.
		Product pro2 = new Product();
		
		pro2.num =2;
		pro2.name="노트북";
		pro2.print();
//		System.out.println(pro2.num);
//		System.out.println(pro2.name);
		
		Product pro3 = new Product();
		
		pro3.num =3;
		pro3.name="태블릿";
		pro3.print();
//		System.out.println(pro3.num);
//		System.out.println(pro3.name);
		
	}//main()
}//class
