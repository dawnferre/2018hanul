import java.util.Scanner;

//static 없는 경우,메모리에 자동설정이 되지 않기 때문에  객체생성.
public class Sum_machine {
	public static void main(String[] args) {
		//시작값(startNum), 종료값(endNum)을 입력받는다.
		//makeSum ()메소드 호출

		//객체 생성.
		Sum_machine sum = new Sum_machine();
		int[] main = sum.getStartNumEndNum();
		sum.makeSum(main[0],main[1]); //생성된 객체 메소드를 호출.
		//메소드를 생성하지 않으면 오류가 뜨고 클릭시 private한 메소드 생성함.
		
	}//main()
	public void makeSum(int startNum, int endNum){
		int result = 0;
		if(startNum> endNum){//처음 입력한 
			for(int i =endNum; i<=startNum; i++){
				result +=i;
			}
		}else{
			for(int i =startNum; i<=endNum; i++){
				result +=i;
			}
			
		}
		System.out.println("입력하신 값은 "+startNum +","+endNum+" 이며 \n두 값의 합은 "+result+"입니다.");
	}

	//입력받는 값을 메소드로 만들다.
	public int[] getStartNumEndNum(){
		Scanner scan = new Scanner(System.in);
		System.out.println("첫번째 정수를 입력해주세요");
		int startNum = scan.nextInt();
		System.out.println("두번째 정수를 입력해주세요");
		int endNum = scan.nextInt();
		scan.close();
		return new int[]{startNum, endNum};
	}
}//class
