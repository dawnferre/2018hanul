import java.util.Scanner;

public class Test_method02 {
	//임의의 실수 두개를 입력받아(su1, su2) hap()메소드 호출, 두 실수의 합 sum을 구하여 return
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("첫번째 실수를 입력해주세요.");
		double su1 = scan.nextDouble();
		System.out.println("두번째 실수를 입력해주세요.");
		double su2 = scan.nextDouble();
		scan.close();
		
		double result =sum(su1,su2);
		System.out.println("당신이 입력한 수는 "+su1+" , "+su2+"입니다. \n그 둘을 더한 값은 "+result+"입니다.");
	}//main()
	public static double sum(double a, double b){
		double result = a+b;
		
		return result;
	}
	
}//class
