import java.text.DecimalFormat;
import java.util.Scanner;

public class Test_method04 {
	public static void main(String[] args) {
		//임의의 두 정수를 입력받는다 su1, su2
		//사칙연산 을 수행하는 메소드를 호출하고 그 결과를 출력
		//나눗셈은 소수 둘째자리까지 표시
		Scanner scan = new Scanner(System.in);
		System.out.println("임의의 정수를 입력해주세요.");
		int su1 = scan.nextInt();
		System.out.println("두번째 정수를 입력해주세요.");
		int su2 = scan.nextInt();
		scan.close();
		

		add(su1,su2);
		minus(su1,su2);
		multi(su1,su2);
		// 소술점 두번째까지 설정해주는 방식.== decimalFormat
//		DecimalFormat def = new DecimalFormat("0.00");
//		def.format(division(su1,su2));
		
		System.out.println("입력하신 수는 "+su1+" , "+su2+" 이며  "
				+"\n덧셈은 "+add(su1,su2)+", 뺄셈은 "+minus(su1,su2)+", 곱셈은 "+multi(su1,su2)+", "
						+ "나눗셈은 "+ division(su1,su2)+"입니다.");
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println(division(su1,su2)+1); //division 메소드의 리턴타입이 String
		//덧셈으로 바꾸고 싶을 땐 == wrapper 클래스!!!!
		System.out.println(Double.parseDouble(division(su1,su2))+1); //division 메소드의 리턴타입이 String
	}//main()
	public static int add(int a ,int b ){
		int result=0;
		result = a+b;
//		return result;
		return a+b; //바로 리턴할 수 있음.
	}
	public static int minus(int a ,int b ){
		int result=0;
		result = a-b;
		
		return result;
	}
	public static int multi(int a ,int b ){
		int result=0;
		
		result = a*b;
		return result;
	}
	//소숫점을 받아야 하기 때문에 double로 설정함.
	public static String division(int a ,int b ){
//		double a1= (double)a;
//		double b1= (double)b;
		DecimalFormat def = new DecimalFormat("0.00");
//		def.format(division(su1,su2));
		double result =((double)a/b);
		return def.format(result); //decimalFormat 사용하면 String type 변환 >> return type : String
	}
}//class
