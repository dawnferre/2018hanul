import java.util.Scanner;

//static 없는 경우,메모리에 자동설정이 되지 않기 때문에  객체생성.
public class Sum_machineReturnPrivate {
	public static void main(String[] args) {
		//시작값(startNum), 종료값(endNum)을 입력받는다.
		Scanner scan = new Scanner(System.in);
		System.out.println("첫번째 정수를 입력해주세요");
		int startNum = scan.nextInt();
		System.out.println("두번째 정수를 입력해주세요");
		int endNum = scan.nextInt();
		scan.close();

		//makeSum ()메소드 호출
		//객체 생성 :Sum_machine.java
		Sum_machine sum = new Sum_machine();
		
		sum.makeSum(startNum,endNum); //return값이 있음
		
		//접근제어
		/*
		 * public -- makeSum() : 접근제어자가 public 선언 ㅣ 외부에서도 사용가능.
		 * private -- : 내부(하나의 class)에서 사용가능.
		 * */
	}//main()

}//class
