import java.text.DecimalFormat;
import java.util.Scanner;

public class BodyMathIndexMachine02 {
	public static void main(String[] args) {
	 //신장과 체중을 입력(height, weight)
		Scanner scan = new Scanner(System.in);
		
		System.out.println("키를 입력해주세요.");
		float height = scan.nextFloat();
		System.out.println("몸무게를 입력해주세요.");
		float weight = scan.nextFloat();
		
	
		scan.close();
		//결과 출력
		System.out.println("당신의 키는 "+height+"이고, 몸무게는 "+weight+"입니다.");
		System.out.println("bmi지수 :"+calculate(height,weight));
		System.out.println(getResult(calculate(height,weight)));
		//처음에 넘겨줄때 parsefloat을 이용해서 메소드 생성시 조금 더 간단하게 만들 수 있음.
		System.out.println(getResult2(Float.parseFloat(calculate(height,weight))));
		
	}//main()
	//비만지수 (bmiresult)구하는 메소드(calculate)을 정의
	public static String calculate(float height,float weight){
		float bmi = weight/((height/100)*(height/100));
		DecimalFormat def = new DecimalFormat("0.00");
//		def.format(bmi); String 이 넘어가기 떄문에 method 리턴값이 String
		return def.format(bmi);
	}
	
	//비만지수를 기준으로 건강상태(result)를 구하는 메소드 getResult를 정의
	public static String getResult(String result){
		//wrapper class
		float bmi = Float.parseFloat(result);
		
		if(bmi<18.5){
			result ="저체중입니다.";
		}else if(18.5<=bmi&& bmi<=24.9){
			result ="정상체중입니다.";
		}else if(25<=bmi && bmi <=29.9){
			result ="과체중입니다.";
		}else if(bmi>=30){
			result ="비만입니다.";
		}
		return result;
	}
	//선생님 답안.
	//비만지수를 기준으로 건강상태(result)를 구하는 메소드 getResult를 정의
	public static String getResult2(float bmi){
		String result = null;
		if(bmi<18.5){
			result ="저체중입니다.";
		}else if(18.5<=bmi&& bmi<=24.9){
			result ="정상체중입니다.";
		}else if(25<=bmi && bmi <=29.9){
			result ="과체중입니다.";
		}else if(bmi>=30){
			result ="비만입니다.";
		}
		return result;
	}
}//class

