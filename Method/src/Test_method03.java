import java.util.Scanner;

public class Test_method03 {
	//임의의 정수 두개를 입력받음 , su1, su2
	//짝수의 합 evenSum , 홀수의 누적합 addSum을 구하는 메소드를 호출
	//evenSum(), addSum() 메소드를 정의하여 계산한 후 결과를 리턴.
	//리턴받은 결과를 출력
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("임의의 정수를 입력해주세요.(처음은 작은값)");
		int su1 = scan.nextInt();
		System.out.println("두번째 정수를 입력해주세요.(큰값)");
		int su2 = scan.nextInt();
		scan.close();
		evenSum(su1,su2);
		addSum(su1,su2);
		
		System.out.println("입력하신 수는 "+su1+" , "+su2+" 이며  "
				+ "숫자범위의 짝수의 합은 "+evenSum(su1,su2)+", 홀수의 합은 "+addSum(su1,su2));
		
	}//main()
	
	public static int evenSum(int su1, int su2){
		int result = 0;
		for(int i = su1; i<=su2; i++){
			if( i % 2 ==0){
				result +=i;
			}
		}
		return result;
	}
	public static int addSum(int su1, int su2){
		int result = 0;
		for(int i = su1; i<=su2; i++){
			if( i % 2 !=0){
				result +=i;
			}
		}
		return result;
	}
}//class
