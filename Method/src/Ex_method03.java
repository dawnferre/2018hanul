import java.util.Arrays;

public class Ex_method03 {
	public static void main(String[] args) {
		//매개변수로 정수형 배열(arr)을 이용하여 배열 원소의 총합(sum)을 구하여 
		//결과값을 리턴하는 메소드 (arrHap)
		//정의 및 호출하고 결과를 출력.
		int[] arr = {10,20,30};
		int sum =arrHap(arr);
		// Arrays.toString() = 배열의 종류에 따라 내용을 볼 수 있는 메소드.
		System.out.println("배열의 내용 : "+ Arrays.toString(arr));
		System.out.println("배열의 총합"+arrHap(arr));
		System.out.println("배열의 총합"+sum);
		
	}//main()
	public static int arrHap(int[] arr){
		int result =0;
		for (int i = 0; i < arr.length; i++) {
			result += arr[i];
		}
		
		return result;
	}
}//class
