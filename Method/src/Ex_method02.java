public class Ex_method02 {
	//hap이라는 메소드에  두개의 정수를 매개변수로 전달하여 합(sum)을 구하는 메소드를 호출
	public static void main(String[] args) {
		int a =10;
		int b =20;
		
		int sum =hap(a,b); //hap() 메소드 호출 : 실인수
		System.out.println(a+"+"+b+"="+sum);
		
		
	}//main()
	
	public static int hap(int a , int b){//hap() 메소드 호출 : 가인수
		int sum = a+b;
		return sum;
	}
}//class
