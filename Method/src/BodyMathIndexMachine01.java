import java.text.DecimalFormat;
import java.util.Scanner;

public class BodyMathIndexMachine01 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("키를 입력해주세요.");
		float height = scan.nextFloat();
		System.out.println("몸무게를 입력해주세요.");
		float weight = scan.nextFloat();
		
		DecimalFormat def = new DecimalFormat("0.00");
	
		scan.close();
		float bmi = weight/((height/100)*(height/100));
//		def.format(bmi);
		
		
		//비만지수를 바탕으로 건강상태(result) 구하기
		String result = null;
			if(bmi<18.5){
				result ="저체중입니다.";
			}else if(18.5<=bmi&& bmi<=24.9){
				result ="정상체중입니다.";
			}else if(25<=bmi && bmi <=29.9){
				result ="과체중입니다.";
			}else if(bmi>=30){
				result ="비만입니다.";
			}
		//결과출력
		System.out.println("당신의 키는 "+height+"이고, 몸무게는 "+weight+"입니다.");
		System.out.println("bmi지수 :"+def.format(bmi));
		System.out.println(result);
		
	}//main()
}//class
