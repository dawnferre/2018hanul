public class Ex_method01 {
	public static void main(String[] args) {
		  //hap()이라는 메서드에   10과 20을 매개변수로 보내는 호출문 작성
		//매개변수 초기화, 인수.
		int a =10; 
		int b =20;
		
		hap(a,b);
		
		//static 사용하지 않을 시에는 무조건 객체생성을 통해 메모리 할당함.
		Ex_method01 me = new Ex_method01();
		me.hap2(a,b);
		
//		System.out.println(hap(a,b));
		
		
	}//main()
	
	//두개의 정수를 매개변수로 받아서 합을 구하여 출력하는 메소드 hap()
	public static int hap(int a, int b){
		int result = a+b;
		System.out.println("a값 : "+a+", b의 값 : "+b+", 결과값 : "+result);
		return result;
	}
	public void hap2(int a, int b){
		int sum = a+b;
		System.out.println("a값 : "+a+", b의 값 : "+b+", 결과값 : "+sum);
	}
}//class
