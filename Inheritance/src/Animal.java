//부모클래스_ 상속(슈퍼클래스, 상위클래스)
public class Animal { //상태정보를 담고있는 DTO class
	//변수선언.
	private String name;
	private int age;
	
	public Animal(){
		//default 생성자
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	// 생성자에 변수를 하나로 묶어줌.
	public Animal(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	
	
	public void eat(){
		System.out.println("animal 클래스의 eat메소드");
	}
	public void sleep(){
		System.out.println("animal 클래스의 sleep메소드 ");
	}
	public void cry(){
		System.out.println("animal 클래스의 cry메소드");
	}
	
}
