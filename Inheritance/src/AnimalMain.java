public class AnimalMain {
	public static void main(String[] args) {
		Dog dog = new Dog("DDongGGo",3); //dog 객체 생성후 초기화
		//상속을 이용해서 animal dto 를 접근하여 쓸 수 있음.
		
		System.out.println(dog.getName());
			
		System.out.println(dog.getAge());
		//dog class 엔 set, get메소드 없지만 상속받아서 사용함.
		dog.setName("SSOJU");
		dog.setAge(5);
		System.out.println(dog.getName());
		System.out.println(dog.getAge());
		
		Cat cat = new Cat("냐옹이",8);
		System.out.println(cat.getName());
		System.out.println(cat.getAge());
		
		cat.setName("멍냥");
		cat.setAge(4);
		System.out.println(cat.getName());
		System.out.println(cat.getAge());
		
		
	}//main
}//class
