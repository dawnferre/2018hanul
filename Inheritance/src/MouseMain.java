public class MouseMain {
	public static void main(String[] args) {
		//자바의 모든 클래스는 기본적으로 object class를 상속받는다.
		
		Mouse m = new Mouse();//mouse class 객체생성
		System.out.println("mouse");
		m.clickLeft();
		m.clickright();
		
		System.out.println("+++++++++++++++++++");
		//mouse자식개체 생성.
		WheelMouse wm = new WheelMouse();
		System.out.println("wheel");
		wm.clickLeft(); //상속받은 클래스
		wm.clickright();//상속받은 클래스
		wm.scoll();
				
		System.out.println("+++++++++++++++++++");
		OpticalMouse op = new OpticalMouse();
		System.out.println("optical");
		op.clickLeft();//상속받은 클래스
		op.clickright();//상속받은 클래스
		op.scoll();//상속받은 클래스
		
		//class diagram
		//-- 도식화 시켜서  상속관계를 나타내는 것.
		//밑에 순으로 class 표시, 상속관계는 자식 -->부모관계에서 화살표는 움직인다.
		
		//class 명
		//상태정보(변수, 필드)
		//행위정보(메서드)
	}//main
}//class
