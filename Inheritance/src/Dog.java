	//extends를 이용한 상속
public class Dog extends Animal {
	public Dog(){
		//default 생성자
	}
	public Dog(String name, int age){
		super(name, age); //super(): 부모클래스(animal)을 의미한다.
	}
}
