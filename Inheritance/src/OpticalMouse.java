
public class OpticalMouse extends WheelMouse{
						//상위 클래스인 wheelmouse를 하위클래스인 opticalmouse class 상속받음 
						//==> 따라서 부모는 wheelmouse<mouse .. 여러부모를 가질수 있음.
	
	//오버라이드(override) :자식클래스에서 부모클래스의 메소드를 다시 정의(재정의)
	//1.부모와 메소드랑 동일해야한다.(매개변수, 매개변수타입, 리턴값)
	@Override //어노테이션 : 표식
	public void clickLeft(){
		System.out.println("광마우스 왼쪽 클릭.");
		
		
	}
	//source - override/implements method 또는 ctrl + space
	@Override
	public void scoll() {
		System.out.println("광마우스 스크롤 기능");
	}
	
}
