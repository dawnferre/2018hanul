import java.util.Arrays;
import java.util.Scanner;

//배열의 크기(cnt)를 입력받은 후 배열생성(arr[])
//arr[] 배열의 크기(길이)에 맞게 임의 정수를 입력받아 할당
//maxmin 값 비교 및 구하기. -Max, Min각각 메소드 구현해서 리턴
public class Test_MaxMin {
	public static void main(String[] args) {
//		Scanner scan = new Scanner(System.in);
//		System.out.println("배열의 길이를 정할 값을 만들어 주세요.");
//		int cnt = scan.nextInt();
//		int[] arr = new int[cnt];
//		for (int i = 0; i < arr.length; i++) {
//			System.out.println("배열의 값을 넣어주세요.");
//			int innerCnt = scan.nextInt();
//			arr[i]=innerCnt;
//		}
//		scan.close();
//		System.out.println("배열의 값 : "+Arrays.toString(arr));
//		
		Test_MaxMin test = new Test_MaxMin();
		int[] arr =test.getArray();
		test.getMax(arr);
		test.getMin(arr);
		
		System.out.println("선생님 메소드_최대값 : "+test.maxMachine(arr));
		System.out.println("선생님 메소드_최소값 : "+test.minMachine(arr));
	}//main()
	public void getMax(int[] arr){
		int max = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if(max <arr[i]){
				max = arr[i];
			}
		
		}
		System.out.println("배열의 최대값 : "+max);
		
	}//getMax =Maxmachine()
	public void getMin(int[] arr){
		int min = arr[0];
		for (int i = 0; i < arr.length; i++) {

			if(min >arr[i]){
				min =arr[i];
			}
		}
		System.out.println("배열의 최소값 : "+min);
	}//getMin = Minmachine()
	public int[] getArray(){
		Scanner scan = new Scanner(System.in);
		System.out.println("배열의 길이를 정할 값을 만들어 주세요.");
		int cnt = scan.nextInt();
		int[] arr = new int[cnt];
		for (int i = 0; i < arr.length; i++) {
			System.out.println("배열의 값을 넣어주세요.");
			int innerCnt = scan.nextInt();
			arr[i]=innerCnt;
		}
		scan.close();
		System.out.println("배열이 만들어졌어요.");
		System.out.println("배열의 값 : "+Arrays.toString(arr));
		
		return arr;
	}//getArray()
	
	//선생님 메소드
	public int maxMachine(int[] arr){
		int max = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if(max <arr[i]){
				max = arr[i];
			}
		
		}
		
		return max;
	}
	public int minMachine(int[] arr){
		int min = arr[0];
		for (int i = 0; i < arr.length; i++) {

			if(min >arr[i]){
				min =arr[i];
			}
		}
		return min;
	}
}//class
