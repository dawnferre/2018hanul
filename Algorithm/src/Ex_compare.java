import java.util.Arrays;

public class Ex_compare {
	public static void main(String[] args) {
		//정수형 배열 arr의 원소값의 최대값을 구하여 출력.
		int[] arr = {4,3,1,2,5}; // 정수형 배열 arr 선언, 값 할당.
//		int max =arr[0]; //배열원소의 무조건 첫번째 값. - 최대값이 저장될 변수
//		int min =arr[0]; //배열원소의 무조건 첫번째 값. - 최소값이 저장될 변수
//		
//		for (int i = 0; i < arr.length; i++) {
//			if(max <arr[i]){
//				max = arr[i];
//			}
//			if(min >arr[i]){
//				min =arr[i];
//			}
//		}
//		System.out.println("배열의 최대값 : "+max);
//		System.out.println("배열의 최소값 : "+min);
		Ex_compare ex = new Ex_compare();
		ex.getMaxMin(arr);
		
	}//main()
	public void getMaxMin(int[] arr){
		int max =arr[0]; //배열원소의 무조건 첫번째 값. - 최대값이 저장될 변수
		int min =arr[0]; //배열원소의 무조건 첫번째 값. - 최소값이 저장될 변수
		
		for (int i = 0; i < arr.length; i++) {
			if(max <arr[i]){
				max = arr[i];
			}
			if(min >arr[i]){
				min =arr[i];
			}
		}
		System.out.println("배열의 값 : "+Arrays.toString(arr));
		System.out.println("배열의 최대값 : "+max);
		System.out.println("배열의 최소값 : "+min);
	}
}//class
