import java.util.Arrays;
import java.util.Scanner;

public class Ex_seqSearch {
	public static void main(String[] args) {
		//순차검색 : sequence Search :데이터를 맨처음부터 끝까지 검색.
		//(주어진 데이터가 소량일경우)
		
		int[] arr ={30,90,10,60,70,20,80,40,50,100};//데이터가 저장된 배열
		
		Scanner scan = new Scanner(System.in);
		System.out.println(Arrays.toString(arr)+"\n찾는 수를 입력하세요.");
		int searchData = scan.nextInt();
		scan.close();
		
		Ex_seqSearch ex = new Ex_seqSearch();
		int result =ex.seqSearch(arr, searchData);// 메소드호출
		if(result ==-1){
			System.out.println("찾는 수는 "+searchData+"이며 결과엔 없습니다.");
		}else{
			System.out.println("찾는 수는 "+searchData+"이며 "+result+"번째에 있습니다.");
		}
	}//main()
	public int seqSearch(int[] arr, int data){ //seqSearch() 메소드 정의
		int index =-1; ;// 프로그램에서 -1(끝, 실패)를 의미 == 아직 값을 못찾았다.
		for (int i = 0; i < arr.length; i++) {
			if(arr[i]== data){
				index =i+1;
			}			
		}
		return index;
	}//seqSearch
}//class
