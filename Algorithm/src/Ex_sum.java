import java.util.Arrays;

public class Ex_sum {
	public static void main(String[] args) {
		//정수형배열(arr)의 원소값의 누적합 (sum) 을 구하여 출력
		int[] arr = {4,3,1,2,5}; // 정수형 배열 arr 선언, 값 할당.
		
		Ex_sum sum1 = new Ex_sum();
		sum1.calculate(arr);
		
	}//main()
	public void calculate(int[] arr){
		int sum =0;
		
		for (int i = 0; i < arr.length; i++) {
			sum+=arr[i];
		}
							//배열안의 값을 알려주는 클래스.
		System.out.println("배열의 값 : "+Arrays.toString(arr));
		System.out.println("arr의 누적합 : "+sum);
	}//calculate()
}//class
