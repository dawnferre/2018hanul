import java.util.Scanner;

public class Ex_cnt {
	public static void main(String[] args) {
		//두개의 정수를 입력받아  두수사이의 정수의 개수 출력
//		Scanner scan = new Scanner(System.in);
//		System.out.println("첫번째정수를 입력해주세요.");
//		int su1 = scan.nextInt();
//		System.out.println("두번째정수를 입력해주세요.");
//		int su2 = scan.nextInt();
//		scan.close();
//		
//		int cnt =0; //갯수를 지정하는 수.
//		if(su1<su2){
//			for(int i =su1; i<=su2; i++){
//				cnt++;
//			}
//			System.out.println("두 수 "+su1+","+su2+"사이의 갯수는 "+cnt+"입니다.");
//		}else{
//			for(int i =su2; i<=su1; i++){
//				cnt++;
//			}
//			System.out.println("두 수 "+su2+","+su1+"사이의 갯수는 "+cnt+"입니다.");
//			
//		}
		Ex_cnt cn = new Ex_cnt();
		cn.printSu();
	}//main()
	
	public int[] inputSu(){
		
		Scanner scan = new Scanner(System.in);
		System.out.println("첫번째정수를 입력해주세요.");
		int su1 = scan.nextInt();
		System.out.println("두번째정수를 입력해주세요.");
		int su2 = scan.nextInt();
		scan.close();
		return new int[]{su1,su2};
	}//inputSu()
	
	public void printSu(){
		Ex_cnt cn = new Ex_cnt();
		int[] main = cn.inputSu();
		int cnt =0; //갯수를 지정하는 수.
		if(main[0]<main[1]){
			for(int i =main[0]; i<=main[1]; i++){
				cnt++;
			}
			System.out.println("두 수 "+main[0]+","+main[1]+"사이의 갯수는 "+cnt+"입니다.");
		}else{
			for(int i =main[1]; i<=main[0]; i++){
				cnt++;
			}
			System.out.println("두 수 "+main[1]+","+main[0]+"사이의 갯수는 "+cnt+"입니다.");
			
		}
	}//printSu()
}//class
