import java.util.Arrays;

public class Ex_sort {
	public static void main(String[] args) {
		//주어진 배열 arr이 있다. 오름차순정렬: 선택정렬(selection sort)
		int[] arr ={4,3,1,2,5};
		// 
//		오름차순정렬: 선택정렬(selection sort)
		Ex_sort sort = new Ex_sort();
		sort.selectionAsc(arr);
		sort.selectionDesc(arr);
	}//main()
	
	public void selectionAsc(int[] arr){
		for (int i = 0; i < arr.length; i++) {
			//그다음 값 비교를 위해서 j값 설정
			for (int j = i+1; j < arr.length; j++) {
				if(arr[i] > arr[j]){// 오름차순
					int temp =arr[i];
					arr[i]= arr[j];
					arr[j]= temp;
				}
			}
		}
		System.out.println("오름차순 : "+Arrays.toString(arr));
		//--------메소드 제공.(오름차순)
		Arrays.sort(arr); //배열 메소드 제공
		System.out.println(Arrays.toString(arr));
	}
	public void selectionDesc(int[] arr){
		for (int i = 0; i < arr.length; i++) {
			//그다음 값 비교를 위해서 j값 설정
			for (int j = i+1; j < arr.length; j++) {
				if(arr[i] < arr[j]){// 오름차순
					int temp =arr[i];
					arr[i]= arr[j];
					arr[j]= temp;
				}
			}
		}
		System.out.println("내림차순 : "+Arrays.toString(arr));
		
		
	}
}//class
