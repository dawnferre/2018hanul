import java.util.Scanner;

public class Ex_seqSearch_practice {
	public static void main(String[] args) {
		//순차검색 : sequence Search :데이터를 맨처음부터 끝까지 검색.
		//(주어진 데이터가 소량일경우)
		
		int[] arr ={30,90,10,60,70,20,80,40,50,100};//데이터가 저장된 배열
		
		
		
		Ex_seqSearch_practice ex = new Ex_seqSearch_practice();
		ex.seqSearch(arr, ex.getFindSu());// 메소드호출
	}//main()
	public void seqSearch(int[] arr, int data){ //seqSearch() 메소드 정의
		int index =-1; ;// 프로그램에서 -1(끝, 실패)를 의미 == 아직 값을 못찾았다.
		for (int i = 0; i < arr.length; i++) {
			if(arr[i]== data){
				index =i+1;
			}			
		}
		System.out.println("찾으시는 수의 위치는 "+index+"입니다.");
		
	}//seqSearch
	public int getFindSu(){
		Scanner scan = new Scanner(System.in);
		System.out.println("찾는 수를 입력하세요.");
		int data = scan.nextInt();
		scan.close();
		return data;
	};
}//class
