import java.util.Arrays;
import java.util.Scanner;

public class Ex_BineSearch {
	public static void main(String[] args) {
		//이진 검색(binary search) :low, high, middie 사용
		//찾고자 하는 데이터를 중앙에 위치한 중간값과 비교하는 방법
		//데이터가 사전에 반드시 오름차순으로 정렬되어 있어야 한다(전제조건)
		int[] arr = {10,20,30,40,50,60,70,80,90,100};
		int[] arr2 ={30,90,10,60,70,20,80,40,50,100};//데이터가 저장된 배열
		Arrays.sort(arr2);
		Arrays.toString(arr2);
//		System.out.println(Arrays.toString(arr2));
		Scanner scan = new Scanner(System.in);
		System.out.println(Arrays.toString(arr2)+"\n찾는 수를 입력하세요.");
		int data = scan.nextInt();
		scan.close();
		
		binSearch(arr, data); // binSearch()메서드 호출
	}//main()
	//이진검색
	public static void binSearch(int[] arr, int data){
		int index = -1;
		int low = 0; // 맨 처음 원소 번지수
		int middle = 0; // 중간값 
		int high =arr.length-1;//맨 마지막 원소 번지수
		
		//값을 비교해서 low보다 작은값 털고, high보다 큰 값 털고, low=middle=high 값이 같아지는 경우 결국  값을 찾게됨
		//없다면 없다고 표시해줄것.
		while(low <= high){
			middle = (low+high)/2; // 중간값의 배열 번지수 (index)
			if(data == arr[middle]){ //값을 찾은 경우
				index = middle;
				break;
			}else if(data> arr[middle]){//찾는 값이 middle 번지보다 위에 있는 경우
				low = middle+1;// arr[middle]까지의 값을 이미 확인했기때문에 그 다음번지부터 확인해야 한다.
				continue;
			}else{//찾는 값이 middle 번지보다 아래에 있는 경우
				high = middle-1;// arr[middle]부터 끝까지의 값을 확인했기때문에 그 전번지부터 확인해야한다.
				continue;
			}
		}
		if(index ==-1){
			
			System.out.println("검색하신 값은 "+data+"이며,찾는 수는 없습니다.");
		}else{
			System.out.println("검색하신 값은 "+data+"이며,"+(index+1)+"번째에 있습니다.");
			
		}
		
	}
}//class
