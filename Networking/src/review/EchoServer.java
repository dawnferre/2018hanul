package review;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServer {
	public static void main(String[] args) {
		//서버의 소켓생성 = 서버소켓, 일반소켓 두가지 생성함.
		ServerSocket ss = null;
		Socket socket = null;
		
		//서버소켓 설정
		try {
			ss = new ServerSocket(9999);
			System.out.println("서버가 구동중입니다.");
			
			//연결할 소켓은 서버소켓이 요청신호를 받았을때 생성됨.
			socket = ss.accept();
			//서버로 넘어온 메세지 수신
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String msg = br.readLine();
			//ip주소표현_이게 client에서 적어줬던 ip주소가 그대로 넘어오는 거였군..
			InetAddress addr = socket.getInetAddress();
			String ip = addr.getHostAddress();
			//서버내에서 받은 메세지 출력
			System.out.println("클라이언트(ip="+ip+"): "+msg);
			
			//서버에 온 메세지 클라이언트로 출력해줌.
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			pw.println(msg);
			pw.flush();
			
		} catch (Exception e) {
			System.out.println("EchoServer");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("EchoServer finally");
				e.printStackTrace();
			}
		}
		
	}//main
}//class
