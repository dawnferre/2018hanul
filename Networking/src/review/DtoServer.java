package review;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DecimalFormat;

public class DtoServer {
	public static void main(String[] args) {
		ServerSocket ss = null;
		Socket socket = null;
		
		try {
			ss = new ServerSocket(9999);
			System.out.println("서버가 구동중입니다.");
			socket = ss.accept();
			
			//dto로 넘어온 메세지 받기
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			//dto로 casting하여 넘어온 메세지 dto로 받음.
			ReviewDTO dto = (ReviewDTO) ois.readObject();
			int su1 = dto.getSu1();
			int su2 = dto.getSu2();
			String opcode = dto.getOpcode();
			
			System.out.println("넘어온 첫번째 수 : "+su1+"\t넘어온 두번째 수 : "+su2+"\t넘어온 연산자 : "+opcode);
			
			//opcode 조건
			String result=null;
			if(opcode.equals("+")){
				result = (su1 +su2)+"";
			}else if(opcode.equals("-")){
				
				result = (su1 -su2)+"";
			}else if(opcode.equals("*")){
				result = (su1 *su2)+"";
				
			}else if(opcode.equals("/")){//소숫점 연산 처리해줌.
				DecimalFormat de = new DecimalFormat("#.##");
				float su11 = Float.parseFloat(su1+"");
				float su22 = Float.parseFloat(su2+"");
				result =de.format(su11/su22);
				
			}
			System.out.println("계산결과 :"+result);
			//계산결과를 클라이언트에 전송하기.
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			pw.println(result);
			pw.flush();
		} catch (Exception e) {
			System.out.println("DtoServer");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("DtoServer finally");
				e.printStackTrace();
			}
		}
	}//main
}//class
