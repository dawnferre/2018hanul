package review;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DecimalFormat;

public class DtoServer_printf {
	public static void main(String[] args) {
		ServerSocket ss = null;
		Socket socket = null;
		
		try {
			ss= new ServerSocket(9000);
			System.out.println("서버구동중입니다.");
			socket = ss.accept();
			
			//메세지 받기
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			PrintfDTO dto = (PrintfDTO) ois.readObject();
			
			double su1 = dto.getSu1();
			double su2 = dto.getSu2();
			String opcode = dto.getOpcode();
			
			System.out.println("넘어온 첫번째 수 : "+su1+"\t넘어온 두번째 수 : "+su2+"\t넘어온 연산자 : "+opcode);
			
			//opcode 조건
			double result = 0;
			if(opcode.equals("+")){
				result = su1 +su2;
			}else if(opcode.equals("-")){
				
				result = su1 -su2;
			}else if(opcode.equals("*")){
				result = su1 *su2;
				
			}else if(opcode.equals("/")){//소숫점 연산 처리해줌.
				result =su1/su2;
				
			}
			System.out.println("계산결과 :"+result);
			
			//서버에서 클라이언트로 다시 보내주기.
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			pw.printf("결과 : %4.2f",result); //서버에서 뿌려주는 형태에서 format을 지정해서 클라이언트에 넘길 수 있다.
			pw.flush();
		} catch (Exception e) {
			System.out.println("DtoServer printf");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("DtoServer printf finally");
				e.printStackTrace();
			}
			
		}
		
	}//main
}//class
