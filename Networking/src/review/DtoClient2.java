package review;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class DtoClient2 {
	public static void main(String[] args) {
		//수 두개 와 연산자를 받아서 결과를 서버에출력 후 다시 클라이언트에 송신하여 출력.
		//ReviewDTO를 이용할 것.
		Socket socket = null;
		
		try {
			socket = new Socket("127.0.0.1", 9999);
			
			ArrayList<ReviewDTO> list = new ArrayList<>();
			
			ReviewDTO dto1 = new ReviewDTO(1, 3, "-");
			ReviewDTO dto2 = new ReviewDTO(4, 3, "+");
			ReviewDTO dto3 = new ReviewDTO(5, 8, "*");
			
			list.add(dto1);
			list.add(dto2);
			list.add(dto3);
			
			//dto를 서버로 넘겨주기.
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(list);
			oos.flush();
			
			//결과값을 받아 출력하기.
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String data = br.readLine();
			System.out.println("서버 :"+data);
			
		} catch (Exception e) {
			System.out.println("DtoClient");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("DtoClient finally");
				e.printStackTrace();
			}
		}
		
	}//main
}//class
