package review;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class DtoServer2 {
	public static void main(String[] args) {
		ServerSocket ss = null;
		Socket socket = null;
		
		try {
			ss = new ServerSocket(9999);
			System.out.println("서버가 구동중입니다.");
			socket = ss.accept();
			
			//dto로 넘어온 메세지 받기
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			//dto로 casting하여 넘어온 메세지 dto로 받음.
			ArrayList<ReviewDTO> list = (ArrayList<ReviewDTO>) ois.readObject();
			
			for(ReviewDTO dto :list){
				System.out.println(dto.getSu1());
				System.out.println(dto.getSu2());
				System.out.println(dto.getOpcode());
			}
			
			//opcode 조건
//			//계산결과를 클라이언트에 전송하기.
//			PrintWriter pw = new PrintWriter(socket.getOutputStream());
//			pw.println(result);
//			pw.flush();
		} catch (Exception e) {
			System.out.println("DtoServer");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("DtoServer finally");
				e.printStackTrace();
			}
		}
	}//main
}//class
