package review;

import java.net.Socket;

public class ChatClient {
	public static void main(String[] args) {
		Socket socket = null;
		try {
			socket = new Socket("127.0.0.1", 9999);
			
			ChatSenderThread st = new ChatSenderThread(socket);
			st.start();
			
			ChatReceiverThread rt = new ChatReceiverThread(socket);
			rt.start();
		} catch (Exception e) {
			System.out.println("ChatClient");
			e.printStackTrace();
		}
		//thread에서 계속 실행하기때문에 socket설정해주면 처음 실행후 소켓이 닫히기 때문에 exception발생.
		
	}//main
}//class
