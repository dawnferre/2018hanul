package review;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class EchoClient {
	public static void main(String[] args) {
		Socket socket = null;
		
		try {
			socket = new Socket("127.0.0.1", 9999);
			
			//키보드로 입력받아 메세지작성
			Scanner scan = new Scanner(System.in);
			System.out.println("보낼 메세지를 입력해주세요.");
			String msg = scan.nextLine();
			scan.close();
			
			//서버로 전송
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			pw.println(msg);
			pw.flush();
			
			//서버에서 메세지 수신
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String serverMsg = br.readLine();
			System.out.println("서버에서 온 메세지 : "+serverMsg);
			
		} catch (Exception e) {
			System.out.println("EcoClient");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("EcoClient finally");
				e.printStackTrace();
			}
		}
	}//main
}//class
