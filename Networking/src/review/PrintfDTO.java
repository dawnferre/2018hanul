package review;

import java.io.Serializable;

public class PrintfDTO implements Serializable {
	double su1;
	double su2;
	String opcode;
	
	public PrintfDTO() {
	}
	
	
	public PrintfDTO(double su1, double su2, String opcode) {
		super();
		this.su1 = su1;
		this.su2 = su2;
		this.opcode = opcode;
	}


	public double getSu1() {
		return su1;
	}

	public void setSu1(double su1) {
		this.su1 = su1;
	}

	public double getSu2() {
		return su2;
	}

	public void setSu2(double su2) {
		this.su2 = su2;
	}

	public String getOpcode() {
		return opcode;
	}

	public void setOpcode(String opcode) {
		this.opcode = opcode;
	}
	
}
