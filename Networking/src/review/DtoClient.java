package review;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class DtoClient {
	public static void main(String[] args) {
		//수 두개 와 연산자를 받아서 결과를 서버에출력 후 다시 클라이언트에 송신하여 출력.
		//ReviewDTO를 이용할 것.
		Socket socket = null;
		
		try {
			socket = new Socket("127.0.0.1", 9999);
			
			//키보드 입력으로 입력조건 받기.
			Scanner scan = new Scanner(System.in);
			System.out.println("첫번째 수를 입력해주세요.");
			int su1 = scan.nextInt();
			System.out.println("두번째 수를 입력해주세요.");
			int su2 = scan.nextInt();
			System.out.println("연산자를 입력해주세요.");
			String opcode = scan.next();
			//엔터값까지 처리하게 되면 결과값이 넘어가는게 아니라 엔터값까지가 값이 되버려서 
			scan.close();
			//입력받은 변수들을 dto로 받기
			ReviewDTO dto = new ReviewDTO(su1, su2, opcode);
			
			//dto를 서버로 넘겨주기.
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(dto);
			oos.flush();
			
			//결과값을 받아 출력하기.
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String data = br.readLine();
			System.out.println("서버 :"+data);
			
		} catch (Exception e) {
			System.out.println("DtoClient");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("DtoClient finally");
				e.printStackTrace();
			}
		}
		
	}//main
}//class
