package review;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ChatReceiverThread extends Thread{
	//기본생성자로 main의 socket을 넘겨받기
	Socket socket;
	public ChatReceiverThread(Socket socket) {
		this.socket =socket;
	}
	@Override
	public void run() {
		//정보를 수신받아 메세지를 출력.==>다시 클라이언트로 메세지를 보내야한다.
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String msg;
			while((msg=br.readLine())!=null){
				System.out.println("클라이언트  : "+msg);
			}
			
			//메세지 보내기
			
		} catch (Exception e) {
			System.out.println("서버와의 연결이 끊어졌습니다..");
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("receiverThread finally");
				e.printStackTrace();
			}
		}
		
		
	}
}
