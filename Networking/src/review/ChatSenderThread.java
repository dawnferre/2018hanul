package review;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ChatSenderThread extends Thread{
	//기본생성자로 main의 socket을 넘겨받기: 소켓을 연결한 곳이 메인이기때문에 소켓설정을 따로 하지 않고, 값을 받아온다.
	//생성자 로  받는 이유 : 기본생성자가 객체호출시 제일 먼저 실행되기 때문에..
	Socket socket;
	public ChatSenderThread(Socket socket) {
		this.socket = socket;
	}
	@Override
	public void run() {
		//클라이언트 ==>서버, 서버==>클라이언트 소켓에 있는 정보를 넘겨주는 스레드
		try {
			//정보 입력
			Scanner scan = new Scanner(System.in);
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			//계속 돌아야 하기 때문에 반복문을 줌.
			while(true){
				System.out.println("보내고자 하는 메세지를 입력해주세요.");
				String msg = scan.nextLine();
				if(msg.equalsIgnoreCase("bye")){
					System.out.println("클라이언트의 채팅이 종료됩니다.");
					break;
				}
				pw.println(msg);
				pw.flush();
			}
		} catch (Exception e) {
			System.out.println("sender Thread");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("senderThread finally");
				e.printStackTrace();
			}
		}
		
	}

}
