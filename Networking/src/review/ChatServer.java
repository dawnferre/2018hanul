package review;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer {
	public static void main(String[] args) {
		ServerSocket ss= null;
		Socket socket = null;
		
		try {
			ss= new ServerSocket(9999);
			System.out.println("서버가 구동중입니다.");
			socket = ss.accept();
			
			ChatSenderThread st = new ChatSenderThread(socket);
			st.start();
			
			ChatReceiverThread rt = new ChatReceiverThread(socket);
			rt.start();
		} catch (Exception e) {
			System.out.println("ChatServer");
			e.printStackTrace();
		}
		//thread에서 계속 실행하기때문에 socket설정해주면 처음 실행후 소켓이 닫히기 때문에 exception발생.
		
	}//main
}//class
