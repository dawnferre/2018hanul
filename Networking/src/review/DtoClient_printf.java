package review;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class DtoClient_printf {
	public static void main(String[] args) {
		Socket socket = null;
		
		try {
			socket = new Socket("127.0.0.1", 9000);
			//메세지 입력
			Scanner scan = new Scanner(System.in);
			System.out.println("첫번째 상수를 입력해주세요.");
			double su1 = scan.nextDouble();
			System.out.println("두번째 상수를 입력해주세요.");
			double su2 = scan.nextDouble();
			System.out.println("연산자를 입력해주세요.");
			String opcode = scan.next();
			scan.close();
			
			PrintfDTO dto = new PrintfDTO(su1, su2, opcode);
			//서버로 메세지 보내기
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(dto);
			oos.flush();
			
			//서버로부터 메세지받기
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String msg = br.readLine();
			System.out.println("서버로부터 : "+msg);
		} catch (Exception e) {
			System.out.println("DtoClient printf");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("DtoClient printf finally");
				e.printStackTrace();
			}
		}
	}//main
}//class
