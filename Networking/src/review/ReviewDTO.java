package review;

import java.io.Serializable;

public class ReviewDTO implements Serializable{
	int su1;
	int su2;
	String opcode;
	public ReviewDTO() {
	}
	public ReviewDTO(int su1, int su2, String opcode) {
		super();
		this.su1 = su1;
		this.su2 = su2;
		this.opcode = opcode;
	}
	public int getSu1() {
		return su1;
	}
	public void setSu1(int su1) {
		this.su1 = su1;
	}
	public int getSu2() {
		return su2;
	}
	public void setSu2(int su2) {
		this.su2 = su2;
	}
	public String getOpcode() {
		return opcode;
	}
	public void setOpcode(String opcode) {
		this.opcode = opcode;
	}
	
	
}
