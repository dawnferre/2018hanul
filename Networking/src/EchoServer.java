import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

//Echoserver : 정보를 제공. =>
//tcp : 연결 경로설정, 오류검사수행, 속도는 느리지만, 신뢰성이 높다.
//udp :연결 설정 x, 속도빠르나, 신뢰성이 낮아서 많이 쓰이지 않음.
public class EchoServer {
	public static void main(String[] args) {
		//클라이언트의 요청이 오면 소켓생성.- 2개의 소켓생성. : serversocket(포트번호지정), socket	
		ServerSocket sever = null;
		Socket socket = null;
		try {
			//1. 소켓생성(serversocket)
			sever = new ServerSocket(9008); //포트번호지정.
			System.out.println("서버가 구동중입니다.");
			
			//2.클라이언트 접속정보가 들어있는 소켓생성.
//			socket = new Socket();
			socket =sever.accept(); //책 예제.- 다형성으로 부모의 객체를 가져다 쓰는 것.
			
			//먼저 클라이언트로부터 들어오는 메세지를 받음.
			 BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String result = br.readLine();
			
			//localport 번호를 가져올때
			int num = socket.getLocalPort();
			//클라이언트의 ip주소를 가져온다.
			InetAddress ip =socket.getInetAddress();
			String ip2 = ip.getHostAddress();
			
			//출력
			//bufferedReader는 한번 생성시켜놓고(한번 생성되고 나서 끝남), 메세지만 무한루프로 빼게 되면 
			//계속 실행되니까.. 여러개할때는 thread이용(inputstream하고는 다른 방식)
			System.out.println("클라이언트 : "+result);
				
			
			//다시 클라이언트로 메세지를 보낸다(송신)
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			pw.println(result); //enter처리
			pw.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				socket.close();
				sever.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}//main
}
