import java.net.Socket;

public class ChatClient {
	public static void main(String[] args) {
		//쓰레드 개념을 사용해서 여러개를 주고 받기 
		Socket socket = null;
		
		try {
			socket = new Socket("192.168.0.34",9008);
			
			//보내는 thread : 키보드로부터 메세지를 입력받아 서버로 보내는 작업 ==>chatsenderThread
			Thread th = new ChatSenderThread(socket);//객체생성
			//다형성
			th.start();
			
			//받는 thread : 메세지를 받아서 화면에 출력하는 작업 ==>chatreceiverThread
			//다형성
			Thread th1 = new ChatReceiverThread(socket);//객체생성
//			th1.start(); //현재 client에서만 보낼수 있으니 받는 thread필요없음 
			
		} catch (Exception e) {
			System.out.println("client");
			e.printStackTrace();
		}
	}//main
}//class
