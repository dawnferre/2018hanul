import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DecimalFormat;

public class CalcServer {
	public static void main(String[] args) {
		ServerSocket server = null;
		Socket socket = null;
		try {
			server = new ServerSocket(9008); //서버 -포트번호 할당.
			System.out.println("서버가 구동중입니다.");
			
			socket =server.accept(); //책 예제.- 다형성으로 부모의 객체를 가져다 쓰는 것.
			
			//dto로 넘어온 값 받기
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			//개체의 역직렬화
			CalcDTO dto = (CalcDTO)(ois.readObject());
			System.out.println("넘어온 su1 :"+ dto.getSu1());
			System.out.println("넘어온 su2 :"+dto.getSu2());
			System.out.println("넘어온 opcode :"+dto.getOpcode());
			//변수에 할당함.
			int su1 = dto.getSu1();
			int su2 = dto.getSu2();
			String opcode = dto.getOpcode();
			
			String result=null;
			//opcode조건 점검
			if(opcode.equals("+")){
				result = (su1+su2)+"";
			}else if(opcode.equals("-")){
				result = (su1-su2)+"";
			}else if(opcode.equals("*")){
				result = (su1*su2)+"";
			}else if(opcode.equals("/")){
				double su11 = Double.parseDouble(su1+"");
				double su22 = Double.parseDouble(su2+"");
				//format처리
				DecimalFormat de = new DecimalFormat("#.##");
				result =de.format(su11/su22);
				
			}
			System.out.println("서버의 계산결과 :"+result);
			
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			pw.println(result);
			pw.flush();
			
		}catch(Exception e){
			System.out.println("calcServer오류");
		}finally{
			try {
				socket.close();
				server.close();
			} catch (Exception e) {
				System.out.println("Server finally오류");
				e.printStackTrace();
			}
		}
	}//main
}//class
