package multichat;

import java.io.IOException;
import java.net.Socket;

public class Client {
	public static void main(String[] args) {
		Socket socket = null;
		
		try {
			socket = new Socket("127.0.0.1", 9999);
			
			Thread st = new SenderThread(socket, args[0]);
			st.start();
			
			Thread rt = new ReceiveThread(socket);
			rt.start();

		} catch (Exception e) {
			System.out.println("client");
//			e.printStackTrace();
		}
		
	}
}
