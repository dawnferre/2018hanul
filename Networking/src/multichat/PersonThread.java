package multichat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

public class PersonThread extends Thread {
	Socket socket;
	PrintWriter pw;
	ArrayList<PrintWriter> list = new ArrayList<>();
	public PersonThread(Socket socket) {
		this.socket = socket;
		list.add(pw);
	}
	@Override
	public void run() {
		InetAddress addr = socket.getInetAddress();
		String ip = addr.getHostAddress();
		try {
			
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String name = br.readLine();
			sendAll("#"+name+"님이 입장하셨습니다.");
			
			while(true){
				sendAll(name+">"+br.readLine());
				
			}
		} catch (IOException e) {
			System.out.println(ip+"사용자가 퇴장하였습니다.");
//			System.out.println("PersonThread Run");
//			e.printStackTrace();
		}
		
	}
	public void sendAll(String string){
		for(PrintWriter pw: list){
			pw.println(string);
			pw.flush();
		}
	}

}
