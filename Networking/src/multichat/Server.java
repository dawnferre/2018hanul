package multichat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	public static void main(String[] args) {
		ServerSocket ss = null;
		Socket socket = null;
		
		try {
			ss = new ServerSocket(9999);
			System.out.println("서버가 구동되었습니다.");
			while(true){
				socket = ss.accept();
				
				Thread th = new PersonThread(socket);
				th.start();

				Thread st = new SenderThread(socket, args[0]);
				st.start();
				
				Thread rt = new ReceiveThread(socket);
				rt.start();
			}
			
			
		} catch (Exception e) {
//			System.out.println("Server");
//			e.printStackTrace();
		}
		
		
	}
	
	
	
}
