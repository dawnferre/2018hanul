package multichat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ReceiveThread extends Thread{
	Socket socket;
	public ReceiveThread(Socket socket) {
		this.socket = socket;
	}
	@Override
	public void run() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String msg = br.readLine();
			while(true){
				if(msg == null){
					System.out.println("받은 메세지가 없어 종료됩니다_ receiveThread");
					System.exit(0);
					break;
				}
				System.out.println(msg);
			}
		} catch (IOException e) {
			System.out.println("Receive Thread run");
//			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (IOException e) {
				System.out.println("Receive Thread run finally");
//				e.printStackTrace();
			}
		}
		
	}

}
