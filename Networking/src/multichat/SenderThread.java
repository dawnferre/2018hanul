package multichat;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class SenderThread extends Thread{
	Socket socket;
	String user;
	PrintWriter pw;
	public SenderThread(Socket socket, String user) {
		this.socket  = socket;
		this.user = user;
	}
	@Override
	public void run() {
		String msg = null;
		Scanner scan = new Scanner(System.in);
			try {
				pw =new PrintWriter(socket.getOutputStream());
				while(true){
					msg = scan.nextLine();
					System.out.println(msg);
					if(msg.equalsIgnoreCase("bye")){
						System.out.println("채팅을 종료합니다_senderThread");
						System.exit(0);
						break;
					}
				}
				pw.println(msg);
				pw.flush();
			} catch (IOException e) {
				System.out.println("SenderThread run");
//				e.printStackTrace();
			}finally{
				try {
					socket.close();
				} catch (IOException e) {
					System.out.println("SenderThread run finally");
//					e.printStackTrace();
				}
			}
		scan.close();
	}
	
}
