import java.io.Serializable;

public class CalcDTO implements Serializable { //객체의 직렬화
	private static final long serialVersionUID = 1L;
	
	private int su1;
	private int su2;
	private String opcode;
	
	public int getSu1() {
		return su1;
	}
	public void setSu1(int su1) {
		this.su1 = su1;
	}
	public int getSu2() {
		return su2;
	}
	public void setSu2(int su2) {
		this.su2 = su2;
	}
	public String getOpcode() {
		return opcode;
	}
	public void setOpcode(String opcode) {
		this.opcode = opcode;
	}
	//default생성자
	public CalcDTO(){
		
	}
	public CalcDTO(int su1, int su2, String opcode) {
		super();
		this.su1 = su1;
		this.su2 = su2;
		this.opcode = opcode;
	}
	
	
}
