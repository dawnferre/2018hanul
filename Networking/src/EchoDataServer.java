import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class EchoDataServer {
	public static void main(String[] args) {
		DatagramSocket socket = null;
		try {
			socket = new DatagramSocket(9008);
			System.out.println("서버에 연결되었습니다.");
			
			byte[] buf = new byte[256];
			DatagramPacket dp = new DatagramPacket(buf, buf.length);
			socket.receive(dp);
			
			String msg = new String(dp.getData(), 0, dp.getLength());
			System.out.println("받은 메세지 :"+msg);
			
			InetAddress addr = dp.getAddress();
			int port = dp.getPort();
			dp = new DatagramPacket(buf, buf.length,addr,port);
			socket.send(dp);
			System.out.println("서버가 종료되었습니다.");
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			socket.close();
		}
	}//main
}//class
