import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

public class ChatReceiverThread extends Thread{
	//받는 쓰레드 :입력받아 보낸 메세지를 받는다.
	
	private Socket socket;
	public ChatReceiverThread(Socket socket){
		this.socket = socket;
	}
	
	@Override
	public void run() {
		try {
			//입력받는 다.
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String msg;
			while(true){
				InetAddress addr = InetAddress.getLocalHost();
				String ip = addr.getHostAddress();
				if((msg=br.readLine())==null){
					System.out.println("종료되었습니다.");
					System.exit(0);
					break;
				}
				System.out.println("메세지 (ip: "+ip+"):"+msg);
			}
			socket.close();
		} catch (Exception e) {
			System.out.println("클라이언트와 연결이 끊어졌습니다.");
			System.out.println("★종료됩니다★");
			System.exit(0);
//			e.printStackTrace();
		}
	}
}
