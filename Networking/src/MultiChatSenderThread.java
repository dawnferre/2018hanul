import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class MultiChatSenderThread extends Thread {
	private Socket socket;
	private String msg;//arg[0]
	public MultiChatSenderThread(Socket socket, String msg) {
		this.socket = socket;
		this.msg = msg;
	}
	@Override
	public void run() {
		try {
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			pw.print(msg);//대화명 
			pw.flush();
			
			Scanner scan = new Scanner(System.in);
			while(true){
				System.out.print("보내는 메세지 :");
				String message = scan.nextLine();
				
				if(message.equalsIgnoreCase("bye")){
					System.out.println("채팅이 종료됩니다. 안녕★");
					System.exit(0);
					break;
				}
				pw.println(message);
				pw.flush();
			}
			scan.close();
			
		} catch (Exception e) {
			System.out.println("senderThread");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("senderThread finally");
				e.printStackTrace();
			}
		}
		
		
	}//run
}
