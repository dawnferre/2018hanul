import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

//UDP : 속도는 빠르지만 신뢰성이 낮다 ==>데이터그램.
//신뢰성이 낮아 거의 쓰이지 않음.
public class EchoDataClient {
	public static void main(String[] args) {
		DatagramSocket dgs = null;
		byte[] buf ={'g','o','o','d','d','a','y'};

		try {
			dgs= new DatagramSocket();
			InetAddress addr = InetAddress.getByName("localhost");
			DatagramPacket dp = new DatagramPacket(buf,buf.length,addr,9008);
			dgs.send(dp);
			
			DatagramPacket dp2 = new DatagramPacket(buf, buf.length);
			dgs.receive(dp2);
			String msg = new String(dp.getData(), 0, dp.getLength());
			System.out.println("받은 메세지 :"+msg);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}//main
}//class
