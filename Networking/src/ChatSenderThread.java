import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ChatSenderThread extends Thread{
// 메세지를 보내주는 쓰레드 :데이터를 입력받아 수행하는 메소드.
	//생성자로 socket처리
	private Socket socket;
	public ChatSenderThread(Socket socket) {
		this.socket = socket;
	}
	
	@Override
	public void run() {
		try {
			//서버로 보내기 위해.
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			//메세지 입력
			Scanner scan = new Scanner(System.in);

			while(true){
				System.out.println("보내실 메세지를 입력하세요.");
				String msg = scan.nextLine();
				//종료문자설정
				if(msg.equalsIgnoreCase("bye")){
					System.out.println("종료되었습니다.");
					System.exit(0); //프로그램완전종료
					break; //반복문종료.
				}
				pw.println(msg);
				pw.flush();
				
			}
			scan.close();
			socket.close();
		} catch (Exception e) {
			System.out.println();
			System.out.println("sender run"+e.getMessage());
			e.printStackTrace();
		}
				
	}
}
