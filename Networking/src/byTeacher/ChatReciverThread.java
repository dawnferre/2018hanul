package byTeacher;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

public class ChatReciverThread extends Thread{
	//받는 Thread : 메세지를 받아서 출력하는 작업
	private Socket socket;
	public ChatReciverThread(Socket socket){
		this.socket = socket;
	}
	
	@Override
	public void run() {
		try {
			InputStream is = socket.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));	//입력(수신)
			while(true){
				String msg = br.readLine();
				if(msg == null){
					System.out.println("종료되었습니다.");
					break;
				}
				InetAddress addr = socket.getInetAddress();
				String ip = addr.getHostAddress();
				System.out.println("수신메세지(" + ip + ") ▶ " + msg);
			}
			socket.close();
			System.exit(0);
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("서버에 접속할 수 없습니다.");
		}
	}
}





