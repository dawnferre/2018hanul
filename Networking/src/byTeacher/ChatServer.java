package byTeacher;

import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer {
	public static void main(String[] args) {
		ServerSocket ss = null;
		Socket socket = null;
		try {
			ss = new ServerSocket(9008);
			System.out.println("서버가 구동중입니다.");
			socket = ss.accept();
			
			//보내는 Thread : 키보드로부터 메세지를 입력받아 보내는 작업 ▶ ChatSenderThread.java
			Thread st = new ChatSenderThread(socket);
			st.start();
			
			//받는 Thread : 메세지를 받아서 화면에 출력하는 작업 ▶ ChatReciverThread.java
			Thread rt = new ChatReciverThread(socket);
			rt.start();
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("서버에 접속할 수 없습니다.");
		}
	}
}
