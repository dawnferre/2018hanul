package byTeacher;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class MultiChatSenderThread extends Thread{
	private Socket socket;
	private String message;
	
	public MultiChatSenderThread(Socket socket, String message){
		this.socket = socket;
		this.message = message;
	}
	
	@Override
	public void run() {
		try {
			PrintWriter pw = new PrintWriter(socket.getOutputStream());	//출력(송신)
			pw.println(message);	//args[0] : message ▶ 대화명
			pw.flush();
			Scanner scanner = new Scanner(System.in);
			while(true){
				//System.out.print("메세지를 입력하세요 : ");
				String msg = scanner.nextLine();
				if(msg.equalsIgnoreCase("bye")){
					System.out.println("종료되었습니다.");
					System.exit(0);
					break;
				}
				pw.println(msg);
				pw.flush();
			}
			scanner.close();
			socket.close();
		} catch (Exception e) {
			//e.printStackTrace();
			//System.out.println("MultiChatSenderThread 예외");
			System.out.println("서버와 접속이 종료되었습니다.");
			System.exit(0);
		}
	}
}//class











