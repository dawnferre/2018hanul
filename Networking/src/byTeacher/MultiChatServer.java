package byTeacher;

import java.net.ServerSocket;
import java.net.Socket;

public class MultiChatServer {
	public static void main(String[] args) {
		ServerSocket ss = null;
		Socket socket = null;
		try {
			ss = new ServerSocket(9008);
			System.out.println("서버가 구동중입니다.");
			while(true){
				socket = ss.accept();
				Thread th = new MultiChatPersonThread(socket);
				th.start();
			}
		} catch (Exception e) {
			//e.printStackTrace();
			//System.out.println("MultiChatServer 예외");
			System.out.println("서버와 접속이 종료되었습니다.");
			System.exit(0);
		}
	}
}
