package byTeacher;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class MultiChatReciverThread extends Thread{
	private Socket socket;
	
	public MultiChatReciverThread(Socket socket){
		this.socket = socket;
	}
	
	@Override
	public void run() {
		try {
			InputStream is = socket.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));	//입력(수신)
			while(true){
				String message = br.readLine();
				if(message == null){
					System.out.println("종료되었습니다.");
					System.exit(0);
					break;
				}
				System.out.println(message);
			}			
		} catch (Exception e) {
			//e.printStackTrace();
			//System.out.println("MultiChatReciverThread 예외");
			System.out.println("서버와 접속이 종료되었습니다.");
			System.exit(0);
		}
	}
}//class




