package byTeacher;

import java.net.Socket;

public class MultiChatClient {
	public static void main(String[] args) {
		Socket socket = null;
		try {
			socket = new Socket("192.168.0.34", 9008);	//서버IP주소, 서버Port번호
			
			//메세지를 읽어서 서버로 보내는 Thread
			Thread st = new MultiChatSenderThread(socket, args[0]);
			st.start();
			
			//메세지를 받아서 화면에 출력하는 Thread
			Thread rt = new MultiChatReciverThread(socket);
			rt.start();
		} catch (Exception e) {
			//e.printStackTrace();
			//System.out.println("MultiChatClient 예외");
			System.out.println("서버와 접속이 종료되었습니다.");
			System.exit(0);
		}
	}
}
