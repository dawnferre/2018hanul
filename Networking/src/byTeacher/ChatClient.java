package byTeacher;

import java.net.Socket;

public class ChatClient {
	public static void main(String[] args) {
		Socket socket = null;
		try {
			socket = new Socket("127.0.0.1", 9008);
			
			//보내는 Thread : 키보드로부터 메세지를 입력받아 보내는 작업 ▶ ChatSenderThread.java
			Thread st = new ChatSenderThread(socket);
			st.start();
			
			//받는 Thread : 메세지를 받아서 화면에 출력하는 작업 ▶ ChatReciverThread.java
			Thread rt = new ChatReciverThread(socket);
			rt.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
