package byTeacher;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ChatSenderThread extends Thread{
	//보내는 Thread : 메세지를 입력받아 보내는 작업
	private Socket socket;
	public ChatSenderThread(Socket socket){
		this.socket = socket;
	}
	
	@Override
	public void run() {
		try {
			PrintWriter pw = new PrintWriter(socket.getOutputStream());	//출력(송신)
			Scanner scanner = new Scanner(System.in);
			while(true){
				System.out.print("메세지를 입력하세요 : ");
				String msg = scanner.nextLine();
				if(msg.equalsIgnoreCase("bye")){
					System.out.println("종료되었습니다.");
					break;
				}
				pw.println(msg);
				pw.flush();
			}
			scanner.close();
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}





