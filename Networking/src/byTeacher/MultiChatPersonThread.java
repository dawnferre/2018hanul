package byTeacher;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

public class MultiChatPersonThread extends Thread{
	private Socket socket;
	private PrintWriter pw;
	static ArrayList<PrintWriter> list = new ArrayList<PrintWriter>();
	
	public MultiChatPersonThread(Socket socket){
		this.socket = socket;
		try {
			pw = new PrintWriter(socket.getOutputStream());	//출력스트림
			list.add(pw);	//클라이언트의 모든 출력스트림을 저장할 배열
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		String name = null;
		InetAddress addr = socket.getInetAddress();
		String ip = addr.getHostAddress();
		try {
			//클라이언트에서 맨처음에 입력된 메세지(대화명)를 받아
			//접속된 클라이언트의 화면에 출력 : sendAll()
			InputStream is = socket.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			name = br.readLine();
			sendAll("#" + name + "(" + ip + ")님이 입장하셨습니다.");
			//#홍길동(192.168.0.X)님이 입장하셨습니다.
			
			//클라이언트에서 입력한 대화내용(메세지)을 받아
			//접속된 클라이언트 화면에 출력 : sendAll()
			while(true){
				String message = br.readLine();
				if(message == null){
					break;
				}
				sendAll(name + "(" + ip + ") : " + message);
				//홍길동(192.168.0.X) : 대화내용
			}
		} catch (Exception e) {
			//e.printStackTrace();
			//System.out.println("MultiChatPersonThread 예외");
			System.out.println(ip + " : 서버와 접속이 종료되었습니다.");
			System.exit(0);
		} finally {
			list.remove(pw);
			sendAll("#" + name + "(" + ip +  ")님이 퇴장하셨습니다.");
			//#홍길동(192.168.0.X)님이 퇴장하셨습니다.
			try {
				socket.close();
			} catch (Exception e) {
				//e.printStackTrace();
				System.out.println("서버와 접속이 종료되었습니다.");
				System.exit(0);
			}
		}
	}
	
	public void sendAll(String message){
		for (PrintWriter pw : list) {
			pw.println(message);
			pw.flush();
		}
	}
}//class









