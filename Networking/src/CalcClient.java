import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class CalcClient {
	public static void main(String[] args) {
		Socket socket = null; //전역변수
		try {
			//소켓생성
			socket = new Socket("192.168.0.16", 9008); 
			
			//정보입력- 여러가지 
			
			
			Scanner scan = new Scanner(System.in);
			System.out.println("첫번째 수를 입력하세요.");
			int su1 = Integer.parseInt(scan.nextLine());
			System.out.println("두번째 수를 입력하세요.");
			int su2 = Integer.parseInt(scan.nextLine());
			String opcode = null;
			//예외처리 _ while문으로 처리함.
			while(true){
				System.out.println("연산자를 입력해주세요(사칙연산 +,-,*,/)");
				opcode = scan.nextLine();
				if(!opcode.equals("+")|opcode.equals("-")|opcode.equals("*")|opcode.equals("/")){
					System.out.println("사칙연산기호를 입력해주세요.");
					continue;
				}
				break;
			}
			
			scan.close();
			//dto에 넣어주기
			CalcDTO dto = new CalcDTO(su1, su2, opcode);
			//dto객체를  서버로 전송 ==>출력
//			PrintWriter pw = new PrintWriter(socket.getOutputStream());
//			pw.println(dto); //문자열 전송.
//			pw.flush();
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(dto);
			oos.flush();
			
			//서버에서 넘어온 값 출력
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String result = br.readLine();
			System.out.println("서버에서 넘어온 메세지 :"+result);
		}catch(Exception e){
			System.out.println("calcClient오류");
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("Client finally오류");
				e.printStackTrace();
			}
		}
	}//main
}//class
