import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

//Echoclient =정보요청 (질의, request)
public class EchoClient {
	public static void main(String[] args) {
		//서버에 접속을 시도하여 접속이 성공되면 socket생성
		//socket :서버의 ip주소 + 서버의 port번호
		
		Socket socket = null;
		try {
			socket = new Socket("127.0.0.1", 9008); //서버의 ip주소, port번호를 가지고 옴.
//			InputStream is = socket.getInputStream(); //소켓으로부터 입력을 받아옴.
//			InputStreamReader isr = new InputStreamReader(is);
			//입력준비
			//socket 출력준비
//			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			//출력 간단히._ bw를 간단히 줄인것.
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			
			
			
			//키보드에서 메세지를 입력받아
			Scanner scan = new Scanner(System.in);
			System.out.println("서버로 보낼 메세지를 입력하세요.");
			String msg = scan.next();
			scan.close();
			// 서버로 보내고 (송신_출력)
			pw.println(msg);
			pw.flush(); 
			// 송신후 소켓을 닫아야 한다
//			pw.close();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			//서버로부터 메세지 수신.
			String serverMsg;
			serverMsg = br.readLine();
			System.out.println("서버에서 온 메세지 : "+serverMsg);
			
			//서버에서 보낸 메세지를 받아(수신) 출력.
		} catch (Exception e) {
			System.out.println("EchoClient예외발생");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("EchoClient finally 오류");
				e.printStackTrace();
			}
		}
	}//main
}
