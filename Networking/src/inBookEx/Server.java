package inBookEx;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	public static void main(String[] args) {
		ServerSocket server = null;
		Socket socket =null;
		try {
			server = new ServerSocket(9999);
			socket = server.accept(); // 연결요청이 오면 소켓생성.(즉, 클라이언트에서 메세지를 보내야 소켓을 설정)
			InputStream in = socket.getInputStream();
			OutputStream out = socket.getOutputStream();

			byte[] arr = new byte[100];
			in.read(arr);
			System.out.println(new String(arr));
			
			String str = "Hello, Client";
			out.write(str.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}//main
}//class
