package inBookEx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ReceiverThread extends Thread {
	private Socket socket = null;
	public ReceiverThread(Socket socket) {
		this.socket = socket;
	}
	
	@Override
	public void run() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			while(true){
				String str = br.readLine();
				if(str == null){
					System.out.println("채팅이 종료되었어요!");
					System.exit(0);
					break;
				}
				System.out.println("채팅: "+str);
			}
		} catch (Exception e) {
			System.out.println("ReceiverThread run");
			e.printStackTrace();
			
		}
		
		
	}

}
