package inBookEx;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SeverExample {
	public static void main(String[] args) {
		ServerSocket ss = null;
		Socket socket = null;
		
		try {
			ss = new ServerSocket(9999);
			System.out.println("서버가 구동되었습니다.");
			//클라이언트는 수를 지정할 수 없지만 여러명이 들어오니 반복문 처리
			while(true){
				socket = ss.accept();
				//클라이언트 수를 지정해서 list에 넣어주는 메소드
				Thread thread = new PerClientThread(socket);
				thread.start();
				
			}
			
		} catch (IOException e) {
			System.out.println("SeverSample");
			e.printStackTrace();
		}
	}
}
