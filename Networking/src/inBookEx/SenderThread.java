package inBookEx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class SenderThread extends Thread{
	private Socket socket;
	private String arg;
	public SenderThread(Socket socket, String arg) {
		this.socket = socket;
		this.arg = arg;
	}
	
	@Override
	public void run() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			
			pw.println(arg);
			pw.flush();
			Scanner scan = new Scanner(System.in);
			while(true){
				String str = scan.nextLine();
				if(str.equalsIgnoreCase("bye")){
					System.out.println("채팅이 종료됩니다. 뿅");
					System.exit(0);//프로그램종료
					break;
				}
				pw.print(str);
				pw.flush();
			}
			scan.close();
		} catch (Exception e) {
			System.out.println("SenderThread run");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("SenderThread run finally");
				e.printStackTrace();
			}
		}
	}
	

}
