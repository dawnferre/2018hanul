package inBookEx;

import java.net.Socket;

public class ClientSample {
	public static void main(String[] args) {
		if(args.length != 1){
			System.out.println("java ClientSample <username> 입력해야 실행가능합니다.");
			return;
		}
		Socket socket = null;
		try {
			socket = new Socket("127.0.0.1", 9999);
			Thread st = new SenderThread(socket, args[0]);
			st.start();
			
			Thread rt = new ReceiverThread(socket);
			rt.start();
			
		} catch (Exception e) {
			System.out.println("ClientSample");
			e.printStackTrace();
		}
		
		
	}//main
}//class
