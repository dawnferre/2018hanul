import java.net.ServerSocket;
import java.net.Socket;

public class MultiChatServer {
	public static void main(String[] args) {
		ServerSocket ss = null;
		Socket socket = null;
		try {
			ss = new ServerSocket(9008);
			System.out.println("서버가 구동중입니다.");
			
			//사용자가 얼마인지 알 수 없어서 사용자 각각의 소켓
			while(true){
				socket = ss.accept();
				Thread th =new MultiChatPersonThread(socket);
				th.start();
			}
		} catch (Exception e) {
			System.out.println("multiServer");
			e.printStackTrace();
		}
	}//main
}//class
