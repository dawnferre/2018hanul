package practice;

import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class PracticeClient {
	public static void main(String[] args) {
		Socket socket = null;
		
		try {
			socket = new Socket("127.0.0.1", 9908);
			
			Scanner scan = new Scanner(System.in);
			System.out.println("첫번째 수를 입력해주세요.");
			int su1 = scan.nextInt();
			System.out.println("두번째 수를 입력해주세요.");
			int su2 = scan.nextInt();
			System.out.println("연산자를 입력해주세요. (+,-,*,/)");
			String opcode = scan.next();
			scan.close();
			
			PracticeDTO dto = new PracticeDTO(su1, su2, opcode);
			
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(dto);
			oos.flush();
			
		} catch (Exception e) {
			System.out.println("client오류");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("client finally");
				e.printStackTrace();
			}
		}
	}//main
}//class
