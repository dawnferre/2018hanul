package practice;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServer {
	public static void main(String[] args) {
		//1.소켓 설정(서버소켓, 클라이언트소켓)
		ServerSocket server = null;
		Socket socket = null;
		
		//2.서버 생성.
		try {
			server = new ServerSocket(9999);//반드시 클라이언트 서버와 동일한 포트번호.
			System.out.println("서버가 구동중입니다.");
			//3. 클라이언트 소켓 설정.
			socket = server.accept();
			
			//4. 클라이언트에서 넘어온 메세지 입력준비(읽음)_문자열처리 (inputStreamReader)
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String msg=br.readLine();
			
			//5.ip번호 
				 InetAddress addr =socket.getInetAddress();
				 String ip = addr.getHostAddress();
			//6.port번호
				 int portnum = socket.getLocalPort();
			
				 //7.클라이언트 메세지 출력.
			System.out.println("클라이언트의 메세지(IP:"+ip+",Port:"+portnum+") : "+msg);
			
				//8.서버에서 클라이언트로 메세지 보내기
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			pw.println(msg);
			pw.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	
		
	}//main 
}//class
