package practice;

import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class PracticeServer {
	public static void main(String[] args) {
		ServerSocket ss = null;
		Socket socket = null;
		
		try {
			ss = new ServerSocket(9908);
			System.out.println("서버가 구동되었습니다.");
			socket = ss.accept();
			
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			PracticeDTO dto =(PracticeDTO)(ois.readObject());
			System.out.println("클라이언트의 수1: "+dto.getSu1());
			System.out.println("클라이언트의 수2: "+dto.getSu2());
			System.out.println("클라이언트의 연산자 : "+dto.getOpcode());
			
			String result=null;
			if(dto.getOpcode().equals("+")){
				result = dto.getSu1()+dto.getSu2()+"";
				
			}else if(dto.getOpcode().equals("-")){
				result = dto.getSu1()-dto.getSu2()+"";
				
			}else if(dto.getOpcode().equals("*")){
				result = dto.getSu1()*dto.getSu2()+"";
			
			}else if(dto.getOpcode().equals("/")){
				result = dto.getSu1()/dto.getSu2()+"";
		
			}
			//서버에서 클라이언트로 넘기기.
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			pw.println(result);
			
		} catch (Exception e) {
			System.out.println("서버 오류");
			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("server finally");
				e.printStackTrace();
			}
		}
	}//main
}//class
