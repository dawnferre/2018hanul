package practice;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class EchoClient {
	public static void main(String[] args) {
		//1.소켓생성
		Socket socket=null;
		BufferedReader br= null;
		PrintWriter pw = null;
		//ip주소 변수로 설정해서 고치기 편하게
		String hanul ="192.168.0.16";
		String home ="";
		
		try {
			socket = new Socket(hanul, 9999);//연결하고자 하는 서버의 ip주소, port번호 동일해야한다. 
//			socket.getInputStream(); //입력을 받을 준비
			
			//1.buffered로 입력을 받음. _서버에서 메세지를 가지고 옴.
			br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			//2.출력준비.
			//2-1.buffered로 받는 방법: bufferd 기능을 가지고 있음.
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			//2-2.printwriter로 받는 방법
			pw = new PrintWriter(socket.getOutputStream());
			
			//3. 사용자의 메세지입력
			Scanner scan = new Scanner(System.in);
			System.out.println("보낼 메세지를 입력해주세요.");
			String msg = scan.next();
			scan.close();
			
			//4. 서버로 메세지 보내기
			pw.println(msg);
			pw.flush();
			
			
			//5.클라이언트에 서버에서 보낸 메세지 출력
			String serverMsg = br.readLine();
			System.out.println("서버에서 온 메세지 : "+serverMsg);
			
		} catch (Exception e) {
			e.getStackTrace();
		}finally{
			//5.반드시 처리될 애들 ==>송신이 제대로 되지 않더라도 소켓을 제때 닫아주는게 중요하다.
			try {
				socket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}//main
}//class
