import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer {
	public static void main(String[] args) {
		ServerSocket ss = null;
		Socket socket = null;
		
		try {
			ss = new ServerSocket(9908);
			System.out.println("서버가 구동중입니다.");
			socket = ss.accept();
			
			//보내는 thread : 키보드로부터 메세지를 입력받아 서버로 보내는 작업 ==>chatsenderThread
			Thread th = new ChatSenderThread(socket);
//			th.start(); //현재client에서만 보낼 수 있으니 
			
			//받는 thread : 메세지를 받아서 화면에 출력하는 작업 ==>chatreceiverThread
			Thread th1 = new ChatReceiverThread(socket);
			th1.start();
			
		} catch (Exception e) {
			System.out.println("서버와의 연결이 끊겼습니다.");
			e.printStackTrace();
		}
	}//main
}//class
