import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class MultiChatReceiverThread extends Thread{
	private Socket socket;
	public MultiChatReceiverThread(Socket socket) {
		this.socket = socket;
	}
	@Override
	public void run() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String msg;
			while(true){
				msg = br.readLine();
				if(msg == null){
					System.out.println("앗!채팅이 끝났어요.");
					System.exit(0);
					break;
				}
				System.out.println(msg);
			}
		} catch (Exception e) {
//			System.out.println("ReceiverThread");
			System.out.println("서버가 종료되었습니다.");
			System.exit(0);
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
				System.out.println("ReceiverThread finally");
				e.printStackTrace();
			}
		}
		
		
		
	}//run
}
