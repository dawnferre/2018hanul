import java.net.Socket;

public class MultiChatClient {
	public static void main(String[] args) {
		Socket socket = null;
		try {
			socket = new Socket("192.168.0.89",9008); //서버 ip주소, 서버포트번호
			//처음 프로그램 불러올때 대화명을 아예 지정할 수 있게끔 해줌.
			if(args.length!=1){
				System.out.println("user명을 지정해주세요 :java MultiClient <user-name>");
				return;
			}
			//메세지를 읽어서 서버로 보내는 thread                 명령형인수로 받는 메세지
			//java MultiChatClient 대화명 (arg[0])
			Thread st = new MultiChatSenderThread(socket,args[0]);
			st.start();
			//메세지를 받아서 화면에 출력하는 thread
			Thread rt = new MultiChatReceiverThread(socket);
			rt.start();
		} catch (Exception e) {
			System.out.println("multiChatClient");
			e.printStackTrace();
		}
	}//main
}//class
