import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

public class MultiChatPersonThread extends Thread{
	private Socket socket;
	private PrintWriter pw;
	static ArrayList<PrintWriter> list = new ArrayList<>();
	
	public MultiChatPersonThread(Socket socket) {
		this.socket = socket;
		
		try {
			//클라이언트의 출력스트림을 받아 list에 저장
			pw = new PrintWriter(socket.getOutputStream());
			list.add(pw);
		} catch (Exception e) {
			System.out.println("constructor personthread");
			e.printStackTrace();
		}
	}
	//★
	@Override
	public void run() {
		String name = null;
		InetAddress addr = socket.getInetAddress();
		String ip = addr.getHostAddress();
		
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			name = br.readLine(); //대화명
			//클라이언트에서 맨처음에 입력된 메세지(대화명)를 받아
			//접속된 클라이언트의 화면에 출력:sendAll()
			sendAll("#"+name+"("+ip+")님이 입장하셨습니다.");
			
			//클라이언트에서 입력한 대화내용(메세지)를 받아 접속된 클라이언트 화면에 출력: sendAll()
			while(true){
				String message = br.readLine();//클라이언트의 채팅
				if(message == null){
					break;
				}
				sendAll(name+"("+ip+"):"+message);
			}
		} catch (Exception e) {
//			System.out.println("personThread run");
			System.out.println(ip+" 서버와 접속이 종료되었습니다.");
		}finally{
			list.remove(pw);
			sendAll("#"+name+"("+ip+")님이 퇴장하셨습니다.");
			try {
				socket.close();
			} catch (Exception e2) {
//				System.out.println("personThread finally");
				System.out.println("서버와 접속이 종료되었습니다.");
			}
		}
		
	}
	public void sendAll(String msg){
		for(PrintWriter pw:list){
			pw.println(msg);
			pw.flush();
		}
	}
}
