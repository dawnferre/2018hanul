package com.hanul.study;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class MemberDAO {
	private Connection conn;
	private PreparedStatement pst;
	private ResultSet rs;
	//DB접속(연결) ==메소드
	public Connection getConn(){
		String url="jdbc:oracle:thin:@127.0.0.1:1521:xe";
		String user ="system";
		String password ="0000";
		
		try {
			Class.forName("oracle.jdbc.OracleDriver");//동적로딩
			conn = DriverManager.getConnection(url, user, password);
			
		} catch (Exception e) {
			System.out.println("커넥션 오류"+e.getMessage());
		}
		return conn;
	}
	public void disConn(){
		try {
			if(conn!=null){
				conn.close();
			}
			if(pst!=null){
				pst.close();
			}
			if(rs!=null){
				rs.close();
			}
		} catch (Exception e) {
			System.out.println("disconn오류"+e.getMessage());
		}
	}
	public int insert(MemberDTO dto){
		int n =0;
		conn =getConn();
		String sql="insert into tblMember values(?,?,?,?,?,?)";
		try {
//			irum varchar2(20),
//			id varchar2(20),
//			pw varchar2(20),
//			age number,
//			addr varchar2(50),
//			tel varchar2(20)
			pst = conn.prepareStatement(sql);
			pst.setString(1, dto.getIrum());
			pst.setString(2, dto.getId());
			pst.setString(3, dto.getPw());
			pst.setInt(4, dto.getAge());
			pst.setString(5, dto.getAddr());
			pst.setString(6, dto.getTel());
			
			n =pst.executeUpdate();
			
		} catch (Exception e) {
			System.out.println("insert 오류"+e.getMessage());
		}
		return n;
	}
	
	public ArrayList<MemberDTO> selectAll(){
		ArrayList<MemberDTO> list = new ArrayList<>();
	
		conn = getConn();
		String sql="select * from tblMember";
		try {
			pst = conn.prepareStatement(sql);
			rs =pst.executeQuery();
			
			while(rs.next()){
				MemberDTO dto = new MemberDTO();
				dto.setIrum(rs.getString("irum"));
				dto.setId(rs.getString("id"));
				dto.setPw(rs.getString("pw"));
				dto.setAge(rs.getInt("age"));
				dto.setAddr(rs.getString("addr"));
				dto.setTel(rs.getString("tel"));
				
				list.add(dto);
			}
		} catch (Exception e) {
			System.out.println("select오류"+e.getMessage());
			e.getStackTrace();
		}
		return list;
	}
	public int deleteMem(String id){
		int num =0;
		conn = getConn();
		String sql="delete from tblMember where id =?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, id);
			num =pst.executeUpdate();
			
		} catch (Exception e) {
			System.out.println("delete 오류"+e.getMessage());
		}
		return num;
	}
	public int updateMem(MemberDTO dto){
		int num =0;
		conn = getConn();
		String sql="update tblMember set irum=?,pw=?,age=?,addr=?,tel=? where id=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, dto.getIrum());
			pst.setString(2, dto.getPw());
			pst.setInt(3, dto.getAge());
			pst.setString(4, dto.getAddr());
			pst.setString(5, dto.getTel());
			pst.setString(6, dto.getId());
			
			
			num =pst.executeUpdate();
			
		} catch (Exception e) {
//			System.out.println("delete 오류"+e.getMessage());
		}
		return num;
	}
	public MemberDTO getbyId(String id){
		int num =0;
		conn = getConn();
		MemberDTO dto = new MemberDTO();
		String sql="select * from tblMember where id=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, id);
			rs =pst.executeQuery();
			
			if(rs.next()){
				dto.setIrum(rs.getString("irum"));
				dto.setId(rs.getString("id"));
				dto.setPw(rs.getString("pw"));
				dto.setAge(rs.getInt("age"));
				dto.setAddr(rs.getString("addr"));
				dto.setTel(rs.getString("tel"));
				
			}
		} catch (Exception e) {
			System.out.println("select오류"+e.getMessage());
			e.getStackTrace();
		}
		return dto;
	}
}
