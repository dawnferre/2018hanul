<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!--
	- scope.jsp 페이지에서  생성한 객체를 scopeUse.jsp로 넘기자.: 공유
	- Scope내장객체를 이용하여 객체를 생성: PageContext, request, session, application
	- 형식: 내장객체.setAttribute(key,value) >> request.setAttribute("dto",dto);
	- key: 식별자 , value: 공유하고자하는 객체(공유객체)
	- getAttribute(): 객체로 받기때문에 기본형으로 받고싶다면 wrapperClass로 받아야한다.★
	
	
	*정적페이지 전환 : 클릭등의 작동으로 인해 전환되는 것
	*동적페이지 전환: 서버내의 작동에 인해 전환되는 것
	
-->
<%
	
	pageContext.setAttribute("pageName", "홍길동");
	request.setAttribute("requestName", "Servlet/JSP");
	session.setAttribute("sessionName", "Android");
	application.setAttribute("application", "Spring");
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Scope</title>
</head>
<body>
	<%
		//object로 인식됨  == >  casting해줘야함
		String pageName =(String)(pageContext.getAttribute("pageName"));
		String requestName =(String)(request.getAttribute("requestName"));
		String sessionName = (String)(session.getAttribute("sessionName"));
		String applicationName = (String)(application.getAttribute("application"));
	%>
	<ul type="square">
		<li>pageContext : <%=pageName %></li>
		<li>request : <%=requestName %></li>
		<li>session : <%=sessionName %></li>
		<li>application : <%=applicationName %></li>
	</ul>
	<a href="scopeUse.jsp">scopeUse.jsp 페이지 이동</a>
</body>
</html>