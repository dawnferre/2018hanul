<%@page import="com.hanul.study.TwoHap"%><!--import하고 있는 파일. -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp02</title>
</head>
<body>
	<!-- 자바파일을 이용한 누적합 구하기 -->
	<%
		int sum =new TwoHap().totalSum(1, 100);
	%>
	
	두 수 사이의 누적합 : <%=sum %>
</body>
</html>