<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.hanul.study.*" %>
<!-- jsp11_Main.html요청 ==>전달받은 매개변수를 이용하여 dto객체 생성 (ActionTag)
	 jsp12.jsp 넘긴다(페이지전환: request forward방식)
 -->
 <% request.setCharacterEncoding("utf-8"); %>
 <%--액션태그를 이용해서 dto 객체를 생성함. --%>
 <jsp:useBean id="dto" class="com.hanul.study.MemberDTO">
	 <jsp:setProperty property="*" name="dto"/>
 </jsp:useBean>
 <%
 	request.setAttribute("dto", dto); //바인딩 객체 생성
//  	RequestDispatcher rd = request.getRequestDispatcher("jsp12.jsp");//페이지 호출
//  	rd.forward(request, response); //페이지전환 (받은 값을 객체로 넘겨줌)

 	String ipaddr = request.getRemoteAddr(); //클라이언트의 ip주소를 가져온다.
//  	request.setAttribute("ipaddr", ipaddr); //바인딩 객체 생성
 %>
<!-- forward Action tag -->
	<!-- forward Action tag안에 바인딩 하지않고 parameter형태의 객체로 보냄 -->
<jsp:forward page="jsp12.jsp"><!--안에 주석처리도 인식해서..-->
	<jsp:param value="<%=ipaddr%>" name="ipaddr"/>
</jsp:forward>