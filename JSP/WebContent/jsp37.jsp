<%@page import="com.hanul.study.MemberDTO"%>
<%@page import="java.util.ArrayList"%>
    <!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	ArrayList<MemberDTO> list = new ArrayList<>();
	list.add(new MemberDTO("홍길동","hong1","1111",2,"광주광역시","010-1234-5678"));
	list.add(new MemberDTO("김길동","hong2","1111",3,"광주광역시","010-1234-5678"));
	list.add(new MemberDTO("홍길동","hong3","1111",4,"광주광역시","010-1234-5678"));
	list.add(new MemberDTO("정길동","hong4","1111",5,"광주광역시","010-1234-5678"));
	list.add(new MemberDTO("박길동","hong5","1111",6,"광주광역시","010-1234-5678"));
	
	pageContext.setAttribute("list", list);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp37</title>
<style>
*{
	margin: 0 auto;
}
</style>
</head>
<body>
<table border="1">
	<caption><b>java for</b></caption>
		<tr>
			<th>이름</th>
			<th>아이디</th>
			<th>비번</th>
			<th>나이</th>
			<th>주소</th>
			<th>전화번호</th>
		</tr>
		<%for(int i =0; i<list.size(); i++){
			%>
			<tr>
			<td><%=list.get(i).getIrum() %></td>
			<td><%=list.get(i).getId() %></td>
			<td><%=list.get(i).getPw() %></td>
			<td><%=list.get(i).getAge() %></td>
			<td><%=list.get(i).getAddr() %></td>
			<td><%=list.get(i).getTel() %></td>
			<tr>
		<% }%>

</table>
<table border="1">
	<caption><b>java 향상for</b></caption>
		<tr>
			<th>이름</th>
			<th>아이디</th>
			<th>비번</th>
			<th>나이</th>
			<th>주소</th>
			<th>전화번호</th>
		</tr>
		<%for(MemberDTO dto: list){
			%>
			<tr>
			<td><%=dto.getIrum() %></td>
			<td><%=dto.getId() %></td>
			<td><%=dto.getPw() %></td>
			<td><%=dto.getAge() %></td>
			<td><%=dto.getAddr() %></td>
			<td><%=dto.getTel() %></td>
			<tr>
		<% }%>

</table>
<table border="1">
	<caption><b>EL태그</b></caption>
		<tr>
			<th>이름</th>
			<th>아이디</th>
			<th>비번</th>
			<th>나이</th>
			<th>주소</th>
			<th>전화번호</th>
		</tr>
			<tr>
			<!-- 너무 많아서 첫행만 가지고 온다... -->
				<td>${list[0].irum}</td>
				<td>${list[0].id}</td>
				<td>${list[0].pw}</td>
				<td>${list[0].age}</td>
				<td>${list[0].addr}</td>
				<td>${list[0].tel}</td>
			<tr>

</table>
<table border="1">
	<caption><b>JSTL core</b></caption>
		<tr>
			<th>이름</th>
			<th>아이디</th>
			<th>비번</th>
			<th>나이</th>
			<th>주소</th>
			<th>전화번호</th>
		</tr>
			<c:forEach var ="j" items="${list}">
			<tr>
				<td>${j.irum}</td>
				<td>${j.id}</td>
				<td>${j.pw}</td>
				<td>${j.age}</td>
				<td>${j.addr}</td>
				<td>${j.tel}</td>
			<tr>
			</c:forEach>

</table>

</body>
</html>