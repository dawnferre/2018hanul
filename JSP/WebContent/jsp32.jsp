<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       <!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp32</title>
</head>
<body>
<!-- jsp32.jsp?su1=100&su2=50 -->
<%--
	<c:if test="조건식">실행문</c:if>
 --%>
 	<h1>su1 : ${param.su1}</h1>
 	<h1>su2 : ${param.su2}</h1>
	<h1> 최대값:
	<c:if test="${param.su1-param.su2>0}">${param.su1}</c:if>
	<c:if test="${param.su1-param.su2<0}">${param.su2}</c:if>
	</h1>
	<h1> 최소값:
	<c:if test="${param.su1-param.su2>0}">${param.su2}</c:if>
	<c:if test="${param.su1-param.su2<0}">${param.su1}</c:if>
	</h1>
</body>
</html>