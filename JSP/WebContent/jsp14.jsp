<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- 화면 비율에 맞춰서 나오게끔 -->
	<table border="1" width="80%" align="center">
		<tr align="center">
			<td colspan="2" height="150">회사로고/네비게이션 메뉴</td>
			
		</tr>
		<tr align="center">
			<td height="300" width="30%">로그인이 들어갈 부분</td>
			<td>메인화면이 들어갈 부분</td>
		</tr>
		<tr align="center">
			<td colspan="2">
				<!-- 회사주소가 들어갈 부분(jsp13.jsp) -->
				<%@ include file="jsp13.jsp" %>
			</td>
		</tr>
	</table>
</body>
</html>