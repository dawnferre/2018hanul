<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 	<!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <!-- format태그설정 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	<!-- function 설정 -->
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp40</title>
</head>
<body>
	<c:set var ="hi" value="   How   are   you?   "></c:set>
	<h2>원래 문자열(자체적으로 공백을 제거) : ${hi}</h2>
	<h2>문자열(공백을 포함==>&nbsp를 이용함.) :&nbsp;&nbsp;&nbsp;how&nbsp;&nbsp;&nbsp;are&nbsp;&nbsp;&nbsp;you?&nbsp;&nbsp;&nbsp;</h2>
	<%-- ★★  function은 기존 core, fmt와 다르게 태그가 아닌 ${}안에 fn: 형식으로 나타나게 된다. 주의주의!!! --%>
	<h2>모두 대문자로 출력 :${fn:toUpperCase(hi)} </h2>
	<h2>모두 소문자로 출력 :${fn:toLowerCase(hi)} </h2>
	<h2>길이:공백포함하여 출력 :${fn:length(hi)} </h2>
	<h2>공백제거하여 출력 :${fn:trim(hi)} </h2>
	<h2>길이:공백제거하고 출력 :${fn:length(fn:trim(hi))} </h2><!-- 원래문자열에서 양쪽의 공백만 없앰(글자사이의 공백은 남아있음) -->
	<h2> are 문자의 위치는? ${fn:indexOf(hi,"are")} </h2>
	<h2> are 문자를 were문자로 변경? ${fn:replace(hi,"are","were")} </h2>
</body>
</html>