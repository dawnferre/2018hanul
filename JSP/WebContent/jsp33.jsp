<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp33</title>
</head>
<body>
<!-- 
	jsp33.jsp?num=1 jsp33.jsp?num=2 jsp33.jsp?num=3 
	
	Java : switch-case-default :>>> 
	
	JSTL :choose- when text="조건"= otherwise
-->
<h3>인사하기 안뇽!(java)</h3>
<%
	int num = Integer.parseInt(request.getParameter("num"));
	if(num ==1){
%><h3>만나서 반가워요  java 1번</h3>	
<% }else if (num ==2){
	%><h3>만나서 반가워요  뿅 java 2번</h3>	
<% 	
}else{
%><h3>만나서 반가워요 뿅 java 3번</h3>	
<% 	
}
%>
<h3>인사하기 안뇽!(JSTL)</h3>
<h3><c:if test="${param.num==1}">만나서 반가워요  JSTL 1번</c:if>
<c:if test="${param.num==2}">만나서 반가워요  핍핍 JSTL 2번</c:if>
<c:if test="${param.num==3}">만나서 반가워요  핍핍핍 JSTL 3번</c:if></h3>


<h3> switch_case (java)</h3>
<%
	switch(num){
	case 1:%>
		<h3>1번을 선택 ㄱㄱ</h3>
		<% break;
	case 2:%>
		<h3>2번을 선택 ㄱㄱ</h3>
		<% break;
	default:%>
		<h3>3번을 선택 ㄱㄱ</h3>
		<% break;
	}
%>
<h3> choose- when text="조건"- otherwise (JSTL)</h3>
<h3>
<c:choose>
	<c:when test="${param.num==1}">1번 선택 뿅뿅</c:when>
	<c:when test="${param.num==2}">2번 선택 뿁뵵</c:when>
	<c:otherwise>3번 선택 뿜뿜</c:otherwise>
</c:choose>
</h3>
</body>
</html>