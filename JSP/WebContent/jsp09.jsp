<%@page import="com.hanul.study.*"%><!-- 패키지의 모든 클래스를 가지고 올때 -->
<%-- <%@page import="com.hanul.study.MemberDTO"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>수정</title>
</head>
<body>
<%
	/* 자바코드로 생성. */
	//한글처리
	request.setCharacterEncoding("utf-8");
%>
<!-- jsp action tag 사용 -->
<jsp:useBean id="dto" class="com.hanul.study.MemberDTO">
<%-- 태그적인 요소를 이용하려면 미리 import를 해줘야 한다. --%>
	<jsp:setProperty property="*" name="dto"/><!-- get/set메소드 생성. -->
	
</jsp:useBean>

<jsp:useBean id="dao" class="com.hanul.study.MemberDAO"/>
<%-- 태그적인 요소를 이용하려면 미리 import를 해줘야 한다. ==? get/set 필요없으니까 --%>

<%
	int succ =dao.updateMem(dto);

	if(succ>0){
		out.println("<script>alert('업데이트성공!');location.href='jsp06.jsp'</script>");
	}else{
		out.println("<script>alert('업데이트실패!');location.href='jsp06.jsp'</script>");
	}
%>
</body>
</html>