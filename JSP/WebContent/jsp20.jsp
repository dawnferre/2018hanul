<%@ page import="com.hanul.study.MemberDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%
request.setCharacterEncoding("utf-8");
String irum = request.getParameter("irum");
String id = request.getParameter("id");
String addr = request.getParameter("addr");

MemberDTO dto = new MemberDTO();
dto.setIrum(irum);
dto.setId(id);
dto.setAddr(addr);
%>

<jsp:useBean id="dto1" class="com.hanul.study.MemberDTO">
	<jsp:setProperty property="*" name="dto1"/>
</jsp:useBean>

<%
//현재 페이지에서 만든 dto를 EL문법으로 사용하기 위해 바인딩 객체를 생성
//→ pageContext 내장객체를 이용하여 page scope 설정
pageContext.setAttribute("dto2", dto);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP20</title>
</head>
<body>
○ JSP 기본 출력<br/>
이름 : <%= irum %><br/>
아이디 : <%=dto.getId() %><br/>
주소 : <%=dto1.getAddr() %><br/>
<br/>

○ Action Tag 출력<br/>
이름 : <jsp:getProperty property="irum" name="dto1"/><br/>
아이디 : <jsp:getProperty property="id" name="dto1"/><br/>
주소 : <jsp:getProperty property="addr" name="dto1"/><br/>
<br/>

○ EL 문법 출력<br/>
이름 : ${param.irum }<br/>
아이디 : ${dto1.id }<br/>
주소 : ${dto2.addr }
</body>
</html>