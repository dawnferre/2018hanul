<%@page import="com.hanul.study.TwoHap"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp03계산결과</title>
</head>
<body>
<%--
	입력받은 매개변수 su1과 su2에 저장 : 클라이언트의 요청을 받는다
	 twohap class의 totalSum 메서드 호출 : 비즈니스 로직
	 결과값을 jsp 04.jsp로 넘겨서 출력: 프레젠테이션 >> request.forward방식으로 페이지 전환
 --%>
<%
	int su1 = Integer.parseInt(request.getParameter("su1"));
	int su2 = Integer.parseInt(request.getParameter("su2"));
	int sum = 0;
	if(su1>su2){
		sum = new TwoHap().totalSum(su2, su1);
		
	}else{
		
		sum = new TwoHap().totalSum(su1, su2);
	}
	
	request.setAttribute("su1", su1);
	request.setAttribute("su2", su2);
	request.setAttribute("sum", sum);
	RequestDispatcher rd = request.getRequestDispatcher("jsp04.jsp");
	//받은 요청과 보낼 응답을 보낸다.
	rd.forward(request, response);
%>
</body>
</html>