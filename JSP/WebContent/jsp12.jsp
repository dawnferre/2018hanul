<%@page import="com.hanul.study.MemberDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP12</title>
</head>
<body>
	<%
		request.setCharacterEncoding("utf-8");
		MemberDTO dto =(MemberDTO)(request.getAttribute("dto"));//바인딩 객체를 받는다. ==> object 상위 부모클래스로 받기 때문에 
// 		String ipaddr = (String)(request.getAttribute("ipaddr"));
		String ipaddr = request.getParameter("ipaddr");
	%>
	<jsp:useBean id="dto1" class="com.hanul.study.MemberDTO">
		<jsp:setProperty property="*" name="dto1"/>
	</jsp:useBean>
	<p>이름 :<%=dto.getIrum() %></p>
	<!-- 바인딩객체를 자바코드로 dto를 만들었음 ==> 액션태그 로 자바코드 불가(★★액션태그는 액션태그로만 받을 수 있다.).-->
	<p>아이디:<jsp:getProperty property="id" name="dto1"/></p>
	<p>주소: ${dto.addr}</p>
	<p>아이피 : <%=ipaddr %></p>
	<p>아이피 : ${param.ipaddr} </p>
</body>
</html>