<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
      <!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String[] arr = {"사과","배","석류","오렌지","바나나"};
	//Scope설정.
	pageContext.setAttribute("arr",arr);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp35</title>
</head>
<body>
	<h3>java반복문</h3>
	<%for(int i =0; i<arr.length; i++){
	%>
		<h5><%=arr[i] %></h5>	
	<% 
	}
	%>
	<%-- 
		JSTL 반복문 : <c:for Each var="반복변수명" items="배열명" >실행문</c:for Each>
	 --%>
	<h3>JSTL 반복문</h3>
	<c:forEach var ="i" items="<%=arr%>">
		<h5>${i}</h5>
	</c:forEach>
	<c:forEach var ="i" items="${arr}">
		<h5>${i}</h5>
	</c:forEach>
</body>
</html>