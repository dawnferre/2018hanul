<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP01</title>
</head>
<body>
<%	//지시자
	int su1 = 1;
	int su2 = 100;
	int sum = totalSum(su1,su2);
	
%>
<%! //선언문
	public int totalSum(int su1,int su2){
		int sum =0;
		for(int i = su1; i<=su2; i++){
			sum +=i;
		}
		return sum;
	}
%>
<%--jsp 주석--%>
<!-- HTML주석 -->
첫번째 수 : <%= su1 %><br>
두번째 수 : <%= su2 %><br>
두 수 사이의 누적합 : <%= sum %><br>
</body>
</html>