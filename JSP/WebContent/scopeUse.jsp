<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>scopeUse</title>
</head>
<body>
	<%
		//object로 인식됨  == >  casting해줘야함
		String pageName =(String)(pageContext.getAttribute("pageName"));//null
		String requestName =(String)(request.getAttribute("requestName"));//null
		//동적페이지 전환시에만 request객체의 값으로 넘겨줄수 있음(제어권을 넘길수 있음) :requestDispatcher
		String sessionName = (String)(session.getAttribute("sessionName"));
		//ip와 localhost를 사용한것을 서로 다르게 인식해서 .. session이 작동이 안됨.
		String applicationName = (String)(application.getAttribute("application"));
	%>
	<ul type="square">
		<li>pageContext : <%=pageName %></li> 
		<li>request : <%=requestName %></li>
		<li>session : <%=sessionName %></li>
		<li>application : <%=applicationName %></li>
	</ul>
</body>
</html>