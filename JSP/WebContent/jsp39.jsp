<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  <!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <!-- format태그설정 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp39</title>
</head>
<body>
	<c:set var="i" value="1234567890"></c:set>
	<h2>숫자포맷 : ${i}</h2>
	<!-- 3자리수마다  , 표현해줌. -->
	<h2>첫번째 수 : <fmt:formatNumber value="${i}" groupingUsed="true"/></h2>
	<!-- pattern: 소수표현까지 가능 , #(0생략가능),0(무조건표현)을 이용하다. -->
	<h2>두번째 수 : <fmt:formatNumber value="${i}" pattern="#,###"/></h2>
	<h2>두번째 수 : <fmt:formatNumber value="${i}" pattern="#,##0"/></h2>
	<!-- 소수에 #,0을 섞어쓰면 오류난다.. -->
	<h2>세번째 수 : <fmt:formatNumber value="0.45" pattern="#.000"/></h2>
	<h2>세번째 수 : <fmt:formatNumber value="0.45" pattern="00.#"/></h2>
	<h2>네번째 수 : <fmt:formatNumber value="123" pattern="00.#"/></h2>
	
	<h3>기타포맷: 통화, 백분율</h3>
	<!-- 통화기호를  입력할 경우 특수문자 : ㄹ->한자 -->
	<h2>첫번째 수 : <fmt:formatNumber value="${i}" type="currency" currencySymbol="￦"/></h2>
	<h2>두번째 수 : <fmt:formatNumber value="${i}" type="currency" currencySymbol="＄"/></h2>
	<h2>세번째 수 : <fmt:formatNumber value="${i}" pattern="￦#,##0원"/></h2>
	<h2>첫번째 백분율 : <fmt:formatNumber value="0.123" type="percent"/></h2>
	<h2>두번째 백분율 : <fmt:formatNumber value="0.123" type="percent" pattern="0.00%"/></h2>
	
</body>
</html>