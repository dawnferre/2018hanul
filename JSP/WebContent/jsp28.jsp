<%@page import="com.hanul.study.MemberDTO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String[] study = (String[])request.getAttribute("study");
	ArrayList<String> list = (ArrayList<String>)request.getAttribute("list");
	MemberDTO dto = (MemberDTO)request.getAttribute("dto");
	ArrayList<MemberDTO> list2 = (ArrayList<MemberDTO>)request.getAttribute("list2");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp28</title>
</head>
<body>
	<h2>[배열을 받는 방법(JSP)]<br>
		<% for(int i =0; i<study.length; i++){ %>
			<%=study[i] %><br>
		<%} %>
	</h2>
	<hr>
	<h2>[배열을 받는 방법(EL)]<br>
		${study[0]}<!--el태그에서 줄때는 . 이런표현없이  그냥 배열이름[배열순서]  -->
		${study[1]}
		${study[2]}
		${study[3]}
		${study[4]}
	</h2>
	<hr>
	<h2>[ArrayList 배열을 받는 방법(JSP)]<br>
		<% for(String i:list){ %>
			<%=i %><br>
		<%} %>
	</h2>
	<hr>
	<h2>[ArrayList 배열을 받는 방법(EL)]<br>
		${list[0]}<!--el태그에서 줄때는 . 이런표현없이  그냥 배열이름[배열순서]  -->
		${list[1]}<!--el태그에서 줄때는 . 이런표현없이  그냥 배열이름[배열순서]  -->
		${list[2]}<!--el태그에서 줄때는 . 이런표현없이  그냥 배열이름[배열순서]  -->
		${list[3]}<!--el태그에서 줄때는 . 이런표현없이  그냥 배열이름[배열순서]  -->
		${list[4]}<!--el태그에서 줄때는 . 이런표현없이  그냥 배열이름[배열순서]  -->
		${list[5]}<!--el태그에서 줄때는 . 이런표현없이  그냥 배열이름[배열순서]  -->
		${list[6]}<!--el태그에서 줄때는 . 이런표현없이  그냥 배열이름[배열순서]  -->
	</h2>
	<hr>
	<h2>[MemberDTO 배열을 받는 방법(JSP)]<br>
		이름: <%=dto.getIrum() %><br>
		아이디: <%=dto.getId() %><br>
		비밀번호: <%=dto.getPw() %><br>
		나이: <%=dto.getAge() %><br>
		주소: <%=dto.getAddr() %><br>
		전화번호 : <%=dto.getTel() %>
	</h2>
	<hr>
	<h2>[ArrayList 배열을 받는 방법(EL)]<br>
		${dto.irum}<br>
		${dto.id}<br>
		${dto.pw}<br>
		${dto.age}<br>
		${dto.addr}<br>
		${dto.tel}<br>
	</h2>
	<hr>
	<h2>[ArrayList(MemberDTO) 배열을 받는 방법(JSP)]<br>
		<% for(MemberDTO dto2:list2){ %>
		이름: <%=dto2.getIrum() %><br>
		아이디: <%=dto2.getId() %><br>
		비밀번호: <%=dto2.getPw() %><br>
		나이: <%=dto2.getAge() %><br>
		주소: <%=dto2.getAddr() %><br>
		전화번호 : <%=dto2.getTel() %>
		<hr>
			
		<%} %>
	</h2>
	<h2>[ArrayList(MemberDTO) 배열을 받는 방법(EL)]<br>
		${list2[0].irum} ${list2[0].id} ${list2[0].pw} ${list2[0].age} ${list2[0].addr} ${list2[0].tel} <br>
		${list2[1].irum} ${list2[1].id} ${list2[1].pw} ${list2[1].age} ${list2[1].addr} ${list2[1].tel} <br>
		${list2[2].irum} ${list2[2].id} ${list2[2].pw} ${list2[2].age} ${list2[2].addr} ${list2[2].tel} <br>
		${list2[3].irum} ${list2[3].id} ${list2[3].pw} ${list2[3].age} ${list2[3].addr} ${list2[3].tel} <br>
		${list2[4].irum} ${list2[4].id} ${list2[4].pw} ${list2[4].age} ${list2[4].addr} ${list2[4].tel} <br>
	</h2>
</body>
</html>