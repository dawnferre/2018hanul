<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ page import="com.hanul.study.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<!-- form에서 넘어가는 값은 ==> 태그의 name속성으로 얻을 수 있음. -->
<%
	request.setCharacterEncoding("utf-8");
%>
<jsp:useBean id="dto" class="com.hanul.study.MemberDTO">
	<%--dto객체를 설정해줌 ==> 각가의 의미 --%>
	<jsp:setProperty property="*" name="dto"/>
</jsp:useBean>

<h2>매개변수의 내용을 출력</h2>
<!-- 1. jsp 즉, 스크립트를 이용한 출력 -->
<p>이름 (JSP): <%=dto.getIrum() %></p>
<!-- 2. 액션태그를 이용한 방법 -->
<p>아이디(ACTION TAG): <jsp:getProperty property="id" name="dto"/></p>
<!-- 3.JSP EL(Express Language): 표기법을 이용한 방법 -->
<p>주소(EL):${ dto.addr}</p>
</body>
</html>