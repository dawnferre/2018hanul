<%@page import="com.hanul.study.MemberDTO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	//study[] 배열을 생성하고 초기화 ==> 바인딩객체 ==>jsp28.jsp
	String[] study = {"JAVA","Servlet","JSP","Android","Spring"};
	request.setAttribute("study", study); //바인딩객체설정
	
	//ArrayList<> 생성 , 초기화  ==> 무한배열 
	ArrayList<String> list = new ArrayList<>();
	list.add("안녕1");
	list.add("안녕2");
	list.add("안녕3");
	list.add("안녕4");
	list.add("안녕5");
	list.add("안녕6");
	list.add("안녕7");
	
	request.setAttribute("list", list); //바인딩객체설정
	
	//memberDTO 객체를 생성하고 초기화 --> 바인딩객체--> jsp28.jsp
	MemberDTO dto = new MemberDTO();
	dto.setIrum("ABC");
	dto.setAge(28);
	dto.setId("ayj111");
	dto.setPw("12345");
	dto.setTel("010-201-2012");
	dto.setAddr("광주광역시 서구 금호동");
	
	
	request.setAttribute("dto", dto);
	
//Arraylist<> 구조에 memberDTO 객체 배열 ==> 바인딩객체 ==> jsp28.jsp출력
	ArrayList<MemberDTO> list2 = new ArrayList<>();
	list2.add(new MemberDTO("홍길동","hong","1111",2,"광주","010-1234-5678"));
	list2.add(new MemberDTO("홍길동","hong","1111",3,"광주","010-1234-5678"));
	list2.add(new MemberDTO("홍길동","hong","1111",4,"광주","010-1234-5678"));
	list2.add(new MemberDTO("홍길동","hong","1111",5,"광주","010-1234-5678"));
	list2.add(new MemberDTO("홍길동","hong","1111",6,"광주","010-1234-5678"));
	
	request.setAttribute("list2", list2);

%>
<jsp:forward page="jsp28.jsp"></jsp:forward> <!-- 28번으로 넘겨줌. -->
