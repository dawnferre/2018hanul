<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp30</title>
</head>
<body>
<!-- JSTL에서 변수를 선언한 수 jsp 31.jsp로 페이지 전환(forward)후 출력 -->
<c:set var ="code" value="abc001" scope="request"/>
<c:set var ="name" value="컴퓨터" scope="request"/>
<c:set var ="price" value="500000" scope="request"/>
<jsp:forward page="jsp31.jsp"/>
</body>
</html>