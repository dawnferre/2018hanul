<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	function fnToday(){
		var now = new Date();
		var hour = now.getHours();
		var min = now.getMinutes();
		var seconds = now.getSeconds();
		
		var setTime = hour>12?"오후 ":"오전 ";
		setTime += (hour>12?hour-12:hour)+"시 ";
		setTime += (min<10?"0"+min:min)+"분 ";
		setTime += (seconds<10?"0"+seconds:seconds)+"초";
		
		document.getElementById("time").innerHTML = setTime;
		setTimeout("fnToday()",1000);
	}
</script>
</head>
<body onload="fnToday()">
<div id="time" align="center">
	현재시간 
</div>
	<!-- 화면 비율에 맞춰서 나오게끔 -->
	<table border="1" width="80%" align="center">
		<tr align="center">
			<td colspan="2" height="150">회사로고/네비게이션 메뉴</td>
			
		</tr>
		<tr align="center">
			<td height="300" width="30%">로그인이 들어갈 부분</td>
			<td>메인화면이 들어갈 부분</td>
		</tr>
		<tr align="center">
			<td colspan="2">
				<!-- 날짜와 시간이 들어갈 부분(jsp15.jsp)==>항상변하는 부분은 액션태그로 가지고 오는것이 좋댜 -->
				<jsp:include page="jsp15.jsp"></jsp:include>
			</td>
		</tr>
	</table>
</body>
</html>