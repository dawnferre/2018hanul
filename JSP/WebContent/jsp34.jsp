<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp34</title>
</head>
<body>
	<%--  
		JAVA 반복문 :for ,while
		JSTL 반복문 : <c:for Each var="반복변수명" begin="초기값",end="최종값",step:"증감값 ></c:for Each>
	 --%>
	 <h3>JAVA 반복문</h3>
	 <% for(int i =0; i<=7; i++){
		 %>
		 <font size="<%=i %>">안녕요</font>
		 <% 
	 } %>
	 <br>
	 <h3>JSTL 반복문</h3>
	 <c:forEach var ="i" begin="0" end="7" step="1" >
		 <font size ="${i}">안녕요</font>
	 </c:forEach>
	 <br>
	 <c:forEach var ="i" begin="0" end="7" step="2" >
		 <font size ="${i}">안녕요</font>
	 </c:forEach>
</body>
</html>