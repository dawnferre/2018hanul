<%@page import="com.hanul.study.MemberDTO"%>
<%@page import="com.hanul.study.MemberDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
	*{
		margin: 0 auto;
	}
	
	table{
		margin-top: 50px;
	}
	
	.home{
		background-color: yellow;
	}
</style>
<script type="text/javascript">
	function fnCheck(){
		var temp =confirm("수정하시겠습니까?");
		
		if(temp){
			return true;
		}else{
			return false;
		}
	}
</script>
</head>
<body>
<%
	String id = request.getParameter("id");
	MemberDAO dao = new MemberDAO();
	MemberDTO dto = dao.getbyId(id);
%>
<form action="jsp09.jsp" method="post" onsubmit="return fnCheck()" >
<table border ="1">
	<caption><b>[회원업데이트]</b></caption>
	<tr>
		<th>이름</th>
		<td><input type="text" value="<%= dto.getIrum() %>" name="irum" required="required" class="home"></td>
	</tr>
	<tr>
		<th>아이디</th>
		<!-- hidden속성으로 넘겨줌.==> 나는 어차피 input type readonly로 주었어서 .. 값을 못넘기는 경우엔 hidden속성 이용하기  -->
		<td><input type="text" value="<%= dto.getId() %>" readonly="readonly" name="id"></td>
	</tr>
	<tr>
		<th>비밀번호</th>
		<td><input type="text" value="<%= dto.getPw() %>"  name="pw" required="required" class="home"></td>
	</tr>
	<tr>
		<th>나이</th>
		<td><input type="number" value="<%= dto.getAge() %>"name="age" required="required" class="home"></td>
	</tr>
	<tr>
		<th>주소</th>
		<td><input type="text" value="<%= dto.getAddr() %>"name="addr" required="required" class="home"></td>
	</tr>
	<tr>
		<th>전화번호</th>
		<td><input type="text" value="<%= dto.getTel() %>" name="tel" required="required" class="home"></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<input type="submit" value="업데이트">
		<input type="button" onclick="location.href='jsp06.jsp'" value="목록보기">
		</td>
	</tr>
	
	</table>
	</form>
</body>
</html>