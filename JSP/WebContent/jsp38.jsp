<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <!-- format태그설정 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp38</title>
</head>
<body>
	<c:set var ="date" value="<%=new Date()%>"></c:set>
	<h2>날짜와 시간의 기본적인 표시 : ${date }</h2>
	<h2>오늘의 날짜 : <fmt:formatDate value="${date}"/></h2>
	<h2>현재의 시간: <fmt:formatDate value="${date}" type="time"/></h2>
	<h2>오늘의 날짜와 시간: <fmt:formatDate value="${date}" type="both"/></h2>
	<h2>dateStyle,timeStyle ==> default: medium</h2>
	<h2>short: <fmt:formatDate value="${date}" type="both" dateStyle="short" timeStyle="short"/></h2>
	<h2>medium: <fmt:formatDate value="${date}" type="both" dateStyle="medium" timeStyle="medium"/></h2>
	<h2>long: <fmt:formatDate value="${date}" type="both" dateStyle="long" timeStyle="long"/></h2>
	<h2>full: <fmt:formatDate value="${date}" type="both" dateStyle="full" timeStyle="full"/></h2>
	<h2>Pattern: 월을 쓸때는 대문자 M</h2>
	<h2>pattern1: <fmt:formatDate value="${date}" type="date" pattern="yyyy년 MM월 dd일(E)"/></h2>
	<h2>pattern2: <fmt:formatDate value="${date}" type="time" pattern="hh시 mm분 ss초 a"/></h2>
	
	<h2>java SimpleDateFormat Class</h2>
	<%
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy년 mm월 dd일 E요일");
		
	%>
	<h2><%=sdf.format(today) %></h2>
</body>
</html>