<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%
int sum = 0;
for(int i = 1; i <= 10; i++){
	sum += i;
}

//sum 값을 jsp22.jsp로 넘기자 : 바인딩객체 → request forward 방식으로 페이지 전환
request.setAttribute("sum", sum);
//RequestDispatcher rd = request.getRequestDispatcher("jsp22.jsp");
//rd.forward(request, response);
%>
<!-- forward 액션태그 -->
<jsp:forward page="jsp22.jsp"/>