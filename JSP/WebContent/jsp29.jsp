<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- JSTL core를 사용하기 위해서는 반드시 문서 상단에 JSTL선언문을 기술,작성 -->
<!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp29</title>
</head>
<body>
	<h2>1.기본 프로그래밍 tag(변수, 배열, if, for, switch_case 등) >> core</h2>
	<%
		int su1 = 100; //자바 변수 초기화
	%>	<!-- 프로그램적 요소 :  -->
	<h4>JAVA su1 = <%=su1 %></h4>
	<!-- JSTL 변수 선언 : EL태그로 확인.-->
	<c:set var ="su2" value="200"/>
	<h4>JSTL su2 = ${su2}</h4>
	<!-- java su1의 값을 JSTL su3의 값으로 바꿈 -->
	<c:set var ="su3" value="<%=su1 %>"/>
	<h4>JSTL su3 = ${su3}</h4>
	<!-- JSTL su2의 값과 JSTL su3값을 더한 값 -->
	<c:set var ="su4" value="${su2+su3}"></c:set>
	<h4>JSTL su3 = ${su4}</h4>
</body>
</html>