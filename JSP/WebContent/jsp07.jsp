<%@page import="com.hanul.study.MemberDTO"%>
<%@page import="com.hanul.study.MemberDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>회원삭제</title>
</head>
<body>
<%
	String id = request.getParameter("id");
	System.out.println(id);
	MemberDAO dao = new MemberDAO();
	int succ = dao.deleteMem(id);

	if(succ>0){
		out.println("<script>alert('회원삭제성공!');location.href='jsp06.jsp'</script>");
	}else{
		out.println("<script>alert('회원삭제실패!');location.href='jsp05_Main.html'</script>");
	}
%>
%>
</body>
</html>