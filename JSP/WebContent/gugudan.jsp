<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GugudanJSP</title>
<style>
	*{
		margin: 0 auto;
	}
</style>
</head>
<body>

<table border="1">
	<caption><b>[구구단을 출력합시당]</b></caption>
	<tr bgcolor="pink">
	<%
		for(int i =2; i<10; i++){%>
			<td align="center"><%=i %>단</td>
		<% }%>
	</tr>
	<%
		for(int i =1; i <10; i++){
			if(i% 2 ==0){
				%><tr style="background-color:yellow"><% 
			for(int j =2; j<10; j++){
				DecimalFormat df = new DecimalFormat("00");
				String result =df.format(i*j);
				%>
				<td><%=j %> x <%=i %> = <%=result%></td>
	 	<% 	}
	
				%></tr><% 
				
			}else{
				%><tr style="background-color:green"><% 
						for(int j =2; j<10; j++){
							DecimalFormat df = new DecimalFormat("00");
							String result =df.format(i*j);
							%>
							<td><%=j %> x <%=i %> = <%=result%></td>
				 	<% 	}
				
							%></tr><% 
			}
		}
	%>
</table>
<%
	
%>
</body>
</html>