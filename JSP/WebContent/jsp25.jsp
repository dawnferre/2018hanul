<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	int su1 = Integer.parseInt(request.getParameter("su1"));
	int su2 = Integer.parseInt(request.getParameter("su2"));
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp25</title>
</head>
<body>
<!-- jsp25.jsp?su1=20&su2=10 -->
<h2>su1의 값(JSP) :<%=su1 %></h2>
<h2>su2의 값(JSP) :<%=su2 %></h2>
<h2>su1의 값(EL) :${param.su1}</h2><!--el은 자바값으로 받을 필요없이 바로 el태그로 처리함.  -->
<h2>su2의 값(EL) :${param.su2}</h2>
<h2>--사칙연산 + 나머지 연산까지 el코드는 가능하다.--</h2>
<h2>su1+su2의 값(EL) : ${param.su1}+${param.su2} =${param.su1 +param.su2}</h2>
<h2>su1-su2의 값(EL) : ${param.su1}-${param.su2} =${param.su1 -param.su2}</h2>
<h2>su1*su2의 값(EL) : ${param.su1}x${param.su2} =${param.su1 *param.su2}</h2>
<h2>su1/su2의 값(EL) : ${param.su1}/${param.su2} =${param.su1 /param.su2}</h2>
<h2>su1%su2의 값(EL) : ${param.su1}%${param.su2} =${param.su1 %param.su2}</h2>
<!-- 삼항연산자를 이용하여 비교 조건 처리를 간단하게 처리할 수 있음. -->
<h2>su1이 더크나요? ${param.su1 -param.su2>0?"맞아요":"틀려요"}</h2>
<h2>su2이 더크나요? ${param.su1 -param.su2<0} </h2>
</body>
</html>