<%@page import="com.hanul.study.MemberDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("utf-8");
	MemberDTO dto = (MemberDTO)request.getAttribute("dto");
%>
<jsp:useBean id="dto1" class="com.hanul.study.MemberDTO">
	<jsp:setProperty property="*" name="dto1"/>
</jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp18</title>
</head>
<body>
<br>
	이름 :<%=dto.getIrum() %><br>
	아이디: <jsp:getProperty property="id" name="dto1"/><br>
	주소 :${dto.addr };
</body>
</html>