<%@page import="com.hanul.study.MemberDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!--  jsp17Main.html 에서 전달된 매개변수를 적용하여 dto객체를 생성하기. -->
<%
	request.setCharacterEncoding("utf-8");
	MemberDTO dto = new MemberDTO();
	

	dto.setIrum(request.getParameter("irum"));
	dto.setId(request.getParameter("id"));
	dto.setPw(request.getParameter("pw"));
	dto.setAge(Integer.parseInt(request.getParameter("age")));
	dto.setAddr(request.getParameter("addr"));
	String addr = request.getParameter("addr");
	dto.setTel(request.getParameter("tel"));

	//request객체를 el코드를 사용하려면 
	//현재페이지에서 사용할 객체를 만들어줘야한다.(★★★★★★)
	pageContext.setAttribute("dto2", dto);

%>
<!-- 액션태그를 사용하는 방법 -->
<jsp:useBean id="dto1" class="com.hanul.study.MemberDTO">
	<jsp:setProperty property="*" name="dto1"/>
</jsp:useBean>
<!-- dto 객체를 jsp18.jsp 넘기자: 바인딩 객체 생성 >> requestForward -->
<%
	request.setAttribute("dto",dto);
// 	RequestDispatcher rd = request.getRequestDispatcher("jsp18.jsp");
// 	rd.forward(request, response);
%>
<jsp:forward page="jsp18.jsp"></jsp:forward>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP17</title>
</head>
<body>
	<div>
		이름:<%=dto.getIrum() %><br>
<%-- 		아이디 :<jsp:getProperty property="id" name="dto1"/><br> --%>
<%-- 		주소 :${dto1.addr}<br> --%>
		주소 :${dto2.addr}
	</div>
</body>
</html>