<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%
//object 객체로 넘어오니 class 타입으로 받아줌(기본형은 wrapper class로 받아줌.)
Integer sum = (Integer) request.getAttribute("sum");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP22</title>
</head>
<body>
<br>
1부터 10까지의 누적합(JSP) : <%=sum %><br/>
1부터 10까지의 누적합(EL) : ${sum }
</body>
</html>