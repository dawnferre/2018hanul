<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
      <!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	ArrayList<String> list = new ArrayList<>();
	list.add("사과");
	list.add("석류");
	list.add("수박");
	list.add("참외");
	list.add("자두");
	list.add("레몬");
	
	pageContext.setAttribute("list", list); //현재페이지에서 el로 만들어줄 수 있는  설정.

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp36</title>
</head>
<body>
	<h2>JAVA for
	<% for(int i =0; i<list.size(); i++){
		%>
		<ul>
			<li><%=list.get(i) %></li>
		</ul>
		
	<% }%>
	</h2>
	<h2>JAVA 향상for
	<% for(String result:list){
		%>
		<ul type="square">
			<li><%=result %></li>
		</ul>
		
	<% }%>
	</h2>
	<h2>EL표기문
		<ul type="circle">
			<li>${list[0] }</li>
			<li>${list[1] }</li>
			<li>${list[2] }</li>
			<li>${list[3] }</li>
			<li>${list[4] }</li>
		</ul>
	</h2>
	<h2>JSTL core
		<c:forEach var ="i" items="${list}">
			<ul>
				<li>${i}</li>
			</ul>
		</c:forEach>
	</h2>
	
	
</body>
</html>