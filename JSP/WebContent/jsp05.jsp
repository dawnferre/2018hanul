<%@page import="com.hanul.study.MemberDAO"%>
<%@page import="com.hanul.study.MemberDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%--
	request.setCharacterEncoding("utf-8");

	String irum = request.getParameter("irum");
	String id = request.getParameter("id");
	String pw = request.getParameter("pw");
	int age = Integer.parseInt(request.getParameter("age"));
	String addr = request.getParameter("addr");
	String tel = request.getParameter("tel");
	

	MemberDTO dto = new MemberDTO();
	dto.setIrum(irum);
	dto.setId(id);
	dto.setPw(pw);
	dto.setAddr(addr);
	dto.setAge(age);
	dto.setTel(tel);
	
	MemberDAO dao = new MemberDAO();
	int succ = dao.insert(dto);
	
--%>
<%
/* 자바코드로 생성. */
	request.setCharacterEncoding("utf-8");
%>
<!-- jsp action tag 사용 -->

<jsp:useBean id="dto" class="com.hanul.study.MemberDTO">
<%-- 태그적인 요소를 이용하려면 미리 import를 해줘야 한다. --%>
	<jsp:setProperty property="*" name="dto"/><!-- get/set메소드 생성. -->
	
</jsp:useBean>

<jsp:useBean id="dao" class="com.hanul.study.MemberDAO"/>
<%-- 태그적인 요소를 이용하려면 미리 import를 해줘야 한다. ==? get/set 필요없으니까 --%>

<%
	int succ =dao.insert(dto);

	if(succ>0){
		out.println("<script>alert('회원가입성공!');location.href='jsp06.jsp'</script>");
	}else{
		out.println("<script>alert('회원가입실패!');location.href='jsp05_Main.html'</script>");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp05</title>
</head>
<body>

</body>
</html>