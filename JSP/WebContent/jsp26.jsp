<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp26</title>
</head>
<body>
	<!--jsp26.jsp?str1=kor&str2=korea -->
	<h2>str1(EL) : ${param.str1}</h2>
	<h2>str2(EL) : ${param.str2}</h2>
	<h2>str1문자길이(EL) : ${param.str1.length()}</h2>
	<h2>str2문자길이(EL) : ${param.str2.length()}</h2>
	<h2>str1과 str은 같나요?(EL) : ${param.str2==param.str1?"같아요":"달라요"}</h2>
	========================================================================
	<%
		String str1 = request.getParameter("str1");
		String str2 = request.getParameter("str2");
	%>
	<h2>str1(JSP): <%=str1 %></h2>
	<h2>str2(JSP): <%=str2 %></h2>
	<h2>str1과 str2는 같나요?(JSP): <%=str2.equals(str1)?"같아요":"달라요" %></h2>
</body>
</html>