<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	//한글처리
	request.setCharacterEncoding("utf-8");
	String irum = request.getParameter("irum");
	String[] animal = request.getParameterValues("animal");

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsp24</title>
</head>
<body>
<%-- 	<h2>회원님의 이름 (JSP):  <%=irum %></h2> --%>
	<h2>회원님의 이름 (EL):  ${param.irum}</h2>
	<h2>회원님이 키우고 싶은 동물(들) (EL):  ${paramValues.animal[0]} ${paramValues.animal[1]} ${paramValues.animal[2]} 
											${paramValues.animal[3]} ${paramValues.animal[4]}</h2>
	<h2>회원님이 키우고 싶은 동물(들) (JSP):
		<% if(animal!= null){
			for(int i =0; i<animal.length; i++){%>
				<%= animal[i]%>
		<%	}		
		}else{
			%>없습니다.<% 	
		}
			%> </h2>
		
							
</body>
</html>