<%@page import="com.hanul.study.MemberDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.hanul.study.MemberDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>전체회원목록</title>
<style>
	*{
		margin: 0 auto;
	}
	
	table{
		margin-top: 50px;
	}
</style>
<%
	MemberDAO dao = new MemberDAO();
	ArrayList<MemberDTO> list = dao.selectAll();
%>
<script type="text/javascript">
	function fndelete(id) {
	var temp =confirm("삭제하시겠습니까?");
		alert(id);
		if(temp){
			location.href="jsp07.jsp?id="+id;
			return true;
		}else{
			return false;
		}
	}
</script>
</head>
<body>
	<table border ="1">
	<caption><b>[전체회원 목록보기]</b></caption>
	<tr>
		<th>이름</th>
		<th>아이디</th>
		<th>비밀번호</th>
		<th>나이</th>
		<th>주소</th>
		<th>전화번호</th>
		<th>삭제</th>
		<th>수정</th>
	</tr>
	<% for(MemberDTO dto: list){ %>
	
	<tr>
		<td><%= dto.getIrum() %></td>
		<td><%= dto.getId() %></td>
		<td><%= dto.getPw() %></td>
		<td><%= dto.getAge() %></td>
		<td><%= dto.getAddr() %></td>
		<td><%= dto.getTel() %></td>
<%-- 		<td><a href="jsp07.jsp?id=<%=dto.getIrum()%>">삭제</a></td> --%>
<%-- 		<td><button onclick="location.href='jsp07.jsp?id=<%=dto.getIrum()%>'">삭제</button></td> --%>
		<td><button onclick="fndelete('<%=dto.getId()%>')">삭제</button></td>
		<td><input type="button" onclick="location.href='jsp08.jsp?id=<%=dto.getId()%>'" value="수정"></td>
	</tr>
	<%} %>
	<tr>
	<td colspan="6" align="center">
		<input type="button" value="회원가입" onclick="location.href='jsp05_Main.html'">
	</td>
	</tr>
	</table>
</body>
</html>