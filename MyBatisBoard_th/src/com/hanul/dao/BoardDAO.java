package com.hanul.dao;

import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.hanul.dto.BoardDTO;

public class BoardDAO {
	//SqlSessionFactory �� sqlMapper
	static SqlSessionFactory sqlMapper;
	static{
		String resource = "com/hanul/mybatis/SqlMapConfig.xml";
		try {
			InputStream inputStream = Resources.getResourceAsStream(resource);
			sqlMapper = new SqlSessionFactoryBuilder().build(inputStream);
		} catch (Exception e) {
			System.out.println("SqlSessionFactory 오류");
			e.printStackTrace();
		}
	}//static
	
	//��ü��ϰ˻�
	public List<BoardDTO> getAllList() {
		SqlSession session = sqlMapper.openSession();
		List<BoardDTO> list = null;
		list = session.selectList("getAllList");
		return list;
	}//getAllList()
	
	
	
	
	
	
}//class
