package com.hanul.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.action.Action;
import com.hanul.action.ActionForward;
import com.hanul.action.BoardListAction;

@WebServlet("/BoardFrontController.do")
public class BoardFrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		//1. Ŭ���̾�Ʈ�� � ��û�� �ߴ� �ľ�
		request.setCharacterEncoding("utf-8");
		String uri = request.getRequestURI();			//uri-pattern �� : /mbb/XXX.do
		String ctx = request.getContextPath();			//Context root �� : /mbb
		String command = uri.substring(ctx.length());	//���� ��û�� ������ : /XXX.do
		System.out.println("uri : " + uri);
		System.out.println("ctx : " + ctx);
		System.out.println("command : " + command);
		
		//2. Ŭ���̾�Ʈ ��û(*.do)�� ���� ó���� Action Class�� ���� : Handler Mapping
		Action action = null;
		ActionForward forward = null;
		
		if(command.equals("/boardList.do")){
			action = new BoardListAction();
			forward = action.execute(request, response);
		}else if (command.equals("/boardInsertForm.do")){ //입력화면만 연결해줌
			forward = new ActionForward();
			forward.setPath("board/boardInsertForm.jsp");
			forward.setRedircet(false); //true는 원 주소 (.jsp)가 찍힘.
		}
		
		
				
		//3. ������ ��ȯ(sendRedirect, forward)
		if(forward != null){
			if(forward.isRedircet()){		//true : sendRedirect ��������ȯ
				response.sendRedirect(forward.getPath());
			}else{							//false : forward ������ ��ȯ
				RequestDispatcher rd = request.getRequestDispatcher(forward.getPath());
				rd.forward(request, response);
			}
		}
	}
}//class
