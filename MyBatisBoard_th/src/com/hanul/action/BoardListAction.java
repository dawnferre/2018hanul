package com.hanul.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.dao.BoardDAO;
import com.hanul.dto.BoardDTO;

public class BoardListAction implements Action{

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		//DAO 연동하여 게시판 전체목록을 가져오는 작업
		BoardDAO dao = new BoardDAO();
		List<BoardDTO> list = dao.getAllList();
		request.setAttribute("list", list);
		
		//BoardListAction.java 작업을 마무리 했다 → BoardFrontController → ActionForward
		//① View Page(path) : boardList.jsp
		//② 페이지 전환방식(idRedirect) : true(sendRedriect), false(forward)
		ActionForward forward = new ActionForward();
		forward.setPath("board/boardList.jsp");		
		forward.setRedircet(false);
		return forward;
	}	
}
