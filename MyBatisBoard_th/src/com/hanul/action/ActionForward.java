package com.hanul.action;

public class ActionForward {
	private String path;			//View page 경로와 파일명
	private boolean isRedircet;	//페이지 전환방식 ▶ true : sendRedirect, false : forward
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public boolean isRedircet() {
		return isRedircet;
	}
	public void setRedircet(boolean isRedircet) {
		this.isRedircet = isRedircet;
	}
	
}
