<%@page import="com.hanul.dto.BoardDTO"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
  <!-- core설정 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	List<BoardDTO> list = (List<BoardDTO>)request.getAttribute("list");
	pageContext.setAttribute("list", list);
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>게시판 목록</title>
</head>
<body>
<p align="center">[게시판 목록 보기]</p>
<table align="center" border="2">
	<tr>
		<th>번호(내가준 번호)</th>
		<th>번호(실제번호)</th>
		<th>제목</th>
		<th>작성자</th>
		<th>작성일</th>
		<th>조회수</th>
	</tr>
	<!-- jstl core 문법 -->
<%-- 	<c:if test="${empty list}"> --%>
<!-- 		<tr> -->
<!-- 			<td colspan="5">※작성된 글이 없습니다.</td> -->
<!-- 		</tr> -->
<%-- 	</c:if> --%>
<%-- 	<c:if test="${list!= null}"> --%>
<%-- 		<c:forEach var ="j" items="${list}"> --%>
<!-- 				<tr> -->
<%-- 					<td>${j.b_num }</td> --%>
<%-- 					<td>${j.b_subject}</td> --%>
<%-- 					<td>${j.b_writer}</td> --%>
<%-- 					<td>${j.b_date}</td> --%>
<%-- 					<td>${j.b_readcount}</td> --%>
<!-- 				</tr> -->
<%-- 		</c:forEach> --%>
<%-- 	</c:if> --%>

<% if(list.size() == 0){%>
	 	<tr> 
			<td colspan="5">※작성된 글이 없습니다.</td>
		</tr>
<%}else{%>
	<%for(int i= 0; i<list.size(); i++){ %>
		<tr> 
			<td><%=list.size()-i%></td>
			<td><%=list.get(i).getB_num()%></td>
			<td><%=list.get(i).getB_subject()%></td>
			<td><%=list.get(i).getB_writer()%></td>
			<td><%=list.get(i).getB_date()%></td>
			<td><%=list.get(i).getB_readcount()%></td>
		</tr>
<%}%>
<%}%>

	<tr align="right">
		<td colspan="6"><input type="button" value="글쓰기" onclick="location.href='boardInsertForm.do'"></td>
	</tr>
</table>
</body>
</html>