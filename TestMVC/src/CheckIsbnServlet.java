

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.study.BookDAO;
@WebServlet("/cis.do")
public class CheckIsbnServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");//한글처리..

		String isbn = request.getParameter("isbn");
		BookDAO dao = new BookDAO();
		System.out.println(isbn);
		response.setContentType("text/html;charset=utf-8");
		PrintWriter output = response.getWriter();
		ResultSet rs =dao.checkIsbn(isbn);
		try {
			if(rs.next()){
				//중복값이 있다
				output.println("<script>alert('이미 등록된 isbn코드입니다.'); history.back();document.reg_form.isbn.value='';</script>");
			}else{
				//중복값이 없다.
				output.println("<script>alert('사용가능한 isbn코드입니다.'); history.back();</script>");
			}
			
		} catch (Exception e) {
			System.out.println("checkisbn오류"+e.getMessage());
		}
		
	}

}
