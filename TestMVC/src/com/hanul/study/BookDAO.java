package com.hanul.study;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.net.aso.r;

public class BookDAO {
	Connection conn;
	PreparedStatement pst;
	ResultSet rs;
	
	public Connection getConn(){
		String url="jdbc:oracle:thin:@127.0.0.1:1521:xe";
		String user ="system";
		String password ="0000";
		
		try {
			Class.forName("oracle.jdbc.OracleDriver");//동적로딩
			conn = DriverManager.getConnection(url, user, password);
			
		} catch (Exception e) {
			System.out.println("커넥션 오류"+e.getMessage());
		}
		return conn;
	}
	
	public void disConn(){
		try {
			if(conn!=null){
				conn.close();
			}
			if(pst!=null){
				pst.close();
			}
			if(rs!=null){
				rs.close();
			}
		} catch (Exception e) {
			System.out.println("disconn오류"+e.getMessage());
		}
	}

	public int insert(BookDTO dto) {
		conn = getConn();
		int result = 0;
		String sql ="insert into book values (?,?,?,?,?,?,?)";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, dto.getTitle());
			pst.setString(2, dto.getAuth());
			pst.setString(3, dto.getIsbn());
			pst.setString(4, dto.getComp());
			pst.setInt(5, dto.getCost());
			pst.setInt(6, dto.getSu());
			pst.setInt(7, dto.getPrice());
			
			result = pst.executeUpdate();

		} catch (Exception e) {
			System.out.println("insert오류"+e.getMessage());
		}
		return result;
	}

	public ResultSet checkIsbn(String isbn) {
		conn = getConn();
		String sql ="select *from book where isbn =?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, isbn);
			rs = pst.executeQuery();
			
		} catch (Exception e) {
			System.out.println("checkID오류"+e.getMessage());
		}
		
		return rs;
	}

	public ArrayList<BookDTO> selectAll() {
		ArrayList<BookDTO> list = new ArrayList<>();
		conn = getConn();
		String sql ="select * from book";
		try {
			pst = conn.prepareStatement(sql);
			rs =pst.executeQuery();
			while(rs.next()){
				BookDTO dto = new BookDTO();
				dto.setTitle(rs.getString("title"));
				dto.setAuth(rs.getString("auth"));
				dto.setComp(rs.getString("comp"));
				dto.setIsbn(rs.getString("isbn"));
				dto.setCost(rs.getInt("cost"));
				dto.setSu(rs.getInt("su"));
				dto.setPrice(rs.getInt("price"));
				
				list.add(dto);
				
			}
		} catch (Exception e) {
			System.out.println("select오류"+e.getMessage());
		}
		return list;
	}

	public int deleteBook(String isbn) {
		int result = 0;
		conn = getConn();
		String sql ="delete from book where isbn =?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, isbn);
			 result = pst.executeUpdate();
		} catch (Exception e) {
			System.out.println("delete오류"+e.getMessage());
		}
		return result;
	}
}
