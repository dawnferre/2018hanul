

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.study.BookDAO;
import com.hanul.study.BookDTO;

@WebServlet("/gals.do")
public class GetAllListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		BookDAO dao = new BookDAO();
		
		ArrayList<BookDTO> list =dao.selectAll();
		
		response.setContentType("text/html;charset=utf-8");
		PrintWriter output = response.getWriter();
		
		output.println("<style>*{margin: 0 auto;}</style>");
		output.println("<table border ='1'>");
		output.println("<tr>");
		output.println("<th>제목</th>");
		output.println("<th>지은이</th>");
		output.println("<th>isbn</th>");
		output.println("<th>출판사</th>");
		output.println("<th>단가</th>");
		output.println("<th>수량</th>");
		output.println("<th>금액</th>");
		output.println("<th>삭제</th>");
		output.println("</tr>");
		
		for(BookDTO dto:list){
			output.println("<tr>");
			System.out.println(dto.getTitle());
			output.println("<td>"+dto.getTitle()+"</td>");
			output.println("<td>"+dto.getAuth()+"</td>");
			output.println("<td>"+dto.getIsbn()+"</td>");
			output.println("<td>"+dto.getComp()+"</td>");
			DecimalFormat decimal = new DecimalFormat("#,###");
			decimal.format(dto.getCost());
			output.println("<td>"+decimal.format(dto.getCost())+"</td>");
			output.println("<td>"+dto.getSu()+"</td>");
			output.println("<td>"+decimal.format(dto.getPrice())+"</td>");
//			output.println("<td>"+dto.getCost()+"</td>");
//			output.println("<td>"+dto.getSu()+"</td>");
//			output.println("<td>"+dto.getPrice()+"</td>");
			output.println("<td><button onclick='location.href=\"ds.do?isbn="+dto.getIsbn()+
										"\"' style=\"background-color: pink\">삭제</button></td>");
			
			output.println("</tr>");
		}
		output.println("<tr>");
		output.println("<td colspan='8'>"
				+ "<button onclick='location.href=\"BookMain.html\"' style=\"background-color: cyan\">도서입력화면</button>"
				+ "</td>");
		
		output.println("</tr>");
		
		output.println("</table>");
	}

}
