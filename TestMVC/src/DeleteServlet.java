

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.study.BookDAO;

@WebServlet("/ds.do")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");//한글처리..
		String isbn = request.getParameter("isbn");
		BookDAO dao = new BookDAO();
		int result = dao.deleteBook(isbn);

		response.setContentType("text/html;charset=utf-8");
		PrintWriter output = response.getWriter();
		if(result>0){
			output.println("<h2>도서삭제가 완료되었습니다.<h2>");
			output.println("<button onclick='location.href=\"BookMain.html\"'>도서입력화면</button>");
			output.println("<button onclick='location.href=\"gals.do\"'>도서목록보기</button>");
			
		}else{
			output.println("<h2>도서삭제가 실패하였습니다. 다시 시도해주세요.<h2>");
			output.println("<button onclick='location.href=\"BookMain.html\"'>도서입력화면</button>");
			output.println("<button onclick='location.href=\"gals.do\"'>도서목록보기</button>");
		}	
	}

}
