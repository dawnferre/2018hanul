

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hanul.study.BookDAO;
import com.hanul.study.BookDTO;

@WebServlet("/is.do")
public class InsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		BookDTO dto = new BookDTO();
		request.setCharacterEncoding("utf-8");//한글처리..
		dto.setTitle(request.getParameter("title"));
		dto.setAuth(request.getParameter("auth"));
		dto.setComp(request.getParameter("comp"));
		dto.setIsbn(request.getParameter("isbn"));
		dto.setCost(Integer.parseInt(request.getParameter("cost")));
		dto.setSu(Integer.parseInt(request.getParameter("su")));
		dto.setPrice(Integer.parseInt(request.getParameter("price")));
		
		BookDAO dao = new BookDAO();
		int result = dao.insert(dto);
		
		response.setContentType("text/html;charset=utf-8");
		PrintWriter output = response.getWriter();
		if(result>0){
			output.println("<h2>도서등록이 완료되었습니다.<h2>");
			output.println("<button onclick='location.href=\"BookMain.html\"'>도서입력화면</button>");
			output.println("<button onclick='location.href=\"gals.do\"'>도서목록보기</button>");
			
		}else{
			output.println("<h2>도서등록이 실패하였습니다. 다시 시도해주세요.<h2>");
			output.println("<button onclick='location.href=\"BookMain.html\"'>도서입력화면</button>");
			output.println("<button onclick='location.href=\"gals.do\"'>도서목록보기</button>");
			
		}
		
		
	}

}
