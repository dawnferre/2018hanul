import java.sql.*;
public class SelectEx06 {
	public static void main(String[] args) {
		//부서명이 executive 인 부서에 소속된 사원의 사번, 성명, 부서코드, 부서명, 급여, 입사일자 조회
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe", "hr", "hr");
			String dep_name ="executive";
			//join을 이용한 방법
			String sql ="select employee_id, last_name||' '||first_name name, department_id, department_name, salary, hire_date "
					+ "from employees e inner join departments d using(department_id) "
					+ "where lower(department_name) =? ";
			//서브쿼리
			String sql2 ="select employee_id, last_name||' '||first_name name, department_id, "
					+ "(select department_name from departments where e.department_id = department_id) department_name, "
					+ "salary, hire_date "
					+ "from employees e "
					+ "where department_id=(select department_id from departments where lower(department_name) =?) ";
			pst = conn.prepareStatement(sql2);
			pst.setString(1, dep_name);
			rs = pst.executeQuery();
			
			//결과출력
			System.out.print("사번\t");
			System.out.print("성명\t");
			System.out.print("부서코드\t");
			System.out.print("부서명\t");
			System.out.print("급여\t");
			System.out.println("입사일자\t");
			while(rs.next()){
				System.out.print(rs.getInt("employee_id")+"\t");
				System.out.print(rs.getString("name")+"\t");
				System.out.print(rs.getInt("department_id")+"\t");
				System.out.print(rs.getString("department_name")+"\t");
				System.out.print(rs.getInt("salary")+"\t");
				System.out.println(rs.getDate("hire_date")+"\t");
				
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			e.getMessage();
		} catch(SQLException e){
			e.printStackTrace();
			e.getMessage();
			
		}
		finally{
			if (rs != null || pst != null || conn != null) {
				try {
					rs.close();
					pst.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					e.getMessage();
				}
			}
		}
		
	}//main()
}//class()
