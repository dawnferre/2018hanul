import java.sql.*;

public class SelectEx05 {
	public static void main(String[] args) {
		// 사번, 성, 부서명 , 업무명 조회

		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			// static method : 객체생성 불필요
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe", "hr", "hr");
			//조건 +  성이 king인 사원의
			// join 이용한 방법
			String name = "king";
			String sql = "select employee_id, last_name, department_name, job_title "
					+ "from employees e inner join departments d using(department_id) "
					+ "inner join jobs j using(job_id) "
					+ "where lower(last_name) =?";
			// 서브쿼리 이용한 방법
			String sql2 = "select employee_id, last_name,(select department_name from departments where e.department_id = department_id) department_name, "
					+ "(select job_title from jobs where e.job_id = job_id)job_title " + "from employees e ";
			pst = conn.prepareStatement(sql);
			// 지정된 변수를 이용하여 결과를 도출하는 방법
			pst.setString(1, name);
			rs = pst.executeQuery();
			
			System.out.print("사번\t");
			System.out.print("성\t");
			System.out.print("부서명\t");
			System.out.println("업무명\t");
			while (rs.next()) {
				System.out.print(rs.getInt("employee_id") + "\t");
				System.out.print(rs.getString("last_name") + "\t");
				System.out.print(rs.getString("department_name") + "\t");
				System.out.println(rs.getString("job_title") + "\t");
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			e.getMessage();
		} finally {
			//finally 부분에 if문 빼지말것.(확인잘하기.)
			if (rs != null || pst != null || conn != null) {
				try {
					rs.close();
					pst.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					e.getMessage();
				}
			}
		}//finally
	}// main()
}// class
