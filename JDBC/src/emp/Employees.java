package emp;

import java.util.Scanner;

import emp.dao.EmpDAO;

//import emp.dao.EmpDAO;
//import emp.dto.EmpDTO;

public class Employees {
	public static void main(String[] args) {
		//메인화면 
		EmpDAO dao = new EmpDAO();
		while(true){
			//1. 사원조회를 할 버튼이 있는 것 ===> 눈으로 보여야함 
			System.out.println("1.사원목록  2.사원등록   3.사원변경   4.사원삭제 0. 나가기");
			//입력
			System.out.println("원하는 번호를 입력하시오.");
			Scanner scan = new Scanner(System.in);
			int num =scan.nextInt();
			if(num ==1){
				//사원목록 화면 이동
				//사원목록 객체 생성.
				System.out.println("사원목록을 조회합니다.");
				new EmpList();
				
				continue;
			}else if( num ==2){
				//사원등록 화면 이동
				System.out.println("등록할 사원의 정보를 입력해주세요.");
				new EmpNew(scan).insertDisplay();
//				break;
				continue;
			}else if(num ==3){
				//사원정보 변경
				
				new EmpUpdate(scan).updateInfo();
				continue;
			}else if(num ==0){
				System.out.println("프로그램을 종료합니다.");
				break;
			}else if(num == 4){
					System.out.println("삭제할 사원의 사번을 입력해주세요.");
					int emp_id = scan.nextInt();
					if(dao.deleteInfo(emp_id)){
						System.out.println("사원정보가 삭제되었습니다.");
					}else{
						System.out.println("삭제가 실패하였습니다. 사번 확인부탁드립니다.");
					}
				//삭제후 목록화면 조회.---1번에 해당함.
				new EmpList();
				continue;
			}else{
				System.out.println("아직 지정되지 않은 번호입니다. 다시입력해주세요.");
				continue;
			}
		}
		
	}//main()
}//class
