package emp.dao;

import java.sql.*;

import emp.dto.EmployeeDTO; //패키지가 다르니 import 해주기.

public class EmployeeDAO {

	private Connection conn = null;
	private PreparedStatement pst = null;
	private ResultSet rs = null;

	public EmployeeDAO() {
		// DB연결과 관련된 처리를 한다.
		String url = "jdbc:oracle:thin:@localhost:1521/xe";
		String user = "hr";
		String password = "hr";
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url, user, password);

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {

		}
	}

	// 사원정보 조회
	public EmployeeDTO[] selectEmpInfo() {
		//107개의 사원정보를 담은 각각의 주소를 저장할 변수 : 배열변수
		// 데이터를 담을 배열변수 생성(ArrayList를 배우지 않았기 때문에.)
		//갯수가 확실치 않다면 넉넉하게 배열을 지정해주는 것이 좋다.
		EmployeeDTO[] dtoArr = new EmployeeDTO[107];
		
		String sql = "select * from employees";
		try {
			pst = conn.prepareStatement(sql);
			// 쿼리문에 ?가 있는지 확인할 것.
			rs = pst.executeQuery();
			int i =0;
			while (rs.next()) {
				//조회한 결과를 dto변수에 지정하여 넣어줌.
				EmployeeDTO dto=new EmployeeDTO(); //객체생성
				//변수에 지정_ 타입확인할 것.
				dto.setEmployee_id(rs.getInt("employee_id"));
				dto.setLast_name(rs.getString("last_name"));
				dto.setFirst_name(rs.getString("first_name"));
				dto.setEmail(rs.getString("email"));
				dto.setPhone_number(rs.getString("phone_number"));
								//날짜는 date.
				dto.setHire_date(rs.getDate("hire_date"));
				dto.setJob_id(rs.getString("job_id"));
				dto.setSalary(rs.getInt("salary"));
									//커미션 요율은 소숫점으로 나타나기 때문에 double,float으로 생성해준다.
				dto.setCommission_pct(rs.getDouble("commission_pct"));
				dto.setManager_id(rs.getInt("manager_id"));
				dto.setDepartment_id(rs.getInt("department_id"));
				
				//0~106개의 정보들을 배열에 담는 
				dtoArr[i++]= dto;
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null | pst!=null | conn!=null) {
				try {
					rs.close();
					pst.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return dtoArr;

	}
}
