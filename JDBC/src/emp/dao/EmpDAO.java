package emp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import emp.dto.EmpDTO;

public class EmpDAO {
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	public EmpDAO(){
		String url = "jdbc:oracle:thin:@localhost:1521/xe";
		String user = "hr";
		String password = "hr";
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url, user, password);
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
	}
	public EmpDTO[] getInfo(){
		EmpDTO[] list = new EmpDTO[200];
		String sql ="select employee_id, last_name, first_name, department_id,"
				+ "(select department_name from departments where e.department_id = department_id)department_name, "
				+ "job_id,(select job_title from jobs where e.job_id = job_id)job_title, hire_date,salary from employees e";
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			
			int temp =0;
			while(rs.next()){
				EmpDTO dto = new EmpDTO();
				dto.setEmployee_id(rs.getInt("employee_id"));
				dto.setLast_name(rs.getString("last_name"));
				dto.setFirst_name(rs.getString("first_name"));
				dto.setDepartment_id(rs.getInt("department_id"));
				dto.setDepartment_name(rs.getString("department_name"));
				dto.setJob_id(rs.getString("job_id"));
				dto.setJob_title(rs.getString("job_title"));
				dto.setHire_date(rs.getDate("hire_date"));
				dto.setSalary(rs.getInt("salary"));
				list[temp++]= dto;
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return list;
	}
	//개별 정보를 조회하는 메소드
	public EmpDTO getInfoEach(int employee_id){
		EmpDTO list = new EmpDTO();
		String sql ="select employee_id, last_name, first_name, salary, email, department_id from employees where employee_id =?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, employee_id);
			rs = pst.executeQuery();
			
			while(rs.next()){
				EmpDTO dto = new EmpDTO();
				dto.setEmployee_id(rs.getInt("employee_id"));
				dto.setLast_name(rs.getString("last_name"));
				dto.setFirst_name(rs.getString("first_name"));
				dto.setDepartment_id(rs.getInt("department_id"));
				dto.setSalary(rs.getInt("salary"));
				dto.setEmail(rs.getString("email"));
				list= dto;
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return list;
	}
	public void disconn(){
		if(rs!=null |pst!=null|conn!=null){
			
		}
	}
	//정보를 삽입하는 메소드
	public boolean insertInfo(EmpDTO dto){
		//emp_id, last_name, first_name, email, job_id
		//변수지정으로 입력이 되었는지 확인가능함.
		boolean insert = false;
		String sql ="insert into employees (employee_id, last_name, first_name, email, job_id, hire_date)"
					+ "values(?,?,?,?,?,sysdate) ";
		try {
			pst = conn.prepareStatement(sql);
			//값을 지정해준다.
			pst.setInt(1, dto.getEmployee_id());
			pst.setString(2, dto.getLast_name());
			pst.setString(3, dto.getFirst_name());
			pst.setString(4, dto.getEmail());
			pst.setString(5, dto.getJob_id());
			
			int num = pst.executeUpdate();
			
			if(num>0){
				insert = true;
			}else{
				insert =false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		
		return insert;
		
	}
	//정보를 업데이트 하는 메소드
	public boolean updateInfo(EmpDTO dto){
		boolean update = false;
		
		String sql ="delete from job_history where employee_id =?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, dto.getEmployee_id());
			//삼항연산자 
			pst.executeUpdate();
		
		sql ="update employees set last_name=?, first_name=?, email=?, salary=?,department_id=? "
				+ "where employee_id=?";

			pst = conn.prepareStatement(sql);
			pst.setString(1, dto.getLast_name());
			pst.setString(2, dto.getFirst_name());
			pst.setString(3, dto.getEmail());
			pst.setInt(4, dto.getSalary());
			pst.setInt(5, dto.getDepartment_id());
			pst.setInt(6, dto.getEmployee_id());
			
			int num = pst.executeUpdate();
			//삼항연산자 
			if(num >0){
				update = true;
			}else{
				update= false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		
		return update;
	}
	//정보를 업데이트 하는 메소드
	public boolean deleteInfo(int employee_id){
		boolean delete = false;
		//job_history에 관리된 사원이라면, job_history의 사원정보부터 삭제처리한다.
		String sql ="delete from job_history where employee_id =?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, employee_id);
			//삼항연산자 
			pst.executeUpdate();
			
			//job_history의 정보 삭제후 delete문 수행
			sql ="delete from employees where employee_id =?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, employee_id);
			
			int num = pst.executeUpdate();
			if(num >0){
				delete = true;
			}else{
				delete= false;
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}finally{
			disconn();
		}
		 
		
		
		return delete;
	}
}
