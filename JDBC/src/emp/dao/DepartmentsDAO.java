package emp.dao;
import java.sql.*;

import emp.dto.DepartmentsDTO;
import emp.dto.EmployeeDTO;
public class DepartmentsDAO {
	private Connection conn= null;
	private PreparedStatement pst= null;
	private ResultSet rs= null;
	
	public DepartmentsDAO(){
		//연결까지만. -- default
		String url = "jdbc:oracle:thin:@localhost:1521/xe";
		String user = "hr";
		String password = "hr";
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url, user, password);
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
	}
	public DepartmentsDTO[] selectDepInfo(){
		//정보를 담아둘 배열변수가 필요함
		//일반적인 배열변수는 생성을 통해 한번 사이즈를 지정하면 그사이즈를 수정할 수 없다.
		DepartmentsDTO[] dtoArr = null;//먼저 선언후
		//우리 회사에 등록되어 있는 부서가 어떤부서가 있는지 조회_ 사원이 소속되어있는 부서만 조회(부서배치받지 않은 사원은 null로 표시뜬다.)
		String sql ="select rownum, d.*,"
				+ "(select department_name from departments where department_id = d.department_id)department_name "
				+ "from (select department_id from employees group by department_id order by 1) d";
		//우리 회사 사원들이 소속된 부서코드 조회.
//		String sql ="select rownum, department_id, department_name from departments ";
//		String sql ="select rownum, dep.* "
//						+ "from(select department_id, department_name from departments order by 2) dep";
				try {
					pst = conn.prepareStatement(sql);
					rs = pst.executeQuery();
					dtoArr = new DepartmentsDTO[30]; 
					//데이터의 숫자를 알려주는 메소드를 이용해 객체를 생성할 수 있음.
					int i =0;
					while(rs.next()){ //조회된 결과행을 담을 DTO 에 담는다.
						DepartmentsDTO  dto =new DepartmentsDTO();
						dto.setNo(rs.getInt("rownum"));
						dto.setDep_id(rs.getInt("department_id"));
						dto.setDep_name(rs.getString("department_name"));
						dtoArr[i++] =dto;
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
				}finally{
//					disConn();
					
				}
				
				return dtoArr;
	}
	
	public EmployeeDTO[] selectEmpByDepId(int num){
		EmployeeDTO[] empArr = null;
																	//3항연산자를 이용해서 null값인 사원의 값을 구한다.
																	//복습_다시 해보기.
		String sql ="select * from employees where department_id "+(num==0?"is null":"=?");
				try {
					pst = conn.prepareStatement(sql);
					if(num !=0)
					pst.setInt(1, num);
					
					rs = pst.executeQuery();
					empArr = new EmployeeDTO[300];
					int i =0;
					while(rs.next()){ //조회된 결과행을 담을 DTO 에 담는다.
						EmployeeDTO dto = new EmployeeDTO();
						dto.setEmployee_id(rs.getInt("employee_id"));
						dto.setLast_name(rs.getString("last_name"));
						dto.setFirst_name(rs.getString("first_name"));
						dto.setEmail(rs.getString("email"));
						dto.setPhone_number(rs.getString("phone_number"));
										//날짜는 date.
						dto.setHire_date(rs.getDate("hire_date"));
						dto.setJob_id(rs.getString("job_id"));
						dto.setSalary(rs.getInt("salary"));
											//커미션 요율은 소숫점으로 나타나기 때문에 double,float으로 생성해준다.
						dto.setCommission_pct(rs.getDouble("commission_pct"));
						dto.setManager_id(rs.getInt("manager_id"));
						dto.setDepartment_id(rs.getInt("department_id"));
						
						empArr[i++]=dto;
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
				}finally{
					disConn();
					
				}
		return empArr;
	}
	//입력된 부서이름으로 사원정보 조회.
	public EmployeeDTO[] selectEmpByDepName(String name){
		EmployeeDTO[] empArr = null;
		String sql ="select * from employees "
				+ "where department_id ="
					+ "(select department_id from departments where department_name=?)";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, name);
			rs = pst.executeQuery();
			empArr = new EmployeeDTO[300];
			int i =0;
			while(rs.next()){ //조회된 결과행을 담을 DTO 에 담는다.
				EmployeeDTO dto = new EmployeeDTO();
				dto.setEmployee_id(rs.getInt("employee_id"));
				dto.setLast_name(rs.getString("last_name"));
				dto.setFirst_name(rs.getString("first_name"));
				dto.setEmail(rs.getString("email"));
				dto.setPhone_number(rs.getString("phone_number"));
				//날짜는 date.
				dto.setHire_date(rs.getDate("hire_date"));
				dto.setJob_id(rs.getString("job_id"));
				dto.setSalary(rs.getInt("salary"));
				//커미션 요율은 소숫점으로 나타나기 때문에 double,float으로 생성해준다.
				dto.setCommission_pct(rs.getDouble("commission_pct"));
				dto.setManager_id(rs.getInt("manager_id"));
				dto.setDepartment_id(rs.getInt("department_id"));
				
				empArr[i++]=dto;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disConn();
			
		}
		return empArr;
	}
	//자원회수 처리 메소드 따로 생성함.
	private void disConn(){
		if(rs!=null|pst !=null|conn!=null){
			try {
				rs.close();
				pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	} 

}
