package emp;

import emp.dao.EmployeeDAO;
import emp.dto.EmployeeDTO;

public class SelectEx2 {
	public static void main(String[] args) {

		/*
		 * Debug : debug 모드로 프로그램을 실행한다. 
		 * 문제가 발생할 지점으로 예상되는 곳에 breakpoint 를 지정한다.
		 * 다음 라인으로 넘어갈때는 f6
		 * 다음 step으로 넘어갈때는 f5
		 * 다음 breakpoint로 넘어갈때는 f8
		 *  f11만 눌러서 실행함.
		*/
		
		// dao의 default 생성자 연결._ db연결객체 생성.
		
		EmployeeDAO dao = new EmployeeDAO();
		EmployeeDTO[] dtoarr = dao.selectEmpInfo();

		// 배열에 들어있는 수를 각각의 dto변수에 담아 화면에 출력한다.
		for (EmployeeDTO dto : dtoarr) {
			if (dto == null) {
				continue;
			} else {

				System.out.print(dto.getEmployee_id() + "\t");
				System.out.print(dto.getLast_name() + "\t");
				System.out.print(dto.getFirst_name() + "\t");
				System.out.print(dto.getEmail() + "\t");
				System.out.print(dto.getPhone_number() + "\t");
				System.out.print(dto.getHire_date() + "\t");
				System.out.print(dto.getJob_id() + "\t");
				System.out.print(dto.getSalary() + "\t");
				System.out.print(dto.getCommission_pct() + "\t");
				System.out.print(dto.getManager_id() + "\t");
				System.out.println(dto.getDepartment_id() + "\t");
			}
		}
	}// main
}// class
