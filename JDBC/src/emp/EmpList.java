package emp;

import emp.dao.EmpDAO;
import emp.dto.EmpDTO;

public class EmpList {
	//메소드를 통해서 사원정보 조회 처리 가능.
	public EmpList() {

		//사번, 성, 명, 부서코드, 부서이름, 업무코드, 업무이름, 급여, 입사일자 조회
		EmpDAO dao =  new EmpDAO();
		//객체생성
		EmpDTO[] list =dao.getInfo();
		
		for(EmpDTO dto : list){
			if(dto == null){
				continue;
			}else{
				System.out.print(dto.getEmployee_id()+"\t");
				System.out.print(dto.getLast_name()+"\t");
				System.out.print(dto.getFirst_name()+"\t");
				System.out.print(dto.getDepartment_id()+"\t");
				System.out.print(dto.getDepartment_name()+"\t");
				System.out.print(dto.getJob_id()+"\t");
				System.out.print(dto.getJob_title()+"\t");
				System.out.print(dto.getSalary()+"\t");
				System.out.println(dto.getHire_date()+"\t");
			}
		}
	}
}
