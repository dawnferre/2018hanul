package emp;

import java.util.Scanner;

import emp.dao.EmpDAO;
import emp.dto.EmpDTO;

public class EmpUpdate {
private Scanner sc; //메인의 주소를 가지고 옴.
	
	//생성자로 scanner를 넘겨받음.
	 public EmpUpdate(Scanner sc){
		this.sc = sc;
	}
	 
	 public boolean updateInfo(){
		 	//원래정보를 보여주고
		 	//어떤 값으로 변경할 것인지 입력받아 새로 입력받은 정보를 DB에 변경저장한다.
		 boolean update = true;
		 
		
		 EmpDAO dao =  new EmpDAO();
		 EmpDTO list = new EmpDTO();
		//해당 사원의 정보를 조회할 특정사번의 사원의 정보를 조회하는 메소드.
		while(true){
			 System.out.println("바꿀 정보가 있는 사원의 사번을 입력해주세요.");
			 int employee_id = sc.nextInt();
			 list =dao.getInfoEach(employee_id);
			 if(list.getEmployee_id() == 0){
				 System.out.println("사번이 데이터에 없습니다.");
				 continue;
			 }else{
				 System.out.println("사번 ["+list.getEmployee_id()+"]번의 데이터입니다.");
				 System.out.println("사번\t성\t이름\t부서코드\t이메일\t급여");
							
				 System.out.print(list.getEmployee_id()+"\t");
				 System.out.print(list.getLast_name()+"\t");
				 System.out.print(list.getFirst_name()+"\t");
				 System.out.print(list.getDepartment_id()+"\t");
				 System.out.print(list.getEmail()+"\t");
				 System.out.println(list.getSalary()+"\t");
				 break;
			 }
			 
		 }
		System.out.println("-------------------------------------------------------");
		System.out.println("성 : ");
		String last_name = sc.next();
		System.out.println("이름 : ");
		String first_name = sc.next();
		System.out.println("부서코드 : ");
		int department_id = sc.nextInt();
		System.out.println("이메일: ");
		String email = sc.next();
		System.out.println("급여: ");
		int salary = sc.nextInt();
		
		list.setLast_name(last_name);
		list.setFirst_name(first_name);;
		list.setDepartment_id(department_id);
		list.setEmail(email);
		list.setSalary(salary);
		
		//DB에 연결하여 데이터를 변경저장처리할 메소드 호출
		if(dao.updateInfo(list)){
			System.out.println("정보가 업데이트되었습니다.");
		}else{
			System.out.println("업데이트가 실패하였습니다.");
		}
		
		 return update;
	 }
}
