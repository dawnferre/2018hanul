package emp;

import emp.dao.EmployeeDAO;
import emp.dto.EmployeeDTO;

public class SelectEx {
	public static void main(String[] args) {
		//DB와 연결해서 사원의 사번, 성명, 부서명, 급여, 입사일자 조회
		//DB 연결과 관련된 처리 : DAO(data access object)
		//데이터를 담아서 이리저리 전달하는데 사용함.:DTO(data transfer object)
		//					-db의 테이블 컬럼과 동일한 필드명으로 선언
		EmployeeDAO dao =new EmployeeDAO(); // 객체 생성
		EmployeeDTO[] dtoArr =dao.selectEmpInfo();//메소드 호출
		
		System.out.print("사번\t");
		System.out.print("성\t");
		System.out.print("이름\t");
		System.out.print("이메일\t");
		System.out.print("전화번호\t");
		System.out.print("입사일자\t");
		System.out.print("직업코드\t");
		System.out.print("급여\t");
		System.out.print("커미션요율\t");
		System.out.print("매니저사번\t");
		System.out.println("부서코드 \t");
//		//일반 for문
//		for (int i = 0; i < dtoArr.length; i++) {
//			System.out.print(dtoArr[i].getEmployee_id()+"\t");
//			System.out.print(dtoArr[i].getLast_name()+"\t");
//			System.out.print(dtoArr[i].getFirst_name()+"\t");
//			System.out.print(dtoArr[i].getEmail()+"\t");
//			System.out.print(dtoArr[i].getPhone_number()+"\t");
//			System.out.print(dtoArr[i].getHire_date()+"\t");
//			System.out.print(dtoArr[i].getJob_id()+"\t");
//			System.out.print(dtoArr[i].getSalary()+"\t");
//			System.out.print(dtoArr[i].getCommission_pct()+"\t");
//			System.out.print(dtoArr[i].getManager_id()+"\t");
//			System.out.println(dtoArr[i].getDepartment_id()+"\t");
//		}
		//향상된 for문
		for(EmployeeDTO dto : dtoArr){
			System.out.print(dto.getEmployee_id()+"\t");
			System.out.print(dto.getLast_name()+"\t");
			System.out.print(dto.getFirst_name()+"\t");
			System.out.print(dto.getEmail()+"\t");
			System.out.print(dto.getPhone_number()+"\t");
			System.out.print(dto.getHire_date()+"\t");
			System.out.print(dto.getJob_id()+"\t");
			System.out.print(dto.getSalary()+"\t");
			System.out.print(dto.getCommission_pct()+"\t");
			System.out.print(dto.getManager_id()+"\t");
			System.out.println(dto.getDepartment_id()+"\t");
			
		}
		/*
		//향상된 for문
		//for(새변수(배열안의 하나의 데이터 타입) :배열변수){
		}
		*/
	}//main()
}//class
