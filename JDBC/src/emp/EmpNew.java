package emp;

import java.util.Scanner;

import emp.dao.EmpDAO;
import emp.dto.EmpDTO;

public class EmpNew {
	private Scanner sc; //메인의 주소를 가지고 옴.
	
	//생성자로 scanner를 넘겨받음.
	public EmpNew(Scanner sc){
		this.sc = sc;
	}
	//메소드를 이용해서 처리함.
	public void insertDisplay(){
		EmpDTO dto = new EmpDTO();
		//DB연결
		EmpDAO dao = new EmpDAO();

		//신규사원 정보를 입력하여 DB에 저장해야 함.
		//Scanner 는 여러개 생성 불가 -프로그램 오류
		System.out.println("사번 : ");
		int emp_id = sc.nextInt();
		System.out.println("성 : ");
		String last_name = sc.next();
		System.out.println("명 : ");
		String first_name = sc.next();
		System.out.println("이메일 : ");
		String email = sc.next();
		System.out.println("업무코드 : ");
		String job_id = sc.next();
		
		dto.setEmployee_id(emp_id);
		dto.setLast_name(last_name);
		dto.setFirst_name(first_name);
		dto.setEmail(email);
		dto.setJob_id(job_id.toUpperCase());
		//데이터를 삽입저장할 메소드 호출
		
		if(dao.insertInfo(dto)){
			System.out.println("신규사원의 정보가 저장되었습니다.");
		}else{
			System.out.println("저장이 실패하였습니다.");
		}
	}
}
