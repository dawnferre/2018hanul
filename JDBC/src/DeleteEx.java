import java.sql.*;
public class DeleteEx {
	public static void main(String[] args) {
		Connection conn =null;
		PreparedStatement pst = null;
//		ResultSet rs = null;
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe", "hr", "hr");
			int emp_id =300;
			String sql ="delete from employees where employee_id >= ?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, emp_id);
			int cnt =pst.executeUpdate();
			
			if(cnt>0){
				System.out.println(cnt+"개 행 삭제완료");
				//데이터 삭제 이후에는 데이터조회 안되기 때문에 삭제되었다 안되었다로만 확인 가능.
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			e.getMessage();
		}finally{
			try {
//				rs.close();
				pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}//main

}//clas
