import java.sql.*;

public class SelectionEx04_03 {
	public static void main(String[] args) {

		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe", "hr", "hr");
			System.out.println("DB연결성공");
			// 성이 king이고 부서코드가 90번 사원의 사번, 성, 부서코드 조회.
			String name = "king";
			int dep_id = 90;
			String sql = "select employee_id, last_name, department_id from employees where lower(last_name)=? and department_id=? ";
			pst = conn.prepareStatement(sql);

			pst.setString(1, name);
			pst.setInt(2, dep_id);

			rs = pst.executeQuery();
			// 출력문이 없어서 안나옴.ㅜㅠ .. 나올구멍이 있어야 나오는건데...확인 잘하자
			while (rs.next()) {
				System.out.println(rs.getInt("employee_id") + "\t" + rs.getString("last_name") + "\t"
						+ rs.getInt("department_id"));
			}

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			e.getMessage();
		} finally {
			if (rs != null || pst != null || conn != null) {
				try {
					rs.close();
					pst.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					e.getMessage();
				}
			}
		}

	}// main()
}// class
