package member;

import java.util.Scanner;

import member.dao.LoginDAO;
import member.dto.LoginDTO;

public class Register {
	public void regigsterMemeber(Scanner scan){
		LoginDTO dto = new LoginDTO();
		
		System.out.println("회원가입할 정보를 입력해주세요.");
		System.out.println("id :");
		String id = scan.next(); //바로입력도 가능  : dto.setId(scan.next())
		System.out.println("비밀번호 :");
		String password = scan.next();
		System.out.println("이름 :");
		String name = scan.next();
		System.out.println("이메일 :");
		String email = scan.next();
		System.out.println("전화번호 :");
		String phone = scan.next();
		
		dto.setId(id);
		dto.setPassword(password);
		dto.setName(name);
		dto.setEmail(email);
		dto.setPhone(phone);
		
		LoginDAO dao = new LoginDAO();
		if(dao.register(dto)){
			System.out.println("회원가입이 완료하였습니다.");
		}else{
			System.out.println("회원가입이 실패하였습니다.");
		}
	}
}
