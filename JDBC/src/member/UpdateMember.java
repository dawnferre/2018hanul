package member;

import java.util.Scanner;

import member.dao.LoginDAO;
import member.dto.LoginDTO;

public class UpdateMember {

	public void update(Scanner scan, String id, String password) {

		LoginDTO dto = new LoginDTO();
		LoginDAO dao = new LoginDAO();

		System.out.println("업데이트할 정보를 입력하세요.");
		System.out.println("이름 : ");
		String name = scan.next();
		System.out.println("이메일 : ");
		String email = scan.next();
		System.out.println("전화번호 : ");
		String phone = scan.next();
//		System.out.println(id);
//		System.out.println(password); //값이 암호화되서 넘어가고 있었네...
		dto.setId(id);
		dto.setPassword(password);
		dto.setName(name);
		dto.setEmail(email);
		dto.setPhone(phone);

		if (dao.updateMember(dto)) {
			System.out.println("업데이트가 완료되었습니다.");
		} else {
			System.out.println("업데이트가 실패하였습니다.");
		}
	}

}
