package member.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import member.dto.LoginDTO;

public class LoginDAO {
	Connection conn;
	PreparedStatement pst ;
	ResultSet rs;
	//연결 메소드 
	public LoginDAO(){
		String url = "jdbc:oracle:thin:@localhost:1521/xe";
		String user = "hr";
		String password = "hr";
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url, user, password);
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
	}
	public LoginDTO selectIdPw(String id, String password){
		LoginDTO dto = new LoginDTO();
		//1.입력한 비번의 정보를 암호화하여 암호화데이터가 관리된 컬럼값과 비교하던지
		//2.암호화데이터가 관리된 컬럼값을 복호화하여 입력한 비번의 정보를 비교를 한다.
		String sql ="select * from member where userid =? and fn_decrypt(userpwd)=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, id);
			pst.setString(2, password);
			rs = pst.executeQuery();
			if(rs.next()){
				dto.setId(rs.getString("userid"));
				dto.setPassword(rs.getString("userpwd"));
				dto.setName(rs.getString("name"));
				dto.setEmail(rs.getString("email"));
				dto.setPhone(rs.getString("phone"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		
		return dto;
	}
	
	
	//회원가입
	public boolean register(LoginDTO dto){
		boolean regi = false;
																			//암호화된 부분은 반드시 제대로 선언해야한다.
		String sql ="insert into member (userid,userpwd,name,email,phone) values(?,fn_encypt(?),?,?,?)";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, dto.getId());
			pst.setString(2, dto.getPassword());
			pst.setString(3, dto.getName());
			pst.setString(4, dto.getEmail());
			pst.setString(5, dto.getPhone());
			
			int num = pst.executeUpdate();
			if(num>0){
				regi =true;
			}else{
				regi = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return regi;
	}
	//회원정보 업데이트
	public boolean updateMember(LoginDTO dto){
		boolean up = false;	
		String sql ="update member set name =?, email=? ,phone=? where userid =? and userpwd=?";
		//update member set name ='leeken', email='ken123', phone='010-9876-5432' where userid ='lee' and fn_decrypt(userpwd)='ken';
			try {
				pst = conn.prepareStatement(sql);
				pst.setString(1, dto.getName());
				pst.setString(2, dto.getEmail());
				pst.setString(3, dto.getPhone());
				pst.setString(4, dto.getId());
				pst.setString(5, dto.getPassword());
				
				int num = pst.executeUpdate();
				
				if(num>0){
					up = true;
					
				}else{
					up = false;
				}
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println("오류");
			}finally{
				disconn();
			}
			return up;
			
			
	}
	//회원탈퇴
	public boolean deleteMember(String id, String password){
		boolean del = false;
		String sql ="delete from member where userid =? and userpwd=?";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, id);
			pst.setString(2, password);
			int num = pst.executeUpdate();
			if(num>0){
				del = true;
			}else{
				del = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return del;
	}
	//자원회수
	public void disconn(){
			try {
				if(conn!=null)
				conn.close();
				if(pst!=null)
				pst.close();
				if(rs!=null)
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
		}
	}
}
