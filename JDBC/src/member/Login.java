package member;

import java.util.Scanner;

import member.dao.LoginDAO;
import member.dto.LoginDTO;

//아이디와 비밀번호를 입력하여
//일치하면 로그인된 회원정보를 확인
//일치하지 않으면 메세지를 출력한다.

public class Login {
	Scanner scan;
	public void login(Scanner scan){
		this.scan = scan;
	}
	
	public void showLogin(Scanner scan){
		System.out.println("로그인");
		System.out.println("아이디 : ");
		String id = scan.next();
		System.out.println("비밀번호 : ");
		String password = scan.next();
		
		//아이디와 비번의 일치여부는 DB와 연결하여 확인하기.
		LoginDAO dao = new LoginDAO();
		LoginDTO dto =dao.selectIdPw(id, password);
		if(dto.getId() == null && dto.getPassword() == null){
			System.out.println("아이디랑 비밀번호가 일치하지 않습니다.");
		}else{
			System.out.println("로그인되었습니다.");
			
			System.out.print("이름\t");
			System.out.print("email\t");
			System.out.println("전화번호\t");
			
			
			System.out.print(dto.getName()+"\t");
			System.out.print(dto.getEmail()+"\t");
			System.out.println(dto.getPhone()+"\t");
			
			System.out.println("1번 정보수정, 2번 회원탈퇴");
			int num = scan.nextInt();
			if( num ==1){
				new UpdateMember().update(scan, dto.getId(), dto.getPassword());
			}else if(num ==2){
				new Delete(dto.getId(), dto.getPassword());
			}else{
				return;
			}
		}
	}//main
}//class
