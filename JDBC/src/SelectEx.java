import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SelectEx {
	public static void main(String[] args) {
		//JDBC -오라클 용 JDBC 드라이버 설치
		//드라이버 설치
		//D:\oraclexe\app\oracle\product\11.2.0\server\jdbc\lib
		//employee테이블의 데이터를 조회 ->콘솔에서 확인
		
		//변수 초기화
		Connection conn = null;
		ResultSet rs = null;
		Statement st = null;
		try {
			//1.JDBC 드라이버 로딩
			Class.forName("oracle.jdbc.driver.OracleDriver");
							//패키지.패키지. 패키지. 클래스
			//2.드라이버관리자를 통해 DB와 연결
			//localhost 127.0.0.1 : cmd- ipconfig 자기자신의 ip설정주소
			//주소를 받을 변수 설정 conn (Connection객체)
			conn=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe", "hr","hr");
			System.out.println("DB연결성공");
			
			//3.쿼리문을 작성한다.
			String sql ="select * from employees";
			//쿼리문을 실행시켜주는 기능을 가진 객체: statement
			st =conn.createStatement();
			//쿼리문 실행 
			st.executeQuery(sql); 
			rs = st.executeQuery(sql);// f9에 대응하는 키 (developer)
			
			while(rs.next()){
//			rs.next();//커서이동_ 1개의 데이터 
			//데이터 읽어오기.
			rs.getInt("employee_id");//사번 데이터
			System.out.print(rs.getInt("employee_id")+"\t\t");
			rs.getString("last_name"); //성 데이터
			System.out.print(rs.getString("last_name")+"\t\t");
			rs.getInt("salary");//급여 데이터
			System.out.print(rs.getInt("salary")+"\t\t");
			rs.getDate("hire_date");
			System.out.println(rs.getDate("hire_date")+"\t\t");
			
			}
			//완료후 데이터와 연결끊기
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				st.close();
				conn.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}//main()
}//class
