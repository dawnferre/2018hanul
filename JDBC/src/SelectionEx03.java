import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SelectionEx03 {
	public static void main(String[] args) {
		//업무코드가 clerk 종류의 업무를 하는 사원들의 사번, 성명, 부서코드, 업무코드를 조회
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			//1.
			Class.forName("oracle.jdbc.driver.OracleDriver");
			//2.
			conn = DriverManager.getConnection("jdbc:oracle:thin:@127.0.0.1", "hr", "hr");
			//3.
//			String sql = "select employee_id, last_name||' '||first_name name, department_id, job_id "
//					+ "from employees where lower(job_id) like '%clerk%'";
//			int dep_id = 30;
		//성이 king ,lee, smith 이신 분들의 사번, 성명, 부서코드 , 업무코드
			String sql ="select employee_id, last_name||' '||first_name name, department_id, job_id "
					+ "from employees where lower(last_name) in ('king','lee','smith') ";
			
			String name1 ="king";
			String name2 ="lee";
			String name3 ="smith";
			String sql2 ="select employee_id, last_name||' '||first_name name, department_id, job_id "
					+ "from employees where lower(last_name) in ('"+name1+"','lee','smith') ";
			
			//조건을  변수로 지정하여 명시
			//4.
			st = conn.createStatement();
			//5.
			rs = st.executeQuery(sql);
			//6.
			while(rs.next()){
				System.out.println(rs.getInt("employee_id")+"\t"+rs.getString("name")+"\t"
									+rs.getInt("department_id")+"\t"+rs.getString("job_id"));
			}	
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			e.getMessage();
		}finally{
			//7.
			try {
				rs.close();
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				e.getMessage();
			}
		}
	}//main()
}//class
