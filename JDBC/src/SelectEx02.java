import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//import java.sql.*; -- sql패키지를 한꺼번에 import하는 방법 
//단축키 ctrl+ shift+O -- 이미 import된 경우에는 사용불가.(처음에 따로 선택하지 않아도 가능.)
public class SelectEx02 {
	public static void main(String[] args) {
		//DB에서 30번 부서에 속한 사원들의 사번, 성명, 부서코드, 급여, 업무코드
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			//1.드라이버 로딩
			Class.forName("oracle.jdbc.driver.OracleDriver");
			//2.드라이버관리자 -db연결
			conn =DriverManager.getConnection("jdbc:oracle:thin:@127.0.0.1","hr","hr");
									//디비 종류, 아이디, 비밀번호
		
			//3.쿼리문 작성_ 문장을 정확하게 공백하고 만들어서 써야한다.
			String sql ="select employee_id, last_name||' '||first_name name, department_id, salary, job_id "
								+ "from employees where department_id =20";
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			System.out.println("사번\t성명\t부서코드\t급여\t업무코드");
		
			while(rs.next()){
//				rs.getInt("employee_id");
//				rs.getString("last_name||' '||first_name name");
//				rs.getInt("department_id");
//				rs.getInt("salary");
//				rs.getString("job_id");
				
				System.out.println(
						rs.getInt("employee_id")+"\t"+rs.getString("name")+"\t"+
						rs.getInt("department_id")+"\t"+rs.getInt("salary")+"\t"+rs.getString("job_id"));
			}
					
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}finally{
			try {
				conn.close();
				st.close();
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}//main()
}//class
