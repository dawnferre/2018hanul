package notice;

import java.util.Scanner;

import notice.dao.NoticeDAO;
import notice.dto.NoticeDTO;

public class NoticeNew {
	//입력화면
	Scanner scan;
	//메인이 가지고 있는 스캐너 가지고 옴.
	public NoticeNew(Scanner scan){
		this.scan = scan;
	}
	public void insertShow(){
		NoticeDTO dto = new NoticeDTO();
		System.out.println("정보를 입력해주세요.");
		System.out.println("제목  : ");
		dto.setTitle(scan.next());
		System.out.println("작성자  : ");
		dto.setWriter(scan.next());
		System.out.println("내용 : ");
		dto.setContent(scan.next());
		
		NoticeDAO dao = new NoticeDAO();
		if(dao.insertInfo(dto)){
			System.out.println("공지글 작성이 성공하였습니다.");
		}else{
			System.out.println("공지글 작성이 실패하였습니다.");
		}
		
		//신규저장된 공지글 포함한 공지글목록환인 ==> 공지글목록화면
		// 최신글이 먼저보이게 처리
		new NoticeList().selectAll(scan);
		
	}
}
