package notice.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import notice.dto.NoticeDTO;

public class NoticeDAO {
	private Connection conn;
	private PreparedStatement pst;
	private ResultSet rs;
	
	public NoticeDAO(){
		String url = "jdbc:oracle:thin:@localhost:1521/xe";
		String user = "hr";
		String password = "hr";
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	//�ڿ�ȸ��
	public void disconn(){
		try {
			if(conn!=null){
				conn.close();
			}
			if(rs!=null){
				rs.close();
			}
			if(pst!=null){
				pst.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public NoticeDTO[] selectInfo(){
		NoticeDTO[] list = new NoticeDTO[20];
		String sql ="select rownum no, n.* from (select * from notice order by id desc) n ";
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			int no = 0;
			while(rs.next()){
				NoticeDTO dto = new NoticeDTO();
				dto.setNo(rs.getInt("no"));
				dto.setId(rs.getInt("id"));
				dto.setTitle(rs.getString("title"));
				dto.setWriter(rs.getString("writer"));
				dto.setContent(rs.getString("content"));
				dto.setWritedate(rs.getDate("writedate"));
				
				list[no++]=dto;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		
		return list;
	}
	public NoticeDTO selectInfoEach(int id){
		NoticeDTO dto = new NoticeDTO();
		String sql ="select * from notice where id =? ";
		try {
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			
			if(rs.next()){
				dto.setId(rs.getInt("id"));
				dto.setTitle(rs.getString("title"));
				dto.setWriter(rs.getString("writer"));
				dto.setContent(rs.getString("content"));
				dto.setWritedate(rs.getDate("writedate"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		
		return dto;
	}
	
	public boolean insertInfo(NoticeDTO dto){
		boolean in = false;
		String sql ="insert into Notice (title , writer, content) values(?,?,?)";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, dto.getTitle());
			pst.setString(2, dto.getWriter());
			pst.setString(3, dto.getContent());
			int num =pst.executeUpdate();
			if(num>0){
				in= true;
			}else{
				in = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return in;
	}
}
