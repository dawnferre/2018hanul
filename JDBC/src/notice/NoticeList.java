package notice;

import java.util.Scanner;

import notice.dao.NoticeDAO;
import notice.dto.NoticeDTO;

public class NoticeList {
	public void selectAll(Scanner scan){
		//db와 연결해서 공지글 정보를 가지고 와서 출력함.
		
		NoticeDAO dao = new NoticeDAO();
		NoticeDTO[] list =dao.selectInfo();
			System.out.println("no\t제목\t작성날짜");
		for(NoticeDTO dto : list){
			if(dto == null){
				continue;
			}else{
				System.out.print(dto.getNo()+"\t");
//				System.out.print(dto.getId()+"\t");
				System.out.print(dto.getTitle()+"\t");
//				System.out.println(dto.getWriter()+"\t");
//				System.out.print(dto.getContent()+"\t");
				System.out.println(dto.getWritedate()+"\t");
			}
		}
		//상세정보 확인시 번호선택
		System.out.println("공지글 상세번호 선택.");
		int no = scan.nextInt();
		
		//상세글 번호 선택시 , 해당번호글의 정보를 조회후 화면에 출력
		//****배열은 0부터 시작, 번호는 1부터 시작
		//배열로 담겨진 정보를 숫자-1 배열에서 뽑아서 넘겨준다.
		new NoticeDetail().showDetail(list[no-1].getId());
	}
}
