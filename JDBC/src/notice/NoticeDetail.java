package notice;

import notice.dao.NoticeDAO;
import notice.dto.NoticeDTO;

//상세화면
public class NoticeDetail {
	public void showDetail(int id){
		//db에서 데이터를 조회후 화면의 출력
		//선택한 글(id)의 정보를 조회할 메소드 호출
		NoticeDTO dto = new NoticeDAO().selectInfoEach(id);
		System.out.println("제목 : "+dto.getTitle()+"\t");
		System.out.println("작성자 : "+dto.getWriter()+"\t");
		System.out.println("내용 : "+dto.getContent()+"\t");
		System.out.println("작성일자 : "+dto.getWritedate()+"\t");

		
	}
}
