package notice;

import java.util.Scanner;

public class Notice {
	public static void main(String[] args) {
		//1.공지글 목록 0.프로그램 종료
		Scanner scan = new Scanner(System.in);
		Menu: //라벨지정
		while(true){
			System.out.println("숫자를 선택해주세요. \n1.공지글목록 2.공지글쓰기0.종료");
			int num = scan.nextInt();
			
			switch(num){
				case 1:
					System.out.println("공지글 목록");
					new NoticeList().selectAll(scan);
//					continue;
					break;//switch문도 반복문이기 때문에 하위껏만 빠져나오고 while은 그대로 돌게 된다.
				case 2:
					//공지글을 입력할 화면 
					new NoticeNew(scan).insertShow();
					break;
				case 0:
					System.out.println("프로그램이 종료됩니다.");
//					break;//switch문도 반복문이기 때문에 하위껏만 빠져나오고 while은 그대로 돌게 된다.
					break Menu; //상위 반복문 나가기
					
			}
//			scan.close();
		}
	
	}//main
}//class
