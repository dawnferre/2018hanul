import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class InsertEx {
	public static void main(String[] args) {
		//employees 테이블에 새로운 사원의 정보를 저장
		Connection conn =null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe", "hr","hr");
			//default 자동 커밋을 해제한다._ 대부분은 그냥 사용 
			conn.setAutoCommit(false); //auto_commit 안되면, conn.commit;, conn.rollback(); 명시해주기.
//			int emp_id = 300;
//			String last_name ="lee";
//			String first_name ="ken";
//			String hire_date="2018-04-01";
//			String email ="ken@naver.com";
//			String job_id ="ST_CLERK"; //제약조건에 의해서 
			int emp_id = 302;
			String last_name ="lee";
			String first_name ="unn";
			String hire_date="2018-04-05";
			String email ="unn1234@naver.com"; //unique 걸려있어 중복불가.
			String job_id ="ST_CLERK"; //제약조건에 의해서 
			String sql ="insert into employees "
					+ "(employee_id, last_name,first_name,email,hire_date, job_id) values(?,?,?,?,?,?)";
			pst = conn.prepareStatement(sql);

			pst.setInt(1, emp_id);
			pst.setString(2, last_name);
			pst.setString(3, first_name);
			pst.setString(4, email);
			pst.setString(5, hire_date);
			pst.setString(6, job_id);
			//insert, update, delete 는 excuteUpdate();
			int cnt =pst.executeUpdate();
			//실행되었다면 cnt 값이 0보다는 큼.
			if(cnt>0){
				System.out.println(cnt+"개의 행을 삽입저장 성공했습니다.");
				
				sql ="select * from employees where employee_id =?";
				pst =conn.prepareStatement(sql);
				pst.setInt(1, emp_id);
				rs = pst.executeQuery();
				if(rs.next()){
					System.out.println("저장된 ["+last_name+" "+first_name+"]씨 정보");
					System.out.println("사번 :"+rs.getInt("employee_id"));
					System.out.println("성명 :"+rs.getString("last_name")+" "+rs.getString("first_name"));
					System.out.println("입사일자 :"+rs.getDate("hire_date"));
					System.out.println("업무코드 :"+rs.getString("job_id"));
					
				}
				
			}else{
				System.out.println(cnt+"개의 행을 삽입저장 실패했습니다.");				
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			e.getMessage();
		}finally{
			try {
				rs.close();
				pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				e.getMessage();
			}
			
		}
	}//main
}//class
