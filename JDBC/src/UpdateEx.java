import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UpdateEx {
	public static void main(String[] args) {
			Connection conn = null;
			PreparedStatement pst = null;
			ResultSet rs = null;
			
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe", "hr", "hr");
				int emp_id =302;
				int salary =5000;
				int dept_id =20;
				String sql="update employees set salary =?, department_id =? where employee_id =?";
				pst = conn.prepareStatement(sql);
				pst.setInt(1, salary);
				pst.setInt(2, dept_id);
				pst.setInt(3, emp_id);

				int cnt =pst.executeUpdate();
				
				if(cnt>0){
					System.out.println(cnt+"개의 업데이트 완료");
					sql="select * from employees where employee_id =?";
					pst.setInt(1, emp_id);
					rs =pst.executeQuery();
					while(rs.next()){ //1개의 행이면 if를 사용해도 된다.
						System.out.println(rs.getString("first_name")+"씨의 정보");
						System.out.println("급여 : "+rs.getInt("salary"));
						System.out.println("부서 : "+rs.getInt("department_id"));
					}
					
				}else{
					System.out.println(cnt+"개의 업데이트 실패");
				}
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}finally{
				try {
					if(rs!=null | pst!= null | conn!= null){
					rs.close();
					pst.close();
					conn.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
	}//main()
}//class
