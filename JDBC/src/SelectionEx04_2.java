import java.sql.*;
public class SelectionEx04_2 {
	public static void main(String[] args) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		//오류 고치기.
		try {
			//1.
			Class.forName("oracle.jdbc.driver.OracleDriver");
			//2.
			conn = DriverManager.getConnection("jdbc:oracle:thin:@127.0.0.1", "hr","hr");
			//3.
			//변수에 조건을 지정.
			String name ="king";
			int dep_id = 90;
			String job_id = "ad_pres";
			//성이 king이고 부서코드가 90번 사원의 사번, 성, 부서코드 조회.
			String sql ="select employee_id, last_name, department_id from employees where lower(last_name)=? and department_id =? and lower(job_id) =?";
			//***4. 미리 쿼리문을 넣어줌.
			//st = conn.createStatement();
			pst =conn.prepareStatement(sql);
			
			// ?에 데이터를 바인딩시킨다.
			pst.setString(1, name);
			pst.setInt(2, dep_id);
			pst.setString(3, job_id);
			//5. 따로 지정할 필요없이  데이터가 위에 설정되었기 때문에 바로 실행가능.
//			rs = st.executeQuery(sql);
			rs = pst.executeQuery();
			
			while(rs.next()){
				System.out.println(rs.getInt("employee_id")+"\t"+rs.getString("last_name")
									+"\t"+rs.getInt("department_id"));
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			e.getMessage();
		}finally{
			if(rs!= null|| pst!=null ||conn!=null){
				try {
					rs.close();
					pst.close();
					conn.close();
				} catch (SQLException e) {
					e.getMessage();
					e.printStackTrace();
				}
			}
		}
		
	}//main()
}//class
