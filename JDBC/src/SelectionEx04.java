import java.sql.*;
public class SelectionEx04 {
	public static void main(String[] args) {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		
		try {
			//1.
			Class.forName("oracle.jdbc.driver.OracleDriver");
			//2.
			conn = DriverManager.getConnection("jdbc:oracle:thin:@127.0.0.1", "hr","hr");
			//3.
			st = conn.createStatement();
			//statment : 쿼리문을 실행시켜주는 기능을 갖고잇다.
			//PreparedStatment : 쿼리문을 미리 준비해두고 준비된 쿼리문을 나중에 실행시킬 수 있음.
			
			//변수에 조건을 지정.
			String name ="king";
			int dep_id = 90;
			String job_id = "ad_pres";
			//성이 king이고 부서코드가 90번 사원의 사번, 성, 부서코드 조회.
			String sql ="select employee_id, last_name, department_id,job_id "
					+ "from employees where department_id="+dep_id+" and lower(last_name)='"+name
					+"' and lower(job_id)='"+job_id+"'";
			//4.
			rs = st.executeQuery(sql);
			while(rs.next()){
				System.out.println(rs.getInt("employee_id")+"\t"+rs.getString("last_name")
									+"\t"+rs.getString("job_id")+"\t"+rs.getInt("department_id"));
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			e.getMessage();
		}finally{
			if(rs!= null|| st!=null ||conn!=null){
				try {
					rs.close();
					st.close();
					conn.close();
				} catch (SQLException e) {
					e.getMessage();
					e.printStackTrace();
				}
			}
		}
		
	}//main()
}//class
