public class Ex_array_05 {
	public static void main(String[] args) {
		//가변길이 배열 : 행은 고정, 열은 가별길이.
		//가변길이 배열 의 선언. 행만 선언.
		int[][] arr = new int[2][]; //[0]?,[1]?
		
		arr[0] = new int [2];  //[0][1],[0][2]; 
		arr[1] = new int [3];  //[1][1],[1][2],[1][3];
	
		for(int i =0; i<arr.length; i++){
			for (int j = 0; j < arr[i].length; j++) {
				arr[i][j] =10; 
				System.out.println("["+i+"]"+"["+j+"] = "+arr[i][j]);
			}
		}
		
		
	}//main()
}//class
