import java.util.Arrays;

public class Ex_swap_array {
	public static void main(String[] args) {
		//배열에 저장된 데이터를 출력
		int[] arr1 = {1,2,3,4,5};
		System.out.print("arr1 배열의 값 :");
		for (int i = 0; i < arr1.length; i++) {// 출력1
			System.out.print(arr1[i]+" "); 
		}
		System.out.println();
		
		//기존에 만들어 진 class를 쓰는 방법.
		//Arrays.toString(배열명)
		System.out.print("arr1 배열의 값 :"+Arrays.toString(arr1)); // 출력2
		
		//배열(arr1)에 저장된 데이터를 역순으로 출력.
		int[] arr2 = new int[arr1.length];
//		arr2[0] = arr1[arr1.length-1]; //arr1[4];
//		arr2[1] = arr1[arr1.length-2]; //arr1[3];
//		arr2[2] = arr1[arr1.length-3]; //arr1[2];
//		arr2[3] = arr1[arr1.length-4]; //arr1[1];
//		arr2[4] = arr1[arr1.length-5]; //arr1[0];
		
		for (int i = 0; i < arr2.length; i++) {
			arr2[i] = arr1[arr1.length-(i+1)]; //arr1[4];
//			System.out.println(arr2[i] = arr1[arr1.length-(i+1)]);
		}
		
		System.out.print("\narr2 배열의 값 :"+Arrays.toString(arr2)); // 출력2
		
		//배열(arr1)에 저장된 데이터를 역순으로 출력2._ 임시변수를 이용하여 만드는 방법.
		int[] arr3 = new int[arr1.length];
		int index = arr1.length-1; // index :4
		
		for (int i = 0; i < arr1.length; i++) {
			arr3[i] = arr1[index--]; //arr1[4];
		}
		
		System.out.print("\narr3 배열의 값 :"+Arrays.toString(arr3)); // 출력2
		
	}//main()
}//class
