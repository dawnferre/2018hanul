public class Ex_array01 {
	public static void main(String[] args) {
		//정수 3개를 저장할 배열(arr)을 생성하고 모든 요소에 10을 할당(초기화)하시오.
		int[] arr =new int[3]; //정수형 배열의 초기화, 배열생성. 배열의 크기:3;
		//배열의 값을 할당.[각각의 인덱스]
		arr[0]=10;
		arr[1]=10;
		arr[2]=10;
		
		System.out.println("arr[0]의 값 : "+ arr[0]);
		System.out.println("arr[1]의 값 : "+ arr[1]);
		System.out.println("arr[2]의 값 : "+ arr[2]);
		
		System.out.println(arr);// 출력은 되지만 해쉬코드(배열이 참조하는 번지수_16진법).
		
	}//main()
}//class
