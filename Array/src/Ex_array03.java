public class Ex_array03 {
	public static void main(String[] args) {
		//2행 3열의 정수형 이차원배열(arr)을 생성하고 각 배열의 요소에 10을 할당하시오.
		int[][] arr = new int[2][3];
		//값을 할당.
//		arr[0][0] =10;
//		arr[0][1] =10;
//		arr[0][2] =10;
//		
//		arr[1][0] =10;
//		arr[1][1] =10;
//		arr[1][2] =10;
//		arr={{10,10,10},{10,10,10}};
							//arr.length = 행의 값. =2
		for (int i = 0; i < arr.length; i++) {
							//arr[i].length =열의 값. =3
			for(int j =0; j<arr[i].length; j++){
				arr[i][j]=10;
				System.out.println("arr["+i+"]["+j+"] = "+arr[i][j]);
			}
		
		}//for
	}//main()
}//class
