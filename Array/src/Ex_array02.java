public class Ex_array02 {
	public static void main(String[] args) {
		//정수 3개를 저장할 배열(arr)을 생성하고,  모든 요소에 10을 할당하시오. =>>초기화
		int[] arr = new int[3];
		arr[0] =10;
		arr[1] =10;
		arr[2] =10;
		
		int[] arr2 = {10,10,10};
	
		System.out.println("arr[0]의 값 : "+ arr[0]);
		System.out.println("arr[1]의 값 : "+ arr[1]);
		System.out.println("arr[2]의 값 : "+ arr[2]);

		System.out.println("arr2[0]의 값 : "+ arr2[0]);
		System.out.println("arr2[1]의 값 : "+ arr2[1]);
		System.out.println("arr2[2]의 값 : "+ arr2[2]);
		
		
		int sum =0;
		sum = arr[0]+arr[1]+arr[2];
		System.out.println("arr의 배열의 합: "+sum);
		
		//배열(arr)의 모든 요소의 값을 더하여 출력.== for문 이용.
		sum =0;
		for(int i=0; i<arr.length; i++){
			sum += arr[i];
		}
		System.out.println("arr의 배열의 합: "+sum);
		System.out.println("arr의 배열의 길이(갯수, 크기): "+arr.length);
	}//main()
}//class

/*배열(Array) :동일한 기억공간을 메모리에 연속적으로 생성하는 구조(리스트 구조) 
 * =>> 객체취급(class, object) -->참조형 자료구조(referenct type)
	-같은 타입을 갖는 변수들의 집합.
	-배열을 선언(공간확보)하고, 나중에 내용물(원소, 요소)를 채우는 방식 : Ex_array01
	-배열을 선언과 동시에 배열안의 원소를 같이 할당하는 방식 :Ex_array02
	-배열의 요소번호(index, 첨자)는 항상 0부터 시작된다. 
	-배열의 크기(길이) : 배열명.length
*/
